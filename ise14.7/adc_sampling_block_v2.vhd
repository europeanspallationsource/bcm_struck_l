----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:14:31 08/24/2010 
-- Design Name: 
-- Module Name:    adc_sampling_block_v2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
--use work.sis8300_pack.all ;

entity adc_sampling_block_v2 is
    Port ( adc_buffer_logic_reset : in  STD_LOGIC;
           adc1_clk : in  STD_LOGIC;
           adc2_clk : in  STD_LOGIC;
           adc3_clk : in  STD_LOGIC;
           adc4_clk : in  STD_LOGIC;
           adc5_clk : in  STD_LOGIC;

           adc_ch1_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch2_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch3_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch4_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch5_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch6_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch7_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch8_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch9_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch10_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);

           adc_sample_length_reg : in   STD_LOGIC_VECTOR(23 downto 0);

           adc1_start_pulse : in  STD_LOGIC;
           adc2_start_pulse : in  STD_LOGIC;
           adc3_start_pulse : in  STD_LOGIC;
           adc4_start_pulse : in  STD_LOGIC;
           adc5_start_pulse : in  STD_LOGIC;
 
 
           adc1_sample_memory_start_addr : in   STD_LOGIC_VECTOR(23 downto 0);
           adc2_sample_memory_start_addr : in   STD_LOGIC_VECTOR(23 downto 0);
           adc3_sample_memory_start_addr : in   STD_LOGIC_VECTOR(23 downto 0);
           adc4_sample_memory_start_addr : in   STD_LOGIC_VECTOR(23 downto 0);
           adc5_sample_memory_start_addr : in   STD_LOGIC_VECTOR(23 downto 0);

           adc1_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc2_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc3_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc4_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc5_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);

           adcx_sample_logic_active_or : OUT std_logic;
           adcx_sample_buffer_not_empty_or : OUT std_logic;

           ddr2_write_logic_enable: in  STD_LOGIC;
           ddr2_write_fifo_clk: in  STD_LOGIC;
           ddr2_write_addr_fifo_wr_count : in  std_logic_vector(9 downto 0);  
           ddr2_write_addr_fifo_wr_en : OUT std_logic;
           ddr2_write_addr_fifo_din : OUT std_logic_vector(31 downto 0) ;
           ddr2_write_data_fifo_wr_en : OUT std_logic;
           ddr2_write_data_fifo_din : OUT std_logic_vector(127 downto 0) 
			  );
end adc_sampling_block_v2;

architecture Behavioral of adc_sampling_block_v2 is

   Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
   begin
   if x then ret := '1';  else ret := '0'; end if;
   return ret;
   end to_std_logic;

	COMPONENT risingedge2pulse_generator
	PORT(
		adc_clk : IN std_logic;
		rising_edge_signal_in : IN std_logic;          
		signal_pulse_out : OUT std_logic
		);
	END COMPONENT;

	COMPONENT two_channel_sample_block_v2
	PORT(
		adc_sample_logic_reset : IN std_logic;
		adc_clk : IN std_logic;
		adc_start_pulse : IN std_logic;
		adc_sample_block_length_reg : IN std_logic_vector(23 downto 0);
		adc_ch1_din : IN std_logic_vector(15 downto 0);
		adc_ch2_din : IN std_logic_vector(15 downto 0);
		adc_buffer_fifo_rd_clk : IN std_logic;
		adc_buffer_fifo_rd_en : IN std_logic;          
		adc_sample_logic_active : OUT std_logic;
		adc_buffer_fifo_rd_dout : OUT std_logic_vector(127 downto 0);
		adc_buffer_fifo_rd_full : OUT std_logic;
		adc_buffer_fifo_rd_empty : OUT std_logic;
		adc_buffer_fifo_rd_rd_count : OUT std_logic_vector(10 downto 0)
		);
	END COMPONENT;






--  type adc_data_array_type is array(0 to 9) of std_logic_vector(13 downto 0) ;



signal adc1_logic_reset: std_logic;
signal adc2_logic_reset: std_logic;
signal adc3_logic_reset: std_logic;
signal adc4_logic_reset: std_logic;
signal adc5_logic_reset: std_logic;

signal adc1_sample_logic_active: std_logic;
signal adc1_buffer_fifo_rd_en: std_logic;
signal adc1_buffer_fifo_dout: std_logic_vector(127 downto 0);
signal adc1_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal adc1_buffer_fifo_empty: std_logic;
signal adc1_mem_addr_counter: std_logic_vector(23 downto 0);

signal adc2_sample_logic_active: std_logic;
signal adc2_buffer_fifo_rd_en: std_logic;
signal adc2_buffer_fifo_dout: std_logic_vector(127 downto 0);
signal adc2_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal adc2_buffer_fifo_empty: std_logic;
signal adc2_mem_addr_counter: std_logic_vector(23 downto 0);

signal adc3_sample_logic_active: std_logic;
signal adc3_buffer_fifo_rd_en: std_logic;
signal adc3_buffer_fifo_dout: std_logic_vector(127 downto 0);
signal adc3_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal adc3_buffer_fifo_empty: std_logic;
signal adc3_mem_addr_counter: std_logic_vector(23 downto 0);

signal adc4_sample_logic_active: std_logic;
signal adc4_buffer_fifo_rd_en: std_logic;
signal adc4_buffer_fifo_dout: std_logic_vector(127 downto 0);
signal adc4_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal adc4_buffer_fifo_empty: std_logic;
signal adc4_mem_addr_counter: std_logic_vector(23 downto 0);

signal adc5_sample_logic_active: std_logic;
signal adc5_buffer_fifo_rd_en: std_logic;
signal adc5_buffer_fifo_dout: std_logic_vector(127 downto 0);
signal adc5_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal adc5_buffer_fifo_empty: std_logic;
signal adc5_mem_addr_counter: std_logic_vector(23 downto 0);





signal adc_sample_logic_active_or: std_logic;
signal fsm_adcx_buffer_fifo_rd_count: std_logic_vector(10 downto 0);
signal fsm_adcx_buffer_fifo_rd_count_ge2: std_logic;
signal fsm_adcx_buffer_fifo_rd_count_ge128: std_logic;


signal fsm_mem_reset: std_logic;
signal fsm_mem_write_logic_enable: std_logic;
signal mem_write_addr_fifo_almost_full: std_logic;
signal mem_write_addr_fifo_almost_empty: std_logic;
signal fsm_mem_addr_inc: std_logic;


signal fsm_mem_active: std_logic;
signal fsm_mem_addr_ld: std_logic;
signal fsm_mem_wr_addr_fifo_wr_en: std_logic;
signal fsm_mem_wr_data_fifo_wr_en: std_logic;
signal fsm_mem_ch_mux_counter: std_logic_vector(3 downto 0);
signal fsm_mem_counter: std_logic_vector(11 downto 0);


type fsm_mem_type is(
		MEM_IDLE,	--  
		MEM_START,	--  
		MEM_CH_CHECK,	--  
		MEM_CH_COPY,	--  
		MEM_CH_END 
);
signal fsm_mem_state		: fsm_mem_type;


begin


	adc1_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc1_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc1_logic_reset
	);


	adc1_two_channel_sample_block_v2: two_channel_sample_block_v2 PORT MAP(
		adc_sample_logic_reset => adc1_logic_reset,
		adc_clk => adc1_clk,
		adc_start_pulse => adc1_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_ch1_din => adc_ch1_ringbuffer_dout,
		adc_ch2_din => adc_ch2_ringbuffer_dout,
		adc_sample_logic_active => adc1_sample_logic_active,
		adc_buffer_fifo_rd_clk => ddr2_write_fifo_clk,
		adc_buffer_fifo_rd_en => adc1_buffer_fifo_rd_en,
		adc_buffer_fifo_rd_dout => adc1_buffer_fifo_dout,
		adc_buffer_fifo_rd_full => open,
		adc_buffer_fifo_rd_empty => adc1_buffer_fifo_empty,
		adc_buffer_fifo_rd_rd_count => adc1_buffer_fifo_rd_count
	);



-- ************************************

	adc2_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc2_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc2_logic_reset
	);

	adc2_two_channel_sample_block_v2: two_channel_sample_block_v2 PORT MAP(
		adc_sample_logic_reset => adc2_logic_reset,
		adc_clk => adc2_clk,
		adc_start_pulse => adc2_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_ch1_din => adc_ch3_ringbuffer_dout,
		adc_ch2_din => adc_ch4_ringbuffer_dout,
		adc_sample_logic_active => adc2_sample_logic_active,
		adc_buffer_fifo_rd_clk => ddr2_write_fifo_clk,
		adc_buffer_fifo_rd_en => adc2_buffer_fifo_rd_en,
		adc_buffer_fifo_rd_dout => adc2_buffer_fifo_dout,
		adc_buffer_fifo_rd_full => open,
		adc_buffer_fifo_rd_empty => adc2_buffer_fifo_empty,
		adc_buffer_fifo_rd_rd_count => adc2_buffer_fifo_rd_count
	);

--   adc2_sample_logic_active      <= adc1_sample_logic_active ;
--   adc2_buffer_fifo_dout         <= adc1_buffer_fifo_dout ;
--   adc2_buffer_fifo_empty        <= adc1_buffer_fifo_empty ;
--   adc2_buffer_fifo_rd_count     <= adc1_buffer_fifo_rd_count ;


-- ************************************

	adc3_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc3_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc3_logic_reset
	);

	adc3_two_channel_sample_block_v2: two_channel_sample_block_v2 PORT MAP(
		adc_sample_logic_reset => adc3_logic_reset,
		adc_clk => adc3_clk,
		adc_start_pulse => adc3_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_ch1_din => adc_ch5_ringbuffer_dout,
		adc_ch2_din => adc_ch6_ringbuffer_dout,
		adc_sample_logic_active => adc3_sample_logic_active,
		adc_buffer_fifo_rd_clk => ddr2_write_fifo_clk,
		adc_buffer_fifo_rd_en => adc3_buffer_fifo_rd_en,
		adc_buffer_fifo_rd_dout => adc3_buffer_fifo_dout,
		adc_buffer_fifo_rd_full => open,
		adc_buffer_fifo_rd_empty => adc3_buffer_fifo_empty,
		adc_buffer_fifo_rd_rd_count => adc3_buffer_fifo_rd_count
	);
--
--   adc3_sample_logic_active      <= adc1_sample_logic_active ;
--   adc3_buffer_fifo_dout         <= adc1_buffer_fifo_dout ;
--   adc3_buffer_fifo_empty        <= adc1_buffer_fifo_empty ;
--   adc3_buffer_fifo_rd_count     <= adc1_buffer_fifo_rd_count ;

-- ************************************

	adc4_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc4_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc4_logic_reset
	);

	adc4_two_channel_sample_block_v2: two_channel_sample_block_v2 PORT MAP(
		adc_sample_logic_reset => adc4_logic_reset,
		adc_clk => adc4_clk,
		adc_start_pulse => adc4_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_ch1_din => adc_ch7_ringbuffer_dout,
		adc_ch2_din => adc_ch8_ringbuffer_dout,
		adc_sample_logic_active => adc4_sample_logic_active,
		adc_buffer_fifo_rd_clk => ddr2_write_fifo_clk,
		adc_buffer_fifo_rd_en => adc4_buffer_fifo_rd_en,
		adc_buffer_fifo_rd_dout => adc4_buffer_fifo_dout,
		adc_buffer_fifo_rd_full => open,
		adc_buffer_fifo_rd_empty => adc4_buffer_fifo_empty,
		adc_buffer_fifo_rd_rd_count => adc4_buffer_fifo_rd_count
	);

-- ************************************

	adc5_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc5_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc5_logic_reset
	);

	adc5_two_channel_sample_block_v2: two_channel_sample_block_v2 PORT MAP(
		adc_sample_logic_reset => adc5_logic_reset,
		adc_clk => adc5_clk,
		adc_start_pulse => adc5_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_ch1_din => adc_ch9_ringbuffer_dout,
		adc_ch2_din => adc_ch10_ringbuffer_dout,
		adc_sample_logic_active => adc5_sample_logic_active,
		adc_buffer_fifo_rd_clk => ddr2_write_fifo_clk,
		adc_buffer_fifo_rd_en => adc5_buffer_fifo_rd_en,
		adc_buffer_fifo_rd_dout => adc5_buffer_fifo_dout,
		adc_buffer_fifo_rd_full => open,
		adc_buffer_fifo_rd_empty => adc5_buffer_fifo_empty,
		adc_buffer_fifo_rd_rd_count => adc5_buffer_fifo_rd_count
	);
--
-- **************************************************************************************************************
-- ddr2 write clock domain


            
   adcx_sample_logic_active_or         <=    adc_sample_logic_active_or ;
   adcx_sample_buffer_not_empty_or     <=       not adc1_buffer_fifo_empty  
                                             or not adc2_buffer_fifo_empty  
                                             or not adc3_buffer_fifo_empty 
                                             or not adc4_buffer_fifo_empty  
                                             or not adc5_buffer_fifo_empty  ;

   adc1_adc_memory_addr_counter(31 downto 24)    <=   X"00" ;
   adc1_adc_memory_addr_counter(23 downto 0)     <=   adc1_mem_addr_counter(23 downto 0) ;

   adc2_adc_memory_addr_counter(31 downto 24)    <=   X"00" ;
   adc2_adc_memory_addr_counter(23 downto 0)     <=   adc2_mem_addr_counter(23 downto 0) ;

   adc3_adc_memory_addr_counter(31 downto 24)    <=   X"00" ;
   adc3_adc_memory_addr_counter(23 downto 0)     <=   adc3_mem_addr_counter(23 downto 0) ;

   adc4_adc_memory_addr_counter(31 downto 24)    <=   X"00" ;
   adc4_adc_memory_addr_counter(23 downto 0)     <=   adc4_mem_addr_counter(23 downto 0) ;

   adc5_adc_memory_addr_counter(31 downto 24)    <=   X"00" ;
   adc5_adc_memory_addr_counter(23 downto 0)     <=   adc5_mem_addr_counter(23 downto 0) ;



memory_write_logic: process (ddr2_write_fifo_clk) 
begin
   if rising_edge(ddr2_write_fifo_clk) then  -- 
      fsm_mem_reset               <=   adc_buffer_logic_reset ;
      fsm_mem_write_logic_enable  <=   ddr2_write_logic_enable ;
   end if;

   if rising_edge(ddr2_write_fifo_clk) then  -- 
      adc_sample_logic_active_or  <=    adc1_sample_logic_active  
                                    or  adc2_sample_logic_active
                                    or  adc3_sample_logic_active 
                                    or  adc4_sample_logic_active 
                                    or  adc5_sample_logic_active ;
   end if;

   if rising_edge(ddr2_write_fifo_clk) then  -- 
      if (fsm_mem_addr_ld = '1') then 
         adc1_mem_addr_counter   <=   adc1_sample_memory_start_addr ;
         adc2_mem_addr_counter   <=   adc2_sample_memory_start_addr ;
         adc3_mem_addr_counter   <=   adc3_sample_memory_start_addr ;
         adc4_mem_addr_counter   <=   adc4_sample_memory_start_addr ;
         adc5_mem_addr_counter   <=   adc5_sample_memory_start_addr ;
       elsif (fsm_mem_addr_inc = '1') then
          case fsm_mem_ch_mux_counter(3 downto 0) is
             when X"0" =>  adc1_mem_addr_counter  <=   adc1_mem_addr_counter + 1  ;
             when X"1" =>  adc2_mem_addr_counter  <=   adc2_mem_addr_counter + 1  ;
             when X"2" =>  adc3_mem_addr_counter  <=   adc3_mem_addr_counter + 1  ;
             when X"3" =>  adc4_mem_addr_counter  <=   adc4_mem_addr_counter + 1  ;
             when X"4" =>  adc5_mem_addr_counter  <=   adc5_mem_addr_counter + 1  ;

             when others => null;
          end case;				
       else
         adc1_mem_addr_counter  <=   adc1_mem_addr_counter ;
         adc2_mem_addr_counter  <=   adc2_mem_addr_counter ;
         adc3_mem_addr_counter  <=   adc3_mem_addr_counter ;
         adc4_mem_addr_counter  <=   adc4_mem_addr_counter ;
         adc5_mem_addr_counter  <=   adc5_mem_addr_counter ;
		end if ; 
   end if;


   if rising_edge(ddr2_write_fifo_clk) then  -- 
      case fsm_mem_ch_mux_counter(3 downto 0) is
          when X"0" =>  fsm_adcx_buffer_fifo_rd_count        <=   adc1_buffer_fifo_rd_count  ;
                        fsm_adcx_buffer_fifo_rd_count_ge2    <=   to_std_logic(adc1_buffer_fifo_rd_count(10 downto 1) /= "00000000") ;
                        fsm_adcx_buffer_fifo_rd_count_ge128  <=   to_std_logic(adc1_buffer_fifo_rd_count(10 downto 7) /= "0000") ;

          when X"1" =>  fsm_adcx_buffer_fifo_rd_count        <=   adc2_buffer_fifo_rd_count  ;
                        fsm_adcx_buffer_fifo_rd_count_ge2    <=   to_std_logic(adc2_buffer_fifo_rd_count(10 downto 1) /= "00000000") ;
                        fsm_adcx_buffer_fifo_rd_count_ge128  <=   to_std_logic(adc2_buffer_fifo_rd_count(10 downto 7) /= "0000") ;

          when X"2" =>  fsm_adcx_buffer_fifo_rd_count        <=   adc3_buffer_fifo_rd_count  ;
                        fsm_adcx_buffer_fifo_rd_count_ge2    <=   to_std_logic(adc3_buffer_fifo_rd_count(10 downto 1) /= "00000000") ;
                        fsm_adcx_buffer_fifo_rd_count_ge128  <=   to_std_logic(adc3_buffer_fifo_rd_count(10 downto 7) /= "0000") ;

          when X"3" =>  fsm_adcx_buffer_fifo_rd_count        <=   adc4_buffer_fifo_rd_count  ;
                        fsm_adcx_buffer_fifo_rd_count_ge2    <=   to_std_logic(adc4_buffer_fifo_rd_count(10 downto 1) /= "00000000") ;
                        fsm_adcx_buffer_fifo_rd_count_ge128  <=   to_std_logic(adc4_buffer_fifo_rd_count(10 downto 7) /= "0000") ;

          when X"4" =>  fsm_adcx_buffer_fifo_rd_count        <=   adc5_buffer_fifo_rd_count  ;
                        fsm_adcx_buffer_fifo_rd_count_ge2    <=   to_std_logic(adc5_buffer_fifo_rd_count(10 downto 1) /= "00000000") ;
                        fsm_adcx_buffer_fifo_rd_count_ge128  <=   to_std_logic(adc5_buffer_fifo_rd_count(10 downto 7) /= "0000") ;


 
          when others => null;
       end case;				
   end if;


   if rising_edge(ddr2_write_fifo_clk) then  -- 
      mem_write_addr_fifo_almost_full   <=  to_std_logic(ddr2_write_addr_fifo_wr_count(9 downto 8) > "01") ; -- gt 1/2 (noch Platz f�r 256 addressen oder 512 Daten)
      mem_write_addr_fifo_almost_empty  <=  to_std_logic(ddr2_write_addr_fifo_wr_count(9 downto 0) = "0000000000") ; -- max. nur ein Address Word im Fifo
   end if;


-- -----

   adc1_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"0") ; --  
   adc2_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"1") ; --  
   adc3_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"2") ; --  
   adc4_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"3") ; --  
   adc5_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"4") ; --  



   fsm_mem_addr_inc              <=   fsm_mem_wr_addr_fifo_wr_en ;
   if rising_edge(ddr2_write_fifo_clk) then  -- 
      ddr2_write_addr_fifo_wr_en    <=   fsm_mem_wr_addr_fifo_wr_en ;
      ddr2_write_data_fifo_wr_en    <=   fsm_mem_wr_data_fifo_wr_en ;

      ddr2_write_addr_fifo_din(31 downto 26)  <=   "000000"  ;
      ddr2_write_addr_fifo_din(1 downto 0)    <=   "00"  ;

      case fsm_mem_ch_mux_counter(3 downto 0) is
          when X"0" =>  ddr2_write_data_fifo_din               <=   adc1_buffer_fifo_dout  ;
                        ddr2_write_addr_fifo_din(25 downto 2)  <=   adc1_mem_addr_counter(23 downto 0)  ;
          when X"1" =>  ddr2_write_data_fifo_din               <=   adc2_buffer_fifo_dout  ;
                        ddr2_write_addr_fifo_din(25 downto 2)  <=   adc2_mem_addr_counter(23 downto 0)  ;
          when X"2" =>  ddr2_write_data_fifo_din               <=   adc3_buffer_fifo_dout  ;
                        ddr2_write_addr_fifo_din(25 downto 2)  <=   adc3_mem_addr_counter(23 downto 0)  ;
          when X"3" =>  ddr2_write_data_fifo_din               <=   adc4_buffer_fifo_dout  ;
                        ddr2_write_addr_fifo_din(25 downto 2)  <=   adc4_mem_addr_counter(23 downto 0)  ;
          when X"4" =>  ddr2_write_data_fifo_din               <=   adc5_buffer_fifo_dout  ;
                        ddr2_write_addr_fifo_din(25 downto 2)  <=   adc5_mem_addr_counter(23 downto 0)  ;
  
          when others => null;
       end case;				
   end if;


end process;



memory_write_controller_fsm: process (ddr2_write_fifo_clk) 
begin



if  rising_edge(ddr2_write_fifo_clk) then
   if (fsm_mem_reset = '1')       then  
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                                   fsm_mem_counter              <=   X"000"      ;  
                                   fsm_mem_state                <=   MEM_IDLE    ;   
    else

     case fsm_mem_state is

---------- 
        when MEM_IDLE =>   
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                                   fsm_mem_counter              <=   X"000"      ;  
                 if (fsm_mem_write_logic_enable = '1') then --  
                                   fsm_mem_addr_ld              <=   '1'    ;  
                                   fsm_mem_state                <=   MEM_START    ;   
                  else
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_state                <=   MEM_IDLE    ;   
                 end if;

---------- 
        when MEM_START =>   
                                   fsm_mem_active               <=   '1'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                 if (fsm_mem_write_logic_enable = '1') then --  
                    if (mem_write_addr_fifo_almost_full = '1') then --  weiter nur wenn noch Platz f�r einen Buffer
                                   fsm_mem_state                <=   MEM_START    ;   
                     else
                                   fsm_mem_state                <=   MEM_CH_CHECK    ;   
                    end if;
                  else
                                   fsm_mem_state                <=   MEM_IDLE    ;   
                 end if;

---------- 

        when MEM_CH_CHECK =>   
                                   fsm_mem_active               <=   fsm_mem_active    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  

                 if (fsm_adcx_buffer_fifo_rd_count_ge2 = '1') then --  dann hat der channel buffer daten else gehe zum n�chsten channel
					   -- wenn buffer > 128 ist oder das Address fifo almost empty ist, dann kann ein neuer Buffer geschrieben werden
						-- else warte bis eine Bedingung zu trifft (dieses soll verhindern, das viele Addressen mit jeweils neuer page ins Fifo kommen)
                    if ((fsm_adcx_buffer_fifo_rd_count_ge128 = '1') or (mem_write_addr_fifo_almost_empty = '1')) then --  
                                   fsm_mem_counter(11)          <=   '0'     ;  
                                   fsm_mem_counter(10 downto 1) <=   fsm_adcx_buffer_fifo_rd_count(10 downto 1)     ;  
                                   fsm_mem_counter(0)           <=   '0'     ;  
                                   fsm_mem_state                <=   MEM_CH_COPY    ;   
                     else
                                   fsm_mem_counter(11)          <=   '0'     ;  
                                   fsm_mem_counter(10 downto 1)  <=   fsm_adcx_buffer_fifo_rd_count(10 downto 1)     ;  
                                   fsm_mem_counter(0)           <=   '0'     ;  
                                   fsm_mem_state                <=   MEM_CH_CHECK    ;   
                    end if;

                  else
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_CH_END    ;   
                 end if;



---------- 

        when MEM_CH_COPY =>   
                                   fsm_mem_active               <=   fsm_mem_active    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  
                 if (fsm_mem_counter = X"000") then --  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_CH_END    ;   
                  else
                                   fsm_mem_wr_addr_fifo_wr_en   <=   fsm_mem_counter(0)    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '1'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter - 1     ;  
                                   fsm_mem_state                <=   MEM_CH_COPY    ;   
                 end if;

---------- 

        when MEM_CH_END =>   
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_START    ;   
                 if (fsm_mem_ch_mux_counter = X"4") then --  
--                 if (fsm_mem_ch_mux_counter = X"9") then --  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                  else
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter + 1  ;  
                 end if;

----------
        when OTHERS => NULL ;
     end case ;
      
   end if;
end if;

end process;

end Behavioral;

