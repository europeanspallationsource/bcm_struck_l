----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:01:25 08/03/2010 
-- Design Name: 
-- Module Name:    adc_ringbuffer_delay_block - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adc_ringbuffer_delay_block is
    Port ( adc_clk : in  STD_LOGIC;
           adc_ringbuffer_logic_reset : in  STD_LOGIC;
           adc_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
--           adc_ringbuffer_trig_in : in  STD_LOGIC;
--           adc_ringbuffer_trig_out : out  STD_LOGIC;
           adc_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0));
end adc_ringbuffer_delay_block;

architecture Behavioral of adc_ringbuffer_delay_block is

signal addr_cnt_A: std_logic_VECTOR (10 downto 0);
signal addr_cnt_B: std_logic_VECTOR (10 downto 0);
signal ringbuffer_ce: std_logic;
signal ringbuffer_delay_ce: std_logic;

signal ringbuffer_we: std_logic_VECTOR(0 downto 0);

signal sy_ringbuffer_delay: std_logic_VECTOR (10 downto 0);


component dual_ram2kx18
	port (
	clka: IN std_logic;
	ena: IN std_logic;
	wea: IN std_logic_VECTOR(0 downto 0);
	addra: IN std_logic_VECTOR(10 downto 0);
	dina: IN std_logic_VECTOR(17 downto 0);
	clkb: IN std_logic;
	enb: IN std_logic;
	addrb: IN std_logic_VECTOR(10 downto 0);
	doutb: OUT std_logic_VECTOR(17 downto 0));
end component;



begin

address_logic: process (adc_clk)
begin
   if rising_edge(adc_clk) then  -- 
      if (adc_ringbuffer_delay_val > X"7FC") then 
--      if (adc_ringbuffer_delay_val > "11111111100") then 
	      sy_ringbuffer_delay  <=  "11111111110" ; -- X"7FE"
       else
	      sy_ringbuffer_delay  <=  adc_ringbuffer_delay_val(10 downto 0) + 2 ;
      end if;
   end if;

   
   if rising_edge(adc_clk) then  -- 
      if (ringbuffer_ce = '0') then
         addr_cnt_B   <=  "00000000000" ;
		 else
         addr_cnt_B   <=  addr_cnt_B + 1 ;
      end if;
      -- !! Achtung: adc_ringbuffer_delay_val darf nicht 0 oder 1 sein
	   addr_cnt_A  <=  addr_cnt_B + sy_ringbuffer_delay ;
		 
   end if;

   if rising_edge(adc_clk) then  -- 
      ringbuffer_ce         <=  not adc_ringbuffer_logic_reset ;
      ringbuffer_delay_ce   <=      ringbuffer_ce  ;
   end if;
   ringbuffer_we(0) <= ringbuffer_ce ;
end process;

ringbuffer_ram2x18 : dual_ram2kx18
		port map (
			clka => adc_clk,
			ena => ringbuffer_ce,
			wea => ringbuffer_we,
			addra => addr_cnt_A(10 downto 0),
			dina => adc_ringbuffer_din(17 downto 0),
			clkb => adc_clk,
			enb => ringbuffer_ce,
			addrb =>  addr_cnt_B(10 downto 0),
			doutb => adc_ringbuffer_dout(17 downto 0));


end Behavioral;

