----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:14:50 04/29/2011 
-- Design Name: 
-- Module Name:    i2c_master_interface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_interface is
	port(
		clk : in std_logic;
		rst : in std_logic;
		--
		i2c_reg_q : in std_logic_vector(32 downto 0);
		i2c_reg_feedback : out std_logic_vector(31 downto 0);
		--
		I2C_SDA : inout std_logic;
		I2C_SCL : inout std_logic 
	);
end i2c_master_interface;

architecture Behavioral of i2c_master_interface is



	COMPONENT i2c_master_fsm
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		i2c_sda_in : IN std_logic;
		i2c_reg_q : IN std_logic_vector(32 downto 0);          
		fsm_busy_out : OUT std_logic;
		fsm_ack_out : OUT std_logic;
		fsm_shiftreg_out : OUT std_logic_vector(7 downto 0);
		fsm_sda_oe_out : OUT std_logic;
		fsm_scl_oe_out : OUT std_logic
		);
	END COMPONENT;


	
	signal fsm_sda_oe : std_logic;
	signal fsm_scl_oe : std_logic;
	
	signal fsm_shiftreg : std_logic_vector(7 downto 0);
	signal fsm_ack : std_logic;
	
	signal fsm_busy : std_logic;
	

	signal i2c_sda_in : std_logic;
	signal i2c_sda_oe : std_logic;
	signal i2c_scl_in : std_logic;
	signal i2c_scl_oe : std_logic;

begin



	process(clk)
	begin
		if rising_edge(clk) then

			-- sda
			if i2c_sda_oe = '1' then
				I2C_SDA <= '0';
			else
				I2C_SDA <= 'Z';
			end if;
			i2c_sda_in <= I2C_SDA;

			-- scl
			if i2c_scl_oe = '1' then
				I2C_SCL <= '0';
			else
				I2C_SCL <= 'Z';
			end if;
			i2c_scl_in <= I2C_SCL;

		end if;
	end process;


	-- register read
	i2c_reg_feedback(31)				<= fsm_busy;
	i2c_reg_feedback(30 downto 9)	<= (others => '0');
	i2c_reg_feedback(8)				<= fsm_ack;
	i2c_reg_feedback(7 downto 0)	<= fsm_shiftreg;
	


	Inst_i2c_master_fsm: i2c_master_fsm PORT MAP(
		clk => clk,
		rst => rst,
		i2c_sda_in => i2c_sda_in,
		i2c_reg_q => i2c_reg_q,
		fsm_busy_out => fsm_busy,
		fsm_ack_out => fsm_ack,
		fsm_shiftreg_out => fsm_shiftreg,
		fsm_sda_oe_out => i2c_sda_oe,
		fsm_scl_oe_out => i2c_scl_oe
	);


end Behavioral;

