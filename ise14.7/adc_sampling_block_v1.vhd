----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:14:31 08/24/2010 
-- Design Name: 
-- Module Name:    adc_sampling_block_v1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
--use work.sis8300_pack.all ;

entity adc_sampling_block_v1 is
    Port ( adc_buffer_logic_reset : in  STD_LOGIC;
           adc1_clk : in  STD_LOGIC;
           adc2_clk : in  STD_LOGIC;
           adc3_clk : in  STD_LOGIC;
           adc4_clk : in  STD_LOGIC;
           adc5_clk : in  STD_LOGIC;

           adc_ch1_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch2_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch3_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch4_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch5_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch6_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch7_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch8_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch9_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch10_ringbuffer_dout : in  STD_LOGIC_VECTOR (15 downto 0);

           adc_sample_length_reg : in   STD_LOGIC_VECTOR(25 downto 0);

           adc_ch1_start_pulse : in  STD_LOGIC;
           adc_ch2_start_pulse : in  STD_LOGIC;
           adc_ch3_start_pulse : in  STD_LOGIC;
           adc_ch4_start_pulse : in  STD_LOGIC;
           adc_ch5_start_pulse : in  STD_LOGIC;
           adc_ch6_start_pulse : in  STD_LOGIC;
           adc_ch7_start_pulse : in  STD_LOGIC;
           adc_ch8_start_pulse : in  STD_LOGIC;
           adc_ch9_start_pulse : in  STD_LOGIC;
           adc_ch10_start_pulse : in  STD_LOGIC;
 
 
           adc_ch1_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch2_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch3_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch4_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch5_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch6_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch7_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch8_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch9_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);
           adc_ch10_sample_memory_start_addr : in   STD_LOGIC_VECTOR(25 downto 0);

           adc_ch1_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch2_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch3_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch4_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch5_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch6_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch7_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch8_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch9_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);
           adc_ch10_adc_memory_addr_counter : OUT   STD_LOGIC_VECTOR(31 downto 0);

           adc_chx_sample_logic_active_or : OUT std_logic;
           adc_chx_sample_buffer_not_empty_or : OUT std_logic;

           ddr3_write_logic_enable: in  STD_LOGIC;
           ddr3_write_fifo_clk: in  STD_LOGIC;
           ddr3_write_addr_fifo_wr_count : in  std_logic_vector(9 downto 0);  
           ddr3_write_addr_fifo_wr_en : OUT std_logic;
           ddr3_write_addr_fifo_din : OUT std_logic_vector(31 downto 0) ;
           ddr3_write_data_fifo_wr_en : OUT std_logic;
           ddr3_write_data_fifo_din : OUT std_logic_vector(255 downto 0) 
			  );
end adc_sampling_block_v1;

architecture Behavioral of adc_sampling_block_v1 is

   Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
   begin
   if x then ret := '1';  else ret := '0'; end if;
   return ret;
   end to_std_logic;


	COMPONENT risingedge2pulse_generator
	PORT(
		adc_clk : IN std_logic;
		rising_edge_signal_in : IN std_logic;          
		signal_pulse_out : OUT std_logic
		);
	END COMPONENT;

	COMPONENT channel_sample_block_v1
	PORT(
		adc_sample_logic_reset : IN std_logic;
		adc_clk : IN std_logic;
		adc_start_pulse : IN std_logic;
		adc_sample_block_length_reg : IN std_logic_vector(25 downto 0);
		adc_din : IN std_logic_vector(15 downto 0);
		ch_buffer_fifo_rd_clk : IN std_logic;
		ch_buffer_fifo_rd_en : IN std_logic;          
		ch_sample_logic_active : OUT std_logic;
		ch_buffer_fifo_dout : OUT std_logic_vector(255 downto 0);
		ch_buffer_fifo_full : OUT std_logic;
		ch_buffer_fifo_empty : OUT std_logic;
		ch_buffer_fifo_almost_empty : OUT std_logic;
		ch_buffer_fifo_rd_count : OUT std_logic_vector(9 downto 0)
		);
	END COMPONENT;

--  type adc_data_array_type is array(0 to 9) of std_logic_vector(13 downto 0) ;



signal adc1_logic_reset: std_logic;
signal adc2_logic_reset: std_logic;
signal adc3_logic_reset: std_logic;
signal adc4_logic_reset: std_logic;
signal adc5_logic_reset: std_logic;

signal ch1_sample_logic_active: std_logic;
signal ch1_buffer_fifo_rd_en: std_logic;
signal ch1_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch1_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch1_buffer_fifo_empty: std_logic;
signal ch1_buffer_fifo_almost_empty: std_logic;
signal ch1_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch2_sample_logic_active: std_logic;
signal ch2_buffer_fifo_rd_en: std_logic;
signal ch2_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch2_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch2_buffer_fifo_empty: std_logic;
signal ch2_buffer_fifo_almost_empty: std_logic;
signal ch2_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch3_sample_logic_active: std_logic;
signal ch3_buffer_fifo_rd_en: std_logic;
signal ch3_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch3_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch3_buffer_fifo_empty: std_logic;
signal ch3_buffer_fifo_almost_empty: std_logic;
signal ch3_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch4_sample_logic_active: std_logic;
signal ch4_buffer_fifo_rd_en: std_logic;
signal ch4_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch4_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch4_buffer_fifo_empty: std_logic;
signal ch4_buffer_fifo_almost_empty: std_logic;
signal ch4_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch5_sample_logic_active: std_logic;
signal ch5_buffer_fifo_rd_en: std_logic;
signal ch5_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch5_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch5_buffer_fifo_empty: std_logic;
signal ch5_buffer_fifo_almost_empty: std_logic;
signal ch5_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch6_sample_logic_active: std_logic;
signal ch6_buffer_fifo_rd_en: std_logic;
signal ch6_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch6_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch6_buffer_fifo_empty: std_logic;
signal ch6_buffer_fifo_almost_empty: std_logic;
signal ch6_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch7_sample_logic_active: std_logic;
signal ch7_buffer_fifo_rd_en: std_logic;
signal ch7_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch7_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch7_buffer_fifo_empty: std_logic;
signal ch7_buffer_fifo_almost_empty: std_logic;
signal ch7_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch8_sample_logic_active: std_logic;
signal ch8_buffer_fifo_rd_en: std_logic;
signal ch8_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch8_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch8_buffer_fifo_empty: std_logic;
signal ch8_buffer_fifo_almost_empty: std_logic;
signal ch8_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch9_sample_logic_active: std_logic;
signal ch9_buffer_fifo_rd_en: std_logic;
signal ch9_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch9_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch9_buffer_fifo_empty: std_logic;
signal ch9_buffer_fifo_almost_empty: std_logic;
signal ch9_mem_addr_counter: std_logic_vector(25 downto 0);

signal ch10_sample_logic_active: std_logic;
signal ch10_buffer_fifo_rd_en: std_logic;
signal ch10_buffer_fifo_dout: std_logic_vector(255 downto 0);
signal ch10_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
signal ch10_buffer_fifo_empty: std_logic;
signal ch10_buffer_fifo_almost_empty: std_logic;
signal ch10_mem_addr_counter: std_logic_vector(25 downto 0);




signal ch_sample_logic_active_or: std_logic;
signal fsm_chx_buffer_fifo_rd_count: std_logic_vector(9 downto 0);
--signal fsm_chx_buffer_fifo_rd_count_ge2: std_logic;
--signal fsm_chx_buffer_fifo_rd_count_ge128: std_logic;
signal fsm_chx_buffer_fifo_rd_count_ge1: std_logic;
signal fsm_chx_buffer_fifo_rd_count_ge64: std_logic;


signal fsm_mem_reset: std_logic;
signal fsm_mem_write_logic_enable: std_logic;
signal mem_write_addr_fifo_almost_full: std_logic;
signal mem_write_addr_fifo_almost_empty: std_logic;
signal fsm_mem_addr_inc: std_logic;


signal fsm_mem_active: std_logic;
signal fsm_mem_addr_ld: std_logic;
signal fsm_mem_wr_addr_fifo_wr_en: std_logic;
signal fsm_mem_wr_data_fifo_wr_en: std_logic;
signal fsm_mem_ch_mux_counter: std_logic_vector(3 downto 0);
signal fsm_mem_counter: std_logic_vector(11 downto 0);


type fsm_mem_type is(
		MEM_IDLE,	--  
		MEM_START,	--  
		MEM_CH_CHECK,	--  
		MEM_CH_COPY,	--  
		MEM_CH_END 
);
signal fsm_mem_state		: fsm_mem_type;


attribute keep : boolean;

attribute keep of fsm_mem_state: signal is true;
attribute keep of ddr3_write_addr_fifo_wr_count: signal is true;
attribute keep of fsm_mem_wr_addr_fifo_wr_en: signal is true;
attribute keep of fsm_mem_wr_data_fifo_wr_en: signal is true;
attribute keep of fsm_mem_addr_ld: signal is true;
attribute keep of fsm_mem_write_logic_enable: signal is true;

begin


	adc1_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc1_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc1_logic_reset
	);

	ch1_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc1_logic_reset,
		adc_clk => adc1_clk,
		adc_start_pulse => adc_ch1_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch1_ringbuffer_dout,
		ch_sample_logic_active => ch1_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch1_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch1_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch1_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch1_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch1_buffer_fifo_rd_count
	);

	ch2_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc1_logic_reset,
		adc_clk => adc1_clk,
		adc_start_pulse => adc_ch2_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch2_ringbuffer_dout,
		ch_sample_logic_active => ch2_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch2_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch2_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch2_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch2_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch2_buffer_fifo_rd_count
	);
--
--   ch2_sample_logic_active      <= ch1_sample_logic_active ;
--   ch2_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch2_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch2_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch2_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;

-- ************************************

	adc2_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc2_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc2_logic_reset
	);

	ch3_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc2_logic_reset,
		adc_clk => adc2_clk,
		adc_start_pulse => adc_ch3_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch3_ringbuffer_dout,
		ch_sample_logic_active => ch3_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch3_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch3_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch3_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch3_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch3_buffer_fifo_rd_count
	);

	ch4_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc2_logic_reset,
		adc_clk => adc2_clk,
		adc_start_pulse => adc_ch4_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch4_ringbuffer_dout,
		ch_sample_logic_active => ch4_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch4_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch4_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch4_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch4_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch4_buffer_fifo_rd_count
	);

--   ch3_sample_logic_active      <= ch1_sample_logic_active ;
--   ch3_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch3_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch3_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch3_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;
--
--   ch4_sample_logic_active      <= ch1_sample_logic_active ;
--   ch4_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch4_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch4_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch4_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;

-- ************************************

	adc3_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc3_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc3_logic_reset
	);

	ch5_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc3_logic_reset,
		adc_clk => adc3_clk,
		adc_start_pulse => adc_ch5_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch5_ringbuffer_dout,
		ch_sample_logic_active => ch5_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch5_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch5_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch5_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch5_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch5_buffer_fifo_rd_count
	);

	ch6_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc3_logic_reset,
		adc_clk => adc3_clk,
		adc_start_pulse => adc_ch6_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch6_ringbuffer_dout,
		ch_sample_logic_active => ch6_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch6_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch6_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch6_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch6_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch6_buffer_fifo_rd_count
	);

--   ch5_sample_logic_active      <= ch1_sample_logic_active ;
--   ch5_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch5_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch5_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch5_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;
--
--   ch6_sample_logic_active      <= ch1_sample_logic_active ;
--   ch6_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch6_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch6_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch6_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;

-- ************************************

	adc4_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc4_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc4_logic_reset
	);

	ch7_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc4_logic_reset,
		adc_clk => adc4_clk,
		adc_start_pulse => adc_ch7_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch7_ringbuffer_dout,
		ch_sample_logic_active => ch7_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch7_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch7_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch7_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch7_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch7_buffer_fifo_rd_count
	);

	ch8_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc4_logic_reset,
		adc_clk => adc4_clk,
		adc_start_pulse => adc_ch8_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch8_ringbuffer_dout,
		ch_sample_logic_active => ch8_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch8_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch8_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch8_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch8_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch8_buffer_fifo_rd_count
	);

--   ch7_sample_logic_active      <= ch1_sample_logic_active ;
--   ch7_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch7_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch7_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch7_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;
--
--   ch8_sample_logic_active      <= ch1_sample_logic_active ;
--   ch8_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch8_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch8_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch8_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;

-- ************************************

	adc5_reset_risingedge2pulse_generator: risingedge2pulse_generator PORT MAP(
		adc_clk => adc5_clk,
		rising_edge_signal_in => adc_buffer_logic_reset,
		signal_pulse_out => adc5_logic_reset
	);

	ch9_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc5_logic_reset,
		adc_clk => adc5_clk,
		adc_start_pulse => adc_ch9_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch9_ringbuffer_dout,
		ch_sample_logic_active => ch9_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch9_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch9_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch9_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch9_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch9_buffer_fifo_rd_count
	);

	ch10_sample_block_v1: channel_sample_block_v1 PORT MAP(
		adc_sample_logic_reset => adc5_logic_reset,
		adc_clk => adc5_clk,
		adc_start_pulse => adc_ch10_start_pulse,
		adc_sample_block_length_reg => adc_sample_length_reg,
		adc_din => adc_ch10_ringbuffer_dout,
		ch_sample_logic_active => ch10_sample_logic_active,
		ch_buffer_fifo_rd_clk => ddr3_write_fifo_clk,
		ch_buffer_fifo_rd_en => ch10_buffer_fifo_rd_en,
		ch_buffer_fifo_dout => ch10_buffer_fifo_dout,
		ch_buffer_fifo_full => open,
		ch_buffer_fifo_empty => ch10_buffer_fifo_empty,
		ch_buffer_fifo_almost_empty => ch10_buffer_fifo_almost_empty,
		ch_buffer_fifo_rd_count => ch10_buffer_fifo_rd_count
	);

--   ch9_sample_logic_active      <= ch1_sample_logic_active ;
--   ch9_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch9_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch9_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch9_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;
--
--   ch10_sample_logic_active      <= ch1_sample_logic_active ;
--   ch10_buffer_fifo_dout         <= ch1_buffer_fifo_dout ;
--   ch10_buffer_fifo_empty        <= ch1_buffer_fifo_empty ;
--   ch10_buffer_fifo_almost_empty <= ch1_buffer_fifo_almost_empty ;
--   ch10_buffer_fifo_rd_count     <= ch1_buffer_fifo_rd_count ;
--
-- **************************************************************************************************************
-- ddr3 write clock domain


            
   adc_chx_sample_logic_active_or         <=    ch_sample_logic_active_or ;
   adc_chx_sample_buffer_not_empty_or     <=    not ch1_buffer_fifo_empty or not ch2_buffer_fifo_empty 
                                             or not ch3_buffer_fifo_empty or not ch4_buffer_fifo_empty  
                                             or not ch5_buffer_fifo_empty or not ch6_buffer_fifo_empty  
                                             or not ch7_buffer_fifo_empty or not ch8_buffer_fifo_empty  
                                             or not ch9_buffer_fifo_empty or not ch10_buffer_fifo_empty  ;

   adc_ch1_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch1_adc_memory_addr_counter(25 downto 0)     <=   ch1_mem_addr_counter(25 downto 0) ;

   adc_ch2_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch2_adc_memory_addr_counter(25 downto 0)     <=   ch2_mem_addr_counter(25 downto 0) ;

   adc_ch3_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch3_adc_memory_addr_counter(25 downto 0)     <=   ch3_mem_addr_counter(25 downto 0) ;

   adc_ch4_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch4_adc_memory_addr_counter(25 downto 0)     <=   ch4_mem_addr_counter(25 downto 0) ;

   adc_ch5_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch5_adc_memory_addr_counter(25 downto 0)     <=   ch5_mem_addr_counter(25 downto 0) ;

   adc_ch6_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch6_adc_memory_addr_counter(25 downto 0)     <=   ch6_mem_addr_counter(25 downto 0) ;

   adc_ch7_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch7_adc_memory_addr_counter(25 downto 0)     <=   ch7_mem_addr_counter(25 downto 0) ;

   adc_ch8_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch8_adc_memory_addr_counter(25 downto 0)     <=   ch8_mem_addr_counter(25 downto 0) ;

   adc_ch9_adc_memory_addr_counter(31 downto 26)    <=   "000000" ;
   adc_ch9_adc_memory_addr_counter(25 downto 0)     <=   ch9_mem_addr_counter(25 downto 0) ;

   adc_ch10_adc_memory_addr_counter(31 downto 26)   <=   "000000"  ;
   adc_ch10_adc_memory_addr_counter(25 downto 0)    <=   ch10_mem_addr_counter(25 downto 0) ;



memory_write_logic: process (ddr3_write_fifo_clk) 
begin
   if rising_edge(ddr3_write_fifo_clk) then  -- 
      fsm_mem_reset               <=   adc_buffer_logic_reset ;
      fsm_mem_write_logic_enable  <=   ddr3_write_logic_enable ;
   end if;

   if rising_edge(ddr3_write_fifo_clk) then  -- 
      ch_sample_logic_active_or  <=    ch1_sample_logic_active or ch2_sample_logic_active 
                                    or ch3_sample_logic_active or ch4_sample_logic_active 
                                    or ch5_sample_logic_active or ch6_sample_logic_active 
                                    or ch7_sample_logic_active or ch8_sample_logic_active 
                                    or ch9_sample_logic_active or ch10_sample_logic_active ;
   end if;

   if rising_edge(ddr3_write_fifo_clk) then  -- 
      if (fsm_mem_addr_ld = '1') then 
         ch1_mem_addr_counter   <=   adc_ch1_sample_memory_start_addr ;
         ch2_mem_addr_counter   <=   adc_ch2_sample_memory_start_addr ;
         ch3_mem_addr_counter   <=   adc_ch3_sample_memory_start_addr ;
         ch4_mem_addr_counter   <=   adc_ch4_sample_memory_start_addr ;
         ch5_mem_addr_counter   <=   adc_ch5_sample_memory_start_addr ;
         ch6_mem_addr_counter   <=   adc_ch6_sample_memory_start_addr ;
         ch7_mem_addr_counter   <=   adc_ch7_sample_memory_start_addr ;
         ch8_mem_addr_counter   <=   adc_ch8_sample_memory_start_addr ;
         ch9_mem_addr_counter   <=   adc_ch9_sample_memory_start_addr ;
         ch10_mem_addr_counter  <=   adc_ch10_sample_memory_start_addr ;
       elsif (fsm_mem_addr_inc = '1') then
          case fsm_mem_ch_mux_counter(3 downto 0) is
             when X"0" =>  ch1_mem_addr_counter  <=   ch1_mem_addr_counter + 1  ;
             when X"1" =>  ch2_mem_addr_counter  <=   ch2_mem_addr_counter + 1  ;
             when X"2" =>  ch3_mem_addr_counter  <=   ch3_mem_addr_counter + 1  ;
             when X"3" =>  ch4_mem_addr_counter  <=   ch4_mem_addr_counter + 1  ;
             when X"4" =>  ch5_mem_addr_counter  <=   ch5_mem_addr_counter + 1  ;
             when X"5" =>  ch6_mem_addr_counter  <=   ch6_mem_addr_counter + 1  ;
             when X"6" =>  ch7_mem_addr_counter  <=   ch7_mem_addr_counter + 1  ;
             when X"7" =>  ch8_mem_addr_counter  <=   ch8_mem_addr_counter + 1  ;
             when X"8" =>  ch9_mem_addr_counter  <=   ch9_mem_addr_counter + 1  ;
             when X"9" =>  ch10_mem_addr_counter <=   ch10_mem_addr_counter + 1  ;


             when others => null;
          end case;				
       else
         ch1_mem_addr_counter  <=   ch1_mem_addr_counter ;
         ch2_mem_addr_counter  <=   ch2_mem_addr_counter ;
         ch3_mem_addr_counter  <=   ch3_mem_addr_counter ;
         ch4_mem_addr_counter  <=   ch4_mem_addr_counter ;
         ch5_mem_addr_counter  <=   ch5_mem_addr_counter ;
         ch6_mem_addr_counter  <=   ch6_mem_addr_counter ;
         ch7_mem_addr_counter  <=   ch7_mem_addr_counter ;
         ch8_mem_addr_counter  <=   ch8_mem_addr_counter ;
         ch9_mem_addr_counter  <=   ch9_mem_addr_counter ;
         ch10_mem_addr_counter  <=   ch10_mem_addr_counter ;
		end if ; 
   end if;


   if rising_edge(ddr3_write_fifo_clk) then  -- 
      case fsm_mem_ch_mux_counter(3 downto 0) is
          when X"0" =>  fsm_chx_buffer_fifo_rd_count        <=   ch1_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch1_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch1_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch1_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch1_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"1" =>  fsm_chx_buffer_fifo_rd_count        <=   ch2_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch2_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch2_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch2_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch2_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"2" =>  fsm_chx_buffer_fifo_rd_count        <=   ch3_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch3_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch3_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch3_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch3_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"3" =>  fsm_chx_buffer_fifo_rd_count        <=   ch4_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch4_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch4_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch4_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch4_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"4" =>  fsm_chx_buffer_fifo_rd_count        <=   ch5_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch5_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch5_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch5_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch5_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"5" =>  fsm_chx_buffer_fifo_rd_count        <=   ch6_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch6_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch6_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch6_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch6_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"6" =>  fsm_chx_buffer_fifo_rd_count        <=   ch7_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch7_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch7_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch7_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch7_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"7" =>  fsm_chx_buffer_fifo_rd_count        <=   ch8_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch8_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch8_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch8_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch8_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"8" =>  fsm_chx_buffer_fifo_rd_count        <=   ch9_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch9_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch9_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch9_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch9_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

          when X"9" =>  fsm_chx_buffer_fifo_rd_count        <=   ch10_buffer_fifo_rd_count  ;
--                        fsm_chx_buffer_fifo_rd_count_ge2    <=   to_std_logic(ch10_buffer_fifo_rd_count(9 downto 1) /= "00000000") ;
--                        fsm_chx_buffer_fifo_rd_count_ge128  <=   to_std_logic(ch10_buffer_fifo_rd_count(9 downto 7) /= "000") ;
                        fsm_chx_buffer_fifo_rd_count_ge1    <=   not ch10_buffer_fifo_empty ;
                        fsm_chx_buffer_fifo_rd_count_ge64   <=   to_std_logic(ch10_buffer_fifo_rd_count(9 downto 6) /= "0000") ;

  
          when others => null;
       end case;				
   end if;


   if rising_edge(ddr3_write_fifo_clk) then  -- 
      mem_write_addr_fifo_almost_full   <=  to_std_logic(ddr3_write_addr_fifo_wr_count(9 downto 8) > "01") ; -- gt 1/2 (noch Platz f�r 256 addressen oder 512 Daten)
--      mem_write_addr_fifo_almost_empty  <=  to_std_logic(ddr3_write_addr_fifo_wr_count(9 downto 0) = "0000000000") ; -- max. nur ein Address Word im Fifo
      mem_write_addr_fifo_almost_empty  <=  to_std_logic(ddr3_write_addr_fifo_wr_count(9 downto 2) = "00000000") ; -- max. nur ein bis 3 Address Word im Fifo !!
-- !! Achtung : Wr_count = 2 after reset
   end if;


-- -----

   ch1_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"0") ; --  
   ch2_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"1") ; --  
   ch3_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"2") ; --  
   ch4_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"3") ; --  
   ch5_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"4") ; --  
   ch6_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"5") ; --  
   ch7_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"6") ; --  
   ch8_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"7") ; --  
   ch9_buffer_fifo_rd_en   <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"8") ; --  
   ch10_buffer_fifo_rd_en  <=  fsm_mem_wr_data_fifo_wr_en and to_std_logic(fsm_mem_ch_mux_counter(3 downto 0) = X"9") ; --  



   fsm_mem_addr_inc              <=   fsm_mem_wr_addr_fifo_wr_en ;
   if rising_edge(ddr3_write_fifo_clk) then  -- 
      ddr3_write_addr_fifo_wr_en    <=   fsm_mem_wr_addr_fifo_wr_en ;
      ddr3_write_data_fifo_wr_en    <=   fsm_mem_wr_data_fifo_wr_en ;

      ddr3_write_addr_fifo_din(31 downto 28)  <=   "0000"  ;
      ddr3_write_addr_fifo_din(1 downto 0)    <=   "00"  ;

      case fsm_mem_ch_mux_counter(3 downto 0) is
          when X"0" =>  ddr3_write_data_fifo_din               <=   ch1_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch1_mem_addr_counter(25 downto 0)  ;
          when X"1" =>  ddr3_write_data_fifo_din               <=   ch2_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch2_mem_addr_counter(25 downto 0)  ;
          when X"2" =>  ddr3_write_data_fifo_din               <=   ch3_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch3_mem_addr_counter(25 downto 0)  ;
          when X"3" =>  ddr3_write_data_fifo_din               <=   ch4_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch4_mem_addr_counter(25 downto 0)  ;
          when X"4" =>  ddr3_write_data_fifo_din               <=   ch5_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch5_mem_addr_counter(25 downto 0)  ;
          when X"5" =>  ddr3_write_data_fifo_din               <=   ch6_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch6_mem_addr_counter(25 downto 0)  ;
          when X"6" =>  ddr3_write_data_fifo_din               <=   ch7_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch7_mem_addr_counter(25 downto 0)  ;
          when X"7" =>  ddr3_write_data_fifo_din               <=   ch8_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch8_mem_addr_counter(25 downto 0)  ;
          when X"8" =>  ddr3_write_data_fifo_din               <=   ch9_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch9_mem_addr_counter(25 downto 0)  ;
          when X"9" =>  ddr3_write_data_fifo_din               <=   ch10_buffer_fifo_dout  ;
                        ddr3_write_addr_fifo_din(27 downto 2)  <=   ch10_mem_addr_counter(25 downto 0)  ;
  
          when others => null;
       end case;				
   end if;


end process;



memory_write_controller_fsm: process (ddr3_write_fifo_clk) 
begin



if  rising_edge(ddr3_write_fifo_clk) then
   if (fsm_mem_reset = '1')       then  
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                                   fsm_mem_counter              <=   X"000"      ;  
                                   fsm_mem_state                <=   MEM_IDLE    ;   
    else

     case fsm_mem_state is

---------- 
        when MEM_IDLE =>   
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                                   fsm_mem_counter              <=   X"000"      ;  
                 if (fsm_mem_write_logic_enable = '1') then --  
                                   fsm_mem_addr_ld              <=   '1'    ;  
                                   fsm_mem_state                <=   MEM_START    ;   
                  else
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_state                <=   MEM_IDLE    ;   
                 end if;

---------- 
        when MEM_START =>   
                                   fsm_mem_active               <=   '1'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                 if (fsm_mem_write_logic_enable = '1') then --  
                    if (mem_write_addr_fifo_almost_full = '1') then --  weiter nur wenn noch Platz f�r einen Buffer
                                   fsm_mem_state                <=   MEM_START    ;   
                     else
                                   fsm_mem_state                <=   MEM_CH_CHECK    ;   
                    end if;
                  else
                                   fsm_mem_state                <=   MEM_IDLE    ;   
                 end if;

---------- 

        when MEM_CH_CHECK =>   
                                   fsm_mem_active               <=   fsm_mem_active    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  

                 if (fsm_chx_buffer_fifo_rd_count_ge1 = '1') then --  dann hat der channel buffer daten else gehe zum n�chsten channel
					   -- wenn buffer > 128 ist oder das Address fifo almost empty ist, dann kann ein neuer Buffer geschrieben werden
						-- else warte bis eine Bedingung zu trifft (dieses soll verhindern, das viele Addressen mit jeweils neuer page ins Fifo kommen)
                    if (fsm_chx_buffer_fifo_rd_count_ge64 = '1')  then --  
                                   fsm_mem_counter(11 downto 10)<=   "00"     ;  
                                   fsm_mem_counter(9 downto 0)  <=   "00"&X"40"     ;  
                                   fsm_mem_state                <=   MEM_CH_COPY    ;   
                     elsif (mem_write_addr_fifo_almost_empty = '1') then --  
                                   fsm_mem_counter(11 downto 10)<=   "00"     ;  
                                   fsm_mem_counter(9 downto 0)  <=   fsm_chx_buffer_fifo_rd_count(9 downto 0)     ;  
                                   fsm_mem_state                <=   MEM_CH_COPY    ;   
                     else
                                   fsm_mem_counter(11 downto 10)<=   "00"     ;  
                                   fsm_mem_counter(9 downto 0)  <=   fsm_chx_buffer_fifo_rd_count(9 downto 0)     ;  
--                                   fsm_mem_counter(0)           <=   '0'     ;  
                                   fsm_mem_state                <=   MEM_CH_CHECK    ;   
                    end if;

                  else
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_CH_END    ;   
                 end if;



---------- 

        when MEM_CH_COPY =>   
                                   fsm_mem_active               <=   fsm_mem_active    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter   ;  
                 if (fsm_mem_counter = X"000") then --  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_CH_END    ;   
                  else
--                                   fsm_mem_wr_addr_fifo_wr_en   <=   fsm_mem_counter(0)    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '1'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '1'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter - 1     ;  
                                   fsm_mem_state                <=   MEM_CH_COPY    ;   
                 end if;

---------- 

        when MEM_CH_END =>   
                                   fsm_mem_active               <=   '0'    ;  
                                   fsm_mem_addr_ld              <=   '0'    ;  
                                   fsm_mem_wr_addr_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_wr_data_fifo_wr_en   <=   '0'    ;  
                                   fsm_mem_counter              <=   fsm_mem_counter      ;  
                                   fsm_mem_state                <=   MEM_START    ;   
                 if (fsm_mem_ch_mux_counter = X"9") then --  
                                   fsm_mem_ch_mux_counter       <=   X"0"   ;  
                  else
                                   fsm_mem_ch_mux_counter       <=   fsm_mem_ch_mux_counter + 1  ;  
                 end if;

----------
        when OTHERS => NULL ;
     end case ;
      
   end if;
end if;

end process;

end Behavioral;

