----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:22:33 03/11/2013 
-- Design Name: 
-- Module Name:    dac_fd_out_logic - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity dac_fd_out_logic is
    Port ( dac_clk_in : in  STD_LOGIC;
           dac_odelay_clk : in  STD_LOGIC;
           dac_odelay_rst : in  STD_LOGIC;
           dac_odelay_value : IN std_logic_vector(4 downto 0);
           dac_dcm_reset_pulse : in  STD_LOGIC;
           dac_select_ch1 : in  STD_LOGIC;
           dac_data_out : IN std_logic_vector(15 downto 0);
           dac_clk_dcm_lock          : OUT std_logic;
           dac_out_synch_clk          : OUT std_logic;
           DAC_SELIQ_P          : OUT std_logic;
           DAC_SELIQ_N          : OUT std_logic;
           DAC_D_P             : OUT std_logic_vector(15 downto 0);
           DAC_D_N             : OUT std_logic_vector(15 downto 0)
			);
end dac_fd_out_logic;

architecture Behavioral of dac_fd_out_logic is


component ip_dac_data_output
generic
 (-- width of the data for the system
  sys_w       : integer := 16;
  -- width of the data for the device
  dev_w       : integer := 16);
port
 (
  -- From the device out to the system
  DATA_OUT_FROM_DEVICE    : in    std_logic_vector(dev_w-1 downto 0);
  DATA_OUT_TO_PINS_P      : out   std_logic_vector(sys_w-1 downto 0);
  DATA_OUT_TO_PINS_N      : out   std_logic_vector(sys_w-1 downto 0);

-- Input, Output delay control signals
  DELAY_CLK            : in    std_logic;
  DELAY_RESET          : in    std_logic;                    -- Active high synchronous reset for input delay
  DELAY_DATA_CE        : in    std_logic_vector(sys_w -1 downto 0);            -- Enable signal for delay for bit 
  DELAY_DATA_INC       : in    std_logic_vector(sys_w -1 downto 0);            -- Delay increment, decrement signal for bit 
  DELAY_LOCKED            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  REF_CLOCK               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.
 
-- Clock and reset signals
  CLK_IN                  : in    std_logic;                    -- Fast clock from PLL/MMCM 
  IO_RESET                : in    std_logic);                   -- Reset signal for IO circuit
end component;





	signal dac_clk_fb_in  : std_logic;
	signal dac_clk_bufg_in  : std_logic;
	signal dac_clk_bufg_out  : std_logic;

	signal dac_select_fd_q : std_logic;
	signal dac_select_fd_delayed : std_logic;
	signal dac_data_fd_delayed: std_logic_vector(15 downto 0);
	signal dac_data_fd_q: std_logic_vector(15 downto 0);


attribute IOB : string;
attribute IOB of Inst_FD_DAC_select_out: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out0: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out1: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out2: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out3: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out4: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out5: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out6: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out7: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out8: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out9: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out10: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out11: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out12: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out13: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out14: label is "TRUE";
attribute IOB of Inst_FD_DAC_data_out15: label is "TRUE";

begin

--	MMCM_BASE_inst : MMCM_BASE
--   generic map (
--      BANDWIDTH => "OPTIMIZED",  -- Jitter programming ("HIGH","LOW","OPTIMIZED")
--      CLKFBOUT_MULT_F => 5.0,    -- Multiply value for all CLKOUT (5.0-64.0).
--      CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB (0.00-360.00).
--      CLKIN1_PERIOD => 4.0,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
--      CLKOUT0_DIVIDE_F => 1.0,   -- Divide amount for CLKOUT0 (1.000-128.000).
--      -- CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.01-0.99).
--      CLKOUT0_DUTY_CYCLE => 0.5,
--      CLKOUT1_DUTY_CYCLE => 0.5,
--      CLKOUT2_DUTY_CYCLE => 0.5,
--      CLKOUT3_DUTY_CYCLE => 0.5,
--      CLKOUT4_DUTY_CYCLE => 0.5,
--      CLKOUT5_DUTY_CYCLE => 0.5,
--      CLKOUT6_DUTY_CYCLE => 0.5,
--      -- CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
--      CLKOUT0_PHASE => 0.0,
--      CLKOUT1_PHASE => 0.0,
--      CLKOUT2_PHASE => 0.0,
--      CLKOUT3_PHASE => 0.0,
--      CLKOUT4_PHASE => 0.0,
--      CLKOUT5_PHASE => 0.0,
--      CLKOUT6_PHASE => 0.0,
--      -- CLKOUT1_DIVIDE - CLKOUT6_DIVIDE: Divide amount for each CLKOUT (1-128)
--      CLKOUT1_DIVIDE => 5,
--      CLKOUT2_DIVIDE => 1,
--      CLKOUT3_DIVIDE => 1,
--      CLKOUT4_DIVIDE => 1,
--      CLKOUT5_DIVIDE => 1,
--      CLKOUT6_DIVIDE => 1,
--      CLKOUT4_CASCADE => FALSE,  -- Cascase CLKOUT4 counter with CLKOUT6 (TRUE/FALSE)
--      CLOCK_HOLD => FALSE,       -- Hold VCO Frequency (TRUE/FALSE)
--      DIVCLK_DIVIDE => 1,        -- Master division value (1-80)
--      REF_JITTER1 => 0.0,        -- Reference input jitter in UI (0.000-0.999).
--      STARTUP_WAIT => FALSE      -- Not supported. Must be set to FALSE.
--   )
--   port map (
--      -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
--      CLKOUT0 => dac_clk_bufg_in,     -- 1-bit output: CLKOUT0 output
--      CLKOUT0B => open,   -- 1-bit output: Inverted CLKOUT0 output
--      CLKOUT1 => open,     -- 1-bit output: CLKOUT1 output
--      CLKOUT1B => open,   -- 1-bit output: Inverted CLKOUT1 output
--      CLKOUT2 => open,     -- 1-bit output: CLKOUT2 output
--      CLKOUT2B => open,   -- 1-bit output: Inverted CLKOUT2 output
--      CLKOUT3 => open,     -- 1-bit output: CLKOUT3 output
--      CLKOUT3B => open,   -- 1-bit output: Inverted CLKOUT3 output
--      CLKOUT4 => open,     -- 1-bit output: CLKOUT4 output
--      CLKOUT5 => open,     -- 1-bit output: CLKOUT5 output
--      CLKOUT6 => open,     -- 1-bit output: CLKOUT6 output
--      -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
--      CLKFBOUT => dac_clk_fb_in,   -- 1-bit output: Feedback clock output
--      CLKFBOUTB => open, -- 1-bit output: Inverted CLKFBOUT output
--      -- Status Port: 1-bit (each) output: MMCM status ports
--      LOCKED => dac_clk_dcm_lock,       -- 1-bit output: LOCK output
--      -- Clock Input: 1-bit (each) input: Clock input
--      CLKIN1 => dac_clk_in,
--      -- Control Ports: 1-bit (each) input: MMCM control ports
--      PWRDWN => '0',       -- 1-bit input: Power-down input
--      RST => dac_dcm_reset_pulse,             -- 1-bit input: Reset input
--      -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
--      CLKFBIN => dac_clk_fb_in      -- 1-bit input: Feedback clock input
--   );
--                   
--	dcm_dac_clk_bufg : BUFG
--            port map (
--                      O => dac_clk_bufg_out,
--                      I => dac_clk_bufg_in
--                      );


dac_clk_dcm_lock   <=    '1' ;
dac_clk_bufg_out   <=    dac_clk_in ;


-- channel select
   Inst_FD_DAC_select_out: FD port map(C => dac_clk_bufg_out, D => dac_select_ch1, Q => dac_select_fd_q);

	  IODELAYE1_inst_select : IODELAYE1
		generic map (
			CINVCTRL_SEL => FALSE,         -- Enable dynamic clock inversion ("TRUE"/"FALSE") 
			DELAY_SRC => "O",                -- Delay input ("I", "CLKIN", "DATAIN", "IO", "O")
			HIGH_PERFORMANCE_MODE => TRUE, -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
			IDELAY_TYPE => "DEFAULT",        -- "DEFAULT", "FIXED", "VARIABLE", or "VAR_LOADABLE" 
			IDELAY_VALUE => 0,               -- Input delay tap setting (0-32)
			ODELAY_TYPE => "VAR_LOADABLE",          -- "FIXED", "VARIABLE", or "VAR_LOADABLE" 
			ODELAY_VALUE => 0,               -- Output delay tap setting (0-32)
			REFCLK_FREQUENCY => 200.0,       -- IDELAYCTRL clock input frequency in MHz
			SIGNAL_PATTERN => "DATA"         -- "DATA" or "CLOCK" input signal
		)
		port map (
			CNTVALUEOUT => open, -- 5-bit output - Counter value for monitoring purpose
			DATAOUT => dac_select_fd_delayed,         -- 1-bit output - Delayed data output
			C => dac_odelay_clk,                     -- 1-bit input - Clock input
			CE => '0',                   -- 1-bit input - Active high enable increment/decrement function
			CINVCTRL => '0',       -- 1-bit input - Dynamically inverts the Clock (C) polarity
			CLKIN => '0',             -- 1-bit input - Clock Access into the IODELAY
			CNTVALUEIN => dac_odelay_value(4 downto 0),   -- 5-bit input - Counter value for loadable counter application
			DATAIN => '0',           -- 1-bit input - Internal delay data
			IDATAIN => '0',         -- 1-bit input - Delay data input
			INC => '0',                 -- 1-bit input - Increment / Decrement tap delay
			ODATAIN => dac_select_fd_q,         -- 1-bit input - Data input for the output datapath from the device
			RST => dac_odelay_rst,                 -- 1-bit input - Active high, synchronous reset, resets delay chain to IDELAY_VALUE/
												 -- ODELAY_VALUE tap. If no value is specified, the default is 0.
			T => '0'                      -- 1-bit input - 3-state input control. Tie high for input-only or internal delay or
												 -- tie low for output only.

		);


   dac_select_obufds : OBUFDS
      port map (
        I  => dac_select_fd_delayed,
        O  => DAC_SELIQ_P,
        OB => DAC_SELIQ_N
      );


-- data
--	Inst_FD_DAC_data_out: for i in 0 to 15 generate
--      Inst_FD_DAC_data_out_i: FD port map(C => dac_clk_bufg_out, D => dac_data_out(i), Q => dac_data_fd_q(i));
--	end generate;

   Inst_FD_DAC_data_out0: FD port map(C => dac_clk_bufg_out, D => dac_data_out(0), Q => dac_data_fd_q(0));
   Inst_FD_DAC_data_out1: FD port map(C => dac_clk_bufg_out, D => dac_data_out(1), Q => dac_data_fd_q(1));
   Inst_FD_DAC_data_out2: FD port map(C => dac_clk_bufg_out, D => dac_data_out(2), Q => dac_data_fd_q(2));
   Inst_FD_DAC_data_out3: FD port map(C => dac_clk_bufg_out, D => dac_data_out(3), Q => dac_data_fd_q(3));
   Inst_FD_DAC_data_out4: FD port map(C => dac_clk_bufg_out, D => dac_data_out(4), Q => dac_data_fd_q(4));
   Inst_FD_DAC_data_out5: FD port map(C => dac_clk_bufg_out, D => dac_data_out(5), Q => dac_data_fd_q(5));
   Inst_FD_DAC_data_out6: FD port map(C => dac_clk_bufg_out, D => dac_data_out(6), Q => dac_data_fd_q(6));
   Inst_FD_DAC_data_out7: FD port map(C => dac_clk_bufg_out, D => dac_data_out(7), Q => dac_data_fd_q(7));
   Inst_FD_DAC_data_out8: FD port map(C => dac_clk_bufg_out, D => dac_data_out(8), Q => dac_data_fd_q(8));
   Inst_FD_DAC_data_out9: FD port map(C => dac_clk_bufg_out, D => dac_data_out(9), Q => dac_data_fd_q(9));
   Inst_FD_DAC_data_out10: FD port map(C => dac_clk_bufg_out, D => dac_data_out(10), Q => dac_data_fd_q(10));
   Inst_FD_DAC_data_out11: FD port map(C => dac_clk_bufg_out, D => dac_data_out(11), Q => dac_data_fd_q(11));
   Inst_FD_DAC_data_out12: FD port map(C => dac_clk_bufg_out, D => dac_data_out(12), Q => dac_data_fd_q(12));
   Inst_FD_DAC_data_out13: FD port map(C => dac_clk_bufg_out, D => dac_data_out(13), Q => dac_data_fd_q(13));
   Inst_FD_DAC_data_out14: FD port map(C => dac_clk_bufg_out, D => dac_data_out(14), Q => dac_data_fd_q(14));
   Inst_FD_DAC_data_out15: FD port map(C => dac_clk_bufg_out, D => dac_data_out(15), Q => dac_data_fd_q(15));

	Inst_FD_DAC_data_odelay: for i in 0 to 15 generate

	  IODELAYE1_inst_i : IODELAYE1
		generic map (
			CINVCTRL_SEL => FALSE,         -- Enable dynamic clock inversion ("TRUE"/"FALSE") 
			DELAY_SRC => "O",                -- Delay input ("I", "CLKIN", "DATAIN", "IO", "O")
			HIGH_PERFORMANCE_MODE => TRUE, -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
			IDELAY_TYPE => "DEFAULT",        -- "DEFAULT", "FIXED", "VARIABLE", or "VAR_LOADABLE" 
			IDELAY_VALUE => 0,               -- Input delay tap setting (0-32)
			ODELAY_TYPE => "VAR_LOADABLE",          -- "FIXED", "VARIABLE", or "VAR_LOADABLE" 
			ODELAY_VALUE => 0,               -- Output delay tap setting (0-32)
			REFCLK_FREQUENCY => 200.0,       -- IDELAYCTRL clock input frequency in MHz
			SIGNAL_PATTERN => "DATA"         -- "DATA" or "CLOCK" input signal
		)
		port map (
			CNTVALUEOUT => open, -- 5-bit output - Counter value for monitoring purpose
			DATAOUT => dac_data_fd_delayed(i),         -- 1-bit output - Delayed data output
			C => dac_odelay_clk,                     -- 1-bit input - Clock input
			CE => '0',                   -- 1-bit input - Active high enable increment/decrement function
			CINVCTRL => '0',       -- 1-bit input - Dynamically inverts the Clock (C) polarity
			CLKIN => '0',             -- 1-bit input - Clock Access into the IODELAY
			CNTVALUEIN => dac_odelay_value(4 downto 0),   -- 5-bit input - Counter value for loadable counter application
			DATAIN => '0',           -- 1-bit input - Internal delay data
			IDATAIN => '0',         -- 1-bit input - Delay data input
			INC => '0',                 -- 1-bit input - Increment / Decrement tap delay
			ODATAIN => dac_data_fd_q(i),         -- 1-bit input - Data input for the output datapath from the device
			RST => dac_odelay_rst,                 -- 1-bit input - Active high, synchronous reset, resets delay chain to IDELAY_VALUE/
												 -- ODELAY_VALUE tap. If no value is specified, the default is 0.
			T => '0'                      -- 1-bit input - 3-state input control. Tie high for input-only or internal delay or
												 -- tie low for output only.

		);
	end generate;


	Inst_OBUFDS_dac_out: for i in 0 to 15 generate
    u_obuf_ck_i : OBUFDS
      port map (
        I  => dac_data_fd_delayed(i),
        O  => DAC_D_P(i),
        OB => DAC_D_N(i)
      );
	end generate;

   dac_out_synch_clk <= dac_clk_bufg_out ;

end Behavioral;

