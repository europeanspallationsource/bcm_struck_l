library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;
		
entity risingedge2pulse_generator is
	Port ( 
	 	adc_clk : in std_logic;
		rising_edge_signal_in : in std_logic;
		signal_pulse_out: out std_logic
			  ) ;
end risingedge2pulse_generator  ;


architecture risingedge2pulse_generator_arch of risingedge2pulse_generator is

			  
 signal signal_cmd_latch_clr:  STD_LOGIC;
 signal signal_cmd_latch:  STD_LOGIC;
 signal signal_cmd_latch_delay1:  STD_LOGIC;
 signal signal_cmd_latch_delay2:  STD_LOGIC;
 signal signal_cmd_latch_delay3:  STD_LOGIC;

begin




--synchronisation with adc_clk
synch_reset_logic: process (adc_clk, rising_edge_signal_in)
begin

   if (signal_cmd_latch_clr = '1') then
	    signal_cmd_latch   <=   '0' ;
     elsif  rising_edge(rising_edge_signal_in) then  
	    signal_cmd_latch   <=   '1' ;
   end if;

   if  rising_edge(adc_clk) then  
      signal_cmd_latch_delay1    <=      signal_cmd_latch ; 	 
      signal_cmd_latch_delay2    <=      signal_cmd_latch_delay1 ; 	 
      signal_cmd_latch_delay3    <=      signal_cmd_latch_delay2 ; 	 

      signal_cmd_latch_clr	      <=      signal_cmd_latch_delay2 ;
      signal_pulse_out           <=      signal_cmd_latch_delay2 and not signal_cmd_latch_delay3 ; 	 
   end if;


end process;



end risingedge2pulse_generator_arch;
