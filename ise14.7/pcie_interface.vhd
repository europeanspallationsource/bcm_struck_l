----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:45:36 08/28/2012 
-- Design Name: 
-- Module Name:    pcie_interface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity pcie_interface is
	port(
		pcie_refclk : in std_logic;
		pcie_rst_n : in std_logic;
		pcie_trn_lnk_up : out std_logic;	
		
		PER_N : in std_logic_vector(3 downto 0);
		PER_P : in std_logic_vector(3 downto 0);
		PET_N : out std_logic_vector(3 downto 0);
		PET_P : out std_logic_vector(3 downto 0);
		
		led_link_up: out std_logic;
		led_pcie_active: out std_logic; -- displays ANY traffic incoming / outgoing on pci bus
		led_system_access: out std_logic; -- displays actual traffic targeted / created to / by the card
		
		CLK_MUXAB_SEL : OUT std_logic_vector(1 downto 0);
		CLK_MUX1A_SEL : OUT std_logic_vector(1 downto 0);
		CLK_MUX1B_SEL : OUT std_logic_vector(1 downto 0);
		CLK_MUX2A_SEL : OUT std_logic_vector(1 downto 0);
		CLK_MUX2B_SEL : OUT std_logic_vector(1 downto 0);

		-- ADC Clock divider control signals
		CLK_DIV_SPI_CS_L : OUT std_logic_vector(1 downto 0);
		CLK_DIV_SPI_SCLK : out std_logic;
		CLK_DIV_SPI_DI : out std_logic;
		CLK_DIV_SPI_DO : in std_logic;
		CLK_DIV_STATUS : IN std_logic_vector(1 downto 0);
		ad9510_spi_function_out : OUT std_logic_vector(2 downto 0);
		
		-- ADC Control signals
		adc_spi_synch_out : out std_logic;
		adc_spi_oe_l_out : out std_logic;
		adc_spi_pwdn_out : out std_logic;
		ADC_SPI_CS_L : OUT std_logic_vector(5 downto 1);
		ADC_SPI_SCLK : out std_logic;
		ADC_SPI_DIO : inout std_logic;

		-- Clock multiplier SI5326 SPI control signals	 
		SI_INT_C1B : IN std_logic;
		SI_LOL : IN std_logic;  
		SI_RST_L : OUT std_logic; -- external reset
		SI_SPI_SCLK : OUT std_logic;
		SI_SPI_SS_L : OUT std_logic; -- slave select
		SI_SPI_DIN : OUT std_logic;
		SI_SPI_DOUT : IN std_logic;

		register_trn_clk : out std_logic;
		register_reset_out : out std_logic;
		
		addr_0x000_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x001_reg_feedback : IN std_logic_vector(31 downto 0);

		addr_0x003_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x003_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x004_status_feedback : IN std_logic_vector(15 downto 0);
		addr_0x004_reg_jk : OUT std_logic_vector(15 downto 0);
		addr_0x005_reg_feedback : IN std_logic_vector(31 downto 0);

		addr_0x10_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x10_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x11_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x11_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x12_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x13_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x13_reg_q : OUT std_logic_vector(32 downto 0);

		addr_0x14_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x14_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x15_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x15_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x16_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x16_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x17_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x17_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x47_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x47_reg_q : OUT std_logic_vector(32 downto 0);
		
		dac_ctrl_reg : out std_logic_vector(32 downto 0);
		dac_data_reg : out std_logic_vector(32 downto 0);
		
		adc_tap_delay_reg_q : out std_logic_vector(32 downto 0);
		adc_tap_delay_feedback : in std_logic_vector(31 downto 0); 

		addr_0x100_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x100_reg_q : out std_logic_vector(31 downto 0);
		addr_0x101_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x101_reg_q : out std_logic_vector(31 downto 0);
		addr_0x102_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x102_reg_q : out std_logic_vector(31 downto 0);
		addr_0x103_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x103_reg_q : out std_logic_vector(31 downto 0);
		addr_0x104_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x104_reg_q : out std_logic_vector(31 downto 0);
		addr_0x105_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x105_reg_q : out std_logic_vector(31 downto 0);
		addr_0x106_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x106_reg_q : out std_logic_vector(31 downto 0);
		addr_0x107_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x107_reg_q : out std_logic_vector(31 downto 0);
		addr_0x108_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x108_reg_q : out std_logic_vector(31 downto 0);
		addr_0x109_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x109_reg_q : out std_logic_vector(31 downto 0);

		addr_0x110_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x110_reg_q : out std_logic_vector(31 downto 0);
		addr_0x111_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x111_reg_q : out std_logic_vector(31 downto 0);
		addr_0x112_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x112_reg_q : out std_logic_vector(31 downto 0);
		addr_0x113_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x113_reg_q : out std_logic_vector(31 downto 0);
		addr_0x114_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x114_reg_q : out std_logic_vector(31 downto 0);
		addr_0x115_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x115_reg_q : out std_logic_vector(31 downto 0);
		addr_0x116_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x116_reg_q : out std_logic_vector(31 downto 0);
		addr_0x117_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x117_reg_q : out std_logic_vector(31 downto 0);
		addr_0x118_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x118_reg_q : out std_logic_vector(31 downto 0);
		addr_0x119_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x119_reg_q : out std_logic_vector(31 downto 0);

		addr_0x120_reg_q : out std_logic_vector(31 downto 0);
		addr_0x120_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x121_reg_q : out std_logic_vector(31 downto 0);
		addr_0x121_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x122_reg_q : out std_logic_vector(31 downto 0);
		addr_0x122_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x123_reg_q : out std_logic_vector(31 downto 0);
		addr_0x123_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x124_reg_q : out std_logic_vector(31 downto 0);
		addr_0x124_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x125_reg_q : out std_logic_vector(31 downto 0);
		addr_0x125_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x126_reg_q : out std_logic_vector(31 downto 0);
		addr_0x126_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x127_reg_q : out std_logic_vector(31 downto 0);
		addr_0x127_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x128_reg_q : out std_logic_vector(31 downto 0);
		addr_0x128_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x129_reg_q : out std_logic_vector(31 downto 0);
		addr_0x129_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12A_reg_q : out std_logic_vector(31 downto 0);
		addr_0x12A_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12B_reg_q : out std_logic_vector(31 downto 0);
		addr_0x12B_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12C_reg_q : out std_logic_vector(32 downto 0);
		addr_0x12C_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12D_reg_q : out std_logic_vector(32 downto 0);
		addr_0x12D_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12E_reg_q : out std_logic_vector(32 downto 0);
		addr_0x12E_reg_feedback : in std_logic_vector(31 downto 0);
		addr_0x12F_reg_q : out std_logic_vector(32 downto 0);
		addr_0x12F_reg_feedback : in std_logic_vector(31 downto 0);

		addr_0x230_ddr3_pcie_test_select : out std_logic_vector(32 downto 0);

		reg_0x400_0x4FF_adr : out std_logic_vector(7 downto 0);
		reg_0x400_0x4FF_wr_data : out std_logic_vector(31 downto 0);
		reg_0x400_0x4FF_rd_data : in std_logic_vector(31 downto 0);
		reg_0x400_0x4FF_wr_en : out std_logic;
		reg_0x400_0x4FF_rd_en : out std_logic;

		bram_dma_clk : out std_logic;
		bram_dma_adr : out std_logic_vector(31 downto 0);
		bram_dma_rd_en : out std_logic;
		bram_dma_rd_data : in std_logic_vector(63 downto 0);

		user_irq : in std_logic;
		user_irq_clear : out std_logic;
		
		dma_request_stall : in std_logic;
		
		MGT_CK_INT : in std_logic;
		MGT_CK_SDA : inout std_logic;
		MGT_CK_SCL : inout std_logic;
		
		flash_mosi : out std_logic;
		flash_miso : in std_logic;
		flash_cs_l : out std_logic;
		flash_clk : out std_logic;
		flash_mux_en : out std_logic;
		
		-- memory controller interface
		-- data: read fifo 
		sis_read_data_fifo_clr : out std_logic;
		sis_read_data_fifo_rd_clk : out std_logic;
		sis_read_data_fifo_rd_en : out std_logic;
		sis_read_data_fifo_dout : in  std_logic_vector(255 downto 0); 
		sis_read_data_fifo_rd_count : in  std_logic_vector(9 downto 0);
		sis_read_data_fifo_empty : in std_logic;
		-- address: read fifo 
		sis_read_addr_fifo_clr : out std_logic;
		sis_read_addr_fifo_wr_clk : out std_logic;
		sis_read_addr_fifo_wr_en : out std_logic;
		sis_read_addr_fifo_din : out std_logic_vector(31 downto 0); 
		sis_read_addr_fifo_wr_count : in  std_logic_vector(9 downto 0);
		-- data: write fifo 
		sis_write_fifo_wr_clk : out std_logic;
		sis_write_data_fifo_wr_en : out std_logic;
		sis_write_data_fifo_din : out  std_logic_vector(255 downto 0); 
		sis_write_data_fifo_wr_count : in  std_logic_vector(9 downto 0); 
		-- address: write fifo 
		sis_write_addr_fifo_wr_en : out std_logic;
		sis_write_addr_fifo_din : out  std_logic_vector(31 downto 0); 
		sis_write_addr_fifo_wr_count : in  std_logic_vector(9 downto 0)
	);
end pcie_interface;

architecture Behavioral of pcie_interface is

	COMPONENT pcie_x4_trn
	PORT(
		pci_exp_rxp : IN std_logic_vector(3 downto 0);
		pci_exp_rxn : IN std_logic_vector(3 downto 0);
		trn_td : IN std_logic_vector(63 downto 0);
		trn_trem_n : IN std_logic;
		trn_tsof_n : IN std_logic;
		trn_teof_n : IN std_logic;
		trn_tsrc_rdy_n : IN std_logic;
		trn_tsrc_dsc_n : IN std_logic;
		trn_terrfwd_n : IN std_logic;
		trn_tcfg_gnt_n : IN std_logic;
		trn_tstr_n : IN std_logic;
		trn_rdst_rdy_n : IN std_logic;
		trn_rnp_ok_n : IN std_logic;
		trn_fc_sel : IN std_logic_vector(2 downto 0);
		cfg_di : IN std_logic_vector(31 downto 0);
		cfg_byte_en_n : IN std_logic_vector(3 downto 0);
		cfg_dwaddr : IN std_logic_vector(9 downto 0);
		cfg_wr_en_n : IN std_logic;
		cfg_rd_en_n : IN std_logic;
		cfg_err_cor_n : IN std_logic;
		cfg_err_ur_n : IN std_logic;
		cfg_err_ecrc_n : IN std_logic;
		cfg_err_cpl_timeout_n : IN std_logic;
		cfg_err_cpl_abort_n : IN std_logic;
		cfg_err_cpl_unexpect_n : IN std_logic;
		cfg_err_posted_n : IN std_logic;
		cfg_err_locked_n : IN std_logic;
		cfg_err_tlp_cpl_header : IN std_logic_vector(47 downto 0);
		cfg_interrupt_n : IN std_logic;
		cfg_interrupt_assert_n : IN std_logic;
		cfg_interrupt_di : IN std_logic_vector(7 downto 0);
		cfg_turnoff_ok_n : IN std_logic;
		cfg_trn_pending_n : IN std_logic;
		cfg_pm_wake_n : IN std_logic;
		cfg_dsn : IN std_logic_vector(63 downto 0);
		pl_directed_link_auton : IN std_logic;
		pl_directed_link_change : IN std_logic_vector(1 downto 0);
		pl_directed_link_speed : IN std_logic;
		pl_directed_link_width : IN std_logic_vector(1 downto 0);
		pl_upstream_prefer_deemph : IN std_logic;
		sys_clk : IN std_logic;
		sys_reset_n : IN std_logic;          
		pci_exp_txp : OUT std_logic_vector(3 downto 0);
		pci_exp_txn : OUT std_logic_vector(3 downto 0);
		trn_clk : OUT std_logic;
		trn_reset_n : OUT std_logic;
		trn_lnk_up_n : OUT std_logic;
		trn_tbuf_av : OUT std_logic_vector(5 downto 0);
		trn_tcfg_req_n : OUT std_logic;
		trn_terr_drop_n : OUT std_logic;
		trn_tdst_rdy_n : OUT std_logic;
		trn_rd : OUT std_logic_vector(63 downto 0);
		trn_rrem_n : OUT std_logic;
		trn_rsof_n : OUT std_logic;
		trn_reof_n : OUT std_logic;
		trn_rsrc_rdy_n : OUT std_logic;
		trn_rsrc_dsc_n : OUT std_logic;
		trn_rerrfwd_n : OUT std_logic;
		trn_rbar_hit_n : OUT std_logic_vector(6 downto 0);
		trn_fc_cpld : OUT std_logic_vector(11 downto 0);
		trn_fc_cplh : OUT std_logic_vector(7 downto 0);
		trn_fc_npd : OUT std_logic_vector(11 downto 0);
		trn_fc_nph : OUT std_logic_vector(7 downto 0);
		trn_fc_pd : OUT std_logic_vector(11 downto 0);
		trn_fc_ph : OUT std_logic_vector(7 downto 0);
		cfg_do : OUT std_logic_vector(31 downto 0);
		cfg_rd_wr_done_n : OUT std_logic;
		cfg_err_cpl_rdy_n : OUT std_logic;
		cfg_interrupt_rdy_n : OUT std_logic;
		cfg_interrupt_do : OUT std_logic_vector(7 downto 0);
		cfg_interrupt_mmenable : OUT std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : OUT std_logic;
		cfg_interrupt_msixenable : OUT std_logic;
		cfg_interrupt_msixfm : OUT std_logic;
		cfg_to_turnoff_n : OUT std_logic;
		cfg_bus_number : OUT std_logic_vector(7 downto 0);
		cfg_device_number : OUT std_logic_vector(4 downto 0);
		cfg_function_number : OUT std_logic_vector(2 downto 0);
		cfg_status : OUT std_logic_vector(15 downto 0);
		cfg_command : OUT std_logic_vector(15 downto 0);
		cfg_dstatus : OUT std_logic_vector(15 downto 0);
		cfg_dcommand : OUT std_logic_vector(15 downto 0);
		cfg_lstatus : OUT std_logic_vector(15 downto 0);
		cfg_lcommand : OUT std_logic_vector(15 downto 0);
		cfg_dcommand2 : OUT std_logic_vector(15 downto 0);
		cfg_pcie_link_state_n : OUT std_logic_vector(2 downto 0);
		cfg_pmcsr_pme_en : OUT std_logic;
		cfg_pmcsr_pme_status : OUT std_logic;
		cfg_pmcsr_powerstate : OUT std_logic_vector(1 downto 0);
		pl_initial_link_width : OUT std_logic_vector(2 downto 0);
		pl_lane_reversal_mode : OUT std_logic_vector(1 downto 0);
		pl_link_gen2_capable : OUT std_logic;
		pl_link_partner_gen2_supported : OUT std_logic;
		pl_link_upcfg_capable : OUT std_logic;
		pl_ltssm_state : OUT std_logic_vector(5 downto 0);
		pl_received_hot_rst : OUT std_logic;
		pl_sel_link_rate : OUT std_logic;
		pl_sel_link_width : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;
	
	COMPONENT sis8300_endpoint_device
	PORT(
		trn_clk : IN std_logic;
		trn_reset_n : IN std_logic;
		trn_lnk_up_n : IN std_logic;
		trn_tdst_rdy_n : IN std_logic;
		trn_tdst_dsc_n : IN std_logic;
		trn_tbuf_av : IN std_logic_vector(5 downto 0);
		trn_rd : IN std_logic_vector(63 downto 0);
		trn_rrem_n : IN std_logic;
		trn_rsof_n : IN std_logic;
		trn_reof_n : IN std_logic;
		trn_rsrc_rdy_n : IN std_logic;
		trn_rsrc_dsc_n : IN std_logic;
		trn_rerrfwd_n : IN std_logic;
		trn_rbar_hit_n : IN std_logic_vector(6 downto 0);
		trn_rfc_nph_av : IN std_logic_vector(7 downto 0);
		trn_rfc_npd_av : IN std_logic_vector(11 downto 0);
		trn_rfc_ph_av : IN std_logic_vector(7 downto 0);
		trn_rfc_pd_av : IN std_logic_vector(11 downto 0);
		cfg_interrupt_rdy_n : IN std_logic;
		cfg_interrupt_do : IN std_logic_vector(7 downto 0);
		cfg_interrupt_mmenable : IN std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : IN std_logic;
		cfg_bus_number : IN std_logic_vector(7 downto 0);
		cfg_device_number : IN std_logic_vector(4 downto 0);
		cfg_function_number : IN std_logic_vector(2 downto 0);
		cfg_dcommand : IN std_logic_vector(15 downto 0);
		addr_0x000_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x001_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x002_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x003_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x004_status_feedback : IN std_logic_vector(15 downto 0);
		addr_0x005_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x10_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x11_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x13_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x14_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x15_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x16_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x17_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x40_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x41_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x42_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x43_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x44_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x45_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x46_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x47_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x48_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x49_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x100_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x101_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x102_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x103_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x104_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x105_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x106_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x107_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x108_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x109_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x110_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x111_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x112_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x113_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x114_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x115_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x116_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x117_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x118_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x119_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x120_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x121_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x122_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x123_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x124_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x125_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x126_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x127_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x128_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x129_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12A_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12B_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12C_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12D_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12E_reg_feedback : IN std_logic_vector(31 downto 0);
		addr_0x12F_reg_feedback : IN std_logic_vector(31 downto 0);
		reg_0x400_0x4FF_rd_data : IN std_logic_vector(31 downto 0);
		bram_dma_rd_data : IN std_logic_vector(63 downto 0);
		user_irq : IN std_logic;
		sysmon_read_data : IN std_logic_vector(15 downto 0);
		sysmon_rdy : IN std_logic;
		sis_read_data_fifo_dout : IN std_logic_vector(255 downto 0);
		sis_read_data_fifo_rd_count : IN std_logic_vector(9 downto 0);
		sis_read_data_fifo_empty : IN std_logic;
		sis_read_addr_fifo_wr_count : IN std_logic_vector(9 downto 0);
		sis_write_data_fifo_wr_count : IN std_logic_vector(9 downto 0);
		sis_write_addr_fifo_wr_count : IN std_logic_vector(9 downto 0); 
		dma_request_stall : in std_logic;         
		trn_td : OUT std_logic_vector(63 downto 0);
		trn_trem_n : OUT std_logic;
		trn_tsof_n : OUT std_logic;
		trn_teof_n : OUT std_logic;
		trn_tsrc_rdy_n : OUT std_logic;
		trn_tsrc_dsc_n : OUT std_logic;
		trn_terrfwd_n : OUT std_logic;
		trn_rdst_rdy_n : OUT std_logic;
		trn_rnp_ok_n : OUT std_logic;
		trn_rcpl_streaming_n : OUT std_logic;
		cfg_interrupt_n : OUT std_logic;
		cfg_interrupt_assert_n : OUT std_logic;
		cfg_interrupt_di : OUT std_logic_vector(7 downto 0);
		led_link_up : OUT std_logic;
		led_pcie_active : OUT std_logic;
		led_system_access : OUT std_logic;
		addr_0x002_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x003_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x004_reg_jk : OUT std_logic_vector(15 downto 0);
		addr_0x10_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x11_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x12_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x13_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x14_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x15_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x16_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x17_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x40_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x41_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x42_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x43_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x44_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x45_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x46_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x47_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x48_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x49_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x100_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x101_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x102_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x103_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x104_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x105_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x106_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x107_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x108_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x109_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x110_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x111_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x112_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x113_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x114_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x115_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x116_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x117_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x118_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x119_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x120_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x121_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x122_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x123_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x124_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x125_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x126_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x127_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x128_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x129_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x12A_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x12B_reg_q : OUT std_logic_vector(31 downto 0);
		addr_0x12C_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x12D_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x12E_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x12F_reg_q : OUT std_logic_vector(32 downto 0);
		addr_0x230_ddr2_pcie_test_select : OUT std_logic_vector(32 downto 0);
		reg_0x400_0x4FF_adr : OUT std_logic_vector(7 downto 0);
		reg_0x400_0x4FF_wr_data : OUT std_logic_vector(31 downto 0);
		reg_0x400_0x4FF_wr_en : OUT std_logic;
		reg_0x400_0x4FF_rd_en : OUT std_logic;
		bram_dma_clk : OUT std_logic;
		bram_dma_adr : OUT std_logic_vector(31 downto 0);
		bram_dma_rd_en : OUT std_logic;
		user_irq_clear : OUT std_logic;
		sysmon_write_data : OUT std_logic_vector(15 downto 0);
		sysmon_adr : OUT std_logic_vector(6 downto 0);
		sysmon_en : OUT std_logic;
		sysmon_wren : OUT std_logic;
		sis_read_data_fifo_clr : OUT std_logic;
		sis_read_data_fifo_rd_clk : OUT std_logic;
		sis_read_data_fifo_rd_en : OUT std_logic;
		sis_read_addr_fifo_clr : OUT std_logic;
		sis_read_addr_fifo_wr_clk : OUT std_logic;
		sis_read_addr_fifo_wr_en : OUT std_logic;
		sis_read_addr_fifo_din : OUT std_logic_vector(31 downto 0);
		sis_write_fifo_wr_clk : OUT std_logic;
		sis_write_data_fifo_wr_en : OUT std_logic;
		sis_write_data_fifo_din : OUT std_logic_vector(255 downto 0);
		sis_write_addr_fifo_wr_en : OUT std_logic;
		sis_write_addr_fifo_din : OUT std_logic_vector(31 downto 0);
		register_reset : out std_logic
		);
	END COMPONENT;
	
	COMPONENT i2c_master_interface
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		i2c_reg_q : IN std_logic_vector(32 downto 0);    
		I2C_SDA : INOUT std_logic;
		I2C_SCL : INOUT std_logic;      
		i2c_reg_feedback : OUT std_logic_vector(31 downto 0)
		);
	END COMPONENT;
	
	COMPONENT spi_flash_interface
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		flash_miso : IN std_logic;
		spi_csr_reg : IN std_logic_vector(32 downto 0);          
		flash_mosi : OUT std_logic;
		flash_cs_l : OUT std_logic;
		flash_clk : OUT std_logic;
		flash_mux_en : OUT std_logic;
		spi_csr_reg_feedback : OUT std_logic_vector(31 downto 0)
		);
	END COMPONENT;
	
	COMPONENT ad9510_spi_interface
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		ad9510_spi_status : IN std_logic_vector(1 downto 0);
		ad9510_spi_reg_q : IN std_logic_vector(32 downto 0);
		ad9510_spi_dout_ireg : IN std_logic;          
		ad9510_spi_reg_feedback : OUT std_logic_vector(31 downto 0);
		ad9510_spi_sclk_oreg : OUT std_logic;
		ad9510_spi_cs_L_oreg : OUT std_logic_vector(1 downto 0);
		ad9510_spi_din_oreg : OUT std_logic;
		ad9510_spi_din_out_en : OUT std_logic;
		ad9510_spi_function_out : OUT std_logic_vector(2 downto 0)
		);
	END COMPONENT;

	COMPONENT adc_spi_interface
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		adc_spi_reg_q : IN std_logic_vector(32 downto 0);
		adc_spi_dout_ireg : IN std_logic;          
		adc_spi_reg_feedback : OUT std_logic_vector(31 downto 0);
		adc_spi_sclk_oreg : OUT std_logic;
		adc_spi_cs_L_oreg : OUT std_logic_vector(4 downto 0);
		adc_spi_din_oreg : OUT std_logic;
		adc_spi_din_out_en : OUT std_logic;
		adc_spi_synch_out : OUT std_logic;
		adc_spi_oe_l_out : out std_logic;
		adc_spi_pwdn_out : out std_logic
		);
	END COMPONENT;

	COMPONENT clock_multiplier_spi_interface
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		Si53xx_spi_reg_q : IN std_logic_vector(32 downto 0);
		Si53xx_spi_dout_ireg : IN std_logic;          
		Si53xx_spi_reg_feedback : OUT std_logic_vector(31 downto 0);
		Si53xx_spi_sclk_oreg : OUT std_logic;
		Si53xx_spi_cs_L_oreg : OUT std_logic;
		Si53xx_spi_din_oreg : OUT std_logic;
		Si53xx_spi_decr_out : OUT std_logic;
		Si53xx_spi_incr_out : OUT std_logic;
		Si53xx_spi_reset_L_out : OUT std_logic
		);
	END COMPONENT;
	
	signal tied_to_vcc_vec: std_logic_vector(7 downto 0) := (others => '1');
	signal tied_to_gnd_vec: std_logic_vector(63 downto 0) := (others => '0');
	signal tied_to_vcc: std_logic := '1';
	signal tied_to_gnd: std_logic := '0';
	
	signal trn_clk: std_logic;
	signal trn_reset: std_logic;
	signal trn_reset_n: std_logic;
	signal trn_lnk_up_n: std_logic;
	
	signal trn_td: std_logic_vector(63 downto 0);
	signal trn_trem_n: std_logic;
	signal trn_tsof_n: std_logic;
	signal trn_teof_n: std_logic;
	signal trn_tsrc_rdy_n: std_logic;
	signal trn_tdst_rdy_n: std_logic;
	signal trn_tsrc_dsc_n: std_logic;
	signal trn_terrfwd_n: std_logic;
	signal trn_tbuf_av: std_logic_VECTOR(5 downto 0);
	
	signal trn_rd: std_logic_vector(63 downto 0);
	signal trn_rrem_n: std_logic;
	signal trn_rsof_n: std_logic;
	signal trn_reof_n: std_logic;
	signal trn_rsrc_rdy_n: std_logic;
	signal trn_rsrc_dsc_n: std_logic;
	signal trn_rdst_rdy_n: std_logic;
	signal trn_rerrfwd_n: std_logic;
	signal trn_rnp_ok_n: std_logic;
	signal trn_rbar_hit_n: std_logic_VECTOR(6 downto 0);
	signal trn_rfc_nph_av: std_logic_VECTOR(7 downto 0);
	signal trn_rfc_npd_av: std_logic_VECTOR(11 downto 0);
	signal trn_rfc_ph_av: std_logic_VECTOR(7 downto 0);
	signal trn_rfc_pd_av: std_logic_VECTOR(11 downto 0);
	signal trn_rcpl_streaming_n: std_logic;

	signal cfg_interrupt_n: std_logic;
	signal cfg_interrupt_rdy_n: std_logic;

	signal cfg_interrupt_assert_n: std_logic;
	signal cfg_interrupt_di: std_logic_VECTOR(7 downto 0);
	signal cfg_interrupt_do: std_logic_VECTOR(7 downto 0);
	signal cfg_interrupt_mmenable: std_logic_VECTOR(2 downto 0);
	signal cfg_interrupt_msienable: std_logic;

	signal cfg_pm_wake_n: std_logic;
	signal cfg_trn_pending_n: std_logic;
	signal cfg_bus_number: std_logic_VECTOR(7 downto 0);
	signal cfg_device_number: std_logic_VECTOR(4 downto 0);
	signal cfg_function_number: std_logic_VECTOR(2 downto 0);

	signal cfg_dcommand: std_logic_VECTOR(15 downto 0);
	signal cfg_dsn: std_logic_VECTOR(63 downto 0);
	
	signal register_reset : std_logic;
	
	signal addr_0x002_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x002_reg_q : std_logic_vector(32 downto 0);

	signal addr_0x40_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x40_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x41_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x41_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x42_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x42_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x43_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x43_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x44_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x44_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x45_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x45_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x46_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x46_reg_q : std_logic_vector(32 downto 0);

	signal addr_0x48_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x48_reg_q : std_logic_vector(32 downto 0);
	signal addr_0x49_reg_feedback : std_logic_vector(31 downto 0);
	signal addr_0x49_reg_q : std_logic_vector(32 downto 0);
	
	signal ad9510_spi_dout_ireg: std_logic;
	signal ad9510_spi_sclk_oreg: std_logic;
	signal ad9510_spi_din_oreg: std_logic;
	signal ad9510_spi_din_out_en: std_logic;
	signal ad9510_spi_cs_L_oreg : std_logic_vector(1 downto 0);

	signal adc_spi_dout_ireg: std_logic;
	signal adc_spi_sclk_oreg: std_logic;
	signal adc_spi_din_oreg: std_logic;
	signal adc_spi_din_out_en: std_logic;
	signal adc_spi_cs_L_oreg : std_logic_vector(4 downto 0);

	signal help_Si53xx_spi_reg_feedback : std_logic_vector(31 downto 0);
	signal Si53xx_spi_dout_ireg: std_logic;
	signal Si53xx_spi_sclk_oreg: std_logic;
	signal Si53xx_spi_cs_L_oreg: std_logic;
	signal Si53xx_spi_din_oreg: std_logic;
	signal Si53xx_spi_decr_out: std_logic;
	signal Si53xx_spi_incr_out: std_logic;
	signal Si53xx_spi_reset_L_out: std_logic;
	signal Si53xx_INT_C1B_ireg: std_logic;
	signal Si53xx_LOL_ireg: std_logic;

begin

	register_trn_clk <= trn_clk;
	register_reset_out   <= register_reset ;

	cfg_dsn <= (others => '0');

	Inst_pcie_x4: pcie_x4_trn PORT MAP(
		pci_exp_txp => PET_P,
		pci_exp_txn => PET_N,
		pci_exp_rxp => PER_P,
		pci_exp_rxn => PER_N,
		
		trn_clk => trn_clk,
		trn_reset_n => trn_reset_n,
		trn_lnk_up_n => trn_lnk_up_n,
		
		trn_tbuf_av => trn_tbuf_av,
		trn_tcfg_req_n => open,
		trn_terr_drop_n => open,
		trn_tdst_rdy_n => trn_tdst_rdy_n,
		trn_td => trn_td,
		trn_trem_n => trn_trem_n,
		trn_tsof_n => trn_tsof_n,
		trn_teof_n => trn_teof_n,
		trn_tsrc_rdy_n => trn_tsrc_rdy_n,
		trn_tsrc_dsc_n => trn_tsrc_dsc_n,
		trn_terrfwd_n => trn_terrfwd_n,
		trn_tcfg_gnt_n => tied_to_gnd,
		trn_tstr_n => tied_to_vcc,
		
		trn_rd => trn_rd,
		trn_rrem_n => trn_rrem_n,
		trn_rsof_n => trn_rsof_n,
		trn_reof_n => trn_reof_n,
		trn_rsrc_rdy_n => trn_rsrc_rdy_n,
		trn_rsrc_dsc_n => trn_rsrc_dsc_n,
		trn_rerrfwd_n => trn_rerrfwd_n,
		trn_rbar_hit_n => trn_rbar_hit_n,
		trn_rdst_rdy_n => trn_rdst_rdy_n,
		trn_rnp_ok_n => trn_rnp_ok_n,
		
		trn_fc_cpld => open,
		trn_fc_cplh => open,
		trn_fc_npd => open,
		trn_fc_nph => open,
		trn_fc_pd => open,
		trn_fc_ph => open,
		trn_fc_sel => tied_to_gnd_vec(2 downto 0),
		
		cfg_do => open,
		cfg_rd_wr_done_n => open,
		cfg_di => tied_to_gnd_vec(31 downto 0),
		cfg_byte_en_n => tied_to_vcc_vec(3 downto 0),
		cfg_dwaddr => tied_to_gnd_vec(9 downto 0),
		cfg_wr_en_n => tied_to_vcc,
		cfg_rd_en_n => tied_to_vcc,
		
		cfg_err_cor_n => tied_to_vcc,
		cfg_err_ur_n => tied_to_vcc,
		cfg_err_ecrc_n => tied_to_vcc,
		cfg_err_cpl_timeout_n => tied_to_vcc,
		cfg_err_cpl_abort_n => tied_to_vcc,
		cfg_err_cpl_unexpect_n => tied_to_vcc,
		cfg_err_posted_n => tied_to_vcc,
		cfg_err_locked_n => tied_to_vcc,
		cfg_err_tlp_cpl_header => tied_to_gnd_vec(47 downto 0),
		cfg_err_cpl_rdy_n => tied_to_vcc,
		
		cfg_interrupt_n => cfg_interrupt_n,
		cfg_interrupt_rdy_n => cfg_interrupt_rdy_n,
		cfg_interrupt_assert_n => cfg_interrupt_assert_n,
		cfg_interrupt_di => cfg_interrupt_di,
		cfg_interrupt_do => cfg_interrupt_do,
		cfg_interrupt_mmenable => cfg_interrupt_mmenable,
		cfg_interrupt_msienable => cfg_interrupt_msienable,
		cfg_interrupt_msixenable => open,
		cfg_interrupt_msixfm => open,
		
		cfg_turnoff_ok_n => tied_to_gnd,
		cfg_to_turnoff_n => open,
		cfg_trn_pending_n => tied_to_vcc,
		cfg_pm_wake_n => tied_to_vcc,
		cfg_bus_number => cfg_bus_number,
		cfg_device_number => cfg_device_number,
		cfg_function_number => cfg_function_number,
		cfg_status => open,
		cfg_command => open,
		cfg_dstatus => open,
		cfg_dcommand => cfg_dcommand,
		cfg_lstatus => open,
		cfg_lcommand => open,
		cfg_dcommand2 => open,
		cfg_pcie_link_state_n => open,
		cfg_dsn => cfg_dsn,
		
		cfg_pmcsr_pme_en => open,
		cfg_pmcsr_pme_status => open,
		cfg_pmcsr_powerstate => open,
		
		pl_initial_link_width => open,
		pl_lane_reversal_mode => open,
		pl_link_gen2_capable => open,
		pl_link_partner_gen2_supported => open,
		pl_link_upcfg_capable => open,
		pl_ltssm_state => open,
		pl_received_hot_rst => open,
		pl_sel_link_rate => open,
		pl_sel_link_width => open,
		pl_directed_link_auton => tied_to_gnd,
		pl_directed_link_change => tied_to_gnd_vec(1 downto 0),
		pl_directed_link_speed => tied_to_gnd,
		pl_directed_link_width => tied_to_gnd_vec(1 downto 0),
		pl_upstream_prefer_deemph => tied_to_gnd,
		
		sys_clk => pcie_refclk,
		sys_reset_n => pcie_rst_n
	);
	
	pcie_trn_lnk_up <= not trn_lnk_up_n;
	
	Inst_sis8300_endpoint_device: sis8300_endpoint_device PORT MAP(
		trn_clk => trn_clk,
		trn_reset_n => trn_reset_n,
		trn_lnk_up_n => trn_lnk_up_n,
		
		trn_td => trn_td,
		trn_trem_n => trn_trem_n,
		trn_tsof_n => trn_tsof_n,
		trn_teof_n => trn_teof_n,
		trn_tsrc_rdy_n => trn_tsrc_rdy_n,
		trn_tdst_rdy_n => trn_tdst_rdy_n,
		trn_tdst_dsc_n => tied_to_vcc,
		trn_tsrc_dsc_n => trn_tsrc_dsc_n,
		trn_terrfwd_n => trn_terrfwd_n,
		trn_tbuf_av => trn_tbuf_av,
		
		trn_rd => trn_rd,
		trn_rrem_n => trn_rrem_n,
		trn_rsof_n => trn_rsof_n,
		trn_reof_n => trn_reof_n,
		trn_rsrc_rdy_n => trn_rsrc_rdy_n,
		trn_rsrc_dsc_n => trn_rsrc_dsc_n,
		trn_rdst_rdy_n => trn_rdst_rdy_n,
		trn_rerrfwd_n => trn_rerrfwd_n,
		trn_rnp_ok_n => trn_rnp_ok_n,
		trn_rbar_hit_n => trn_rbar_hit_n,
		
		trn_rfc_nph_av => tied_to_gnd_vec(7 downto 0),
		trn_rfc_npd_av => tied_to_gnd_vec(11 downto 0),
		trn_rfc_ph_av => tied_to_gnd_vec(7 downto 0),
		trn_rfc_pd_av => tied_to_gnd_vec(11 downto 0),
		trn_rcpl_streaming_n => open,
		
		cfg_interrupt_n => cfg_interrupt_n,
		cfg_interrupt_rdy_n => cfg_interrupt_rdy_n,
		cfg_interrupt_assert_n => cfg_interrupt_assert_n,
		cfg_interrupt_di => cfg_interrupt_di,
		cfg_interrupt_do => cfg_interrupt_do,
		cfg_interrupt_mmenable => cfg_interrupt_mmenable,
		cfg_interrupt_msienable => cfg_interrupt_msienable,
		
		cfg_bus_number => cfg_bus_number,
		cfg_device_number => cfg_device_number,
		cfg_function_number => cfg_function_number,
		cfg_dcommand => cfg_dcommand,
		
		led_link_up => led_link_up,
		led_pcie_active => led_pcie_active,
		led_system_access => led_system_access,
		
		register_reset => register_reset,
		
		addr_0x000_reg_feedback => addr_0x000_reg_feedback,
		addr_0x001_reg_feedback => addr_0x001_reg_feedback,
		
		addr_0x002_reg_feedback => addr_0x002_reg_feedback,
		addr_0x002_reg_q => addr_0x002_reg_q,
		
		addr_0x003_reg_feedback => addr_0x003_reg_feedback,
		addr_0x003_reg_q => addr_0x003_reg_q,
		addr_0x004_status_feedback => addr_0x004_status_feedback,
		addr_0x004_reg_jk => addr_0x004_reg_jk,
		addr_0x005_reg_feedback => addr_0x005_reg_feedback,
		
		addr_0x10_reg_feedback => addr_0x10_reg_feedback,
		addr_0x10_reg_q => addr_0x10_reg_q,
		addr_0x11_reg_feedback => addr_0x11_reg_feedback,
		addr_0x11_reg_q => addr_0x11_reg_q,
		addr_0x12_reg_feedback => addr_0x12_reg_feedback,
		addr_0x12_reg_q => addr_0x12_reg_q,
		addr_0x13_reg_feedback => addr_0x13_reg_feedback,
		addr_0x13_reg_q => addr_0x13_reg_q,
		addr_0x14_reg_feedback => addr_0x14_reg_feedback,
		addr_0x14_reg_q => addr_0x14_reg_q,
		addr_0x15_reg_feedback => addr_0x15_reg_feedback,
		addr_0x15_reg_q => addr_0x15_reg_q,
		addr_0x16_reg_feedback => addr_0x16_reg_feedback,
		addr_0x16_reg_q => addr_0x16_reg_q,
		addr_0x17_reg_feedback => addr_0x17_reg_feedback,
		addr_0x17_reg_q => addr_0x17_reg_q,
		
		addr_0x40_reg_feedback => addr_0x40_reg_feedback,
		addr_0x40_reg_q => addr_0x40_reg_q,
		addr_0x41_reg_feedback => addr_0x41_reg_feedback,
		addr_0x41_reg_q => addr_0x41_reg_q,
		addr_0x42_reg_feedback => addr_0x42_reg_feedback,
		addr_0x42_reg_q => addr_0x42_reg_q,
		addr_0x43_reg_feedback => addr_0x43_reg_feedback,
		addr_0x43_reg_q => addr_0x43_reg_q,
		addr_0x44_reg_feedback => addr_0x44_reg_feedback,
		addr_0x44_reg_q => addr_0x44_reg_q,
		addr_0x45_reg_feedback => addr_0x45_reg_feedback,
		addr_0x45_reg_q => addr_0x45_reg_q,
		addr_0x46_reg_feedback => addr_0x46_reg_feedback,
		addr_0x46_reg_q => addr_0x46_reg_q,
		addr_0x47_reg_feedback => addr_0x47_reg_feedback,
		addr_0x47_reg_q => addr_0x47_reg_q,
		addr_0x48_reg_feedback => addr_0x48_reg_feedback,
		addr_0x48_reg_q => addr_0x48_reg_q,
		addr_0x49_reg_feedback => addr_0x49_reg_feedback,
		addr_0x49_reg_q => addr_0x49_reg_q,
		
		addr_0x100_reg_feedback => addr_0x100_reg_feedback,
		addr_0x100_reg_q => addr_0x100_reg_q,
		addr_0x101_reg_feedback => addr_0x101_reg_feedback,
		addr_0x101_reg_q => addr_0x101_reg_q,
		addr_0x102_reg_feedback => addr_0x102_reg_feedback,
		addr_0x102_reg_q => addr_0x102_reg_q,
		addr_0x103_reg_feedback => addr_0x103_reg_feedback,
		addr_0x103_reg_q => addr_0x103_reg_q,
		addr_0x104_reg_feedback => addr_0x104_reg_feedback,
		addr_0x104_reg_q => addr_0x104_reg_q,
		addr_0x105_reg_feedback => addr_0x105_reg_feedback,
		addr_0x105_reg_q => addr_0x105_reg_q,
		addr_0x106_reg_feedback => addr_0x106_reg_feedback,
		addr_0x106_reg_q => addr_0x106_reg_q,
		addr_0x107_reg_feedback => addr_0x107_reg_feedback,
		addr_0x107_reg_q => addr_0x107_reg_q,
		addr_0x108_reg_feedback => addr_0x108_reg_feedback,
		addr_0x108_reg_q => addr_0x108_reg_q,
		addr_0x109_reg_feedback => addr_0x109_reg_feedback,
		addr_0x109_reg_q => addr_0x109_reg_q,
		addr_0x110_reg_feedback => addr_0x110_reg_feedback,
		addr_0x110_reg_q => addr_0x110_reg_q,
		addr_0x111_reg_feedback => addr_0x111_reg_feedback,
		addr_0x111_reg_q => addr_0x111_reg_q,
		addr_0x112_reg_feedback => addr_0x112_reg_feedback,
		addr_0x112_reg_q => addr_0x112_reg_q,
		addr_0x113_reg_feedback => addr_0x113_reg_feedback,
		addr_0x113_reg_q => addr_0x113_reg_q,
		addr_0x114_reg_feedback => addr_0x114_reg_feedback,
		addr_0x114_reg_q => addr_0x114_reg_q,
		addr_0x115_reg_feedback => addr_0x115_reg_feedback,
		addr_0x115_reg_q => addr_0x115_reg_q,
		addr_0x116_reg_feedback => addr_0x116_reg_feedback,
		addr_0x116_reg_q => addr_0x116_reg_q,
		addr_0x117_reg_feedback => addr_0x117_reg_feedback,
		addr_0x117_reg_q => addr_0x117_reg_q,
		addr_0x118_reg_feedback => addr_0x118_reg_feedback,
		addr_0x118_reg_q => addr_0x118_reg_q,
		addr_0x119_reg_feedback => addr_0x119_reg_feedback,
		addr_0x119_reg_q => addr_0x119_reg_q,
		addr_0x120_reg_q => addr_0x120_reg_q,
		addr_0x120_reg_feedback => addr_0x120_reg_feedback,
		addr_0x121_reg_q => addr_0x121_reg_q,
		addr_0x121_reg_feedback => addr_0x121_reg_feedback,
		addr_0x122_reg_q => addr_0x122_reg_q,
		addr_0x122_reg_feedback => addr_0x122_reg_feedback,
		addr_0x123_reg_q => addr_0x123_reg_q,
		addr_0x123_reg_feedback => addr_0x123_reg_feedback,
		addr_0x124_reg_q => addr_0x124_reg_q,
		addr_0x124_reg_feedback => addr_0x124_reg_feedback,
		addr_0x125_reg_q => addr_0x125_reg_q,
		addr_0x125_reg_feedback => addr_0x125_reg_feedback,
		addr_0x126_reg_q => addr_0x126_reg_q,
		addr_0x126_reg_feedback => addr_0x126_reg_feedback,
		addr_0x127_reg_q => addr_0x127_reg_q,
		addr_0x127_reg_feedback => addr_0x127_reg_feedback,
		addr_0x128_reg_q => addr_0x128_reg_q,
		addr_0x128_reg_feedback => addr_0x128_reg_feedback,
		addr_0x129_reg_q => addr_0x129_reg_q,
		addr_0x129_reg_feedback => addr_0x129_reg_feedback,
		addr_0x12A_reg_q => addr_0x12A_reg_q,
		addr_0x12A_reg_feedback => addr_0x12A_reg_feedback,
		addr_0x12B_reg_q => addr_0x12B_reg_q,
		addr_0x12B_reg_feedback => addr_0x12B_reg_feedback,
		addr_0x12C_reg_q => addr_0x12C_reg_q,
		addr_0x12C_reg_feedback => addr_0x12C_reg_feedback,
		addr_0x12D_reg_q => addr_0x12D_reg_q,
		addr_0x12D_reg_feedback => addr_0x12D_reg_feedback,
		addr_0x12E_reg_q => addr_0x12E_reg_q,
		addr_0x12E_reg_feedback => addr_0x12E_reg_feedback,
		addr_0x12F_reg_q => addr_0x12F_reg_q,
		addr_0x12F_reg_feedback => addr_0x12F_reg_feedback,
		
		addr_0x230_ddr2_pcie_test_select => addr_0x230_ddr3_pcie_test_select,
		
		reg_0x400_0x4FF_adr => reg_0x400_0x4FF_adr,
		reg_0x400_0x4FF_wr_data => reg_0x400_0x4FF_wr_data,
		reg_0x400_0x4FF_rd_data => reg_0x400_0x4FF_rd_data,
		reg_0x400_0x4FF_wr_en => reg_0x400_0x4FF_wr_en,
		reg_0x400_0x4FF_rd_en => reg_0x400_0x4FF_rd_en,
		
		bram_dma_clk => bram_dma_clk,
		bram_dma_adr => bram_dma_adr,
		bram_dma_rd_en => bram_dma_rd_en,
		bram_dma_rd_data => bram_dma_rd_data,
		
		user_irq => tied_to_gnd,
		user_irq_clear => user_irq_clear,
		
		dma_request_stall => dma_request_stall,
		
		sysmon_write_data => open,
		sysmon_read_data => tied_to_gnd_vec(15 downto 0),
		sysmon_adr => open,
		sysmon_en => open,
		sysmon_wren => open,
		sysmon_rdy => tied_to_gnd,
		
		sis_read_data_fifo_clr => sis_read_data_fifo_clr,
		sis_read_data_fifo_rd_clk => sis_read_data_fifo_rd_clk,
		sis_read_data_fifo_rd_en => sis_read_data_fifo_rd_en,
		sis_read_data_fifo_dout => sis_read_data_fifo_dout,
		sis_read_data_fifo_rd_count => sis_read_data_fifo_rd_count,
		sis_read_data_fifo_empty => sis_read_data_fifo_empty,
		sis_read_addr_fifo_clr => sis_read_addr_fifo_clr,
		sis_read_addr_fifo_wr_clk => sis_read_addr_fifo_wr_clk,
		sis_read_addr_fifo_wr_en => sis_read_addr_fifo_wr_en,
		sis_read_addr_fifo_din => sis_read_addr_fifo_din,
		sis_read_addr_fifo_wr_count => sis_read_addr_fifo_wr_count,
		
		sis_write_fifo_wr_clk => sis_write_fifo_wr_clk,
		sis_write_data_fifo_wr_en => sis_write_data_fifo_wr_en,
		sis_write_data_fifo_din => sis_write_data_fifo_din,
		sis_write_data_fifo_wr_count => sis_write_data_fifo_wr_count,
		sis_write_addr_fifo_wr_en => sis_write_addr_fifo_wr_en,
		sis_write_addr_fifo_din => sis_write_addr_fifo_din,
		sis_write_addr_fifo_wr_count => sis_write_addr_fifo_wr_count
	);
	
-- **************************************************************************************************
--  Clock Distribution Multiplexer control register 
	addr_0x40_reg_feedback  <=  addr_0x40_reg_q(31 downto 0);

	Inst_FD_clk_muxA_out: for i in 0 to 1 generate
		FD_clk_mux1A_D: FD port map(C => trn_clk, D => addr_0x40_reg_q(i), Q => CLK_MUX1A_SEL(i));
	end generate;

	Inst_FD_clk_muxB_out: for i in 0 to 1 generate
		FD_clk_mux1B_D: FD port map(C => trn_clk, D => addr_0x40_reg_q(i+2), Q => CLK_MUX1B_SEL(i));
	end generate;

	Inst_FD_clk_muxC_out: for i in 0 to 1 generate
		FD_clk_mux1C_D: FD port map(C => trn_clk, D => addr_0x40_reg_q(i+4), Q => CLK_MUXAB_SEL(i));
	end generate;

	Inst_FD_clk_muxD_out: for i in 0 to 1 generate
		FD_clk_mux1D_D: FD port map(C => trn_clk, D => addr_0x40_reg_q(i+8), Q => CLK_MUX2A_SEL(i));
	end generate;

	Inst_FD_clk_muxE_out: for i in 0 to 1 generate
		FD_clk_mux1E_D: FD port map(C => trn_clk, D => addr_0x40_reg_q(i+10), Q => CLK_MUX2B_SEL(i));
	end generate;

-- **************************************************************************************************
	
	
-- **************************************************************************************************
-- AD9510 SPI interface

	Inst_ad9510_spi_interface: ad9510_spi_interface PORT MAP(
		clk => trn_clk,
		reset => trn_reset,
		ad9510_spi_status => CLK_DIV_STATUS,
		ad9510_spi_reg_q => addr_0x41_reg_q,
		ad9510_spi_reg_feedback => addr_0x41_reg_feedback,
		ad9510_spi_dout_ireg => ad9510_spi_dout_ireg,
		ad9510_spi_sclk_oreg => ad9510_spi_sclk_oreg,
		ad9510_spi_cs_L_oreg => ad9510_spi_cs_L_oreg,
		ad9510_spi_din_oreg => ad9510_spi_din_oreg,
		ad9510_spi_din_out_en => ad9510_spi_din_out_en,
		ad9510_spi_function_out => ad9510_spi_function_out
	);

	Inst_FD_ad9510_sclk_out: FD port map(C => trn_clk, D => ad9510_spi_sclk_oreg, Q => CLK_DIV_SPI_SCLK);

	Inst_FD_ad9510_out: for i in 0 to 1 generate
		FD_ad9510_D: FD port map(C => trn_clk, D => ad9510_spi_cs_L_oreg(i), Q => CLK_DIV_SPI_CS_L(i));
	end generate;

	ad9510_spi_out_logic: process (trn_clk) 
	begin
		if rising_edge(trn_clk) then
			CLK_DIV_SPI_DI <= ad9510_spi_din_oreg;
		end if;
		if rising_edge(trn_clk) then
			ad9510_spi_dout_ireg <= CLK_DIV_SPI_DO;
		end if;
	end process;
	
-- **************************************************************************************************

-- **************************************************************************************************
   -- Clock multiplier SI5326 SPI interface
	Inst_clock_multiplier_spi_interface: clock_multiplier_spi_interface PORT MAP(
		clk => trn_clk,
		reset => trn_reset,
		Si53xx_spi_reg_q => addr_0x42_reg_q,
		Si53xx_spi_reg_feedback => help_Si53xx_spi_reg_feedback,
		Si53xx_spi_dout_ireg => Si53xx_spi_dout_ireg,
		Si53xx_spi_sclk_oreg => Si53xx_spi_sclk_oreg,
		Si53xx_spi_cs_L_oreg => Si53xx_spi_cs_L_oreg,
		Si53xx_spi_din_oreg => Si53xx_spi_din_oreg,
		Si53xx_spi_decr_out => Si53xx_spi_decr_out,
		Si53xx_spi_incr_out => Si53xx_spi_incr_out,
		Si53xx_spi_reset_L_out => Si53xx_spi_reset_L_out
	);	
	
	adc_si5326_inout_logic: process (trn_clk) 
	begin
		addr_0x42_reg_feedback(31 downto 20)  <=  help_Si53xx_spi_reg_feedback(31 downto 20) ;
		addr_0x42_reg_feedback(15 downto 0)   <=  help_Si53xx_spi_reg_feedback(15 downto 0) ;

		addr_0x42_reg_feedback(19)            <=  '0';
		addr_0x42_reg_feedback(18)            <=  '0';
		addr_0x42_reg_feedback(17)            <=  Si53xx_LOL_ireg;
		addr_0x42_reg_feedback(16)            <=  Si53xx_INT_C1B_ireg;

		if  rising_edge(trn_clk) then  
			 Si53xx_INT_C1B_ireg <= SI_INT_C1B;
			 Si53xx_LOL_ireg     <= SI_LOL;
			 
			 Si53xx_spi_dout_ireg <= SI_SPI_DOUT;
		end if;
	end process;

	Inst_FD_Si53xx_sclk_out: FD port map(C => trn_clk, D => Si53xx_spi_sclk_oreg, Q => SI_SPI_SCLK);
	Inst_FD_Si53xx_cs_out: FD port map(C => trn_clk, D => Si53xx_spi_cs_L_oreg, Q => SI_SPI_SS_L);

	Inst_FD_Si53xx_din_out: FD port map(C => trn_clk, D => Si53xx_spi_din_oreg, Q => SI_SPI_DIN);
	Inst_FD_Si53xx_reset_out: FD port map(C => trn_clk, D => Si53xx_spi_reset_L_out, Q => SI_RST_L);

-- **************************************************************************************************

-- **************************************************************************************************
-- DAC
   dac_ctrl_reg				<=  addr_0x45_reg_q(32 downto 0);
   addr_0x45_reg_feedback	<=  addr_0x45_reg_q(31 downto 0);

   dac_data_reg				<=  addr_0x46_reg_q(32 downto 0);
   addr_0x46_reg_feedback	<=  addr_0x46_reg_q(31 downto 0);
	
	
	addr_0x002_reg_feedback <= addr_0x002_reg_q(31 downto 0);

-- **************************************************************************************************
-- ADC SPI interface
	Inst_adc_spi_interface: adc_spi_interface PORT MAP(
		clk => trn_clk,
		reset => trn_reset,
		adc_spi_reg_q => addr_0x48_reg_q,
		adc_spi_reg_feedback => addr_0x48_reg_feedback,
		adc_spi_dout_ireg => adc_spi_dout_ireg,
		adc_spi_sclk_oreg => adc_spi_sclk_oreg,
		adc_spi_cs_L_oreg => adc_spi_cs_L_oreg,
		adc_spi_din_oreg => adc_spi_din_oreg,
		adc_spi_din_out_en => adc_spi_din_out_en,
		adc_spi_synch_out => adc_spi_synch_out,
		adc_spi_oe_l_out => adc_spi_oe_l_out,
		adc_spi_pwdn_out => adc_spi_pwdn_out
	);

	Inst_FD_adc_sclk_out: FD port map(C => trn_clk, D => adc_spi_sclk_oreg, Q => ADC_SPI_SCLK);

	Inst_FD_adc_out: for i in 0 to 4 generate
		FD_adc_D: FD port map(C => trn_clk, D => adc_spi_cs_L_oreg(i), Q => ADC_SPI_CS_L(i+1));
	end generate;

	adc_spi_inout_logic: process (trn_clk) 
	begin
		if (adc_spi_din_out_en='0') then	      ADC_SPI_DIO <= 'Z' ;
			 elsif rising_edge(trn_clk) then    ADC_SPI_DIO <= adc_spi_din_oreg ;
		end if;
		if  rising_edge(trn_clk) then  
						 adc_spi_dout_ireg <= ADC_SPI_DIO  ;
						 
		end if;
	end process;

-- **************************************************************************************************
	
-- **************************************************************************************************
-- **************************************************************************************************
-- ADC Taps
	adc_tap_delay_reg_q		<= addr_0x49_reg_q;
	addr_0x49_reg_feedback	<= adc_tap_delay_feedback; 
	
		
-- **************************************************************************************************
-- **************************************************************************************************
-- MGTCLK Synthesizer Si5338A

--	MGT_CK_INT : in std_logic;

	Inst_i2c_mgtclk_si5338a: i2c_master_interface PORT MAP(
		clk => trn_clk,
		rst => register_reset,
		i2c_reg_q => addr_0x43_reg_q,
		i2c_reg_feedback => addr_0x43_reg_feedback,
		I2C_SDA => MGT_CK_SDA,
		I2C_SCL => MGT_CK_SCL
	);
	
		
-- ******************************************
-- ** SPI Flash access
-- ******************************************	
	Inst_spi_flash_interface: spi_flash_interface PORT MAP(
		clk => trn_clk,
		rst => register_reset,
		
		flash_mosi => flash_mosi,
		flash_miso => flash_miso,
		flash_cs_l => flash_cs_l,
		flash_clk => flash_clk,
		flash_mux_en => flash_mux_en,
		
		spi_csr_reg => addr_0x44_reg_q,
		spi_csr_reg_feedback => addr_0x44_reg_feedback
	);
	
end Behavioral;
