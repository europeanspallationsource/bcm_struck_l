----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:17:41 08/05/2010 
-- Design Name: 
-- Module Name:    adc_ringbuffer_delay_blocks - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
 
entity adc_ringbuffer_delay_blocks is
    Port ( adc_ringbuffer_logic_reset : in  STD_LOGIC;
           adc1_clk : in  STD_LOGIC;
           adc2_clk : in  STD_LOGIC;
           adc3_clk : in  STD_LOGIC;
           adc4_clk : in  STD_LOGIC;
           adc5_clk : in  STD_LOGIC;
           adc_ch1_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch2_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch3_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch4_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch5_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch6_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch7_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch8_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch9_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch10_ringbuffer_delay_val : in  STD_LOGIC_VECTOR (11 downto 0);
           adc_ch1_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch2_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch3_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch4_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch5_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch6_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch7_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch8_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch9_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch10_ringbuffer_din : in  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch1_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch2_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch3_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch4_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch5_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch6_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch7_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch8_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch9_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0);
           adc_ch10_ringbuffer_dout : out  STD_LOGIC_VECTOR (17 downto 0));
end adc_ringbuffer_delay_blocks;

architecture Behavioral of adc_ringbuffer_delay_blocks is

	COMPONENT adc_ringbuffer_delay_block
	PORT(
		adc_clk : IN std_logic;
		adc_ringbuffer_logic_reset : IN std_logic;
		adc_ringbuffer_delay_val : IN std_logic_vector(11 downto 0);
		adc_ringbuffer_din : IN std_logic_vector(17 downto 0);          
		adc_ringbuffer_dout : OUT std_logic_vector(17 downto 0)
		);
	END COMPONENT;

begin


	adc_ch1_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc1_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch1_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch1_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch1_ringbuffer_dout
	);

	adc_ch2_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc1_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch2_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch2_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch2_ringbuffer_dout
	);

--

	adc_ch3_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc2_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch3_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch3_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch3_ringbuffer_dout
	);

	adc_ch4_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc2_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch4_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch4_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch4_ringbuffer_dout
	);

--

	adc_ch5_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc3_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch5_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch5_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch5_ringbuffer_dout
	);

	adc_ch6_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc3_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch6_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch6_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch6_ringbuffer_dout
	);

--

	adc_ch7_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc4_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch7_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch7_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch7_ringbuffer_dout
	);

	adc_ch8_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc4_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch8_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch8_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch8_ringbuffer_dout
	);

--

	adc_ch9_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc5_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch9_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch9_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch9_ringbuffer_dout
	);

	adc_ch10_ringbuffer_delay_block: adc_ringbuffer_delay_block PORT MAP(
		adc_clk => adc5_clk,
		adc_ringbuffer_logic_reset => adc_ringbuffer_logic_reset,
		adc_ringbuffer_delay_val => adc_ch10_ringbuffer_delay_val,
		adc_ringbuffer_din => adc_ch10_ringbuffer_din,
		adc_ringbuffer_dout => adc_ch10_ringbuffer_dout
	);



end Behavioral;

