----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    13:48:29 08/29/2012
-- Design Name:
-- Module Name:    sis8300l_top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.bcm_package.all;   --! ulegat MOD24092013
use work.sis8300l_top_package.all;   --! maurizio.donna MOD25082014

entity sis8300l_top is
  generic(
        RTM_ZONE3_I2C_EN                      : integer := 1; --
        TRIGGER_BLOCK_EN                      : integer := 0; -- 1 insert Trigger Blocks
        RINGBUFFER_DELAY_EN                   : integer := 1; -- 1 insert Ringbuffer Delay Blocks
        DUAL_CHANNEL_SAMPLING                 : integer := 0; -- 1 insert DUAL CHANNEL SAMPLE Block (two channels/one Meory Block)
        DUAL_OPTICAL_INTERFACE_EN             : integer := 0; --
        QUAD_PORT12_13_14_15_INTERFACE_EN     : integer := 0
   );
    port(
--        ###################################################
--        ##        System Clock 125MHz oscillator
--        ###################################################
        SYS_CLK_P : in std_logic;
        SYS_CLK_N : in std_logic;
--        ###################################################
--        ##        Clocks for MGTs   SI5338 (U571) control
--        ###################################################
        MGT_CK_INT : in std_logic;
        MGT_CK_SDA : inout std_logic;
        MGT_CK_SCL : inout std_logic;
--        ###################################################
--        ##        optical links
--        ###################################################
        MGTCLK0_112_P : in std_logic;
        MGTCLK0_112_N : in std_logic;
--        ### link 1
        SFF_TX_P : out std_logic_vector(1 downto 0);
        SFF_TX_N : out std_logic_vector(1 downto 0);
        SFF_RX_P : in std_logic_vector(1 downto 0);
        SFF_RX_N : in std_logic_vector(1 downto 0);
--        ### link 2
--        ###################################################
--        ##        2 lanes 1GbE
--        ###################################################
--        MGTCLK0_113_P : in std_logic;
--        MGTCLK0_113_N : in std_logic;
--        MGTCKL1_113_P : in std_logic;
--        MGTCKL1_113_N : in std_logic;

--        ETT_P : out std_logic_vector(1 downto 0);
--        ETT_N : out std_logic_vector(1 downto 0);

--        ETR_P : in std_logic_vector(1 downto 0);
--        ETR_N : in std_logic_vector(1 downto 0);
--        ###################################################
--        ##        4 lanes P2P
--        ###################################################
        MGTCLK0_114_P : in std_logic;
        MGTCLK0_114_N : in std_logic;
--        MGTCLK1_114_P : in std_logic;
--        MGTCLK1_114_N : in std_logic;

        P2PT_P : out std_logic_vector(3 downto 0);
        P2PT_N : out std_logic_vector(3 downto 0);

        P2PR_P : in std_logic_vector(3 downto 0);
        P2PR_N : in std_logic_vector(3 downto 0);
--        ###################################################
--        ##         PCIe lanes
--        ###################################################
--        ### ref_clk
        PCIE_REFCLK_MGT_P : in std_logic;
        PCIE_REFCLK_MGT_N : in std_logic;
        --PCIE_REFCLK_MGT_P : in std_logic;
        --PCIE_REFCLK_MGT_N : in std_logic;

--        ### lanes
        PET_P : out std_logic_vector(3 downto 0);
        PET_N : out std_logic_vector(3 downto 0);

        PER_P : in std_logic_vector(3 downto 0);
        PER_N : in std_logic_vector(3 downto 0);
--        ###################################################
--        ##        Z3 lane (optional)
--        ###################################################
        --Z3_CLK_P : in std_logic;
        --Z3_CLK_N : in std_logic;

        --Z3_TX_P : out std_logic;
        --Z3_TX_N : out std_logic;

        --Z3_RX_P : in std_logic;
        --Z3_RX_N : in std_logic;
--        ###################################################
--        ##        DDR3 RAM
--        ###################################################
        ddr3_dq: inout std_logic_vector(63 downto 0);
        ddr3_dm : out std_logic_vector(7 downto 0);
        ddr3_dqs_p : inout std_logic_vector(7 downto 0);
        ddr3_dqs_n : inout std_logic_vector(7 downto 0);

        ddr3_addr : out std_logic_vector(14 downto 0);
        ddr3_ba : out std_logic_vector(2 downto 0);
        ddr3_ras_n : out std_logic;
        ddr3_cas_n : out std_logic;
        ddr3_we_n : out std_logic;
        ddr3_reset_n : out std_logic;
        ddr3_cke : out std_logic_vector(0 downto 0);
        ddr3_odt : out std_logic_vector(0 downto 0);
--        # liegt direkt auf GND: ddr3_cs_n[0] : out std_logic;
        ddr3_ck_p : out std_logic_vector(0 downto 0);
        ddr3_ck_n : out std_logic_vector(0 downto 0);
--        ###################################################
--        ##        Analog Digital Converter
--        ###################################################
--        ### ADC SPY Clocks
        DIV0_CLK05_P : in std_logic;
        DIV0_CLK05_N : in std_logic;
--        DIV1_CLK69_P : in std_logic;
--        DIV1_CLK69_N : in std_logic;

--        ### ADC Control signals
        ADC_SYNC : out std_logic;
        ADC_OEB_L : out std_logic;
        ADC_PDWN : out std_logic;
        ADC_SPI_CS_L : out std_logic_vector(5 downto 1);
        ADC_SPI_SCLK : out std_logic;
        ADC_SPI_DIO : inout std_logic;

--        #
--        ## ADC signals
--        # ADC 1
        ADC1_DCO_P : in std_logic;
        ADC1_DCO_N : in std_logic;

        ADC1_D_P : in std_logic_vector(16 downto 0);
        ADC1_D_N : in std_logic_vector(16 downto 0);

--        ### Zur Info
--        ### Zusätzlich auf dieser Bank
        --DIV0_CLK05_P : in std_logic;
        --DIV0_CLK05_N : in std_logic;
        --DIV1_CLK69_P : in std_logic;
        --DIV1_CLK69_N : in std_logic;

--        # ADC 2
        ADC2_DCO_P : in std_logic;
        ADC2_DCO_N : in std_logic;

        ADC2_D_P : in std_logic_vector(16 downto 0);
        ADC2_D_N : in std_logic_vector(16 downto 0);

--        ### Zur Info
--        ### Zusätzlich auf dieser Bank
        --EXT_TRIG1_P : in std_logic;
        --EXT_TRIG1_N : in std_logic;
        --DAC_CK_P : in std_logic;
        --DAC_CK_N : in std_logic;

--        # ADC 3
        ADC3_DCO_P : in std_logic;
        ADC3_DCO_N : in std_logic;

        ADC3_D_P : in std_logic_vector(16 downto 0);
        ADC3_D_N : in std_logic_vector(16 downto 0);

--        ### Zur Info
--        ### Zusätzlich auf dieser Bank
        --EXT_TRG2_P : in std_logic;
        --EXT_TRG2_N : in std_logic;
        --EXT_TRG3_P : in std_logic;
        --EXT_TRG3_N : in std_logic;

--        # ADC 4
        ADC4_DCO_P : in std_logic;
        ADC4_DCO_N : in std_logic;

        ADC4_D_P : in std_logic_vector(16 downto 0);
        ADC4_D_N : in std_logic_vector(16 downto 0);

--        ### Zur Info
--        ### Zusätzlich auf dieser Bank
        --DIV0_CS_L : in std_logic;
        --DIV1_CS_L : in std_logic;
        --EXT_TRG4_P : in std_logic;
        --EXT_TRG4_N : in std_logic;

--        # ADC 5
        ADC5_DCO_P : in std_logic;
        ADC5_DCO_N : in std_logic;

        ADC5_D_P : in std_logic_vector(16 downto 0);
        ADC5_D_N : in std_logic_vector(16 downto 0);

--        ### Zur Info
--        ### Zusätzlich auf dieser Bank
--        RTM_D4_P : inout std_logic;
--        RTM_D4_N : inout std_logic;
--        RTM_D6_P : inout std_logic;
--        RTM_D6_N : inout std_logic;
--        ###################################################
--        ##        Digital Analog Converter
--        ###################################################
--        #  Virtex Outputs zum DAC Clock MUX
        DAC_CLK_P : out std_logic;
        DAC_CLK_N : out std_logic;

--        #  Virtex Inputs, kommt vom DAC Clock MUX Out, ist extern terminiert
        DAC_MUX_CLK_P : in std_logic;
        DAC_MUX_CLK_N : in std_logic;

        DAC_PD_H : out std_logic;
        DAC_TORB : out std_logic;

        DAC_D_P : out std_logic_vector(15 downto 0);
        DAC_D_N : out std_logic_vector(15 downto 0);

        DAC_SELIQ_P : out std_logic;
        DAC_SELIQ_N : out std_logic;


--        ###################################################
--        ##        Clock multiplier SI5326 (U242) control
--        ###################################################
        SI_RST_L : out std_logic;
        SI_INT_C1B : in std_logic;
        SI_LOL : in std_logic;

        SI_SPI_SS_L : out std_logic;
        SI_SPI_SCLK : out std_logic;
        SI_SPI_DIN : out std_logic;
        SI_SPI_DOUT : in std_logic;
--        ###################################################
--        ##        RTM
--        ###################################################
--        RTM_IIC_INT_SCL : inout std_logic;
--        RTM_IIC_INT_SDA : inout std_logic;

--        RTM_IIC_EXT_SCL : inout std_logic;
--        RTM_IIC_EXT_SDA : inout std_logic;

        --RTM_IIC_USR_SCL : inout std_logic;
        --RTM_IIC_USR_SDA : inout std_logic;

--        RTM_D_P : in std_logic_vector(5 downto 3);
--        RTM_D_N : in std_logic_vector(5 downto 3);
--        RTM_D_P : out std_logic_vector(8 downto 6);
--        RTM_D_N : out std_logic_vector(8 downto 6);

--        +++++++++++++++++++++++++++++++++++++++++++++++++++
--        Enable only one connection type at the time!

--   gen_option_rtm_zone3_i2c_block0_off: if (RTM_ZONE3_I2C_EN = 0) generate
--        RTM_D_P : inout std_logic_vector(8 downto 3);
--        RTM_D_N : inout std_logic_vector(8 downto 3);
--   end generate;

--   gen_option_rtm_zone3_i2c_block0_on: if (RTM_ZONE3_I2C_EN = 1) generate
        RTM_I2C_SDA    : inout std_logic;
        RTM_I2C_SCL    : inout std_logic;
--   end generate;

--        +++++++++++++++++++++++++++++++++++++++++++++++++++


        Z3_ILOCK0 : out std_logic;
        Z3_ILOCK1 : out std_logic;

--        # auch verbunden mit Output der MMC
        Z3_ILOCK_EN_L : inout std_logic; -- OpenCollector
        Z3_TCLK_EN_L : inout std_logic;  -- OpenCollector

--        RTM_CLK5_P : in std_logic;
--        RTM_CLK5_N : in std_logic;


--        ###################################################
--        ##        Harlink Data Out
--        ###################################################
        EXT_DO_P : out std_logic_vector(4 downto 1);
        EXT_DO_N : out std_logic_vector(4 downto 1);

--        ###################################################
--        ##        Harlink ADC External Trigger
--        ###################################################
        EXT_TRG_P : in std_logic_vector(4 downto 1);
        EXT_TRG_N : in std_logic_vector(4 downto 1);


--        ###################################################
--        ##        Clock Multiplexer
--        ###################################################
        CLK_MUXAB_SEL : out std_logic_vector(1 downto 0);
        CLK_MUX1A_SEL : out std_logic_vector(1 downto 0);
        CLK_MUX1B_SEL : out std_logic_vector(1 downto 0);
        CLK_MUX2A_SEL : out std_logic_vector(1 downto 0);
        CLK_MUX2B_SEL : out std_logic_vector(1 downto 0);

--        ###################################################
--        ##        ADC Clock Divider control
--        ###################################################
        CLK_DIV_SPI_SCLK : out std_logic;
        CLK_DIV_SPI_DI : out std_logic;
        CLK_DIV_SPI_DO : in std_logic;
        CLK_DIV_SPI_CS_L : out std_logic_vector(1 downto 0);
        CLK_DIV_STATUS : in std_logic_vector(1 downto 0);
        CLK_DIV_FUNCTION : out std_logic;

--        ###################################################
--        ##    MLVDS Bus (Buffer - single ended - FPGA)
--        ###################################################
        P1720_DE_H : out std_logic_vector(7 downto 0);
        MLVDS_DOUT : out std_logic_vector(7 downto 0);
        MLVDS_RIN : in std_logic_vector(7 downto 0);




--        ###################################################
--        ##    MUX DAC control
--        ###################################################
        MUX_DAC_SEL0 : out std_logic;
        MUX_DAC_SEL1 : out std_logic;

--        ###################################################
--        ##    MMC signals
--        ###################################################
        PCIE_RESET_L : in std_logic;

        MMC_SPI_MISO : out std_logic;
        MMC_SPI_MOSI : in std_logic;
        MMC_SPI_SCK : in std_logic;
        MMC_SPI_SS_L : in std_logic;

        MMU_FPGA_RXD : in std_logic;
        MMU_FPGA_TXD : in std_logic;
--        ###################################################
--        ##    diverse
--        ###################################################
--        # Test points
        TEST1 : out std_logic;
        TEST2 : out std_logic;

--        EN_JTAG_CON_L : out std_logic;

--        # LED signals
        LED_SERIAL_DATA : out std_logic;

--        # WATCHDOG signal
        FPGA_WATCHDOG : out std_logic;

        FLASH_CS_L : out std_logic;
        FLASH_DIN : out std_logic
    );
end sis8300l_top;

architecture Behavioral of sis8300l_top is

    signal tied_to_vcc_vec: std_logic_vector(7 downto 0);
    signal tied_to_gnd_vec: std_logic_vector(127 downto 0);
    signal tied_to_vcc: std_logic := '1';
    signal tied_to_gnd: std_logic := '0';

    signal pcie_refclk: std_logic;
    signal pcie_rst_n: std_logic;
    signal pcie_trn_lnk_up: std_logic;

    signal harlink_out:  STD_LOGIC_VECTOR(4 downto 1);
    signal harlink_in_buf:  STD_LOGIC_VECTOR(4 downto 1);
    signal harlink_in_reg:  STD_LOGIC_VECTOR(4 downto 1);

    signal mlvds_in_reg:  STD_LOGIC_VECTOR(7 downto 0);

    signal module_version_register: std_logic_vector(31 downto 0);
    signal module_serial_no_register: std_logic_vector(31 downto 0);
    signal firmware_options_register: std_logic_vector(31 downto 0);

    signal addr_0x000_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x001_reg_feedback: std_logic_vector(31 downto 0);

    signal addr_0x003_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x003_reg_q: std_logic_vector(32 downto 0);


    signal addr_0x004_status_feedback: std_logic_vector(15 downto 0);
    signal addr_0x004_reg_jk: std_logic_vector(15 downto 0);
    signal addr_0x005_reg_feedback: std_logic_vector(31 downto 0);


    signal addr_0x10_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x10_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x11_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x11_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x12_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x12_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x13_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x13_reg_q: std_logic_vector(32 downto 0);

    signal addr_0x14_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x14_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x15_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x15_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x16_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x16_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x17_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x17_reg_q: std_logic_vector(32 downto 0);
    signal addr_0x47_reg_feedback: std_logic_vector(31 downto 0);
    signal addr_0x47_reg_q: std_logic_vector(32 downto 0);


    signal addr_0x100_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x100_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x101_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x101_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x102_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x102_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x103_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x103_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x104_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x104_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x105_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x105_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x106_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x106_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x107_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x107_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x108_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x108_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x109_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x109_reg_feedback : std_logic_vector(31 downto 0);

    signal addr_0x110_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x110_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x111_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x111_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x112_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x112_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x113_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x113_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x114_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x114_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x115_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x115_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x116_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x116_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x117_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x117_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x118_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x118_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x119_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x119_reg_feedback : std_logic_vector(31 downto 0);

    signal addr_0x120_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x120_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x121_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x121_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x122_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x122_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x123_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x123_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x124_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x124_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x125_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x125_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x126_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x126_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x127_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x127_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x128_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x128_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x129_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x129_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12A_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x12A_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12B_reg_q : std_logic_vector(31 downto 0);
    signal addr_0x12B_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12C_reg_q : std_logic_vector(32 downto 0);
    signal addr_0x12C_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12D_reg_q : std_logic_vector(32 downto 0);
    signal addr_0x12D_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12E_reg_q : std_logic_vector(32 downto 0);
    signal addr_0x12E_reg_feedback : std_logic_vector(31 downto 0);
    signal addr_0x12F_reg_q : std_logic_vector(32 downto 0);
    signal addr_0x12F_reg_feedback : std_logic_vector(31 downto 0);

    signal addr_0x230_ddr3_pcie_test_select : std_logic_vector(32 downto 0);

    signal wd_count:  STD_LOGIC_VECTOR(27 downto 0);
    signal timer_524us_signal : std_logic;
    signal timer_2096us_signal : std_logic;
    signal timer_134ms_signal : std_logic;

    signal led_inputs : std_logic_vector(15 downto 0);
    signal led_serial_out : std_logic;
    signal led_link_up : std_logic;
    signal led_pcie_active : std_logic;
    signal led_system_access : std_logic;
    signal user_led : std_logic;

    signal sys_clk125_in: std_logic;
    signal sys_clk125_bufg: std_logic;

    signal clock_reset_count : std_logic_vector(3 downto 0);
    signal clock_reset  : std_logic;

    signal sys_clk50 : std_logic;
    signal sys_clk125    : std_logic;
    signal sys_clk200    : std_logic;

    signal sys_clk_ddr3 : std_logic;
    signal sys_clk_ddr3_rd_base : std_logic;
    signal sys_clk_ddr3_usr : std_logic;

    signal sys_clock_ok    : std_logic;
    signal sys_clk_dll_reset : std_logic;
    signal sys_clk_dll_reset_count : STD_LOGIC_VECTOR(3 downto 0);

    signal sys_idelayctrl_rdy : std_logic;
    signal sys_idelayctrl_rdy_cnt : std_logic_vector(15 downto 0);
    signal sys_clk_ddr3_rdy : std_logic;

    signal ddr3_phy_init_done : std_logic;

    signal adc1_clk, adc2_clk, adc3_clk, adc4_clk, adc5_clk : std_logic;
    signal adc1_buf_din, adc2_buf_din, adc3_buf_din, adc4_buf_din, adc5_buf_din : std_logic_vector(16 downto 0);
    signal adc_ch1_pipe_din, adc_ch2_pipe_din, adc_ch3_pipe_din, adc_ch4_pipe_din, adc_ch5_pipe_din : std_logic_vector(16 downto 0);
    signal adc_ch6_pipe_din, adc_ch7_pipe_din, adc_ch8_pipe_din, adc_ch9_pipe_din, adc_ch10_pipe_din : std_logic_vector(16 downto 0);


    signal adc_ch1_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch2_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch3_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch4_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch5_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch6_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch7_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch8_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch9_trigger_threshold_reg: std_logic_vector(31 downto 0);
    signal adc_ch10_trigger_threshold_reg: std_logic_vector(31 downto 0);

    signal adc_ch1_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch2_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch3_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch4_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch5_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch6_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch7_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch8_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch9_trigger_setup_reg: std_logic_vector(31 downto 0);
    signal adc_ch10_trigger_setup_reg: std_logic_vector(31 downto 0);

    signal adc_ch1_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch1_trigger_out_length_pulse        : std_logic;
    signal adc_ch2_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch2_trigger_out_length_pulse        : std_logic;
    signal adc_ch3_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch3_trigger_out_length_pulse        : std_logic;
    signal adc_ch4_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch4_trigger_out_length_pulse        : std_logic;
    signal adc_ch5_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch5_trigger_out_length_pulse        : std_logic;
    signal adc_ch6_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch6_trigger_out_length_pulse        : std_logic;
    signal adc_ch7_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch7_trigger_out_length_pulse        : std_logic;
    signal adc_ch8_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch8_trigger_out_length_pulse        : std_logic;
    signal adc_ch9_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch9_trigger_out_length_pulse        : std_logic;
    signal adc_ch10_trigger_out_1clk_pulse        : std_logic;
    signal adc_ch10_trigger_out_length_pulse        : std_logic;

    signal adc_chx_trigger_or_pulse        : std_logic;

    -- external trigger
    signal external_ored_trigger_signal        : std_logic;

    signal adc_ch1_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch2_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch3_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch4_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch5_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch6_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch7_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch8_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch9_ringbuffer_din: std_logic_vector(17 downto 0);
    signal adc_ch10_ringbuffer_din: std_logic_vector(17 downto 0);

    signal adc_ch1_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch2_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch3_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch4_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch5_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch6_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch7_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch8_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch9_ringbuffer_dout: std_logic_vector(17 downto 0);
    signal adc_ch10_ringbuffer_dout: std_logic_vector(17 downto 0);


    signal adc_ch1_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch2_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch3_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch4_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch5_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch6_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch7_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch8_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch9_ringbuffer_delay_val: std_logic_vector(11 downto 0);
    signal adc_ch10_ringbuffer_delay_val: std_logic_vector(11 downto 0);


    signal ext_trigger_harlink_delayA:  STD_LOGIC_VECTOR(4 downto 1);
    signal ext_trigger_harlink_delayB:  STD_LOGIC_VECTOR(4 downto 1);
    signal ext_trigger_harlink_rising_pulse:  STD_LOGIC_VECTOR(4 downto 1);
    signal ext_trigger_harlink_falling_pulse:  STD_LOGIC_VECTOR(4 downto 1);

    signal ext_trigger_mlvds_delayA:  STD_LOGIC_VECTOR(7 downto 0);
    signal ext_trigger_mlvds_delayB:  STD_LOGIC_VECTOR(7 downto 0);
    signal ext_trigger_mlvds_rising_pulse:  STD_LOGIC_VECTOR(7 downto 0);
    signal ext_trigger_mlvds_falling_pulse:  STD_LOGIC_VECTOR(7 downto 0);

    signal adc_sample_external_trigger_enable: std_logic;
    signal adc_sample_internal_trigger_enable: std_logic;
    signal adc_sample_channel_trigger_disable: std_logic_vector(9 downto 0);

    signal adc_sample_disable_pulse: std_logic;
    signal adc_sample_arm_tigger_pulse: std_logic;
    signal adc_sample_start_pulse: std_logic;
    signal adc_sample_start_pulse_delay1: std_logic;
    signal adc_sample_start_pulse_delay2: std_logic;

    signal adc_sample_arm_for_trigger: std_logic;
    signal adc_chx_sample_logic_active_or_delay1: std_logic;
    signal adc_chx_sample_logic_active_or_delay2: std_logic;
    signal adc_chx_sample_logic_active_or_delay3: std_logic;

    signal synch_adc1_clk_adc_sample_arm_for_trigger: std_logic;
    signal synch_adc2_clk_adc_sample_arm_for_trigger: std_logic;
    signal synch_adc3_clk_adc_sample_arm_for_trigger: std_logic;
    signal synch_adc4_clk_adc_sample_arm_for_trigger: std_logic;
    signal synch_adc5_clk_adc_sample_arm_for_trigger: std_logic;


    signal synch_adc1_clk_pcie_trigger: std_logic;
    signal synch_adc2_clk_pcie_trigger: std_logic;
    signal synch_adc3_clk_pcie_trigger: std_logic;
    signal synch_adc4_clk_pcie_trigger: std_logic;
    signal synch_adc5_clk_pcie_trigger: std_logic;


    signal synch_adc1_clk_external_trigger: std_logic;
    signal synch_adc2_clk_external_trigger: std_logic;
    signal synch_adc3_clk_external_trigger: std_logic;
    signal synch_adc4_clk_external_trigger: std_logic;
    signal synch_adc5_clk_external_trigger: std_logic;

    signal synch_adc1_clk_internal_trigger: std_logic;
    signal synch_adc2_clk_internal_trigger: std_logic;
    signal synch_adc3_clk_internal_trigger: std_logic;
    signal synch_adc4_clk_internal_trigger: std_logic;
    signal synch_adc5_clk_internal_trigger: std_logic;

    signal adc_ch1_start_pulse: std_logic;
    signal adc_ch2_start_pulse: std_logic;
    signal adc_ch3_start_pulse: std_logic;
    signal adc_ch4_start_pulse: std_logic;
    signal adc_ch5_start_pulse: std_logic;
    signal adc_ch6_start_pulse: std_logic;
    signal adc_ch7_start_pulse: std_logic;
    signal adc_ch8_start_pulse: std_logic;
    signal adc_ch9_start_pulse: std_logic;
    signal adc_ch10_start_pulse: std_logic;

    signal adc_buffer_logic_reset: std_logic;
    signal adc_chx_sample_logic_active_or: std_logic;
    signal adc_chx_sample_buffer_not_empty_or: std_logic;
    signal adc_ddr3_write_logic_enable: std_logic;


    signal sys_dac_in : std_logic;
    signal dac_clk_in : std_logic;
    signal dac1_data_out, dac2_data_out : std_logic_vector(15 downto 0);
    signal dac_ctrl_reg:  STD_LOGIC_VECTOR(32 downto 0);
    signal dac_data_reg:  STD_LOGIC_VECTOR(32 downto 0);
    signal dac_test_mode: std_logic_vector(1 downto 0);
    signal dac_dcm_reset_pulse : std_logic;
    signal dac_test_counter:  STD_LOGIC_VECTOR(19 downto 0);
    signal dac_tap_write_pulse : std_logic;

    signal dac_clk_dcm_lock : std_logic;
    signal dac_data_select_reset : std_logic;
    signal dac_select_ch1 : std_logic;
    signal dac_data_mux_out: std_logic_vector(15 downto 0);

    signal register_trn_clk : std_logic;
    signal register_reset : std_logic;
    signal gtx_usr_side_fifo_clk : std_logic;


    signal port12_15_gtx_linkup : std_logic_vector(3 downto 0);
    signal port12_15_gtx_reset : std_logic;
    signal opt1_2_gtx_linkup : std_logic_vector(1 downto 0);
    signal opt1_2_gtx_reset : std_logic;
    signal opt1_2_serdes_reset : std_logic;
    signal gtx_usrclk : std_logic;
    signal GTX0_LOOPBACK_IN : std_logic_vector(2 downto 0);
    signal GTX1_LOOPBACK_IN : std_logic_vector(2 downto 0);

    signal opt1_select_flag : std_logic;
    signal opt1_fifo_reset : std_logic;
    signal opt1_in_fifo_rden : std_logic;
    signal opt1_in_fifo_dout : std_logic_vector(32 downto 0);
    signal opt1_out_fifo_wren : std_logic;
    signal opt1_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal opt1_out_fifo_din : std_logic_vector(32 downto 0);
    signal opt1_in_fifo_rcnt : std_logic_vector(8 downto 0);

    signal opt2_select_flag : std_logic;
    signal opt2_fifo_reset : std_logic;
    signal opt2_in_fifo_rden : std_logic;
    signal opt2_in_fifo_dout : std_logic_vector(32 downto 0);
    signal opt2_out_fifo_wren : std_logic;
    signal opt2_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal opt2_out_fifo_din : std_logic_vector(32 downto 0);
    signal opt2_in_fifo_rcnt : std_logic_vector(8 downto 0);

    signal port12_select_flag, port13_select_flag, port14_select_flag, port15_select_flag : std_logic;
    signal port12_fifo_reset : std_logic;
    signal port12_in_fifo_rden : std_logic;
    signal port12_out_fifo_wren : std_logic;
    signal port12_out_fifo_din : std_logic_vector(32 downto 0);
    signal port12_in_fifo_dout : std_logic_vector(32 downto 0);
    signal port12_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal port12_in_fifo_rcnt : std_logic_vector(8 downto 0);
    signal port13_fifo_reset : std_logic;
    signal port13_in_fifo_rden : std_logic;
    signal port13_out_fifo_wren : std_logic;
    signal port13_out_fifo_din : std_logic_vector(32 downto 0);
    signal port13_in_fifo_dout : std_logic_vector(32 downto 0);
    signal port13_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal port13_in_fifo_rcnt : std_logic_vector(8 downto 0);
    signal port14_fifo_reset : std_logic;
    signal port14_in_fifo_rden : std_logic;
    signal port14_out_fifo_wren : std_logic;
    signal port14_out_fifo_din : std_logic_vector(32 downto 0);
    signal port14_in_fifo_dout : std_logic_vector(32 downto 0);
    signal port14_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal port14_in_fifo_rcnt : std_logic_vector(8 downto 0);
    signal port15_fifo_reset : std_logic;
    signal port15_in_fifo_rden : std_logic;
    signal port15_out_fifo_wren : std_logic;
    signal port15_out_fifo_din : std_logic_vector(32 downto 0);
    signal port15_in_fifo_dout : std_logic_vector(32 downto 0);
    signal port15_out_fifo_wcnt : std_logic_vector(8 downto 0);
    signal port15_in_fifo_rcnt : std_logic_vector(8 downto 0);

    signal adc_tap_delay_reg_q : std_logic_vector(32 downto 0);
    signal adc_tap_delay_feedback : std_logic_vector(31 downto 0);

    signal ddr3_controller_reset : std_logic;
    signal ddr3_usr_clk : std_logic;
    signal ddr3_test_rst : std_logic;
    signal pcie_test_ddr3_write_mode : std_logic;

    signal ddr3_read_data_fifo_dout : std_logic_vector(255 downto 0);
    signal ddr3_read_data_fifo_rd_count : std_logic_vector(9 downto 0);
    signal ddr3_read_data_fifo_empty : std_logic;
    signal ddr3_read_data_fifo_rd_en : std_logic;
    signal ddr3_read_data_fifo_clr : std_logic;
    signal ddr3_read_data_fifo_rd_clk : std_logic;

    signal ddr3_read_addr_fifo_clr : std_logic;
    signal ddr3_read_addr_fifo_wr_en : std_logic;
    signal ddr3_read_addr_fifo_din : std_logic_vector(31 downto 0);
    signal ddr3_read_addr_fifo_wr_count : std_logic_vector(9 downto 0);
    signal ddr3_read_addr_fifo_wr_clk : std_logic;

    signal ddr3_write_fifo_wr_clr : std_logic;
    signal ddr3_write_fifo_wr_clk : std_logic;
    signal ddr3_write_data_fifo_wr_count : std_logic_vector(9 downto 0);
    signal ddr3_write_addr_fifo_wr_count : std_logic_vector(9 downto 0);

    signal pci_ddr3_write_data_fifo_wr_en : std_logic;
    signal pci_ddr3_write_data_fifo_din : std_logic_vector(255 downto 0);
--    signal pci_ddr3_write_data_fifo_wr_count : std_logic_vector(9 downto 0);
    signal pci_ddr3_write_addr_fifo_wr_en : std_logic;
    signal pci_ddr3_write_addr_fifo_din : std_logic_vector(31 downto 0);
--    signal pci_ddr3_write_addr_fifo_wr_count : std_logic_vector(9 downto 0);

    signal pcie_ddr3_test_write_addr_fifo_wr_en : std_logic;
    signal pcie_ddr3_test_write_addr_fifo_din : std_logic_vector(31 downto 0);
    signal pcie_ddr3_test_write_data_fifo_wr_en : std_logic;
    signal pcie_ddr3_test_write_data_fifo_din : std_logic_vector(255 downto 0);

    signal sis_write_addr_fifo_wr_en : std_logic;
    signal sis_write_addr_fifo_din : std_logic_vector(31 downto 0);
    signal sis_write_data_fifo_wr_en : std_logic;
    signal sis_write_data_fifo_din : std_logic_vector(255 downto 0);

    signal adc_ddr3_write_addr_fifo_wr_en : std_logic;
    signal adc_ddr3_write_addr_fifo_din : std_logic_vector(31 downto 0);
    signal adc_ddr3_write_data_fifo_wr_en : std_logic;
    signal adc_ddr3_write_data_fifo_din : std_logic_vector(255 downto 0);

    signal pci_flash_mosi : std_logic;
    signal pci_flash_miso : std_logic;
    signal pci_flash_cs_l : std_logic;
    signal pci_flash_clk : std_logic;
    signal pci_flash_mux_en : std_logic;

    signal ad9510_no1_clkin: std_logic;
    signal sys_ad9510_no1_bufg_clk: std_logic;

    --signal ad9510_no2_clkin: std_logic;
    signal sys_ad9510_no2_bufg_clk: std_logic;

    signal ad9510_spi_function_out: std_logic_vector(2 downto 0);
    signal synch_ad9510_spi_function_out: std_logic;
    signal synch_ad9510_spi_function_out_delay: std_logic;
    signal synch_ad9510_spi_function_synch_pulse: std_logic;

    signal adc_spi_function_out: std_logic;
    signal adc_spi_function_delay1_out: std_logic;
    signal adc_spi_function_delay2_out: std_logic;
    signal adc_spi_function_synch_pulse: std_logic;
    signal adc_spi_synch_out: std_logic;
    signal adc_spi_oe_l_out : std_logic;
    signal adc_spi_pwdn_out : std_logic;

    signal rtm_lvds_in_buf:  STD_LOGIC_VECTOR(2 downto 0);
    signal rtm_lvds_out:  STD_LOGIC_VECTOR(2 downto 0);
    signal rtm_lvds_out_t:  STD_LOGIC_VECTOR(2 downto 0);

    signal dac_mmcm_lock : std_logic;
    signal dac_out_synch_clk: std_logic;

    signal adc_ddr3_daq_done, adc_ddr3_daq_done_lat : std_logic;
    signal adc_ddr3_daq_done_pulse : std_logic;

    attribute IOB: string;

    attribute IOB of Inst_IREG_harlink_in_reg: label is "TRUE";
    attribute IOB of Inst_IREG_MLVDS_in_reg: label is "TRUE";

--    attribute keep : boolean;
--    attribute keep of adc_chx_trigger_or_pulse: signal is true;
--    attribute keep of adc_ch1_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch2_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch3_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch4_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch5_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch6_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch7_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch8_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch9_ringbuffer_dout: signal is true;
--    attribute keep of adc_ch10_ringbuffer_dout: signal is true;


-----------------------------------------------------------------
-- ulegat MOD24092013
-----------------------------------------------------------------

    --! Declaration of internal signals
    signal s_rst : std_logic;

    signal s_ch1_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch2_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch3_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch4_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch5_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch6_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch7_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch8_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch9_processed_dout : std_logic_vector(15 downto 0);
    signal s_ch10_processed_dout : std_logic_vector(15 downto 0);

    signal s_ch1_start_pulse  : std_logic;
    signal s_ch2_start_pulse  : std_logic;
    signal s_ch3_start_pulse  : std_logic;
    signal s_ch4_start_pulse  : std_logic;
    signal s_ch5_start_pulse  : std_logic;
    signal s_ch6_start_pulse  : std_logic;
    signal s_ch7_start_pulse  : std_logic;
    signal s_ch8_start_pulse  : std_logic;
    signal s_ch9_start_pulse  : std_logic;
    signal s_ch10_start_pulse : std_logic;

    --! Interlock to output
    signal s_itl    : std_logic;
    signal s_itl_hb : std_logic;

    signal r_ctrl : t_ctrl_register_record;
    signal r_stat : t_stat_register_record;

    --! End pulse trigger conditioning
    signal s_end_pulse_ored_signal : std_logic;

    signal s_end_pulse_adc1_synch : std_logic;
    signal s_end_pulse_adc2_synch : std_logic;
    signal s_end_pulse_adc3_synch : std_logic;
    signal s_end_pulse_adc4_synch : std_logic;
    signal s_end_pulse_adc5_synch : std_logic;

    signal s_adc_ch1_end_pulse : std_logic;
    signal s_adc_ch2_end_pulse : std_logic;
    signal s_adc_ch3_end_pulse : std_logic;
    signal s_adc_ch4_end_pulse : std_logic;
    signal s_adc_ch5_end_pulse : std_logic;
    signal s_adc_ch6_end_pulse : std_logic;
    signal s_adc_ch7_end_pulse : std_logic;
    signal s_adc_ch8_end_pulse : std_logic;
    signal s_adc_ch9_end_pulse : std_logic;
    signal s_adc_ch10_end_pulse : std_logic;

    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    signal bram_dma_clk : std_logic;
    signal bram_dma_adr : std_logic_vector(31 downto 0);
    signal bram_dma_rd_en : std_logic;
    signal bram_dma_rd_data : std_logic_vector(63 downto 0);

    signal reg_0x400_0x4FF_adr : std_logic_vector(7 downto 0);
    signal reg_0x400_0x4FF_wr_data : std_logic_vector(31 downto 0);
    signal reg_0x400_0x4FF_rd_data : std_logic_vector(31 downto 0);
    signal reg_0x400_0x4FF_wr_en : std_logic;
    signal reg_0x400_0x4FF_rd_en : std_logic;


    component led_serial_fsm
    port(
        sys_clk125 : in std_logic;
        reset : in std_logic;
        timer_524us_signal : in std_logic;
        timer_2096us_signal : in std_logic;
        led_inputs : in std_logic_vector(15 downto 0);
        led_serial_out : out std_logic
        );
    end component;

    component mmc_flash_bridge
    port(
        sys_clk125 : in std_logic;
        mmc_spi_mosi : in std_logic;
        mmc_spi_sck : in std_logic;
        mmc_spi_ss_l : in std_logic;
        fpga_spi_mosi : in std_logic;
        fpga_spi_sck : in std_logic;
        fpga_spi_ss_l : in std_logic;
        mmc_fpga_flash_sel : in std_logic;
        spi_flash_din : out std_logic;
        spi_flash_cs_n : out std_logic;
        mmc_spi_miso : out std_logic;
        fpga_spi_miso : out std_logic
        );
    end component;

    component serial_no_fsm
    port(
        sys_clk125 : in std_logic;
        reset : in std_logic;
        mmu_fpga_clk : in std_logic;
        mmu_fpga_d : in std_logic;
        serial_number : out std_logic_vector(31 downto 0)
        );
    end component;

    component adc_input_block
    port(
        tap_reg_clk : in std_logic;
        adc_tap_delay_reg_q : in std_logic_vector(32 downto 0);
        adc1_clk : in std_logic;
        adc2_clk : in std_logic;
        adc3_clk : in std_logic;
        adc4_clk : in std_logic;
        adc5_clk : in std_logic;
        adc1_buf_din : in std_logic_vector(16 downto 0);
        adc2_buf_din : in std_logic_vector(16 downto 0);
        adc3_buf_din : in std_logic_vector(16 downto 0);
        adc4_buf_din : in std_logic_vector(16 downto 0);
        adc5_buf_din : in std_logic_vector(16 downto 0);
        adc_tap_delay_feedback : out std_logic_vector(31 downto 0);
        adc_ch1_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch2_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch3_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch4_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch5_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch6_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch7_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch8_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch9_pipe_din : out std_logic_vector(16 downto 0);
        adc_ch10_pipe_din : out std_logic_vector(16 downto 0)
        );
    end component;

    component adc_fir_and_threshold_trigger_block
    port(
        adc_clk : in std_logic;
        trigger_reset : in std_logic;
        adc_trigger_threshold_reg : in std_logic_vector(31 downto 0);
        adc_trigger_setup_reg : in std_logic_vector(31 downto 0);
        adc_din : in std_logic_vector(16 downto 0);
        adc_delayed_din : out std_logic_vector(16 downto 0);
        adc_trigger_gate_valid : out std_logic;
        adc_trigger_out_1clk_pulse : out std_logic;
        adc_trigger_out_length_pulse : out std_logic
        );
    end component;






begin

    adc1_clk_ODDR_inst : ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
        INIT => '0',   -- Initial value for Q port ('1' or '0')
        SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
        Q => TEST1,   -- 1-bit DDR output
--        C => adc1_clk,    -- 1-bit clock input
--        C => dac_clk_in,    -- 1-bit clock input
        C => gtx_usrclk,    -- 1-bit clock input
        CE => '1',  -- 1-bit clock enable input
        D1 => '1',  -- 1-bit data input (positive edge)
        D2 => '0',  -- 1-bit data input (negative edge)
        R => '0',    -- 1-bit reset input
        S => '0'     -- 1-bit set input
    );

    dac_clk_ODDR_inst : ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
        INIT => '0',   -- Initial value for Q port ('1' or '0')
        SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
        Q => TEST2,   -- 1-bit DDR output
--        C => dac_clk_in,    -- 1-bit clock input
        C => dac_out_synch_clk,    -- 1-bit clock input
        CE => '1',  -- 1-bit clock enable input
        D1 => '1',  -- 1-bit data input (positive edge)
        D2 => '0',  -- 1-bit data input (negative edge)
        R => '0',    -- 1-bit reset input
        S => '0'     -- 1-bit set input
    );

    tied_to_vcc_vec <= (others => '1');
    tied_to_gnd_vec <= (others => '0');

    -- revision history
    module_version_register(31 downto 16) <=   X"8301"; -- Module: SIS8300-L
    module_version_register(15 downto 12) <=   X"2";    -- upper Major reg: SIS8300-L hardware - Generische Firmware
    module_version_register(11 downto 8)  <=   X"8";    -- lower Major reg: Generische Firmware (Macro version)
    module_version_register(7 downto 0)   <=   X"08";   -- Minor Firmware version reg

----------------------------------------------------------------------------------------------------
-- define firmware options

   firmware_options_register(31 downto 12)   <=     X"00000"     ; -- reserved

   firmware_options_register(11)   <=    '0'     ; -- reserved
   firmware_options_register(10)   <=    '0'     ; -- reserved
   firmware_options_register(9)    <=    '0'     ; -- reserved
   firmware_options_register(8)    <=    '0'     ; -- reserved
   firmware_options_register(7)    <=    '0'     ; -- reserved

    addr_0x005_reg_feedback <=  firmware_options_register ;

-- -----------------

-- -----------------
   gen_option_dual_optical_block_off: if (DUAL_OPTICAL_INTERFACE_EN = 0) generate
        firmware_options_register(6)            <=     '0'  ; --
   end generate;

   gen_option_dual_optical_block_on: if (DUAL_OPTICAL_INTERFACE_EN = 1) generate
        firmware_options_register(6)            <=     '1'  ; --
   end generate;

-- -----------------
    firmware_options_register(5)            <=     '0'  ; --
-- -----------------

   gen_option_quad_port12_15_block_off: if (QUAD_PORT12_13_14_15_INTERFACE_EN = 0) generate
        firmware_options_register(4)            <=     '0'  ; --
   end generate;

   gen_option_quad_port12_15_block_on: if (QUAD_PORT12_13_14_15_INTERFACE_EN = 1) generate
        firmware_options_register(4)            <=     '1'  ; --
   end generate;

-- -----------------
   firmware_options_register(3)    <=    '0'     ; -- reserved
   firmware_options_register(2)    <=    '0'     ; -- reserved

-- -----------------
   gen_option_ringbuffer_delay_block_off: if (RINGBUFFER_DELAY_EN = 0) generate
        firmware_options_register(1)            <=     '0'  ; --
   end generate;

   gen_option_ringbuffer_delay_block_on: if (RINGBUFFER_DELAY_EN = 1) generate
        firmware_options_register(1)            <=     '1'  ; --
   end generate;

-- -----------------
   gen_option_trigger_block_off: if (TRIGGER_BLOCK_EN = 0) generate
        firmware_options_register(0)            <=     '0'  ; --
   end generate;

   gen_option_trigger_block_on: if (TRIGGER_BLOCK_EN = 1) generate
        firmware_options_register(0)            <=     '1'  ; --
   end generate;
-- -----------------
----------------------------------------------------------------------------------------------------



    -- 100MHz vom slot
    pcie_refclk_ibufds_inst : IBUFDS_GTXE1
    port map(
        O       => pcie_refclk,
        ODIV2   => open,
        I       => PCIE_REFCLK_MGT_P,
        IB      => PCIE_REFCLK_MGT_N,
        CEB     => '0'
    );

    -- Reset vom slot
    pcie_rst_n_ibuf_inst: IBUF
    port map(
        O => pcie_rst_n,
        I => PCIE_RESET_L
    );

    addr_0x000_reg_feedback <= module_version_register;

    Inst_pcie_interface: entity work.pcie_interface
    port map(
        pcie_refclk => pcie_refclk,
        pcie_rst_n => pcie_rst_n,
        pcie_trn_lnk_up => pcie_trn_lnk_up,

        PER_N => PER_N,
        PER_P => PER_P,
        PET_N => PET_N,
        PET_P => PET_P,

        led_link_up => led_link_up,
        led_pcie_active => led_pcie_active,
        led_system_access => led_system_access,

        CLK_MUXAB_SEL => CLK_MUXAB_SEL,
        CLK_MUX1A_SEL => CLK_MUX1A_SEL,
        CLK_MUX1B_SEL => CLK_MUX1B_SEL,
        CLK_MUX2A_SEL => CLK_MUX2A_SEL,
        CLK_MUX2B_SEL => CLK_MUX2B_SEL,

        -- ADC Clock divider control signals
        CLK_DIV_SPI_CS_L => CLK_DIV_SPI_CS_L,
        CLK_DIV_SPI_SCLK => CLK_DIV_SPI_SCLK,
        CLK_DIV_SPI_DI => CLK_DIV_SPI_DI,
        CLK_DIV_SPI_DO => CLK_DIV_SPI_DO,
        CLK_DIV_STATUS => CLK_DIV_STATUS,
        ad9510_spi_function_out => ad9510_spi_function_out,

        -- ADC Control signals
        adc_spi_synch_out => adc_spi_synch_out,
        adc_spi_oe_l_out => adc_spi_oe_l_out,
        adc_spi_pwdn_out => adc_spi_pwdn_out,
        ADC_SPI_CS_L => ADC_SPI_CS_L,
        ADC_SPI_SCLK => ADC_SPI_SCLK,
        ADC_SPI_DIO => ADC_SPI_DIO,

        -- Clock multiplier SI5326 SPI control signals
        SI_INT_C1B => SI_INT_C1B,
        SI_LOL => SI_LOL,
        SI_RST_L => SI_RST_L,
        SI_SPI_SCLK => SI_SPI_SCLK,
        SI_SPI_SS_L => SI_SPI_SS_L,
        SI_SPI_DIN => SI_SPI_DIN,
        SI_SPI_DOUT => SI_SPI_DOUT,

        register_trn_clk => register_trn_clk,
        register_reset_out => register_reset,

        addr_0x000_reg_feedback => addr_0x000_reg_feedback,
        addr_0x001_reg_feedback => addr_0x001_reg_feedback,
        addr_0x003_reg_feedback => addr_0x003_reg_feedback,
        addr_0x003_reg_q => addr_0x003_reg_q,
        addr_0x004_status_feedback => addr_0x004_status_feedback,
        addr_0x004_reg_jk => addr_0x004_reg_jk,
        addr_0x005_reg_feedback => addr_0x005_reg_feedback,

        addr_0x10_reg_feedback => addr_0x10_reg_feedback,
        addr_0x10_reg_q => addr_0x10_reg_q,
        addr_0x11_reg_feedback => addr_0x11_reg_feedback,
        addr_0x11_reg_q => addr_0x11_reg_q,
        addr_0x12_reg_feedback => addr_0x12_reg_feedback,
        addr_0x12_reg_q => addr_0x12_reg_q,
        addr_0x13_reg_feedback => addr_0x13_reg_feedback,
        addr_0x13_reg_q => addr_0x13_reg_q,

        addr_0x14_reg_feedback => addr_0x14_reg_feedback,
        addr_0x14_reg_q => addr_0x14_reg_q,
        addr_0x15_reg_feedback => addr_0x15_reg_feedback,
        addr_0x15_reg_q => addr_0x15_reg_q,
        addr_0x16_reg_feedback => addr_0x16_reg_feedback,
        addr_0x16_reg_q => addr_0x16_reg_q,
        addr_0x17_reg_feedback => addr_0x17_reg_feedback,
        addr_0x17_reg_q => addr_0x17_reg_q,
        addr_0x47_reg_feedback => addr_0x47_reg_feedback,
        addr_0x47_reg_q => addr_0x47_reg_q,

        dac_ctrl_reg => dac_ctrl_reg,
        dac_data_reg => dac_data_reg,

        addr_0x100_reg_feedback => addr_0x100_reg_feedback,
        addr_0x100_reg_q => addr_0x100_reg_q,
        addr_0x101_reg_feedback => addr_0x101_reg_feedback,
        addr_0x101_reg_q => addr_0x101_reg_q,
        addr_0x102_reg_feedback => addr_0x102_reg_feedback,
        addr_0x102_reg_q => addr_0x102_reg_q,
        addr_0x103_reg_feedback => addr_0x103_reg_feedback,
        addr_0x103_reg_q => addr_0x103_reg_q,
        addr_0x104_reg_feedback => addr_0x104_reg_feedback,
        addr_0x104_reg_q => addr_0x104_reg_q,
        addr_0x105_reg_feedback => addr_0x105_reg_feedback,
        addr_0x105_reg_q => addr_0x105_reg_q,
        addr_0x106_reg_feedback => addr_0x106_reg_feedback,
        addr_0x106_reg_q => addr_0x106_reg_q,
        addr_0x107_reg_feedback => addr_0x107_reg_feedback,
        addr_0x107_reg_q => addr_0x107_reg_q,
        addr_0x108_reg_feedback => addr_0x108_reg_feedback,
        addr_0x108_reg_q => addr_0x108_reg_q,
        addr_0x109_reg_feedback => addr_0x109_reg_feedback,
        addr_0x109_reg_q => addr_0x109_reg_q,
        addr_0x110_reg_feedback => addr_0x110_reg_feedback,
        addr_0x110_reg_q => addr_0x110_reg_q,
        addr_0x111_reg_feedback => addr_0x111_reg_feedback,
        addr_0x111_reg_q => addr_0x111_reg_q,
        addr_0x112_reg_feedback => addr_0x112_reg_feedback,
        addr_0x112_reg_q => addr_0x112_reg_q,
        addr_0x113_reg_feedback => addr_0x113_reg_feedback,
        addr_0x113_reg_q => addr_0x113_reg_q,
        addr_0x114_reg_feedback => addr_0x114_reg_feedback,
        addr_0x114_reg_q => addr_0x114_reg_q,
        addr_0x115_reg_feedback => addr_0x115_reg_feedback,
        addr_0x115_reg_q => addr_0x115_reg_q,
        addr_0x116_reg_feedback => addr_0x116_reg_feedback,
        addr_0x116_reg_q => addr_0x116_reg_q,
        addr_0x117_reg_feedback => addr_0x117_reg_feedback,
        addr_0x117_reg_q => addr_0x117_reg_q,
        addr_0x118_reg_feedback => addr_0x118_reg_feedback,
        addr_0x118_reg_q => addr_0x118_reg_q,
        addr_0x119_reg_feedback => addr_0x119_reg_feedback,
        addr_0x119_reg_q => addr_0x119_reg_q,
        addr_0x120_reg_q => addr_0x120_reg_q,
        addr_0x120_reg_feedback => addr_0x120_reg_feedback,
        addr_0x121_reg_q => addr_0x121_reg_q,
        addr_0x121_reg_feedback => addr_0x121_reg_feedback,
        addr_0x122_reg_q => addr_0x122_reg_q,
        addr_0x122_reg_feedback => addr_0x122_reg_feedback,
        addr_0x123_reg_q => addr_0x123_reg_q,
        addr_0x123_reg_feedback => addr_0x123_reg_feedback,
        addr_0x124_reg_q => addr_0x124_reg_q,
        addr_0x124_reg_feedback => addr_0x124_reg_feedback,
        addr_0x125_reg_q => addr_0x125_reg_q,
        addr_0x125_reg_feedback => addr_0x125_reg_feedback,
        addr_0x126_reg_q => addr_0x126_reg_q,
        addr_0x126_reg_feedback => addr_0x126_reg_feedback,
        addr_0x127_reg_q => addr_0x127_reg_q,
        addr_0x127_reg_feedback => addr_0x127_reg_feedback,
        addr_0x128_reg_q => addr_0x128_reg_q,
        addr_0x128_reg_feedback => addr_0x128_reg_feedback,
        addr_0x129_reg_q => addr_0x129_reg_q,
        addr_0x129_reg_feedback => addr_0x129_reg_feedback,
        addr_0x12A_reg_q => addr_0x12A_reg_q,
        addr_0x12A_reg_feedback => addr_0x12A_reg_feedback,
        addr_0x12B_reg_q => addr_0x12B_reg_q,
        addr_0x12B_reg_feedback => addr_0x12B_reg_feedback,
        addr_0x12C_reg_q => addr_0x12C_reg_q,
        addr_0x12C_reg_feedback => addr_0x12C_reg_feedback,
        addr_0x12D_reg_q => addr_0x12D_reg_q,
        addr_0x12D_reg_feedback => addr_0x12D_reg_feedback,
        addr_0x12E_reg_q => addr_0x12E_reg_q,
        addr_0x12E_reg_feedback => addr_0x12E_reg_feedback,
        addr_0x12F_reg_q => addr_0x12F_reg_q,
        addr_0x12F_reg_feedback => addr_0x12F_reg_feedback,

        addr_0x230_ddr3_pcie_test_select => addr_0x230_ddr3_pcie_test_select,

        reg_0x400_0x4FF_adr => reg_0x400_0x4FF_adr,
        reg_0x400_0x4FF_wr_data => reg_0x400_0x4FF_wr_data,
        reg_0x400_0x4FF_rd_data => reg_0x400_0x4FF_rd_data,
        reg_0x400_0x4FF_wr_en => reg_0x400_0x4FF_wr_en,
        reg_0x400_0x4FF_rd_en => reg_0x400_0x4FF_rd_en,

        bram_dma_clk => bram_dma_clk,
        bram_dma_adr => bram_dma_adr,
        bram_dma_rd_en => bram_dma_rd_en,
        bram_dma_rd_data => bram_dma_rd_data,

        user_irq => s_itl, -- tied_to_gnd,
        user_irq_clear => open,

        dma_request_stall => '0',

        sis_read_data_fifo_clr => ddr3_read_data_fifo_clr,
        sis_read_data_fifo_rd_clk => ddr3_read_data_fifo_rd_clk,
        sis_read_data_fifo_rd_en => ddr3_read_data_fifo_rd_en,
        sis_read_data_fifo_dout => ddr3_read_data_fifo_dout,
        sis_read_data_fifo_rd_count => ddr3_read_data_fifo_rd_count,
        sis_read_data_fifo_empty => ddr3_read_data_fifo_empty,

        sis_read_addr_fifo_clr => ddr3_read_addr_fifo_clr,
        sis_read_addr_fifo_wr_clk => ddr3_read_addr_fifo_wr_clk,
        sis_read_addr_fifo_wr_en => ddr3_read_addr_fifo_wr_en,
        sis_read_addr_fifo_din => ddr3_read_addr_fifo_din,
        sis_read_addr_fifo_wr_count => ddr3_read_addr_fifo_wr_count,

        sis_write_fifo_wr_clk => ddr3_write_fifo_wr_clk,
        sis_write_data_fifo_wr_en => pci_ddr3_write_data_fifo_wr_en,
        sis_write_data_fifo_din => pci_ddr3_write_data_fifo_din,
        sis_write_data_fifo_wr_count => ddr3_write_data_fifo_wr_count,

        sis_write_addr_fifo_wr_en => pci_ddr3_write_addr_fifo_wr_en,
        sis_write_addr_fifo_din => pci_ddr3_write_addr_fifo_din,
        sis_write_addr_fifo_wr_count => ddr3_write_addr_fifo_wr_count,

        adc_tap_delay_reg_q => adc_tap_delay_reg_q,
        adc_tap_delay_feedback => adc_tap_delay_feedback,

        MGT_CK_INT => MGT_CK_INT,
        MGT_CK_SDA => MGT_CK_SDA,
        MGT_CK_SCL => MGT_CK_SCL,

        flash_mosi => pci_flash_mosi,
        flash_miso => pci_flash_miso,
        flash_cs_l => pci_flash_cs_l,
        flash_clk => pci_flash_clk,
        flash_mux_en => pci_flash_mux_en
    );

    addr_0x003_reg_feedback <= addr_0x003_reg_q(31 downto 0); -- reserved

    -- empty register lines
    -- addr_0x110_reg_feedback <= addr_0x110_reg_q;
    -- addr_0x111_reg_feedback <= addr_0x111_reg_q;
    -- addr_0x112_reg_feedback <= addr_0x112_reg_q;
    -- addr_0x113_reg_feedback <= addr_0x113_reg_q;
    -- addr_0x114_reg_feedback <= addr_0x114_reg_q;
    -- addr_0x115_reg_feedback <= addr_0x115_reg_q;
    -- addr_0x116_reg_feedback <= addr_0x116_reg_q;
    -- addr_0x117_reg_feedback <= addr_0x117_reg_q;
    -- addr_0x118_reg_feedback <= addr_0x118_reg_q;
    -- addr_0x119_reg_feedback <= addr_0x119_reg_q;

    -- addr_0x120_reg_feedback <= addr_0x120_reg_q;
    -- addr_0x121_reg_feedback <= addr_0x121_reg_q;
    -- addr_0x122_reg_feedback <= addr_0x122_reg_q;
    -- addr_0x123_reg_feedback <= addr_0x123_reg_q;
    -- addr_0x124_reg_feedback <= addr_0x124_reg_q;
    -- addr_0x125_reg_feedback <= addr_0x125_reg_q;
    -- addr_0x126_reg_feedback <= addr_0x126_reg_q;
    -- addr_0x127_reg_feedback <= addr_0x127_reg_q;
    -- addr_0x128_reg_feedback <= addr_0x128_reg_q;
    -- addr_0x129_reg_feedback <= addr_0x129_reg_q;
    -- addr_0x12A_reg_feedback <= addr_0x12A_reg_q;
    -- addr_0x12B_reg_feedback <= addr_0x12B_reg_q;
    addr_0x12C_reg_feedback <= addr_0x12C_reg_q(31 downto 0);
    addr_0x12D_reg_feedback <= addr_0x12D_reg_q(31 downto 0);
    addr_0x12E_reg_feedback <= addr_0x12E_reg_q(31 downto 0);
--    addr_0x12F_reg_feedback <= addr_0x12F_reg_q(31 downto 0);


--**************************************************************************************************
--**************************************************************************************************

-- Sysclock 125MHz oscillator

--**************************************************************************************************
--**************************************************************************************************
    UIBUFG1 : IBUFDS
    port map (
        I => SYS_CLK_P,
        IB => SYS_CLK_N,
        O => sys_clk125_in
    );

    sys125_bufg : BUFG
    port map (I => sys_clk125_in, O => sys_clk125_bufg);

    clock_reset_delay_logic: process (sys_clk125_bufg)
    begin
        if rising_edge(sys_clk125_bufg) then
            clock_reset_count(0)  <=    '1' ;
            clock_reset_count(1)  <=    clock_reset_count(0) ;
            clock_reset_count(2)  <=    clock_reset_count(1) ;
            clock_reset_count(3)  <=    clock_reset_count(2) ;
            clock_reset           <=  not clock_reset_count(3) ;
        end if;
    end process;

    sys_clk125 <= sys_clk125_bufg;

    sys_clock_ok <= not clock_reset;
    sys_clk_dll_reset <= clock_reset;

--**************************************************************************************************
--**************************************************************************************************

-- Watchdog + LEDs

--**************************************************************************************************
--**************************************************************************************************

    watchdog_counter: process (sys_clk125, sys_clk_dll_reset, wd_count)
    begin
        if sys_clk_dll_reset = '1' then
            wd_count <= X"0000000";
         elsif rising_edge(sys_clk125) then  --
                        wd_count <= wd_count + 1;
        end if;

        FPGA_WATCHDOG        <=  wd_count(16); -- 1.048ms
        timer_524us_signal   <=  wd_count(15); -- 0.524ms
        timer_2096us_signal  <=  wd_count(17); -- 2.096ms
        timer_134ms_signal   <=  wd_count(23); -- 134ms

    end process;

    Inst_led_serial_fsm: led_serial_fsm
    port map(
        sys_clk125 => sys_clk125,
        reset => sys_clk_dll_reset,
        timer_524us_signal => timer_524us_signal,
        timer_2096us_signal => timer_2096us_signal,
        led_inputs => led_inputs,
        led_serial_out => led_serial_out
    );

    LED_SERIAL_DATA <= led_serial_out;

    user_led                 <= s_itl_hb;--! addr_0x004_reg_jk(0);

    led_test_logic: process (addr_0x004_reg_jk)
    begin
        if (addr_0x004_reg_jk(1) = '0') then --led test
        led_inputs(0)  <=   opt1_2_gtx_linkup(0);  -- board left side
        led_inputs(1)  <=   opt1_2_gtx_linkup(1);

        led_inputs(2)  <=   port12_15_gtx_linkup(0);
        led_inputs(3)  <=   port12_15_gtx_linkup(1);

        led_inputs(4)  <=   port12_15_gtx_linkup(2);
        led_inputs(5)  <=   port12_15_gtx_linkup(3);
    else
        led_inputs(0)  <=   wd_count(27);  -- board left side
        led_inputs(1)  <=   wd_count(26);

        led_inputs(2)  <=   wd_count(25);
        led_inputs(3)  <=   wd_count(24);

        led_inputs(4)  <=   wd_count(23);
        led_inputs(5)  <=   wd_count(22);
    end if;
    end process;

    led_inputs(6)  <=   wd_count(21);
    led_inputs(7)  <=   wd_count(20); -- board right side (front)

    led_inputs(8)  <=   led_pcie_active; -- (L1)  Access
    led_inputs(9)  <=   user_led; -- (R1)  User

    led_inputs(10)  <=   pcie_trn_lnk_up; -- (L2) L1
    led_inputs(11)  <=   adc_chx_sample_logic_active_or; -- (R2) L2

    led_inputs(12)  <=   wd_count(21);
    led_inputs(13)  <= not wd_count(21);

    led_inputs(14)  <=   wd_count(20);
    led_inputs(15)  <= not wd_count(20);

--**************************************************************************************************
--**************************************************************************************************

-- Board Serialnumber from MMC

--**************************************************************************************************
--**************************************************************************************************

    Inst_serial_no_fsm: serial_no_fsm
    port map(
        sys_clk125 => sys_clk125,
        reset => sys_clk_dll_reset,
        MMU_FPGA_CLK => MMU_FPGA_RXD,
        MMU_FPGA_D => MMU_FPGA_TXD,
        serial_number => module_serial_no_register
    );

    addr_0x001_reg_feedback <= module_serial_no_register;

--**************************************************************************************************
--**************************************************************************************************
--**************************************************************************************************

--   AMC LVDS outputs/inputs
--**************************************************************************************************
--**************************************************************************************************

    P1720_DE_H(7 downto 0)    <= addr_0x12_reg_q(31 downto 24) ;
    MLVDS_DOUT(7 downto 0)    <= addr_0x12_reg_q(23 downto 16) ;


    addr_0x12_reg_feedback(31 downto 16)  <= addr_0x12_reg_q(31 downto 16) ;
    addr_0x12_reg_feedback(15 downto 8)   <= addr_0x12_reg_q(15 downto 8) ;
    addr_0x12_reg_feedback(7 downto 0)    <= MLVDS_RIN(7 downto 0) ;

-- use for trigger (synch with )
    Inst_IREG_MLVDS_in_reg: for i in 0 to 7 generate
        FD_MLVDS_IN_REG_D: FD
        port map(
            C => sys_ad9510_no1_bufg_clk,
            D => MLVDS_RIN(i),
            Q => mlvds_in_reg(i)
        );
    end generate;

--**************************************************************************************************
--**************************************************************************************************

--   Harlink Data outputs/inputs


--**************************************************************************************************
--**************************************************************************************************
--**************************************************************************************************

    Inst_OBUFDS_harlink_out: for i in 1 to 4 generate
    obufds_harlink_i : OBUFDS
      port map (
        I  => harlink_out(i),
        O  => EXT_DO_P(i),
        OB => EXT_DO_N(i)
      );
    end generate;


    addr_0x13_reg_feedback(31 downto 24) <=  X"00" ;
    addr_0x13_reg_feedback(23 downto 21) <=  "000" ;
    addr_0x13_reg_feedback(20 downto 16) <=  addr_0x13_reg_q(20 downto 16) ;
    addr_0x13_reg_feedback(15 downto 12) <=  X"0" ;
    addr_0x13_reg_feedback(11 downto 8)  <=  addr_0x13_reg_q(11 downto 8) ;
    addr_0x13_reg_feedback(7 downto 4)   <=  X"0" ;
    addr_0x13_reg_feedback(3 downto 0)   <=  harlink_in_buf(4 downto 1) ;


    harlink_test_output_logic: process (sys_clk50)
    begin
        if (addr_0x13_reg_q(20) = '1') then -- test Output Enable
             harlink_out(4 downto 1)              <=  addr_0x13_reg_q(19 downto 16) ;
         else
             harlink_out(1) <=  adc_chx_trigger_or_pulse ;
             harlink_out(2) <=  adc_ch5_start_pulse;
             harlink_out(3) <=  s_itl_hb;
             harlink_out(4) <=  s_itl ; -- ulegat MOD18112013
        end if ;
    end process;


    Inst_IBUFDS_harlink_in_buf: for i in 1 to 4 generate
        ibufds_harlink_i: IBUFDS
        port map(
            O => harlink_in_buf(i),
            I => EXT_TRG_P(i),
            IB => EXT_TRG_N(i)
        );
    end generate;

-- use for trigger (synch with )
    Inst_IREG_harlink_in_reg: for i in 1 to 4 generate
        FD_HARLINK_IN_REG_D: FD
        port map(
            C => sys_ad9510_no1_bufg_clk,
            D => harlink_in_buf(i),
            Q => harlink_in_reg(i)
        );
    end generate;



--**************************************************************************************************
--**************************************************************************************************
-- External Trigger

    external_trigger_enable_logic: process (sys_ad9510_no1_bufg_clk)
    begin

        if rising_edge(sys_ad9510_no1_bufg_clk) then  --
            ext_trigger_harlink_delayA              <=  harlink_in_reg ;
            ext_trigger_harlink_delayB              <=  ext_trigger_harlink_delayA ;
            ext_trigger_harlink_rising_pulse        <=  ext_trigger_harlink_delayA and not ext_trigger_harlink_delayB;
            ext_trigger_harlink_falling_pulse       <=  ext_trigger_harlink_delayB and not ext_trigger_harlink_delayA;
        end if;

        if rising_edge(sys_ad9510_no1_bufg_clk) then  --
            ext_trigger_mlvds_delayA                <=  mlvds_in_reg ;
            ext_trigger_mlvds_delayB                <=  ext_trigger_mlvds_delayA ;
            ext_trigger_mlvds_rising_pulse          <=  ext_trigger_mlvds_delayA and not ext_trigger_mlvds_delayB;
            ext_trigger_mlvds_falling_pulse         <=  ext_trigger_mlvds_delayB and not ext_trigger_mlvds_delayA;
        end if;

        if rising_edge(sys_ad9510_no1_bufg_clk) then  --
            external_ored_trigger_signal <= (ext_trigger_harlink_rising_pulse(1)  and addr_0x13_reg_q(8)  and not addr_0x13_reg_q(12))
                                              or (ext_trigger_harlink_falling_pulse(1) and addr_0x13_reg_q(8)  and     addr_0x13_reg_q(12))
                                              or (ext_trigger_harlink_rising_pulse(2)  and addr_0x13_reg_q(9)  and not addr_0x13_reg_q(13))
                                              or (ext_trigger_harlink_falling_pulse(2) and addr_0x13_reg_q(9)  and     addr_0x13_reg_q(13))
                                              or (ext_trigger_harlink_rising_pulse(3)  and addr_0x13_reg_q(10) and not addr_0x13_reg_q(14))
                                              or (ext_trigger_harlink_falling_pulse(3) and addr_0x13_reg_q(10) and     addr_0x13_reg_q(14))
                                              -- or (ext_trigger_harlink_rising_pulse(4)  and addr_0x13_reg_q(11) and not addr_0x13_reg_q(15))
                                              -- or (ext_trigger_harlink_falling_pulse(4) and addr_0x13_reg_q(11) and     addr_0x13_reg_q(15))
                                              --
                                              or (ext_trigger_mlvds_rising_pulse(0)  and addr_0x12_reg_q(8) and not addr_0x12_reg_q(0))
                                              or (ext_trigger_mlvds_falling_pulse(0) and addr_0x12_reg_q(8) and     addr_0x12_reg_q(0))
                                              or (ext_trigger_mlvds_rising_pulse(1)  and addr_0x12_reg_q(9) and not addr_0x12_reg_q(1))
                                              or (ext_trigger_mlvds_falling_pulse(1) and addr_0x12_reg_q(9) and     addr_0x12_reg_q(1))
                                              or (ext_trigger_mlvds_rising_pulse(2)  and addr_0x12_reg_q(10) and not addr_0x12_reg_q(2))
                                              or (ext_trigger_mlvds_falling_pulse(2) and addr_0x12_reg_q(10) and     addr_0x12_reg_q(2))
                                              or (ext_trigger_mlvds_rising_pulse(3)  and addr_0x12_reg_q(11) and not addr_0x12_reg_q(3))
                                              or (ext_trigger_mlvds_falling_pulse(3) and addr_0x12_reg_q(11) and     addr_0x12_reg_q(3))
                                              --
                                              or (ext_trigger_mlvds_rising_pulse(4)  and addr_0x12_reg_q(12) and not addr_0x12_reg_q(4))
                                              or (ext_trigger_mlvds_falling_pulse(4) and addr_0x12_reg_q(12) and     addr_0x12_reg_q(4))
                                              or (ext_trigger_mlvds_rising_pulse(5)  and addr_0x12_reg_q(13) and not addr_0x12_reg_q(5))
                                              or (ext_trigger_mlvds_falling_pulse(5) and addr_0x12_reg_q(13) and     addr_0x12_reg_q(5))
                                              or (ext_trigger_mlvds_rising_pulse(6)  and addr_0x12_reg_q(14) and not addr_0x12_reg_q(6))
                                              or (ext_trigger_mlvds_falling_pulse(6) and addr_0x12_reg_q(14) and     addr_0x12_reg_q(6));
                                              -- or (ext_trigger_mlvds_rising_pulse(7)  and addr_0x12_reg_q(15) and not addr_0x12_reg_q(7))       -- mod. 22.08.2014
                                              -- or (ext_trigger_mlvds_falling_pulse(7) and addr_0x12_reg_q(15) and     addr_0x12_reg_q(7)) ;     -- mod. 22.08.2014
        end if;

        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        -- ulegat MOD26112013 removed enable TODO sw supports it add it back as above
        if rising_edge(sys_ad9510_no1_bufg_clk) then  --
            s_end_pulse_ored_signal <= (ext_trigger_harlink_rising_pulse(4)  and  not addr_0x13_reg_q(15))
                or (ext_trigger_harlink_falling_pulse(4) and      addr_0x13_reg_q(15))
                or (ext_trigger_mlvds_rising_pulse(7)    and  not addr_0x12_reg_q(7)  )
                or (ext_trigger_mlvds_falling_pulse(7)   and      addr_0x12_reg_q(7)  );
        end if;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------

    end process;


--**************************************************************************************************



-- RTM I2C signals

   gen_option_rtm_zone3_i2c_block1_off: if (RTM_ZONE3_I2C_EN = 0) generate
    -- RTM MLVDS signals

--        Inst_IBUFDS_RTM_LVDS_in_buf: for i in 0 to 2 generate
--         ibufds_rtm_lvds_i: IBUFDS
--          port map(
--              O => rtm_lvds_in_buf(i),
--              I => RTM_D_P(3+i),
--              IB => RTM_D_N(3+i)
--          );
--        end generate;
--
--
--        Inst_IBUFDS_RTM_LVDS_out_buft: for i in 0 to 2 generate
--         obufds_rtm_lvds_i : OBUFTDS
--          port map (
--            O => RTM_D_P(6+i),     -- Diff_p output (connect directly to top-level port)
--            OB => RTM_D_N(6+i),   -- Diff_n output (connect directly to top-level port)
--            I => rtm_lvds_out(i),     -- Buffer input
--            T => rtm_lvds_out_t(i)      -- 3-state enable input
--          );
--        end generate;


        rtm_lvds_out_t(2 downto 0)           <=  not addr_0x12F_reg_q(26 downto 24) ;
        rtm_lvds_out(2 downto 0)             <=      addr_0x12F_reg_q(18 downto 16) ;



        addr_0x12F_reg_feedback(31 downto 27) <=  "00000" ;
        addr_0x12F_reg_feedback(26 downto 24) <=  addr_0x12F_reg_q(26 downto 24) ;
        addr_0x12F_reg_feedback(23 downto 19) <=  "00000" ;
        addr_0x12F_reg_feedback(18 downto 16) <=  addr_0x12F_reg_q(18 downto 16) ;


        addr_0x12F_reg_feedback(7 downto 6)   <=  "00" ;
        addr_0x12F_reg_feedback(5 downto 3)   <=  rtm_lvds_in_buf(2 downto 0) ;
        addr_0x12F_reg_feedback(2 downto 0)   <=  "000" ;
    end generate;




    addr_0x12F_reg_feedback(15 downto 12)  <=  X"0" ;
    addr_0x12F_reg_feedback(11 downto 8)   <=  addr_0x12F_reg_q(11 downto 8) ;

    rtm_out_oc_logic: process (addr_0x12F_reg_q)
    begin
        if (addr_0x12F_reg_q(11)='0') then
            Z3_TCLK_EN_L <= 'Z' ;
        else
            Z3_TCLK_EN_L <= '0' ;
        end if;
        if (addr_0x12F_reg_q(10)='0') then
            Z3_ILOCK_EN_L <= 'Z' ;
        else
            Z3_ILOCK_EN_L <= '0' ;
        end if;
        Z3_ILOCK1 <= addr_0x12F_reg_q(9) ;
        Z3_ILOCK0 <= addr_0x12F_reg_q(8) ;
    end process;




   gen_option_rtm_zone3_i2c_block1_on: if (RTM_ZONE3_I2C_EN = 1) generate

        Inst_RTM_i2c_master_interface: entity work.i2c_master_interface
        port map(
            clk => register_trn_clk,
            rst => register_reset,
            i2c_reg_q => addr_0x47_reg_q,
            i2c_reg_feedback => addr_0x47_reg_feedback,
            I2C_SDA => RTM_I2C_SDA,
            I2C_SCL => RTM_I2C_SCL
        );

        addr_0x12F_reg_feedback(31 downto 16) <=  X"0000" ;
        addr_0x12F_reg_feedback(7 downto 0)   <=  X"00" ;

   end generate;


--**************************************************************************************************
--**************************************************************************************************

    -- ADC SPI Synch

--**************************************************************************************************
--**************************************************************************************************

    ADC_SYNC <= adc_spi_function_synch_pulse ;
    ADC_OEB_L <= adc_spi_oe_l_out ;
    ADC_PDWN <= adc_spi_pwdn_out;

    adc_synch_logic: process (sys_ad9510_no1_bufg_clk)
    begin
        if rising_edge(sys_ad9510_no1_bufg_clk) then  --
             adc_spi_function_delay1_out  <=  adc_spi_synch_out  ;
             adc_spi_function_delay2_out  <=  adc_spi_function_delay1_out ;
             adc_spi_function_synch_pulse <=  adc_spi_function_delay1_out and not adc_spi_function_delay2_out  ;
        end if;
    end process;

--**************************************************************************************************
--**************************************************************************************************

-- AD9510 Clock/Synch

--**************************************************************************************************
--**************************************************************************************************
    UIBUFDS_ad9510_no1_clkin : IBUFDS
    port map (
        I => DIV0_CLK05_P,
        IB => DIV0_CLK05_N,
        O => ad9510_no1_clkin
    );

    bufg_ad9510_no1_clk : BUFG
    port map (
        I => ad9510_no1_clkin ,
        O => sys_ad9510_no1_bufg_clk
    );

    -- UIBUFDS_ad9510_no2_clkin : IBUFDS
    -- port map (
    --     I => DIV1_CLK69_P,
    --     IB => DIV1_CLK69_N,
    --     O => ad9510_no2_clkin
    -- );
    --
    -- bufg_ad9510_no2_clk : BUFG
    -- port map (
    --     I => ad9510_no2_clkin,
    --     O => sys_ad9510_no2_bufg_clk
    -- );

   BUFGCTRL_inst : BUFGCTRL
   generic map (
      INIT_OUT => 0,         -- Inital value of 0 or 1 after configuration
      PRESELECT_I0 => FALSE, -- TRUE/FALSE set the I0 input after configuration
      PRESELECT_I1 => FALSE) -- TRUE/FALSE set the I1 input after configuration
   port map (
      O => sys_ad9510_no2_bufg_clk,              -- Clock MUX output
      CE0 => not ad9510_spi_function_out(0),          -- Clock enable0 input
      CE1 => ad9510_spi_function_out(0),          -- Clock enable1 input
      I0 => ddr3_write_fifo_wr_clk,            -- Clock0 input
      I1 => ad9510_no1_clkin,       --ad9510_no2_clkin,            -- Clock1 input
      IGNORE0 => '0',  -- Ignore clock select0 input
      IGNORE1 => '0',  -- Ignore clock select1 input
      S0 => not ad9510_spi_function_out(0),            -- Clock select0 input
      S1 => ad9510_spi_function_out(0)             -- Clock select1 input
   );

    ad9510_synch_logic_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk                 => sys_ad9510_no2_bufg_clk,
        rising_edge_signal_in   => ad9510_spi_function_out(1),
        signal_pulse_out        => synch_ad9510_spi_function_out
    );

    ad9510_no1_synch_logic: process (sys_ad9510_no2_bufg_clk)
    begin
        if rising_edge(sys_ad9510_no2_bufg_clk) then  --
             synch_ad9510_spi_function_out_delay      <= synch_ad9510_spi_function_out ;
             synch_ad9510_spi_function_synch_pulse    <= (synch_ad9510_spi_function_out or
                                                          synch_ad9510_spi_function_out_delay) or
                                                         ad9510_spi_function_out(2);
        end if;
    end process;

    FD_CLK_DIV_FUNCTION_Q: FD
    port map(
        C => sys_ad9510_no2_bufg_clk,
        D => not synch_ad9510_spi_function_synch_pulse,
        Q => CLK_DIV_FUNCTION
    );

    --**************************************************************************************************
    -- SPI Bootflash MMC/FPGA Access switcher
    --**************************************************************************************************
    Inst_mmc_flash_bridge: mmc_flash_bridge
    port map(
        sys_clk125 => sys_clk125,
        spi_flash_din => FLASH_DIN,
        spi_flash_cs_n => FLASH_CS_L,

        mmc_spi_miso => MMC_SPI_MISO,
        mmc_spi_mosi => MMC_SPI_MOSI,
        mmc_spi_sck => MMC_SPI_SCK,
        mmc_spi_ss_l => MMC_SPI_SS_L,

        fpga_spi_miso => pci_flash_miso,
        fpga_spi_mosi => pci_flash_mosi,
        fpga_spi_sck => pci_flash_clk,
        fpga_spi_ss_l => pci_flash_cs_l,

        mmc_fpga_flash_sel => pci_flash_mux_en
    );

    --**************************************************************************************************
    -- DDR3 Memory Controller
    --**************************************************************************************************
    -- PCI write access mux
    Inst_pcie_to_ddr3_write_clockdomain_bridge: entity work.pcie_to_ddr3_write_clockdomain_bridge
    port map(
        pcie_rst                            => sys_clk_dll_reset, -- erstmal,
        pcie_write_clk                      => ddr3_write_fifo_wr_clk,
        pci_write_data_fifo_wr_en           => pci_ddr3_write_data_fifo_wr_en,
        pci_write_data_fifo_din             => pci_ddr3_write_data_fifo_din,
        pci_write_addr_fifo_wr_en           => pci_ddr3_write_addr_fifo_wr_en,
        pci_write_addr_fifo_din             => pci_ddr3_write_addr_fifo_din,

        ddr3_rst                            => ddr3_controller_reset,
        ddr3_write_clk                      => ddr3_usr_clk,
        ddr3_test_write_addr_fifo_wr_en     => pcie_ddr3_test_write_addr_fifo_wr_en,
        ddr3_test_write_addr_fifo_din       => pcie_ddr3_test_write_addr_fifo_din,
        ddr3_test_write_data_fifo_wr_en     => pcie_ddr3_test_write_data_fifo_wr_en,
        ddr3_test_write_data_fifo_din       => pcie_ddr3_test_write_data_fifo_din
    );

    ddr3_data_data_write_mux_logic: process (ddr3_usr_clk)
    begin
        if rising_edge(ddr3_usr_clk) then  --
            pcie_test_ddr3_write_mode <= addr_0x230_ddr3_pcie_test_select(0) ;
        end if;

        if rising_edge(ddr3_usr_clk) then  --
            if (pcie_test_ddr3_write_mode = '1') then
                sis_write_addr_fifo_wr_en   <=   pcie_ddr3_test_write_addr_fifo_wr_en ;
                sis_write_addr_fifo_din     <=   pcie_ddr3_test_write_addr_fifo_din ;
                sis_write_data_fifo_wr_en   <=   pcie_ddr3_test_write_data_fifo_wr_en ;
                sis_write_data_fifo_din     <=   pcie_ddr3_test_write_data_fifo_din ;
            else
                sis_write_addr_fifo_wr_en   <=   adc_ddr3_write_addr_fifo_wr_en ;
                sis_write_addr_fifo_din     <=   adc_ddr3_write_addr_fifo_din ;
                sis_write_data_fifo_wr_en   <=   adc_ddr3_write_data_fifo_wr_en ;
                sis_write_data_fifo_din     <=   adc_ddr3_write_data_fifo_din ;
            end if;
        end if;
    end process;

    Inst_ddr3_interface: entity work.ddr3_interface
    port map(
        sys_clk125 => sys_clk125,
        sys_clk_ok => sys_clock_ok,

        memory_controller_reset => ddr3_controller_reset,
        ddr3_usr_clk => ddr3_usr_clk,
        ddr3_init_done => ddr3_phy_init_done,

        read_data_fifo_clr => ddr3_read_data_fifo_clr,
        read_data_fifo_rd_clk => ddr3_read_data_fifo_rd_clk,
        read_data_fifo_rd_en => ddr3_read_data_fifo_rd_en,
        read_data_fifo_dout => ddr3_read_data_fifo_dout,
        read_data_fifo_rd_count => ddr3_read_data_fifo_rd_count,
        read_data_fifo_empty => ddr3_read_data_fifo_empty,

        read_addr_fifo_clr => ddr3_read_addr_fifo_clr,
        read_addr_fifo_wr_clk => ddr3_read_addr_fifo_wr_clk,
        read_addr_fifo_wr_en => ddr3_read_addr_fifo_wr_en,
        read_addr_fifo_din => ddr3_read_addr_fifo_din,
        read_addr_fifo_wr_count => ddr3_read_addr_fifo_wr_count,

        write_fifo_wr_clk => ddr3_usr_clk,
        write_fifo_wr_clr => ddr3_write_fifo_wr_clr,

        write_data_fifo_wr_en => sis_write_data_fifo_wr_en,
        write_data_fifo_din => sis_write_data_fifo_din,
        write_data_fifo_wr_count => ddr3_write_data_fifo_wr_count,

        write_addr_fifo_wr_en => sis_write_addr_fifo_wr_en,
        write_addr_fifo_din => sis_write_addr_fifo_din,
        write_addr_fifo_wr_count => ddr3_write_addr_fifo_wr_count,

        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_dm => ddr3_dm,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_cs_n => open, -- hardwired to gnd on pcb, workaround in phy_control_io.vhd, cs helper signals worked into ras, cas, we
        ddr3_odt => ddr3_odt,
        ddr3_cke => ddr3_cke,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n
    );

--    addr_0x103_reg_feedback <= addr_0x103_reg_q;
--    addr_0x104_reg_feedback <= addr_0x104_reg_q;
--    addr_0x105_reg_feedback <= addr_0x105_reg_q;
--    addr_0x106_reg_feedback <= addr_0x106_reg_q;
--    addr_0x107_reg_feedback <= addr_0x107_reg_q;
--    addr_0x108_reg_feedback <= addr_0x108_reg_q;
--    addr_0x109_reg_feedback <= addr_0x109_reg_q;
--
--    addr_0x100_reg_feedback(31 downto 1) <= addr_0x100_reg_q(31 downto 1);
--    addr_0x100_reg_feedback(0) <= ddr3_phy_init_done;


    --**************************************************************************************************
    -- ADC Sample Clock inputs
    --**************************************************************************************************
    Inst_adc1_clock_buffer: entity work.adc_clock_buffer
    port map(
        adc_clk_p => ADC1_DCO_P,
        adc_clk_n => ADC1_DCO_N,
        adc_clk_bufr_ce => '1',
        adc_clk_bufr_clr => '0',
        adc_io_clk => open,
        adc_logic_clk => adc1_clk
    );

    Inst_adc2_clock_buffer: entity work.adc_clock_buffer
    port map(
        adc_clk_p => ADC2_DCO_P,
        adc_clk_n => ADC2_DCO_N,
        adc_clk_bufr_ce => '1',
        adc_clk_bufr_clr => '0',
        adc_io_clk => open,
        adc_logic_clk => adc2_clk
    );

    Inst_adc3_clock_buffer: entity work.adc_clock_buffer
    port map(
        adc_clk_p => ADC3_DCO_P,
        adc_clk_n => ADC3_DCO_N,
        adc_clk_bufr_ce => '1',
        adc_clk_bufr_clr => '0',
        adc_io_clk => open,
        adc_logic_clk => adc3_clk
    );

    Inst_adc4_clock_buffer: entity work.adc_clock_buffer
    port map(
        adc_clk_p => ADC4_DCO_P,
        adc_clk_n => ADC4_DCO_N,
        adc_clk_bufr_ce => '1',
        adc_clk_bufr_clr => '0',
        adc_io_clk => open,
        adc_logic_clk => adc4_clk
    );

    Inst_adc5_clock_buffer: entity work.adc_clock_buffer
    port map(
        adc_clk_p => ADC5_DCO_P,
        adc_clk_n => ADC5_DCO_N,
        adc_clk_bufr_ce => '1',
        adc_clk_bufr_clr => '0',
        adc_io_clk => open,
        adc_logic_clk => adc5_clk
    );

    --**************************************************************************************************
    -- ADC Data inputs
    --**************************************************************************************************
    -- all the LVDS ADCs input are inverted so they match with the SIS8900 board.
    adc1_InputBuffer: for I in 0 to 16 generate
        UIBUFDSADC1: IBUFDS
        port map (
            I => ADC1_D_N(I),
            IB => ADC1_D_P(I),
            O => adc1_buf_din(I)
        );

        UIBUFDSADC2: IBUFDS
        port map (
            I => ADC2_D_N(I),
            IB => ADC2_D_P(I),
            O => adc2_buf_din(I)
        );

        UIBUFDSADC3: IBUFDS
        port map (
            I => ADC3_D_N(I),
            IB => ADC3_D_P(I), O => adc3_buf_din(I)
        );

        UIBUFDSADC4: IBUFDS
        port map (
            I => ADC4_D_N(I),
            IB => ADC4_D_P(I),
            O => adc4_buf_din(I)
        );

        UIBUFDSADC5: IBUFDS
        port map (
            I => ADC5_D_N(I),
            IB => ADC5_D_P(I),
            O => adc5_buf_din(I)
        );

   end generate;

    Inst_adc_input_block: adc_input_block
    port map(
        tap_reg_clk => register_trn_clk,
        adc_tap_delay_reg_q => adc_tap_delay_reg_q,
        adc_tap_delay_feedback => adc_tap_delay_feedback,
        adc1_clk => adc1_clk,
        adc2_clk => adc2_clk,
        adc3_clk => adc3_clk,
        adc4_clk => adc4_clk,
        adc5_clk => adc5_clk,
        adc1_buf_din => adc1_buf_din,
        adc2_buf_din => adc2_buf_din,
        adc3_buf_din => adc3_buf_din,
        adc4_buf_din => adc4_buf_din,
        adc5_buf_din => adc5_buf_din,
        adc_ch1_pipe_din => adc_ch1_pipe_din,
        adc_ch2_pipe_din => adc_ch2_pipe_din,
        adc_ch3_pipe_din => adc_ch3_pipe_din,
        adc_ch4_pipe_din => adc_ch4_pipe_din,
        adc_ch5_pipe_din => adc_ch5_pipe_din,
        adc_ch6_pipe_din => adc_ch6_pipe_din,
        adc_ch7_pipe_din => adc_ch7_pipe_din,
        adc_ch8_pipe_din => adc_ch8_pipe_din,
        adc_ch9_pipe_din => adc_ch9_pipe_din,
        adc_ch10_pipe_din => adc_ch10_pipe_din
    );

--******************************************************************
-- ADC  Trigger Block
--******************************************************************

    adc_ch1_trigger_setup_reg       <=  addr_0x100_reg_q(31 downto 0);
    adc_ch1_trigger_threshold_reg   <=  addr_0x110_reg_q(31 downto 0);

    adc_ch2_trigger_setup_reg       <=  addr_0x101_reg_q(31 downto 0);
    adc_ch2_trigger_threshold_reg   <=  addr_0x111_reg_q(31 downto 0);

    adc_ch3_trigger_setup_reg       <=  addr_0x102_reg_q(31 downto 0);
    adc_ch3_trigger_threshold_reg   <=  addr_0x112_reg_q(31 downto 0);

    adc_ch4_trigger_setup_reg       <=  addr_0x103_reg_q(31 downto 0);
    adc_ch4_trigger_threshold_reg   <=  addr_0x113_reg_q(31 downto 0);

    adc_ch5_trigger_setup_reg       <=  addr_0x104_reg_q(31 downto 0);
    adc_ch5_trigger_threshold_reg   <=  addr_0x114_reg_q(31 downto 0);

    adc_ch6_trigger_setup_reg       <=  addr_0x105_reg_q(31 downto 0);
    adc_ch6_trigger_threshold_reg   <=  addr_0x115_reg_q(31 downto 0);

    adc_ch7_trigger_setup_reg       <=  addr_0x106_reg_q(31 downto 0);
    adc_ch7_trigger_threshold_reg   <=  addr_0x116_reg_q(31 downto 0);

    adc_ch8_trigger_setup_reg       <=  addr_0x107_reg_q(31 downto 0);
    adc_ch8_trigger_threshold_reg   <=  addr_0x117_reg_q(31 downto 0);

    adc_ch9_trigger_setup_reg       <=  addr_0x108_reg_q(31 downto 0);
    adc_ch9_trigger_threshold_reg   <=  addr_0x118_reg_q(31 downto 0);

    adc_ch10_trigger_setup_reg      <=  addr_0x109_reg_q(31 downto 0);
    adc_ch10_trigger_threshold_reg  <=  addr_0x119_reg_q(31 downto 0);


    -- Trigger Block off
    gen_trigger_block_off: if (TRIGGER_BLOCK_EN = 0) generate

        addr_0x100_reg_feedback       <=  X"00000000";
        addr_0x110_reg_feedback       <=  X"00000000";
        addr_0x101_reg_feedback       <=  X"00000000";
        addr_0x111_reg_feedback       <=  X"00000000";

        addr_0x102_reg_feedback       <=  X"00000000";
        addr_0x112_reg_feedback       <=  X"00000000";
        addr_0x103_reg_feedback       <=  X"00000000";
        addr_0x113_reg_feedback       <=  X"00000000";

        addr_0x104_reg_feedback       <=  X"00000000";
        addr_0x114_reg_feedback       <=  X"00000000";
        addr_0x105_reg_feedback       <=  X"00000000";
        addr_0x115_reg_feedback       <=  X"00000000";
        addr_0x106_reg_feedback       <=  X"00000000";
        addr_0x116_reg_feedback       <=  X"00000000";
        addr_0x107_reg_feedback       <=  X"00000000";
        addr_0x117_reg_feedback       <=  X"00000000";

        addr_0x108_reg_feedback       <=  X"00000000";
        addr_0x118_reg_feedback       <=  X"00000000";
        addr_0x109_reg_feedback       <=  X"00000000";
        addr_0x119_reg_feedback       <=  X"00000000";
    --

       adc_ch1_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch2_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch3_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch4_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch5_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch6_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch7_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch8_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch9_trigger_out_1clk_pulse     <=  '0' ;
       adc_ch10_trigger_out_1clk_pulse    <=  '0' ;

       adc_ch1_trigger_out_length_pulse   <=  '0' ;
       adc_ch2_trigger_out_length_pulse   <=  '0' ;
       adc_ch3_trigger_out_length_pulse   <=  '0' ;
       adc_ch4_trigger_out_length_pulse   <=  '0' ;
       adc_ch5_trigger_out_length_pulse   <=  '0' ;
       adc_ch6_trigger_out_length_pulse   <=  '0' ;
       adc_ch7_trigger_out_length_pulse   <=  '0' ;
       adc_ch8_trigger_out_length_pulse   <=  '0' ;
       adc_ch9_trigger_out_length_pulse   <=  '0' ;
       adc_ch10_trigger_out_length_pulse  <=  '0' ;

       adc_ch1_ringbuffer_din  <=  '0' & adc_ch1_pipe_din;
       adc_ch2_ringbuffer_din  <=  '0' & adc_ch2_pipe_din;
       adc_ch3_ringbuffer_din  <=  '0' & adc_ch3_pipe_din;
       adc_ch4_ringbuffer_din  <=  '0' & adc_ch4_pipe_din;
       adc_ch5_ringbuffer_din  <=  '0' & adc_ch5_pipe_din;
       adc_ch6_ringbuffer_din  <=  '0' & adc_ch6_pipe_din;
       adc_ch7_ringbuffer_din  <=  '0' & adc_ch7_pipe_din;
       adc_ch8_ringbuffer_din  <=  '0' & adc_ch8_pipe_din;
       adc_ch9_ringbuffer_din  <=  '0' & adc_ch9_pipe_din;
       adc_ch10_ringbuffer_din <=  '0' & adc_ch10_pipe_din;
   end generate;

    -- Trigger Block on
    gen_trigger_block_on: if (TRIGGER_BLOCK_EN = 1) generate

        addr_0x100_reg_feedback       <=  addr_0x100_reg_q(31 downto 0);
        addr_0x110_reg_feedback       <=  addr_0x110_reg_q(31 downto 0);
        addr_0x101_reg_feedback       <=  addr_0x101_reg_q(31 downto 0);
        addr_0x111_reg_feedback       <=  addr_0x111_reg_q(31 downto 0);

        addr_0x102_reg_feedback       <=  addr_0x102_reg_q(31 downto 0);
        addr_0x112_reg_feedback       <=  addr_0x112_reg_q(31 downto 0);
        addr_0x103_reg_feedback       <=  addr_0x103_reg_q(31 downto 0);
        addr_0x113_reg_feedback       <=  addr_0x113_reg_q(31 downto 0);

        addr_0x104_reg_feedback       <=  addr_0x104_reg_q(31 downto 0);
        addr_0x114_reg_feedback       <=  addr_0x114_reg_q(31 downto 0);
        addr_0x105_reg_feedback       <=  addr_0x105_reg_q(31 downto 0);
        addr_0x115_reg_feedback       <=  addr_0x115_reg_q(31 downto 0);
        addr_0x106_reg_feedback       <=  addr_0x106_reg_q(31 downto 0);
        addr_0x116_reg_feedback       <=  addr_0x116_reg_q(31 downto 0);
        addr_0x107_reg_feedback       <=  addr_0x107_reg_q(31 downto 0);
        addr_0x117_reg_feedback       <=  addr_0x117_reg_q(31 downto 0);

        addr_0x108_reg_feedback       <=  addr_0x108_reg_q(31 downto 0);
        addr_0x118_reg_feedback       <=  addr_0x118_reg_q(31 downto 0);
        addr_0x109_reg_feedback       <=  addr_0x109_reg_q(31 downto 0);
        addr_0x119_reg_feedback       <=  addr_0x119_reg_q(31 downto 0);
    --

    -- *****************************************************************************************************
    -- trigger logic
    -- *****************************************************************************************************

        adc_ch1_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc1_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch1_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch1_trigger_setup_reg,
            adc_din => adc_ch1_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch1_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch1_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch1_trigger_out_length_pulse
        );
        adc_ch1_ringbuffer_din(17)  <=  adc_ch1_trigger_out_1clk_pulse ;

        adc_ch2_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc1_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch2_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch2_trigger_setup_reg,
            adc_din => adc_ch2_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch2_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch2_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch2_trigger_out_length_pulse
        );
        adc_ch2_ringbuffer_din(17)  <=  adc_ch2_trigger_out_1clk_pulse ;

        adc_ch3_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc2_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch3_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch3_trigger_setup_reg,
            adc_din => adc_ch3_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch3_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch3_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch3_trigger_out_length_pulse
        );
        adc_ch3_ringbuffer_din(17)  <=  adc_ch3_trigger_out_1clk_pulse ;

        adc_ch4_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc2_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch4_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch4_trigger_setup_reg,
            adc_din => adc_ch4_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch4_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch4_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch4_trigger_out_length_pulse
        );
        adc_ch4_ringbuffer_din(17)  <=  adc_ch4_trigger_out_1clk_pulse ;

        adc_ch5_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc3_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch5_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch5_trigger_setup_reg,
            adc_din => adc_ch5_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch5_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch5_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch5_trigger_out_length_pulse
        );
        adc_ch5_ringbuffer_din(17)  <=  adc_ch5_trigger_out_1clk_pulse ;

        adc_ch6_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc3_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch6_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch6_trigger_setup_reg,
            adc_din => adc_ch6_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch6_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch6_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch6_trigger_out_length_pulse
        );
        adc_ch6_ringbuffer_din(17)  <=  adc_ch6_trigger_out_1clk_pulse ;

        adc_ch7_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc4_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch7_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch7_trigger_setup_reg,
            adc_din => adc_ch7_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch7_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch7_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch7_trigger_out_length_pulse
        );
        adc_ch7_ringbuffer_din(17)  <=  adc_ch7_trigger_out_1clk_pulse ;

        adc_ch8_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc4_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch8_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch8_trigger_setup_reg,
            adc_din => adc_ch8_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch8_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch8_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch8_trigger_out_length_pulse
        );
        adc_ch8_ringbuffer_din(17)  <=  adc_ch8_trigger_out_1clk_pulse ;

        adc_ch9_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc5_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch9_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch9_trigger_setup_reg,
            adc_din => adc_ch9_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch9_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch9_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch9_trigger_out_length_pulse
        );
        adc_ch9_ringbuffer_din(17)  <=  adc_ch9_trigger_out_1clk_pulse ;

        adc_ch10_fir_and_threshold_trigger_block: adc_fir_and_threshold_trigger_block
        port map(
            adc_clk => adc5_clk,
            trigger_reset => sys_clk_dll_reset,
            adc_trigger_threshold_reg => adc_ch10_trigger_threshold_reg,
            adc_trigger_setup_reg => adc_ch10_trigger_setup_reg,
            adc_din => adc_ch10_pipe_din(16 downto 0),
            adc_delayed_din => adc_ch10_ringbuffer_din(16 downto 0),
            adc_trigger_gate_valid => open,
            adc_trigger_out_1clk_pulse => adc_ch10_trigger_out_1clk_pulse,
            adc_trigger_out_length_pulse => adc_ch10_trigger_out_length_pulse
        );
        adc_ch10_ringbuffer_din(17)  <=  adc_ch10_trigger_out_1clk_pulse ;

    end generate;

    adc_chx_trigger_or_pulse  <= adc_ch1_trigger_out_length_pulse
                                 or adc_ch2_trigger_out_length_pulse
                                 or adc_ch3_trigger_out_length_pulse
                                 or adc_ch4_trigger_out_length_pulse
                                 or adc_ch5_trigger_out_length_pulse
                                 or adc_ch6_trigger_out_length_pulse
                                 or adc_ch7_trigger_out_length_pulse
                                 or adc_ch8_trigger_out_length_pulse
                                 or adc_ch9_trigger_out_length_pulse
                                 or adc_ch10_trigger_out_length_pulse ;

----**************************************************************************************************
--  ADC Ringbuffer Delay Block
----**************************************************************************************************
    adc_ch1_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch2_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch3_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch4_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch5_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch6_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch7_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch8_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch9_ringbuffer_delay_val  <=  addr_0x12B_reg_q(11 downto 0) ;
    adc_ch10_ringbuffer_delay_val <=  addr_0x12B_reg_q(11 downto 0) ;

    gen_ringbuffer_delay_off: if (RINGBUFFER_DELAY_EN = 0) generate
        addr_0x12B_reg_feedback <= X"00000000";
        adc_ch1_ringbuffer_dout   <=  adc_ch1_ringbuffer_din;
        adc_ch2_ringbuffer_dout   <=  adc_ch2_ringbuffer_din;
        adc_ch3_ringbuffer_dout   <=  adc_ch3_ringbuffer_din;
        adc_ch4_ringbuffer_dout   <=  adc_ch4_ringbuffer_din;
        adc_ch5_ringbuffer_dout   <=  adc_ch5_ringbuffer_din;
        adc_ch6_ringbuffer_dout   <=  adc_ch6_ringbuffer_din;
        adc_ch7_ringbuffer_dout   <=  adc_ch7_ringbuffer_din;
        adc_ch8_ringbuffer_dout   <=  adc_ch8_ringbuffer_din;
        adc_ch9_ringbuffer_dout   <=  adc_ch9_ringbuffer_din;
        adc_ch10_ringbuffer_dout  <=  adc_ch10_ringbuffer_din;
    end generate;

    gen_ringbuffer_delay_on: if (RINGBUFFER_DELAY_EN = 1) generate
        addr_0x12B_reg_feedback <= addr_0x12B_reg_q;

        Inst_adc_ringbuffer_delay_blocks: entity work.adc_ringbuffer_delay_blocks
            port map(
            adc_ringbuffer_logic_reset => sys_clk_dll_reset,
            adc1_clk => adc1_clk,
            adc2_clk => adc2_clk,
            adc3_clk => adc3_clk,
            adc4_clk => adc4_clk,
            adc5_clk => adc5_clk,
            adc_ch1_ringbuffer_delay_val => adc_ch1_ringbuffer_delay_val,
            adc_ch2_ringbuffer_delay_val => adc_ch2_ringbuffer_delay_val,
            adc_ch3_ringbuffer_delay_val => adc_ch3_ringbuffer_delay_val,
            adc_ch4_ringbuffer_delay_val => adc_ch4_ringbuffer_delay_val,
            adc_ch5_ringbuffer_delay_val => adc_ch5_ringbuffer_delay_val,
            adc_ch6_ringbuffer_delay_val => adc_ch6_ringbuffer_delay_val,
            adc_ch7_ringbuffer_delay_val => adc_ch7_ringbuffer_delay_val,
            adc_ch8_ringbuffer_delay_val => adc_ch8_ringbuffer_delay_val,
            adc_ch9_ringbuffer_delay_val => adc_ch9_ringbuffer_delay_val,
            adc_ch10_ringbuffer_delay_val => adc_ch10_ringbuffer_delay_val,
            adc_ch1_ringbuffer_din => adc_ch1_ringbuffer_din,
            adc_ch2_ringbuffer_din => adc_ch2_ringbuffer_din,
            adc_ch3_ringbuffer_din => adc_ch3_ringbuffer_din,
            adc_ch4_ringbuffer_din => adc_ch4_ringbuffer_din,
            adc_ch5_ringbuffer_din => adc_ch5_ringbuffer_din,
            adc_ch6_ringbuffer_din => adc_ch6_ringbuffer_din,
            adc_ch7_ringbuffer_din => adc_ch7_ringbuffer_din,
            adc_ch8_ringbuffer_din => adc_ch8_ringbuffer_din,
            adc_ch9_ringbuffer_din => adc_ch9_ringbuffer_din,
            adc_ch10_ringbuffer_din => adc_ch10_ringbuffer_din,
            adc_ch1_ringbuffer_dout => adc_ch1_ringbuffer_dout,
            adc_ch2_ringbuffer_dout => adc_ch2_ringbuffer_dout,
            adc_ch3_ringbuffer_dout => adc_ch3_ringbuffer_dout,
            adc_ch4_ringbuffer_dout => adc_ch4_ringbuffer_dout,
            adc_ch5_ringbuffer_dout => adc_ch5_ringbuffer_dout,
            adc_ch6_ringbuffer_dout => adc_ch6_ringbuffer_dout,
            adc_ch7_ringbuffer_dout => adc_ch7_ringbuffer_dout,
            adc_ch8_ringbuffer_dout => adc_ch8_ringbuffer_dout,
            adc_ch9_ringbuffer_dout => adc_ch9_ringbuffer_dout,
            adc_ch10_ringbuffer_dout => adc_ch10_ringbuffer_dout
        );
    end generate;

----**************************************************************************************************
--  ADC sampling control
----**************************************************************************************************

    adc_sample_ctrl_logic: process (register_trn_clk)
    begin

        addr_0x11_reg_feedback        <=  addr_0x11_reg_q(31 downto 0) ;

        if rising_edge(register_trn_clk) then  --
             adc_sample_external_trigger_enable  <=  addr_0x11_reg_q(11) ;
             adc_sample_internal_trigger_enable  <=  addr_0x11_reg_q(10) ;
             adc_sample_channel_trigger_disable  <=  addr_0x11_reg_q(9 downto 0) ;
        end if;

        adc_buffer_logic_reset <= adc_sample_disable_pulse ; -- copy logic mit DDR2 clk und ADC logic wird mit adc-clk gehandelt

        if rising_edge(register_trn_clk) then  --
             adc_sample_disable_pulse       <=  addr_0x10_reg_q(32) and addr_0x10_reg_q(2) ;
             adc_sample_arm_tigger_pulse    <=  addr_0x10_reg_q(32) and addr_0x10_reg_q(1) ;
             adc_sample_start_pulse         <=  addr_0x10_reg_q(32) and addr_0x10_reg_q(0) ;
             adc_sample_start_pulse_delay1  <=  adc_sample_start_pulse  ;
             adc_sample_start_pulse_delay2  <=  adc_sample_start_pulse_delay1  ;
        end if;

        if (sys_clk_dll_reset = '1') then
            adc_sample_arm_for_trigger <= '0' ;
        elsif rising_edge(register_trn_clk) then  --
            if (adc_sample_disable_pulse = '1') then
                adc_sample_arm_for_trigger <= '0' ;
            elsif (adc_chx_sample_logic_active_or_delay3 = '1') then
                adc_sample_arm_for_trigger <= '0' ;
            elsif ((adc_sample_arm_tigger_pulse = '1') or (adc_sample_start_pulse = '1')) then
                adc_sample_arm_for_trigger <= '1' ;
            else
                adc_sample_arm_for_trigger <= adc_sample_arm_for_trigger ;
            end if;
        end if;

        if rising_edge(register_trn_clk) then  --
            adc_chx_sample_logic_active_or_delay1  <=  adc_chx_sample_logic_active_or ;
            adc_chx_sample_logic_active_or_delay2  <=  adc_chx_sample_logic_active_or_delay1 ;
            adc_chx_sample_logic_active_or_delay3  <=  adc_chx_sample_logic_active_or_delay2 ;
        end if;

        if rising_edge(register_trn_clk) then  --
            adc_ddr3_write_logic_enable  <= adc_sample_arm_for_trigger
                                             or adc_chx_sample_logic_active_or_delay2
                                             or adc_chx_sample_buffer_not_empty_or ;
        end if;

        -- daq done irq glue logic
        if rising_edge(register_trn_clk) then  --
            adc_ddr3_daq_done <= not adc_ddr3_write_logic_enable;
            adc_ddr3_daq_done_lat <= adc_ddr3_daq_done;

            adc_ddr3_daq_done_pulse <= adc_ddr3_daq_done and not adc_ddr3_daq_done_lat;
        end if;

    end process;

    addr_0x10_reg_feedback(31 downto 16)  <=  X"0000" ;
    addr_0x10_reg_feedback(15 downto 8)   <=  X"00" ;
    addr_0x10_reg_feedback(7)             <=  ddr3_phy_init_done ; --
    addr_0x10_reg_feedback(6)             <=  '0'; --
    addr_0x10_reg_feedback(5)             <=  adc_chx_sample_buffer_not_empty_or ; -- erstmal
    addr_0x10_reg_feedback(4)             <=  adc_chx_sample_logic_active_or_delay2 ; -- erstmal
    addr_0x10_reg_feedback(3)             <=  '0' ;
    addr_0x10_reg_feedback(2)             <=  '0' ;
    addr_0x10_reg_feedback(1)             <=  adc_sample_arm_for_trigger ;
    addr_0x10_reg_feedback(0)             <=  adc_ddr3_write_logic_enable ;

--*********************************************************************************************************************
-- trigger synch with ADC clk1
--*********************************************************************************************************************

    -- software pcie trigger synch with ADC1-clk domain
    adc1_clk_pcie_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc1_clk,
        rising_edge_signal_in => adc_sample_start_pulse_delay2,
        signal_pulse_out => synch_adc1_clk_pcie_trigger
    );

    -- external trigger synch with ADC1-clk domain
    adc1_clk_external_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc1_clk,
        rising_edge_signal_in => external_ored_trigger_signal,
        signal_pulse_out => synch_adc1_clk_external_trigger
    );

    -- internal trigger synch with ADC1-clk domain
    adc1_clk_internal_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc1_clk,
        rising_edge_signal_in => adc_chx_trigger_or_pulse,
        signal_pulse_out => synch_adc1_clk_internal_trigger
    );

    -----------------------------------------------------------------
    -- ulegat MOD24092013
    -----------------------------------------------------------------
    -- end pulse trigger synch with ADC1-clk domain
    adc1_clk_end_pulse_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc1_clk,
        rising_edge_signal_in => s_end_pulse_ored_signal,
        signal_pulse_out => s_end_pulse_adc1_synch
    );
    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    --*********************************************************************************************************************
    -- trigger synch with ADC clk2
    --*********************************************************************************************************************

    -- software pcie trigger synch with ADC2-clk domain
    adc2_clk_pcie_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc2_clk,
        rising_edge_signal_in => adc_sample_start_pulse_delay2,
        signal_pulse_out => synch_adc2_clk_pcie_trigger
    );

    -- external trigger synch with ADC2-clk domain
    adc2_clk_external_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc2_clk,
        rising_edge_signal_in => external_ored_trigger_signal,
        signal_pulse_out => synch_adc2_clk_external_trigger
    );

    -- internal trigger synch with ADC2-clk domain
    adc2_clk_internal_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc2_clk,
        rising_edge_signal_in => adc_chx_trigger_or_pulse,
        signal_pulse_out => synch_adc2_clk_internal_trigger
    );

    -----------------------------------------------------------------
    -- ulegat MOD24092013
    -----------------------------------------------------------------
    -- end pulse trigger synch with ADC2-clk domain
    adc2_clk_end_pulse_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc2_clk,
        rising_edge_signal_in => s_end_pulse_ored_signal,
        signal_pulse_out => s_end_pulse_adc2_synch
    );

    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    --*********************************************************************************************************************
    -- trigger synch with ADC clk3
    --*********************************************************************************************************************

    -- software pcie trigger synch with ADC3-clk domain
    adc3_clk_pcie_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc3_clk,
        rising_edge_signal_in => adc_sample_start_pulse_delay2,
        signal_pulse_out => synch_adc3_clk_pcie_trigger
    );

    -- external trigger synch with ADC3-clk domain
    adc3_clk_external_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc3_clk,
        rising_edge_signal_in => external_ored_trigger_signal,
        signal_pulse_out => synch_adc3_clk_external_trigger
    );

    -- internal trigger synch with ADC3-clk domain
    adc3_clk_internal_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc3_clk,
        rising_edge_signal_in => adc_chx_trigger_or_pulse,
        signal_pulse_out => synch_adc3_clk_internal_trigger
    );

    -----------------------------------------------------------------
    -- ulegat MOD24092013
    -----------------------------------------------------------------
    -- end pulse trigger synch with ADC3-clk domain
    adc3_clk_end_pulse_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc3_clk,
        rising_edge_signal_in => s_end_pulse_ored_signal,
        signal_pulse_out => s_end_pulse_adc3_synch
    );

    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    --*********************************************************************************************************************
    -- trigger synch with ADC clk4
    --*********************************************************************************************************************

    -- software pcie trigger synch with ADC4-clk domain
    adc4_clk_pcie_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc4_clk,
        rising_edge_signal_in => adc_sample_start_pulse_delay2,
        signal_pulse_out => synch_adc4_clk_pcie_trigger
    );

    -- external trigger synch with ADC4-clk domain
    adc4_clk_external_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc4_clk,
        rising_edge_signal_in => external_ored_trigger_signal,
        signal_pulse_out => synch_adc4_clk_external_trigger
    );

    -- internal trigger synch with ADC4-clk domain
    adc4_clk_internal_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc4_clk,
        rising_edge_signal_in => adc_chx_trigger_or_pulse,
        signal_pulse_out => synch_adc4_clk_internal_trigger
    );


    -----------------------------------------------------------------
    -- ulegat MOD24092013
    -----------------------------------------------------------------
    -- end pulse trigger synch with ADC4-clk domain
    adc4_clk_end_pulse_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc4_clk,
        rising_edge_signal_in => s_end_pulse_ored_signal,
        signal_pulse_out => s_end_pulse_adc4_synch
    );

    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    --*********************************************************************************************************************
    -- trigger synch with ADC clk5
    --*********************************************************************************************************************

    -- software pcie trigger synch with ADC4-clk domain
    adc5_clk_pcie_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc5_clk,
        rising_edge_signal_in => adc_sample_start_pulse_delay2,
        signal_pulse_out => synch_adc5_clk_pcie_trigger
    );

    -- external trigger synch with ADC4-clk domain
    adc5_clk_external_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc5_clk,
        rising_edge_signal_in => external_ored_trigger_signal,
        signal_pulse_out => synch_adc5_clk_external_trigger
    );

    -- internal trigger synch with ADC4-clk domain
    adc5_clk_internal_trigger_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc5_clk,
        rising_edge_signal_in => adc_chx_trigger_or_pulse,
        signal_pulse_out => synch_adc5_clk_internal_trigger
    );

    -----------------------------------------------------------------
    -- ulegat MOD24092013
    -----------------------------------------------------------------
    -- end pulse trigger synch with ADC5-clk domain
    adc5_clk_end_pulse_risingedge2pulse_generator: entity work.risingedge2pulse_generator
    port map(
        adc_clk => adc5_clk,
        rising_edge_signal_in => s_end_pulse_ored_signal,
        signal_pulse_out => s_end_pulse_adc5_synch
    );

    -----------------------------------------------------------------
    -- end ulegat MOD24092013
    -----------------------------------------------------------------

    adc1_sample_trigger_logic: process (adc1_clk)
    begin
        if rising_edge(adc1_clk) then  --
            synch_adc1_clk_adc_sample_arm_for_trigger   <=  adc_sample_arm_for_trigger ;
        end if ;

        if rising_edge(adc1_clk) then  --
            adc_ch1_start_pulse  <= synch_adc1_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(0)
                                    and ((synch_adc1_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc1_clk_pcie_trigger)
                                    or (synch_adc1_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc1_clk) then  --
            adc_ch2_start_pulse  <= synch_adc1_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(1)
                                    and ((synch_adc1_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc1_clk_pcie_trigger)
                                    or (synch_adc1_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;

        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        if rising_edge(adc1_clk) then  --
            s_adc_ch1_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(0)
                                    and ((s_end_pulse_adc1_synch  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc1_clk) then  --
            s_adc_ch2_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(1)
                                    and ((s_end_pulse_adc1_synch  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------
    end process;

-- ***************************************************************

    adc2_sample_trigger_logic: process (adc2_clk)
    begin
        if rising_edge(adc2_clk) then  --
            synch_adc2_clk_adc_sample_arm_for_trigger   <=  adc_sample_arm_for_trigger ;
        end if ;

        if rising_edge(adc2_clk) then  --
            adc_ch3_start_pulse  <= synch_adc2_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(2)
                                    and ((synch_adc2_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc2_clk_pcie_trigger)
                                    or (synch_adc2_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        if rising_edge(adc2_clk) then  --
            adc_ch4_start_pulse  <= synch_adc2_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(3)
                                    and ((synch_adc2_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc2_clk_pcie_trigger)
                                    or (synch_adc2_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        if rising_edge(adc2_clk) then  --
            s_adc_ch3_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(2)
                                    and ((s_end_pulse_adc2_synch  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc2_clk) then  --
            s_adc_ch4_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(3)
                                    and ((s_end_pulse_adc2_synch  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------
    end process;

-- ***************************************************************


    adc3_sample_trigger_logic: process (adc3_clk)
    begin
        if rising_edge(adc3_clk) then  --
            synch_adc3_clk_adc_sample_arm_for_trigger   <=  adc_sample_arm_for_trigger ;
        end if ;

        if rising_edge(adc3_clk) then  --
            adc_ch5_start_pulse  <= synch_adc3_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(4)
                                    and ((synch_adc3_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc3_clk_pcie_trigger)
                                    or (synch_adc3_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        if rising_edge(adc3_clk) then  --
            adc_ch6_start_pulse  <= synch_adc3_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(5)
                                    and ((synch_adc3_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc3_clk_pcie_trigger)
                                    or (synch_adc3_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        if rising_edge(adc3_clk) then  --
            s_adc_ch5_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(4)
                                    and ((s_end_pulse_adc3_synch  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc3_clk) then  --
            s_adc_ch6_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(5)
                                    and ((s_end_pulse_adc3_synch  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------
    end process;

-- ***************************************************************


    adc4_sample_trigger_logic: process (adc4_clk)
    begin
        if rising_edge(adc4_clk) then  --
            synch_adc4_clk_adc_sample_arm_for_trigger   <=  adc_sample_arm_for_trigger ;
        end if ;

        if rising_edge(adc4_clk) then  --
            adc_ch7_start_pulse  <= synch_adc4_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(6)
                                    and ((synch_adc4_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc4_clk_pcie_trigger)
                                    or (synch_adc4_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        if rising_edge(adc4_clk) then  --
            adc_ch8_start_pulse  <= synch_adc4_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(7)
                                    and ((synch_adc4_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc4_clk_pcie_trigger)
                                    or (synch_adc4_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        if rising_edge(adc4_clk) then  --
            s_adc_ch7_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(6)
                                    and ((s_end_pulse_adc4_synch  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc4_clk) then  --
            s_adc_ch8_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(7)
                                    and ((s_end_pulse_adc4_synch  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------

    end process;

-- ***************************************************************


    adc5_sample_trigger_logic: process (adc5_clk)
    begin
        if rising_edge(adc5_clk) then  --
            synch_adc5_clk_adc_sample_arm_for_trigger   <=  adc_sample_arm_for_trigger ;
        end if ;

        if rising_edge(adc5_clk) then  --
            adc_ch9_start_pulse  <= synch_adc5_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(8)
                                    and ((synch_adc5_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc5_clk_pcie_trigger)
                                    or (synch_adc5_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        if rising_edge(adc5_clk) then  --
            adc_ch10_start_pulse <= synch_adc5_clk_adc_sample_arm_for_trigger and not adc_sample_channel_trigger_disable(9)
                                    and ((synch_adc5_clk_internal_trigger and adc_sample_internal_trigger_enable)
                                    or (synch_adc5_clk_pcie_trigger)
                                    or (synch_adc5_clk_external_trigger  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- ulegat MOD24092013
        -----------------------------------------------------------------
        if rising_edge(adc5_clk) then  --
            s_adc_ch9_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(8)
                                    and ((s_end_pulse_adc5_synch  and adc_sample_external_trigger_enable)) ;
        end if ;

        if rising_edge(adc5_clk) then  --
            s_adc_ch10_end_pulse  <= not adc_buffer_logic_reset and not adc_sample_channel_trigger_disable(9)
                                     and ((s_end_pulse_adc5_synch  and adc_sample_external_trigger_enable)) ;
        end if ;
        -----------------------------------------------------------------
        -- end ulegat MOD24092013
        -----------------------------------------------------------------
    end process;


--*********************************************************************************************************************


    -- sample length feedback
    addr_0x12A_reg_feedback(31 downto 26) <=  "000000";
    addr_0x12A_reg_feedback(25 downto 0)  <=  addr_0x12A_reg_q(25 downto 0);

    inst_adc_sampling_block_v1: entity work.adc_sampling_block_v1
    port map(
        adc_buffer_logic_reset => adc_buffer_logic_reset,
        adc1_clk => adc1_clk,
        adc2_clk => adc2_clk,
        adc3_clk => adc3_clk,
        adc4_clk => adc4_clk,
        adc5_clk => adc5_clk,
        adc_ch1_ringbuffer_dout => s_ch1_processed_dout,        -- adc_ch1_ringbuffer_dout(15 downto 0),
        adc_ch2_ringbuffer_dout => s_ch2_processed_dout,        -- adc_ch2_ringbuffer_dout(15 downto 0),
        adc_ch3_ringbuffer_dout => s_ch3_processed_dout,        -- adc_ch3_ringbuffer_dout(15 downto 0),
        adc_ch4_ringbuffer_dout => s_ch4_processed_dout,        -- adc_ch4_ringbuffer_dout(15 downto 0),
        adc_ch5_ringbuffer_dout => s_ch5_processed_dout,        -- adc_ch5_ringbuffer_dout(15 downto 0),
        adc_ch6_ringbuffer_dout => s_ch6_processed_dout,        -- adc_ch6_ringbuffer_dout(15 downto 0),
        adc_ch7_ringbuffer_dout => s_ch7_processed_dout,        -- adc_ch7_ringbuffer_dout(15 downto 0),
        adc_ch8_ringbuffer_dout => s_ch8_processed_dout,        -- adc_ch8_ringbuffer_dout(15 downto 0),
        adc_ch9_ringbuffer_dout => s_ch9_processed_dout,        -- adc_ch9_ringbuffer_dout(15 downto 0),
        adc_ch10_ringbuffer_dout => s_ch10_processed_dout,      -- adc_ch10_ringbuffer_dout(15 downto 0),
        adc_sample_length_reg => addr_0x12A_reg_q(25 downto 0),
        adc_ch1_start_pulse => s_ch1_start_pulse,               -- adc_ch1_start_pulse,
        adc_ch2_start_pulse => s_ch2_start_pulse,               -- adc_ch2_start_pulse,
        adc_ch3_start_pulse => s_ch3_start_pulse,               -- adc_ch3_start_pulse,
        adc_ch4_start_pulse => s_ch4_start_pulse,               -- adc_ch4_start_pulse,
        adc_ch5_start_pulse => s_ch5_start_pulse,               -- adc_ch5_start_pulse,
        adc_ch6_start_pulse => s_ch6_start_pulse,               -- adc_ch6_start_pulse,
        adc_ch7_start_pulse => s_ch7_start_pulse,               -- adc_ch7_start_pulse,
        adc_ch8_start_pulse => s_ch8_start_pulse,               -- adc_ch8_start_pulse,
        adc_ch9_start_pulse => s_ch9_start_pulse,               -- adc_ch9_start_pulse,
        adc_ch10_start_pulse => s_ch10_start_pulse,             -- adc_ch10_start_pulse,
        adc_ch1_sample_memory_start_addr => addr_0x120_reg_q(25 downto 0),
        adc_ch2_sample_memory_start_addr => addr_0x121_reg_q(25 downto 0),
        adc_ch3_sample_memory_start_addr => addr_0x122_reg_q(25 downto 0),
        adc_ch4_sample_memory_start_addr => addr_0x123_reg_q(25 downto 0),
        adc_ch5_sample_memory_start_addr => addr_0x124_reg_q(25 downto 0),
        adc_ch6_sample_memory_start_addr => addr_0x125_reg_q(25 downto 0),
        adc_ch7_sample_memory_start_addr => addr_0x126_reg_q(25 downto 0),
        adc_ch8_sample_memory_start_addr => addr_0x127_reg_q(25 downto 0),
        adc_ch9_sample_memory_start_addr => addr_0x128_reg_q(25 downto 0),
        adc_ch10_sample_memory_start_addr => addr_0x129_reg_q(25 downto 0),
        adc_ch1_adc_memory_addr_counter => addr_0x120_reg_feedback,
        adc_ch2_adc_memory_addr_counter => addr_0x121_reg_feedback,
        adc_ch3_adc_memory_addr_counter => addr_0x122_reg_feedback,
        adc_ch4_adc_memory_addr_counter => addr_0x123_reg_feedback,
        adc_ch5_adc_memory_addr_counter => addr_0x124_reg_feedback,
        adc_ch6_adc_memory_addr_counter => addr_0x125_reg_feedback,
        adc_ch7_adc_memory_addr_counter => addr_0x126_reg_feedback,
        adc_ch8_adc_memory_addr_counter => addr_0x127_reg_feedback,
        adc_ch9_adc_memory_addr_counter => addr_0x128_reg_feedback,
        adc_ch10_adc_memory_addr_counter => addr_0x129_reg_feedback,
        adc_chx_sample_logic_active_or => adc_chx_sample_logic_active_or,
        adc_chx_sample_buffer_not_empty_or => adc_chx_sample_buffer_not_empty_or,
        ddr3_write_logic_enable => adc_ddr3_write_logic_enable,
        ddr3_write_fifo_clk => ddr3_usr_clk,
        ddr3_write_addr_fifo_wr_count => ddr3_write_addr_fifo_wr_count,
        ddr3_write_addr_fifo_wr_en => adc_ddr3_write_addr_fifo_wr_en,
        ddr3_write_addr_fifo_din => adc_ddr3_write_addr_fifo_din,
        ddr3_write_data_fifo_wr_en => adc_ddr3_write_data_fifo_wr_en,
        ddr3_write_data_fifo_din => adc_ddr3_write_data_fifo_din
    );





















--**************************************************************************************************
--**************************************************************************************************

    -- DAC Data outputs

--**************************************************************************************************
--**************************************************************************************************

    -- input: ADC1_CLK
    -- output to DAC-CLock-Multiplexer: 2 x ADC1_CLK
    Inst_dac_clk_out_mmcm: entity work.dac_clk_out_mmcm
    port map(
        dac_clkout_in => adc1_clk,
        dac_mmcm_reset => dac_dcm_reset_pulse,
        dac_mmcm_lock => dac_mmcm_lock,
        DAC_CLK_P => DAC_CLK_P,
        DAC_CLK_N => DAC_CLK_N
    );

    dac_logic: process (register_trn_clk)
    begin
        MUX_DAC_SEL1      <=    dac_ctrl_reg(17)  ;
        MUX_DAC_SEL0      <=    dac_ctrl_reg(16)  ;
        DAC_PD_H      <=  not dac_ctrl_reg(5)  ;
        DAC_TORB      <=    dac_ctrl_reg(4)  ;
        if rising_edge(register_trn_clk) then  --
             dac_dcm_reset_pulse  <=  dac_ctrl_reg(32) and dac_ctrl_reg(8)  ;
             dac_tap_write_pulse  <=  dac_ctrl_reg(32) and dac_ctrl_reg(31)  ;
        end if;
    end process;

    UIBUFG_DAC : IBUFDS
    port map (
        I => DAC_MUX_CLK_P,
        IB => DAC_MUX_CLK_N,
        O => sys_dac_in
    );
    dac_clk_bufg : BUFG
    port map (
        I => sys_dac_in,
        O => dac_clk_in
    );

    dac_test_logic: process (dac_out_synch_clk)
    begin
        if rising_edge(dac_out_synch_clk) then  --
             dac_test_mode <=  dac_ctrl_reg(1 downto 0)  ;
        end if;
        if ((sys_clk_dll_reset='1') or (dac_test_mode /= "01")) then
            dac_test_counter <= X"00000";
         elsif rising_edge(dac_out_synch_clk) then  --
            if (dac_select_ch1 = '1') then
               dac_test_counter <= dac_test_counter + 1;
          else
               dac_test_counter <= dac_test_counter  ;
           end if;
        end if;

        if rising_edge(dac_out_synch_clk) then  --
            case dac_test_mode(1 downto 0) is
                when "00" =>
                    dac1_data_out(15)           <= not dac_data_reg(15);
                    dac1_data_out(14 downto 0)  <=     dac_data_reg(14 downto 0);
                    dac2_data_out(15)           <= not dac_data_reg(31);
                    dac2_data_out(14 downto 0)  <=     dac_data_reg(30 downto 16);
                when "01" =>
                    dac1_data_out(15)           <= not dac_test_counter(15);
                    dac1_data_out(14 downto 0)  <=     dac_test_counter(14 downto 0);
                    dac2_data_out(15)           <= not dac_test_counter(16);
                    dac2_data_out(14 downto 0)  <=     dac_test_counter(15 downto 1);
                when "10" =>
                    dac1_data_out(15)           <= not adc_ch1_pipe_din(15);
                    dac1_data_out(14 downto 0)  <=     adc_ch1_pipe_din(14 downto 0);
                    dac2_data_out(15)           <= not adc_ch2_pipe_din(15);
                    dac2_data_out(14 downto 0)  <=     adc_ch2_pipe_din(14 downto 0);
                when "11" =>
                    dac1_data_out(15 downto 0)  <=     X"0000";
                    dac2_data_out(15 downto 0)  <=     X"0000";
                when others =>
                    dac1_data_out(15 downto 0)  <=     X"0000";
                    dac2_data_out(15 downto 0)  <=     X"0000";
            end case;
        end if;

    end process;



    dac_mux_out_logic: process (dac_out_synch_clk)
    begin
      dac_data_select_reset  <=  dac_dcm_reset_pulse or not dac_clk_dcm_lock ;

        if (dac_data_select_reset = '1') then  --
             dac_select_ch1 <=  '0' ;
         elsif rising_edge(dac_out_synch_clk) then  --
             dac_select_ch1 <=  not dac_select_ch1 ;
        end if;

        if rising_edge(dac_out_synch_clk) then  --
           if (dac_select_ch1 = '0') then  --
              dac_data_mux_out  <=  dac1_data_out ;
            else
              dac_data_mux_out  <=  dac2_data_out ;
           end if;
        end if;

    end process;


    Inst_dac_fd_out_logic: entity work.dac_fd_out_logic
    port map(
        dac_clk_in => dac_clk_in,
        dac_odelay_clk => register_trn_clk,
        dac_odelay_rst => dac_tap_write_pulse,
        dac_odelay_value => dac_ctrl_reg(28 downto 24),
        dac_dcm_reset_pulse => dac_dcm_reset_pulse,
        dac_select_ch1 => dac_select_ch1,
        dac_data_out => dac_data_mux_out,
        dac_clk_dcm_lock => dac_clk_dcm_lock,
        dac_out_synch_clk => dac_out_synch_clk,
        DAC_SELIQ_P => DAC_SELIQ_P,
        DAC_SELIQ_N => DAC_SELIQ_N,
        DAC_D_P => DAC_D_P,
        DAC_D_N => DAC_D_N
    );

    --******************************************************************************************
    -- Serial (GTXs) interfaces
    --******************************************************************************************
    gtx_interface_logic:  process(register_trn_clk)
    begin
        gtx_usr_side_fifo_clk                <= register_trn_clk ;

        addr_0x15_reg_feedback(31 downto 28) <= "0000" ;
        addr_0x15_reg_feedback(27)           <=   port12_15_gtx_linkup(2) ;
        addr_0x15_reg_feedback(26)           <=   port12_15_gtx_linkup(0) ;
        addr_0x15_reg_feedback(25)           <= opt1_2_gtx_linkup(0) ;

        addr_0x15_reg_feedback(23 downto 21) <= "000" ;
        addr_0x15_reg_feedback(11 downto 9)  <= "000" ;

        case addr_0x15_reg_q(1 downto 0) is
            when "00" =>
                opt1_select_flag     <= '1';
                port12_select_flag   <= '0';
                port14_select_flag   <= '0';
                addr_0x14_reg_feedback(31 downto 0)  <= opt1_in_fifo_dout(31 downto 0) ;
                addr_0x15_reg_feedback(24)           <= opt1_in_fifo_dout(32) ;
                addr_0x15_reg_feedback(20 downto 12) <= opt1_out_fifo_wcnt(8 downto 0) ;
                addr_0x15_reg_feedback(8 downto 0)   <= opt1_in_fifo_rcnt(8 downto 0) ;
            when "10" =>
                opt1_select_flag     <= '0';
                port12_select_flag   <= '1';
                port14_select_flag   <= '0';
                addr_0x14_reg_feedback(31 downto 0)  <= port12_in_fifo_dout(31 downto 0) ;
                addr_0x15_reg_feedback(24)           <= port12_in_fifo_dout(32) ;
                addr_0x15_reg_feedback(20 downto 12) <= port12_out_fifo_wcnt(8 downto 0) ;
                addr_0x15_reg_feedback(8 downto 0)   <= port12_in_fifo_rcnt(8 downto 0) ;
            when "11" =>
                opt1_select_flag     <= '0';
                port12_select_flag   <= '0';
                port14_select_flag   <= '1';
                addr_0x14_reg_feedback(31 downto 0)  <= port14_in_fifo_dout(31 downto 0) ;
                addr_0x15_reg_feedback(24)           <= port14_in_fifo_dout(32) ;
                addr_0x15_reg_feedback(20 downto 12) <= port14_out_fifo_wcnt(8 downto 0) ;
                addr_0x15_reg_feedback(8 downto 0)   <= port14_in_fifo_rcnt(8 downto 0) ;
            when others =>
                opt1_select_flag     <= '1';
                port12_select_flag   <= '0';
                port14_select_flag   <= '0';
                addr_0x14_reg_feedback(31 downto 0)  <= opt1_in_fifo_dout(31 downto 0) ;
                addr_0x15_reg_feedback(24)           <= opt1_in_fifo_dout(32) ;
                addr_0x15_reg_feedback(20 downto 12) <= opt1_out_fifo_wcnt(8 downto 0) ;
                addr_0x15_reg_feedback(8 downto 0)   <= opt1_in_fifo_rcnt(8 downto 0) ;
        end case;

        addr_0x17_reg_feedback(31 downto 28) <= "0000" ;
        addr_0x17_reg_feedback(27)           <= port12_15_gtx_linkup(3) ;
        addr_0x17_reg_feedback(26)           <= port12_15_gtx_linkup(1) ;
        addr_0x17_reg_feedback(25)           <= opt1_2_gtx_linkup(1) ;

        addr_0x17_reg_feedback(23 downto 21) <= "000" ;
        addr_0x17_reg_feedback(11 downto 9)  <= "000" ;

        case addr_0x17_reg_q(1 downto 0) is
            when "00" =>
                opt2_select_flag     <= '1';
                port13_select_flag   <= '0';
                port15_select_flag   <= '0';
                addr_0x16_reg_feedback(31 downto 0)  <= opt2_in_fifo_dout(31 downto 0) ;
                addr_0x17_reg_feedback(24)           <= opt2_in_fifo_dout(32) ;
                addr_0x17_reg_feedback(20 downto 12) <= opt2_out_fifo_wcnt(8 downto 0) ;
                addr_0x17_reg_feedback(8 downto 0)   <= opt2_in_fifo_rcnt(8 downto 0) ;
            when "10" =>
                opt2_select_flag     <= '0';
                port13_select_flag   <= '1';
                port15_select_flag   <= '0';
                addr_0x16_reg_feedback(31 downto 0)  <= port13_in_fifo_dout(31 downto 0) ;
                addr_0x17_reg_feedback(24)           <= port13_in_fifo_dout(32) ;
                addr_0x17_reg_feedback(20 downto 12) <= port13_out_fifo_wcnt(8 downto 0) ;
                addr_0x17_reg_feedback(8 downto 0)   <= port13_in_fifo_rcnt(8 downto 0) ;
            when "11" =>
                opt2_select_flag     <= '0';
                port13_select_flag   <= '0';
                port15_select_flag   <= '1';
                addr_0x16_reg_feedback(31 downto 0)  <= port15_in_fifo_dout(31 downto 0) ;
                addr_0x17_reg_feedback(24)           <= port15_in_fifo_dout(32) ;
                addr_0x17_reg_feedback(20 downto 12) <= port15_out_fifo_wcnt(8 downto 0) ;
                addr_0x17_reg_feedback(8 downto 0)   <= port15_in_fifo_rcnt(8 downto 0) ;
            when others =>
                opt2_select_flag     <= '1';
                port13_select_flag   <= '0';
                port15_select_flag   <= '0';
                addr_0x16_reg_feedback(31 downto 0)  <= opt2_in_fifo_dout(31 downto 0) ;
                addr_0x17_reg_feedback(24)           <= opt2_in_fifo_dout(32) ;
                addr_0x17_reg_feedback(20 downto 12) <= opt2_out_fifo_wcnt(8 downto 0) ;
                addr_0x17_reg_feedback(8 downto 0)   <= opt2_in_fifo_rcnt(8 downto 0) ;
        end case;

    end process gtx_interface_logic;

    --******************************************************************
    -- optical interface
    --******************************************************************

    gen_optical_interface_off: if (DUAL_OPTICAL_INTERFACE_EN = 0) generate
        opt1_2_serdes_reset <= sys_clk_dll_reset;
        opt1_2_gtx_reset <= sys_clk_dll_reset;

        opt1_2_gtx_linkup(0)            <= '0';
        opt1_select_flag                <= '0';
        opt1_in_fifo_dout(32)           <= '0';
        opt1_in_fifo_dout(31 downto 0)  <= X"00000000";
        opt1_out_fifo_wcnt(8 downto 0)  <= "000000000";
        opt1_in_fifo_rcnt(8 downto 0)   <= "000000000";


        opt1_2_gtx_linkup(1)            <= '0';
        opt2_select_flag                <= '0';
        opt2_in_fifo_dout(32)           <= '0';
        opt2_in_fifo_dout(31 downto 0)  <= X"00000000";
        opt2_out_fifo_wcnt(8 downto 0)  <= "000000000";
        opt2_in_fifo_rcnt(8 downto 0)   <= "000000000";
    end generate;

    gen_optical_interface_on: if (DUAL_OPTICAL_INTERFACE_EN = 1) generate
        opt1_2_serdes_reset                           <=      sys_clk_dll_reset
                                                             or (opt1_select_flag and addr_0x15_reg_q(2))
                                                             or (opt2_select_flag and addr_0x17_reg_q(2));

        opt1_2_gtx_reset                                 <=      sys_clk_dll_reset
                                                             or (opt1_select_flag and addr_0x15_reg_q(3))
                                                             or (opt2_select_flag and addr_0x17_reg_q(3));

-----------------------------------------------------------------------------------------------------------------------
          opt1_fifo_reset                      <=   opt1_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(31)) ;
          opt1_in_fifo_rden                    <=   opt1_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(30)) ;
          opt1_out_fifo_wren                   <=   opt1_select_flag and (addr_0x14_reg_q(32)) ;
          opt1_out_fifo_din(31 downto 0)       <=   addr_0x14_reg_q(31 downto 0) ;
          opt1_out_fifo_din(32)                <=   addr_0x15_reg_q(24) ;

          opt2_fifo_reset                      <=   opt2_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(31)) ;
          opt2_in_fifo_rden                    <=   opt2_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(30)) ;
          opt2_out_fifo_wren                   <=   opt2_select_flag and (addr_0x16_reg_q(32)) ;
          opt2_out_fifo_din(31 downto 0)       <=   addr_0x16_reg_q(31 downto 0) ;
          opt2_out_fifo_din(32)                <=   addr_0x17_reg_q(24) ;

    -----------------------------------------------------------------------------------------------------------------------

        Inst_dual_gtx_serdes_interface: dual_gtx_serdes_interface
        PORT MAP(
            rst => opt1_2_gtx_reset,
            gtx_refclk_p => MGTCLK0_112_P,
            gtx_refclk_n => MGTCLK0_112_N,
            gtx_rx_p => SFF_RX_P,
            gtx_rx_n => SFF_RX_N,
            gtx_tx_p => SFF_TX_P,
            gtx_tx_n => SFF_TX_N,
            gtx_linkup => opt1_2_gtx_linkup,
            gtx_usrclk => gtx_usrclk,
            serdes_reset => opt1_2_serdes_reset,

            GTX0_LOOPBACK_IN => addr_0x12C_reg_q(2 downto 0),
            GTX1_LOOPBACK_IN => addr_0x12C_reg_q(5 downto 3),

            opt0_fifo_reset => opt1_fifo_reset,
            opt0_fifo_clk => gtx_usr_side_fifo_clk,
            opt0_in_fifo_dout => opt1_in_fifo_dout,
            opt0_in_fifo_rden => opt1_in_fifo_rden,
            opt0_in_fifo_rcnt => opt1_in_fifo_rcnt,
            opt0_out_fifo_din => opt1_out_fifo_din,
            opt0_out_fifo_wren => opt1_out_fifo_wren,
            opt0_out_fifo_wcnt => opt1_out_fifo_wcnt,

            opt1_fifo_reset => opt2_fifo_reset,
            opt1_fifo_clk => gtx_usr_side_fifo_clk,
            opt1_in_fifo_dout => opt2_in_fifo_dout,
            opt1_in_fifo_rden => opt2_in_fifo_rden,
            opt1_in_fifo_rcnt => opt2_in_fifo_rcnt,
            opt1_out_fifo_din => opt2_out_fifo_din,
            opt1_out_fifo_wren => opt2_out_fifo_wren,
            opt1_out_fifo_wcnt => opt2_out_fifo_wcnt
        );

   end generate;


-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------


    --******************************************************************
    -- Port12 - Port15 interface
    --******************************************************************
-----------------------------------------------------------------------------------------------------------------------
    gen_port12_15_interface_off: if (QUAD_PORT12_13_14_15_INTERFACE_EN = 0) generate

        port12_15_gtx_reset               <= sys_clk_dll_reset;

        port12_15_gtx_linkup(0)           <= '0';
        port12_select_flag                <= '0';
        port12_in_fifo_dout(32)           <= '0';
        port12_in_fifo_dout(31 downto 0)  <= X"00000000";
        port12_out_fifo_wcnt(8 downto 0)  <= "000000000";
        port12_in_fifo_rcnt(8 downto 0)   <= "000000000";

        port12_15_gtx_linkup(1)           <= '0';
        port13_select_flag                <= '0';
        port13_in_fifo_dout(32)           <= '0';
        port13_in_fifo_dout(31 downto 0)  <= X"00000000";
        port13_out_fifo_wcnt(8 downto 0)  <= "000000000";
        port13_in_fifo_rcnt(8 downto 0)   <= "000000000";

        port12_15_gtx_linkup(2)           <= '0';
        port14_select_flag                <= '0';
        port14_in_fifo_dout(32)           <= '0';
        port14_in_fifo_dout(31 downto 0)  <= X"00000000";
        port14_out_fifo_wcnt(8 downto 0)  <= "000000000";
        port14_in_fifo_rcnt(8 downto 0)   <= "000000000";

        port12_15_gtx_linkup(3)           <= '0';
        port15_select_flag                <= '0';
        port15_in_fifo_dout(32)           <= '0';
        port15_in_fifo_dout(31 downto 0)  <= X"00000000";
        port15_out_fifo_wcnt(8 downto 0)  <= "000000000";
        port15_in_fifo_rcnt(8 downto 0)   <= "000000000";
    end generate;
-----------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------------------
   gen_port12_15_interface_on: if (QUAD_PORT12_13_14_15_INTERFACE_EN = 1) generate

        port12_15_gtx_reset                             <=      sys_clk_dll_reset
                                                             or ((port12_select_flag or port14_select_flag) and addr_0x15_reg_q(3))
                                                             or ((port13_select_flag or port15_select_flag) and addr_0x17_reg_q(3));

        port12_fifo_reset                      <=   port12_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(31)) ;
        port12_in_fifo_rden                    <=   port12_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(30)) ;
        port12_out_fifo_wren                   <=   port12_select_flag and (addr_0x14_reg_q(32)) ;
        port12_out_fifo_din(31 downto 0)       <=   addr_0x14_reg_q(31 downto 0) ;
        port12_out_fifo_din(32)                <=   addr_0x15_reg_q(24) ;

        port13_fifo_reset                      <=   port13_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(31)) ;
        port13_in_fifo_rden                    <=   port13_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(30)) ;
        port13_out_fifo_wren                   <=   port13_select_flag and (addr_0x16_reg_q(32)) ;
        port13_out_fifo_din(31 downto 0)       <=   addr_0x16_reg_q(31 downto 0) ;
        port13_out_fifo_din(32)                <=   addr_0x17_reg_q(24) ;

        port14_fifo_reset                      <=   port14_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(31)) ;
        port14_in_fifo_rden                    <=   port14_select_flag and (addr_0x15_reg_q(32) and addr_0x15_reg_q(30)) ;
        port14_out_fifo_wren                   <=   port14_select_flag and (addr_0x14_reg_q(32)) ;
        port14_out_fifo_din(31 downto 0)       <=   addr_0x14_reg_q(31 downto 0) ;
        port14_out_fifo_din(32)                <=   addr_0x15_reg_q(24) ;

        port15_fifo_reset                      <=   port15_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(31)) ;
        port15_in_fifo_rden                    <=   port15_select_flag and (addr_0x17_reg_q(32) and addr_0x17_reg_q(30)) ;
        port15_out_fifo_wren                   <=   port15_select_flag and (addr_0x16_reg_q(32)) ;
        port15_out_fifo_din(31 downto 0)       <=   addr_0x16_reg_q(31 downto 0) ;
        port15_out_fifo_din(32)                <=   addr_0x17_reg_q(24) ;
        -----------------------------------------------------------------------------------------------------------------------

        Inst_quad_gtx_serdes_interface: quad_gtx_serdes_interface PORT MAP(
            rst => port12_15_gtx_reset,
            gtx_refclk_p => MGTCLK0_114_P,
            gtx_refclk_n => MGTCLK0_114_N,
            gtx_rx_p => P2PR_P,
            gtx_rx_n => P2PR_N,
            gtx_tx_p => P2PT_P,
            gtx_tx_n => P2PT_N,
            gtx_linkup => port12_15_gtx_linkup,
            serdes_reset => sys_clk_dll_reset,

            opt0_fifo_reset => port12_fifo_reset,
            opt0_fifo_clk => gtx_usr_side_fifo_clk,
            opt0_in_fifo_dout => port12_in_fifo_dout,
            opt0_in_fifo_rden => port12_in_fifo_rden,
            opt0_in_fifo_rcnt => port12_in_fifo_rcnt,
            opt0_out_fifo_din => port12_out_fifo_din,
            opt0_out_fifo_wren => port12_out_fifo_wren,
            opt0_out_fifo_wcnt => port12_out_fifo_wcnt,

            opt1_fifo_reset => port13_fifo_reset,
            opt1_fifo_clk => gtx_usr_side_fifo_clk,
            opt1_in_fifo_dout => port13_in_fifo_dout,
            opt1_in_fifo_rden => port13_in_fifo_rden,
            opt1_in_fifo_rcnt => port13_in_fifo_rcnt,
            opt1_out_fifo_din => port13_out_fifo_din,
            opt1_out_fifo_wren => port13_out_fifo_wren,
            opt1_out_fifo_wcnt => port13_out_fifo_wcnt,

            opt2_fifo_reset => port14_fifo_reset,
            opt2_fifo_clk => gtx_usr_side_fifo_clk,
            opt2_in_fifo_dout => port14_in_fifo_dout,
            opt2_in_fifo_rden => port14_in_fifo_rden,
            opt2_in_fifo_rcnt => port14_in_fifo_rcnt,
            opt2_out_fifo_din => port14_out_fifo_din,
            opt2_out_fifo_wren => port14_out_fifo_wren,
            opt2_out_fifo_wcnt => port14_out_fifo_wcnt,

            opt3_fifo_reset => port15_fifo_reset,
            opt3_fifo_clk => gtx_usr_side_fifo_clk,
            opt3_in_fifo_dout => port15_in_fifo_dout,
            opt3_in_fifo_rden => port15_in_fifo_rden,
            opt3_in_fifo_rcnt => port15_in_fifo_rcnt,
            opt3_out_fifo_din => port15_out_fifo_din,
            opt3_out_fifo_wren => port15_out_fifo_wren,
            opt3_out_fifo_wcnt => port15_out_fifo_wcnt
        );
    end generate;

    -----------------------------------------------------------------
    -- Added by Maurizio Donna
    -----------------------------------------------------------------
    -- Custom signal processing

    s_rst <= not pcie_rst_n;

    comp_custom_register_array : entity work.custom_register_array
    port map(
        clk_i => bram_dma_clk,
        rst_i => s_rst,
        data_i => reg_0x400_0x4FF_wr_data,
        data_o => reg_0x400_0x4FF_rd_data,
        addr_i => reg_0x400_0x4FF_adr,
        wr_en_i => reg_0x400_0x4FF_wr_en,
        rd_en_i => reg_0x400_0x4FF_rd_en,
        r_ctrl_o => r_ctrl,
        r_stat_i => r_stat
    );

    comp_custom_signal_processing : entity work.custom_signal_processing
    port map(
        adc1_clk_i => adc1_clk,
        adc2_clk_i => adc2_clk,
        adc3_clk_i => adc3_clk,
        adc4_clk_i => adc4_clk,
        adc5_clk_i => adc5_clk,
        rst_i => s_rst,
        r_ctrl_i => r_ctrl,
        r_stat_o => r_stat,
        itl_o => s_itl,
        itl_hb_o => s_itl_hb,
        sampling_active_i => adc_chx_sample_logic_active_or,
        ch1_start_trig_i => adc_ch1_start_pulse,
        ch2_start_trig_i => adc_ch2_start_pulse,
        ch3_start_trig_i => adc_ch3_start_pulse,
        ch4_start_trig_i => adc_ch4_start_pulse,
        ch5_start_trig_i => adc_ch5_start_pulse,
        ch6_start_trig_i => adc_ch6_start_pulse,
        ch7_start_trig_i => adc_ch7_start_pulse,
        ch8_start_trig_i => adc_ch8_start_pulse,
        ch9_start_trig_i => adc_ch9_start_pulse,
        ch10_start_trig_i => adc_ch10_start_pulse,
        ch1_end_trig_i => s_adc_ch1_end_pulse,
        ch2_end_trig_i => s_adc_ch2_end_pulse,
        ch3_end_trig_i => s_adc_ch3_end_pulse,
        ch4_end_trig_i => s_adc_ch4_end_pulse,
        ch5_end_trig_i => s_adc_ch5_end_pulse,
        ch6_end_trig_i => s_adc_ch6_end_pulse,
        ch7_end_trig_i => s_adc_ch7_end_pulse,
        ch8_end_trig_i => s_adc_ch8_end_pulse,
        ch9_end_trig_i => s_adc_ch9_end_pulse,
        ch10_end_trig_i => s_adc_ch10_end_pulse,
        ch1_start_trig_o => s_ch1_start_pulse,
        ch2_start_trig_o => s_ch2_start_pulse,
        ch3_start_trig_o => s_ch3_start_pulse,
        ch4_start_trig_o => s_ch4_start_pulse,
        ch5_start_trig_o => s_ch5_start_pulse,
        ch6_start_trig_o => s_ch6_start_pulse,
        ch7_start_trig_o => s_ch7_start_pulse,
        ch8_start_trig_o => s_ch8_start_pulse,
        ch9_start_trig_o => s_ch9_start_pulse,
        ch10_start_trig_o => s_ch10_start_pulse,
        prebuf_dly_i => addr_0x12B_reg_q(11 downto 0),
        ch1_data_i => adc_ch1_ringbuffer_dout(15 downto 0),
        ch2_data_i => adc_ch2_ringbuffer_dout(15 downto 0),
        ch3_data_i => adc_ch3_ringbuffer_dout(15 downto 0),
        ch4_data_i => adc_ch4_ringbuffer_dout(15 downto 0),
        ch5_data_i => adc_ch5_ringbuffer_dout(15 downto 0),
        ch6_data_i => adc_ch6_ringbuffer_dout(15 downto 0),
        ch7_data_i => adc_ch7_ringbuffer_dout(15 downto 0),
        ch8_data_i => adc_ch8_ringbuffer_dout(15 downto 0),
        ch9_data_i => adc_ch9_ringbuffer_dout(15 downto 0),
        ch10_data_i => adc_ch10_ringbuffer_dout(15 downto 0),
        ch1_data_o => s_ch1_processed_dout,
        ch2_data_o => s_ch2_processed_dout,
        ch3_data_o => s_ch3_processed_dout,
        ch4_data_o => s_ch4_processed_dout,
        ch5_data_o => s_ch5_processed_dout,
        ch6_data_o => s_ch6_processed_dout,
        ch7_data_o => s_ch7_processed_dout,
        ch8_data_o => s_ch8_processed_dout,
        ch9_data_o => s_ch9_processed_dout,
        ch10_data_o => s_ch10_processed_dout
    );

    -----------------------------------------------------------------
    -- end Added by Maurizio Donna
    -----------------------------------------------------------------
end Behavioral;
