----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:12:10 10/26/2010 
-- Design Name: 
-- Module Name:    dual_gtp_serdes_interface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity quad_gtx_serdes_interface is
    Port (  
		rst : in std_logic;
		gtx_refclk_p : in std_logic;
		gtx_refclk_n : in std_logic;
		gtx_rx_p : in std_logic_vector(3 downto 0);
		gtx_rx_n : in std_logic_vector(3 downto 0);
		gtx_tx_p : out std_logic_vector(3 downto 0);
		gtx_tx_n : out std_logic_vector(3 downto 0);
		--
		gtx_linkup : out std_logic_vector(3 downto 0);


		serdes_reset : in std_logic;
		-- Input Fifo
		opt0_fifo_reset	: in	std_logic;
		opt0_fifo_clk	: in	std_logic;
		opt0_in_fifo_dout	: out	std_logic_vector(32 downto 0);
		opt0_in_fifo_rden	: in	std_logic;
		opt0_in_fifo_rcnt	: out	std_logic_vector(8 downto 0);
		-- Output Fifo
		opt0_out_fifo_din	: in	std_logic_vector(32 downto 0);
		opt0_out_fifo_wren	: in	std_logic;
		opt0_out_fifo_wcnt	: out	std_logic_vector(8 downto 0);

		-- Input Fifo
		opt1_fifo_reset	: in	std_logic;
		opt1_fifo_clk	: in	std_logic;
		opt1_in_fifo_dout	: out	std_logic_vector(32 downto 0);
		opt1_in_fifo_rden	: in	std_logic;
		opt1_in_fifo_rcnt	: out	std_logic_vector(8 downto 0);
		-- Output Fifo
		opt1_out_fifo_din	: in	std_logic_vector(32 downto 0);
		opt1_out_fifo_wren	: in	std_logic;
		opt1_out_fifo_wcnt	: out	std_logic_vector(8 downto 0);

		-- Input Fifo
		opt2_fifo_reset	: in	std_logic;
		opt2_fifo_clk	: in	std_logic;
		opt2_in_fifo_dout	: out	std_logic_vector(32 downto 0);
		opt2_in_fifo_rden	: in	std_logic;
		opt2_in_fifo_rcnt	: out	std_logic_vector(8 downto 0);
		-- Output Fifo
		opt2_out_fifo_din	: in	std_logic_vector(32 downto 0);
		opt2_out_fifo_wren	: in	std_logic;
		opt2_out_fifo_wcnt	: out	std_logic_vector(8 downto 0); 

		-- Input Fifo
		opt3_fifo_reset	: in	std_logic;
		opt3_fifo_clk	: in	std_logic;
		opt3_in_fifo_dout	: out	std_logic_vector(32 downto 0);
		opt3_in_fifo_rden	: in	std_logic;
		opt3_in_fifo_rcnt	: out	std_logic_vector(8 downto 0);
		-- Output Fifo
		opt3_out_fifo_din	: in	std_logic_vector(32 downto 0);
		opt3_out_fifo_wren	: in	std_logic;
		opt3_out_fifo_wcnt	: out	std_logic_vector(8 downto 0) 		

		
		  );
end quad_gtx_serdes_interface;

architecture Behavioral of quad_gtx_serdes_interface is

	component v6_gtx_quad_backplane_p2p_114 
	generic
	(
		 -- Simulation attributes
		 WRAPPER_SIM_GTXRESET_SPEEDUP    : integer   := 0 -- Set to 1 to speed up sim reset
	);
	port
	(

		 --_________________________________________________________________________
		 --_________________________________________________________________________
		 --GTX0  (X0_Y8)

		 ------------------------ Loopback and Powerdown Ports ----------------------
		 GTX0_LOOPBACK_IN                        : in   std_logic_vector(2 downto 0);
		 ----------------------- Receive Ports - 8b10b Decoder ----------------------
		 GTX0_RXCHARISK_OUT                      : out  std_logic;
		 GTX0_RXDISPERR_OUT                      : out  std_logic;
		 GTX0_RXNOTINTABLE_OUT                   : out  std_logic;
		 ------------------- Receive Ports - Clock Correction Ports -----------------
		 GTX0_RXCLKCORCNT_OUT                    : out  std_logic_vector(2 downto 0);
		 --------------- Receive Ports - Comma Detection and Alignment --------------
		 GTX0_RXCOMMADET_OUT                     : out  std_logic;
		 GTX0_RXENMCOMMAALIGN_IN                 : in   std_logic;
		 GTX0_RXENPCOMMAALIGN_IN                 : in   std_logic;
		 ------------------- Receive Ports - RX Data Path interface -----------------
		 GTX0_RXDATA_OUT                         : out  std_logic_vector(7 downto 0);
		 GTX0_RXRESET_IN                         : in   std_logic;
		 GTX0_RXUSRCLK_IN                        : in   std_logic;
		 GTX0_RXUSRCLK2_IN                       : in   std_logic;
		 ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		 GTX0_RXEQMIX_IN                         : in   std_logic_vector(2 downto 0);
		 GTX0_RXN_IN                             : in   std_logic;
		 GTX0_RXP_IN                             : in   std_logic;
		 ------------------------ Receive Ports - RX PLL Ports ----------------------
		 GTX0_GREFCLKRX_IN                       : in   std_logic;
		 GTX0_GTXRXRESET_IN                      : in   std_logic;
		 GTX0_MGTREFCLKRX_IN                     : in   std_logic_vector(1 downto 0);
		 GTX0_NORTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX0_PERFCLKRX_IN                       : in   std_logic;
		 GTX0_PLLRXRESET_IN                      : in   std_logic;
		 GTX0_RXPLLLKDET_OUT                     : out  std_logic;
		 GTX0_RXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX0_RXRESETDONE_OUT                    : out  std_logic;
		 GTX0_SOUTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 ----------------- Receive Ports - RX Polarity Control Ports ----------------
		 GTX0_RXPOLARITY_IN                      : in   std_logic;
		 ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		 GTX0_DADDR_IN                           : in   std_logic_vector(7 downto 0);
		 GTX0_DCLK_IN                            : in   std_logic;
		 GTX0_DEN_IN                             : in   std_logic;
		 GTX0_DI_IN                              : in   std_logic_vector(15 downto 0);
		 GTX0_DRDY_OUT                           : out  std_logic;
		 GTX0_DRPDO_OUT                          : out  std_logic_vector(15 downto 0);
		 GTX0_DWE_IN                             : in   std_logic;
		 ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		 GTX0_TXCHARISK_IN                       : in   std_logic;
		 ------------------ Transmit Ports - TX Data Path interface -----------------
		 GTX0_TXDATA_IN                          : in   std_logic_vector(7 downto 0);
		 GTX0_TXOUTCLK_OUT                       : out  std_logic;
		 GTX0_TXRESET_IN                         : in   std_logic;
		 GTX0_TXUSRCLK_IN                        : in   std_logic;
		 GTX0_TXUSRCLK2_IN                       : in   std_logic;
		 ---------------- Transmit Ports - TX Driver and OOB signaling --------------
		 GTX0_TXDIFFCTRL_IN                      : in   std_logic_vector(3 downto 0);
		 GTX0_TXN_OUT                            : out  std_logic;
		 GTX0_TXP_OUT                            : out  std_logic;
		 GTX0_TXPOSTEMPHASIS_IN                  : in   std_logic_vector(4 downto 0);
		 --------------- Transmit Ports - TX Driver and OOB signalling --------------
		 GTX0_TXPREEMPHASIS_IN                   : in   std_logic_vector(3 downto 0);
		 ----------------------- Transmit Ports - TX PLL Ports ----------------------
		 GTX0_GREFCLKTX_IN                       : in   std_logic;
		 GTX0_GTXTXRESET_IN                      : in   std_logic;
		 GTX0_NORTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX0_PERFCLKTX_IN                       : in   std_logic;
		 GTX0_SOUTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX0_TXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX0_TXRESETDONE_OUT                    : out  std_logic;
		 -------------------- Transmit Ports - TX Polarity Control ------------------
		 GTX0_TXPOLARITY_IN                      : in   std_logic;



		 --_________________________________________________________________________
		 --_________________________________________________________________________
		 --GTX1  (X0_Y9)

		 ------------------------ Loopback and Powerdown Ports ----------------------
		 GTX1_LOOPBACK_IN                        : in   std_logic_vector(2 downto 0);
		 ----------------------- Receive Ports - 8b10b Decoder ----------------------
		 GTX1_RXCHARISK_OUT                      : out  std_logic;
		 GTX1_RXDISPERR_OUT                      : out  std_logic;
		 GTX1_RXNOTINTABLE_OUT                   : out  std_logic;
		 ------------------- Receive Ports - Clock Correction Ports -----------------
		 GTX1_RXCLKCORCNT_OUT                    : out  std_logic_vector(2 downto 0);
		 --------------- Receive Ports - Comma Detection and Alignment --------------
		 GTX1_RXCOMMADET_OUT                     : out  std_logic;
		 GTX1_RXENMCOMMAALIGN_IN                 : in   std_logic;
		 GTX1_RXENPCOMMAALIGN_IN                 : in   std_logic;
		 ------------------- Receive Ports - RX Data Path interface -----------------
		 GTX1_RXDATA_OUT                         : out  std_logic_vector(7 downto 0);
		 GTX1_RXRESET_IN                         : in   std_logic;
		 GTX1_RXUSRCLK_IN                        : in   std_logic;
		 GTX1_RXUSRCLK2_IN                       : in   std_logic;
		 ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		 GTX1_RXEQMIX_IN                         : in   std_logic_vector(2 downto 0);
		 GTX1_RXN_IN                             : in   std_logic;
		 GTX1_RXP_IN                             : in   std_logic;
		 ------------------------ Receive Ports - RX PLL Ports ----------------------
		 GTX1_GREFCLKRX_IN                       : in   std_logic;
		 GTX1_GTXRXRESET_IN                      : in   std_logic;
		 GTX1_MGTREFCLKRX_IN                     : in   std_logic_vector(1 downto 0);
		 GTX1_NORTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX1_PERFCLKRX_IN                       : in   std_logic;
		 GTX1_PLLRXRESET_IN                      : in   std_logic;
		 GTX1_RXPLLLKDET_OUT                     : out  std_logic;
		 GTX1_RXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX1_RXRESETDONE_OUT                    : out  std_logic;
		 GTX1_SOUTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 ----------------- Receive Ports - RX Polarity Control Ports ----------------
		 GTX1_RXPOLARITY_IN                      : in   std_logic;
		 ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		 GTX1_DADDR_IN                           : in   std_logic_vector(7 downto 0);
		 GTX1_DCLK_IN                            : in   std_logic;
		 GTX1_DEN_IN                             : in   std_logic;
		 GTX1_DI_IN                              : in   std_logic_vector(15 downto 0);
		 GTX1_DRDY_OUT                           : out  std_logic;
		 GTX1_DRPDO_OUT                          : out  std_logic_vector(15 downto 0);
		 GTX1_DWE_IN                             : in   std_logic;
		 ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		 GTX1_TXCHARISK_IN                       : in   std_logic;
		 ------------------ Transmit Ports - TX Data Path interface -----------------
		 GTX1_TXDATA_IN                          : in   std_logic_vector(7 downto 0);
		 GTX1_TXOUTCLK_OUT                       : out  std_logic;
		 GTX1_TXRESET_IN                         : in   std_logic;
		 GTX1_TXUSRCLK_IN                        : in   std_logic;
		 GTX1_TXUSRCLK2_IN                       : in   std_logic;
		 ---------------- Transmit Ports - TX Driver and OOB signaling --------------
		 GTX1_TXDIFFCTRL_IN                      : in   std_logic_vector(3 downto 0);
		 GTX1_TXN_OUT                            : out  std_logic;
		 GTX1_TXP_OUT                            : out  std_logic;
		 GTX1_TXPOSTEMPHASIS_IN                  : in   std_logic_vector(4 downto 0);
		 --------------- Transmit Ports - TX Driver and OOB signalling --------------
		 GTX1_TXPREEMPHASIS_IN                   : in   std_logic_vector(3 downto 0);
		 ----------------------- Transmit Ports - TX PLL Ports ----------------------
		 GTX1_GREFCLKTX_IN                       : in   std_logic;
		 GTX1_GTXTXRESET_IN                      : in   std_logic;
		 GTX1_NORTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX1_PERFCLKTX_IN                       : in   std_logic;
		 GTX1_SOUTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX1_TXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX1_TXRESETDONE_OUT                    : out  std_logic;
		 -------------------- Transmit Ports - TX Polarity Control ------------------
		 GTX1_TXPOLARITY_IN                      : in   std_logic;



		 --_________________________________________________________________________
		 --_________________________________________________________________________
		 --GTX2  (X0_Y10)

		 ------------------------ Loopback and Powerdown Ports ----------------------
		 GTX2_LOOPBACK_IN                        : in   std_logic_vector(2 downto 0);
		 ----------------------- Receive Ports - 8b10b Decoder ----------------------
		 GTX2_RXCHARISK_OUT                      : out  std_logic;
		 GTX2_RXDISPERR_OUT                      : out  std_logic;
		 GTX2_RXNOTINTABLE_OUT                   : out  std_logic;
		 ------------------- Receive Ports - Clock Correction Ports -----------------
		 GTX2_RXCLKCORCNT_OUT                    : out  std_logic_vector(2 downto 0);
		 --------------- Receive Ports - Comma Detection and Alignment --------------
		 GTX2_RXCOMMADET_OUT                     : out  std_logic;
		 GTX2_RXENMCOMMAALIGN_IN                 : in   std_logic;
		 GTX2_RXENPCOMMAALIGN_IN                 : in   std_logic;
		 ------------------- Receive Ports - RX Data Path interface -----------------
		 GTX2_RXDATA_OUT                         : out  std_logic_vector(7 downto 0);
		 GTX2_RXRESET_IN                         : in   std_logic;
		 GTX2_RXUSRCLK_IN                        : in   std_logic;
		 GTX2_RXUSRCLK2_IN                       : in   std_logic;
		 ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		 GTX2_RXEQMIX_IN                         : in   std_logic_vector(2 downto 0);
		 GTX2_RXN_IN                             : in   std_logic;
		 GTX2_RXP_IN                             : in   std_logic;
		 ------------------------ Receive Ports - RX PLL Ports ----------------------
		 GTX2_GREFCLKRX_IN                       : in   std_logic;
		 GTX2_GTXRXRESET_IN                      : in   std_logic;
		 GTX2_MGTREFCLKRX_IN                     : in   std_logic_vector(1 downto 0);
		 GTX2_NORTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX2_PERFCLKRX_IN                       : in   std_logic;
		 GTX2_PLLRXRESET_IN                      : in   std_logic;
		 GTX2_RXPLLLKDET_OUT                     : out  std_logic;
		 GTX2_RXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX2_RXRESETDONE_OUT                    : out  std_logic;
		 GTX2_SOUTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 ----------------- Receive Ports - RX Polarity Control Ports ----------------
		 GTX2_RXPOLARITY_IN                      : in   std_logic;
		 ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		 GTX2_DADDR_IN                           : in   std_logic_vector(7 downto 0);
		 GTX2_DCLK_IN                            : in   std_logic;
		 GTX2_DEN_IN                             : in   std_logic;
		 GTX2_DI_IN                              : in   std_logic_vector(15 downto 0);
		 GTX2_DRDY_OUT                           : out  std_logic;
		 GTX2_DRPDO_OUT                          : out  std_logic_vector(15 downto 0);
		 GTX2_DWE_IN                             : in   std_logic;
		 ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		 GTX2_TXCHARISK_IN                       : in   std_logic;
		 ------------------ Transmit Ports - TX Data Path interface -----------------
		 GTX2_TXDATA_IN                          : in   std_logic_vector(7 downto 0);
		 GTX2_TXOUTCLK_OUT                       : out  std_logic;
		 GTX2_TXRESET_IN                         : in   std_logic;
		 GTX2_TXUSRCLK_IN                        : in   std_logic;
		 GTX2_TXUSRCLK2_IN                       : in   std_logic;
		 ---------------- Transmit Ports - TX Driver and OOB signaling --------------
		 GTX2_TXDIFFCTRL_IN                      : in   std_logic_vector(3 downto 0);
		 GTX2_TXN_OUT                            : out  std_logic;
		 GTX2_TXP_OUT                            : out  std_logic;
		 GTX2_TXPOSTEMPHASIS_IN                  : in   std_logic_vector(4 downto 0);
		 --------------- Transmit Ports - TX Driver and OOB signalling --------------
		 GTX2_TXPREEMPHASIS_IN                   : in   std_logic_vector(3 downto 0);
		 ----------------------- Transmit Ports - TX PLL Ports ----------------------
		 GTX2_GREFCLKTX_IN                       : in   std_logic;
		 GTX2_GTXTXRESET_IN                      : in   std_logic;
		 GTX2_NORTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX2_PERFCLKTX_IN                       : in   std_logic;
		 GTX2_SOUTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX2_TXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX2_TXRESETDONE_OUT                    : out  std_logic;
		 -------------------- Transmit Ports - TX Polarity Control ------------------
		 GTX2_TXPOLARITY_IN                      : in   std_logic;



		 --_________________________________________________________________________
		 --_________________________________________________________________________
		 --GTX3  (X0_Y11)

		 ------------------------ Loopback and Powerdown Ports ----------------------
		 GTX3_LOOPBACK_IN                        : in   std_logic_vector(2 downto 0);
		 ----------------------- Receive Ports - 8b10b Decoder ----------------------
		 GTX3_RXCHARISK_OUT                      : out  std_logic;
		 GTX3_RXDISPERR_OUT                      : out  std_logic;
		 GTX3_RXNOTINTABLE_OUT                   : out  std_logic;
		 ------------------- Receive Ports - Clock Correction Ports -----------------
		 GTX3_RXCLKCORCNT_OUT                    : out  std_logic_vector(2 downto 0);
		 --------------- Receive Ports - Comma Detection and Alignment --------------
		 GTX3_RXCOMMADET_OUT                     : out  std_logic;
		 GTX3_RXENMCOMMAALIGN_IN                 : in   std_logic;
		 GTX3_RXENPCOMMAALIGN_IN                 : in   std_logic;
		 ------------------- Receive Ports - RX Data Path interface -----------------
		 GTX3_RXDATA_OUT                         : out  std_logic_vector(7 downto 0);
		 GTX3_RXRESET_IN                         : in   std_logic;
		 GTX3_RXUSRCLK_IN                        : in   std_logic;
		 GTX3_RXUSRCLK2_IN                       : in   std_logic;
		 ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		 GTX3_RXEQMIX_IN                         : in   std_logic_vector(2 downto 0);
		 GTX3_RXN_IN                             : in   std_logic;
		 GTX3_RXP_IN                             : in   std_logic;
		 ------------------------ Receive Ports - RX PLL Ports ----------------------
		 GTX3_GREFCLKRX_IN                       : in   std_logic;
		 GTX3_GTXRXRESET_IN                      : in   std_logic;
		 GTX3_MGTREFCLKRX_IN                     : in   std_logic_vector(1 downto 0);
		 GTX3_NORTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX3_PERFCLKRX_IN                       : in   std_logic;
		 GTX3_PLLRXRESET_IN                      : in   std_logic;
		 GTX3_RXPLLLKDET_OUT                     : out  std_logic;
		 GTX3_RXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX3_RXRESETDONE_OUT                    : out  std_logic;
		 GTX3_SOUTHREFCLKRX_IN                   : in   std_logic_vector(1 downto 0);
		 ----------------- Receive Ports - RX Polarity Control Ports ----------------
		 GTX3_RXPOLARITY_IN                      : in   std_logic;
		 ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		 GTX3_DADDR_IN                           : in   std_logic_vector(7 downto 0);
		 GTX3_DCLK_IN                            : in   std_logic;
		 GTX3_DEN_IN                             : in   std_logic;
		 GTX3_DI_IN                              : in   std_logic_vector(15 downto 0);
		 GTX3_DRDY_OUT                           : out  std_logic;
		 GTX3_DRPDO_OUT                          : out  std_logic_vector(15 downto 0);
		 GTX3_DWE_IN                             : in   std_logic;
		 ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		 GTX3_TXCHARISK_IN                       : in   std_logic;
		 ------------------ Transmit Ports - TX Data Path interface -----------------
		 GTX3_TXDATA_IN                          : in   std_logic_vector(7 downto 0);
		 GTX3_TXOUTCLK_OUT                       : out  std_logic;
		 GTX3_TXRESET_IN                         : in   std_logic;
		 GTX3_TXUSRCLK_IN                        : in   std_logic;
		 GTX3_TXUSRCLK2_IN                       : in   std_logic;
		 ---------------- Transmit Ports - TX Driver and OOB signaling --------------
		 GTX3_TXDIFFCTRL_IN                      : in   std_logic_vector(3 downto 0);
		 GTX3_TXN_OUT                            : out  std_logic;
		 GTX3_TXP_OUT                            : out  std_logic;
		 GTX3_TXPOSTEMPHASIS_IN                  : in   std_logic_vector(4 downto 0);
		 --------------- Transmit Ports - TX Driver and OOB signalling --------------
		 GTX3_TXPREEMPHASIS_IN                   : in   std_logic_vector(3 downto 0);
		 ----------------------- Transmit Ports - TX PLL Ports ----------------------
		 GTX3_GREFCLKTX_IN                       : in   std_logic;
		 GTX3_GTXTXRESET_IN                      : in   std_logic;
		 GTX3_NORTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX3_PERFCLKTX_IN                       : in   std_logic;
		 GTX3_SOUTHREFCLKTX_IN                   : in   std_logic_vector(1 downto 0);
		 GTX3_TXPLLREFSELDY_IN                   : in   std_logic_vector(2 downto 0);
		 GTX3_TXRESETDONE_OUT                    : out  std_logic;
		 -------------------- Transmit Ports - TX Polarity Control ------------------
		 GTX3_TXPOLARITY_IN                      : in   std_logic


	);
	end component;
	
	component MGT_USRCLK_SOURCE 
	generic
	(
		 FREQUENCY_MODE   : string   := "LOW";    
		 PERFORMANCE_MODE : string   := "MAX_SPEED"    
	);
	port
	(
		 DIV1_OUT                : out std_logic;
		 DIV2_OUT                : out std_logic;
		 DCM_LOCKED_OUT          : out std_logic;
		 CLK_IN                  : in  std_logic;
		 DCM_RESET_IN            : in  std_logic

	);
	end component;
	
	component MGT_USRCLK_SOURCE_MMCM
	generic
	(
		 MULT                 : real             := 2.0;
		 DIVIDE               : integer          := 2;    
		 CLK_PERIOD           : real             := 6.4;    
		 OUT0_DIVIDE          : real             := 2.0;
		 OUT1_DIVIDE          : integer          := 2;
		 OUT2_DIVIDE          : integer          := 2;
		 OUT3_DIVIDE          : integer          := 2
	);
	port
	(
		 CLKFBOUT                : out std_logic; 
		 CLK0_OUT                : out std_logic;
		 CLK1_OUT                : out std_logic;
		 CLK2_OUT                : out std_logic;
		 CLK3_OUT                : out std_logic;
		 CLK_IN                  : in  std_logic;
		 MMCM_LOCKED_OUT         : out std_logic;
		 MMCM_RESET_IN           : in  std_logic
	);
	end component;



	COMPONENT serdes_top
	PORT(
		RX_DATA : IN std_logic_vector(7 downto 0);
		RX_CHARISK : IN std_logic;
		RX_DERR : IN std_logic;
		RX_OOT : IN std_logic;
		SYS_CLK : IN std_logic;
		SYS_CLK_HALF : IN std_logic;
		SYS_CLK_LOCKED : IN std_logic;
		IN_FIFO_RESET : IN std_logic;
		IN_FIFO_RDCLK : IN std_logic;
		IN_FIFO_RDEN : IN std_logic;
		OUT_FIFO_DIN : IN std_logic_vector(32 downto 0);
		OUT_FIFO_WRCLK : IN std_logic;
		OUT_FIFO_WREN : IN std_logic;
		I_OPT_CTRL_0 : IN std_logic;
		I_OPT_CTRL_4 : IN std_logic;
		I_OPT_CTRL_5 : IN std_logic;
		I_OPT_CTRL_6 : IN std_logic;
		I_OPT_CTRL_11 : IN std_logic;
		I_OPT_CTRL_18 : IN std_logic;
		I_OPT_CTRL_19 : IN std_logic;
		I_OPT_CTRL_20 : IN std_logic;
		I_OPT_CTRL_21 : IN std_logic;
		I_OPT_CTRL_22 : IN std_logic;       
		TX_DATA : OUT std_logic_vector(7 downto 0);
		TX_CHARISK : OUT std_logic;
		IN_FIFO_DOUT : OUT std_logic_vector(32 downto 0);
		IN_FIFO_RCNT : OUT std_logic_vector(8 downto 0);
		OUT_FIFO_WCNT : OUT std_logic_vector(8 downto 0);
		O_OPT_CTRL_1 : OUT std_logic;
		O_OPT_CTRL_2 : OUT std_logic;
		O_OPT_CTRL_3 : OUT std_logic;
		O_OPT_CTRL_7 : OUT std_logic;
		O_OPT_CTRL_8 : OUT std_logic;
		O_OPT_CTRL_9 : OUT std_logic;
		O_OPT_CTRL_10 : OUT std_logic;
		O_OPT_CTRL_12 : OUT std_logic;
		O_OPT_CTRL_13 : OUT std_logic;
		O_OPT_CTRL_14 : OUT std_logic;
		O_OPT_CTRL_15 : OUT std_logic;
		O_OPT_CTRL_16 : OUT std_logic;
		O_OPT_CTRL_17 : OUT std_logic;
		P_LED : OUT std_logic_vector(7 downto 0);
		F_LED_ACTIVE : OUT std_logic;
		F_LED_LINK_OK : OUT std_logic;
		F_LED_D_UP : OUT std_logic;
		F_LED_D_DOWN : OUT std_logic;
		F_LED_USER_0 : OUT std_logic;
		F_LED_USER_1 : OUT std_logic;
		WATCHDOG : OUT std_logic;
		--
		remote_kchar_start_pulse : in std_logic;
		remote_kchar_stop_pulse : in std_logic;
		remote_kchar_error_pulse : in std_logic;
		GET_REMOTE_ERROR_PULS			: out  STD_LOGIC
		);
	END COMPONENT;

	signal tied8_to_gnd : std_logic_vector(7 downto 0) := (others => '0');
	signal tied8_to_vcc : std_logic_vector(7 downto 0) := (others => '1');
	signal tied33_to_gnd : std_logic_vector(32 downto 0) := (others => '0');
	signal tied1_to_vcc : std_logic_vector(0 downto 0) := (others => '1');
	
	signal   gtx0_txresetdone_r2             : std_logic;
	signal   gtx0_rxresetdone_i_r            : std_logic;
	signal   gtx0_rxresetdone_r              : std_logic;
	signal   gtx0_rxresetdone_r2             : std_logic;
	signal   gtx0_rxresetdone_r3             : std_logic;
	signal   gtx0_rxdata_r                   : std_logic_vector(7 downto 0);
	signal   gtx0_rxcharisk_r                : std_logic_vector(0 downto 0);    
	signal   gtx1_txresetdone_r              : std_logic;
	signal   gtx1_txresetdone_r2             : std_logic;
	signal   gtx1_rxresetdone_i_r            : std_logic;
	signal   gtx1_rxresetdone_r              : std_logic;
	signal   gtx1_rxresetdone_r2             : std_logic;
	signal   gtx1_rxresetdone_r3             : std_logic;
	signal   gtx1_rxdata_r                   : std_logic_vector(7 downto 0);
	signal   gtx1_rxcharisk_r                : std_logic_vector(0 downto 0);    
	signal   gtx2_txresetdone_r              : std_logic;
	signal   gtx2_txresetdone_r2             : std_logic;
	signal   gtx2_rxresetdone_i_r            : std_logic;
	signal   gtx2_rxresetdone_r              : std_logic;
	signal   gtx2_rxresetdone_r2             : std_logic;
	signal   gtx2_rxresetdone_r3             : std_logic;
	signal   gtx2_rxdata_r                   : std_logic_vector(7 downto 0);
	signal   gtx2_rxcharisk_r                : std_logic_vector(0 downto 0);    
	signal   gtx3_txresetdone_r              : std_logic;
	signal   gtx3_txresetdone_r2             : std_logic;
	signal   gtx3_rxresetdone_i_r            : std_logic;
	signal   gtx3_rxresetdone_r              : std_logic;
	signal   gtx3_rxresetdone_r2             : std_logic;
	signal   gtx3_rxresetdone_r3             : std_logic;
	signal   gtx3_rxdata_r                   : std_logic_vector(7 downto 0);
	signal   gtx3_rxcharisk_r                : std_logic_vector(0 downto 0);    


--**************************** Wire Declarations ******************************
    -------------------------- MGT Wrapper Wires ------------------------------
    --________________________________________________________________________
    --________________________________________________________________________
    --GTX0   (X0Y8)

    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    signal  gtx0_rxcharisk_i                : std_logic;
    signal  gtx0_rxdisperr_i                : std_logic;
    signal  gtx0_rxnotintable_i             : std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    signal  gtx0_rxcommadet_i               : std_logic;
    signal  gtx0_rxenmcommaalign_i          : std_logic;
    signal  gtx0_rxenpcommaalign_i          : std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  gtx0_rxdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx0_rxreset_i                  : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  gtx0_rxeqmix_i                  : std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    signal  gtx0_gtxrxreset_i               : std_logic;
    signal  gtx0_pllrxreset_i               : std_logic;
    signal  gtx0_rxplllkdet_i               : std_logic;
    signal  gtx0_rxpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx0_rxresetdone_i              : std_logic;
    ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
    signal  gtx0_daddr_i                    : std_logic_vector(7 downto 0);
    signal  gtx0_dclk_i                     : std_logic;
    signal  gtx0_den_i                      : std_logic;
    signal  gtx0_di_i                       : std_logic_vector(15 downto 0);
    signal  gtx0_drdy_i                     : std_logic;
    signal  gtx0_drpdo_i                    : std_logic_vector(15 downto 0);
    signal  gtx0_dwe_i                      : std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    signal  gtx0_txcharisk_i                : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  gtx0_txdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx0_txoutclk_i                 : std_logic;
    signal  gtx0_txreset_i                  : std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    signal  gtx0_txdiffctrl_i               : std_logic_vector(3 downto 0);
    signal  gtx0_txpostemphasis_i           : std_logic_vector(4 downto 0);
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  gtx0_txpreemphasis_i            : std_logic_vector(3 downto 0);
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    signal  gtx0_gtxtxreset_i               : std_logic;
    signal  gtx0_txpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx0_txresetdone_i              : std_logic;


    --________________________________________________________________________
    --________________________________________________________________________
    --GTX1   (X0Y9)

    ------------------------ Loopback and Powerdown Ports ----------------------
    signal  gtx1_loopback_i                 : std_logic_vector(2 downto 0);
    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    signal  gtx1_rxcharisk_i                : std_logic;
    signal  gtx1_rxdisperr_i                : std_logic;
    signal  gtx1_rxnotintable_i             : std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    signal  gtx1_rxclkcorcnt_i              : std_logic_vector(2 downto 0);
    --------------- Receive Ports - Comma Detection and Alignment --------------
    signal  gtx1_rxcommadet_i               : std_logic;
    signal  gtx1_rxenmcommaalign_i          : std_logic;
    signal  gtx1_rxenpcommaalign_i          : std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  gtx1_rxdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx1_rxreset_i                  : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  gtx1_rxeqmix_i                  : std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    signal  gtx1_gtxrxreset_i               : std_logic;
    signal  gtx1_pllrxreset_i               : std_logic;
    signal  gtx1_rxplllkdet_i               : std_logic;
    signal  gtx1_rxpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx1_rxresetdone_i              : std_logic;
    ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
    signal  gtx1_daddr_i                    : std_logic_vector(7 downto 0);
    signal  gtx1_dclk_i                     : std_logic;
    signal  gtx1_den_i                      : std_logic;
    signal  gtx1_di_i                       : std_logic_vector(15 downto 0);
    signal  gtx1_drdy_i                     : std_logic;
    signal  gtx1_drpdo_i                    : std_logic_vector(15 downto 0);
    signal  gtx1_dwe_i                      : std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    signal  gtx1_txcharisk_i                : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  gtx1_txdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx1_txoutclk_i                 : std_logic;
    signal  gtx1_txreset_i                  : std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    signal  gtx1_txdiffctrl_i               : std_logic_vector(3 downto 0);
    signal  gtx1_txpostemphasis_i           : std_logic_vector(4 downto 0);
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  gtx1_txpreemphasis_i            : std_logic_vector(3 downto 0);
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    signal  gtx1_gtxtxreset_i               : std_logic;
    signal  gtx1_txpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx1_txresetdone_i              : std_logic;


    --________________________________________________________________________
    --________________________________________________________________________
    --GTX2   (X0Y10)

    ------------------------ Loopback and Powerdown Ports ----------------------
    signal  gtx2_loopback_i                 : std_logic_vector(2 downto 0);
    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    signal  gtx2_rxcharisk_i                : std_logic;
    signal  gtx2_rxdisperr_i                : std_logic;
    signal  gtx2_rxnotintable_i             : std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    signal  gtx2_rxclkcorcnt_i              : std_logic_vector(2 downto 0);
    --------------- Receive Ports - Comma Detection and Alignment --------------
    signal  gtx2_rxcommadet_i               : std_logic;
    signal  gtx2_rxenmcommaalign_i          : std_logic;
    signal  gtx2_rxenpcommaalign_i          : std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  gtx2_rxdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx2_rxreset_i                  : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  gtx2_rxeqmix_i                  : std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    signal  gtx2_gtxrxreset_i               : std_logic;
    signal  gtx2_pllrxreset_i               : std_logic;
    signal  gtx2_rxplllkdet_i               : std_logic;
    signal  gtx2_rxpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx2_rxresetdone_i              : std_logic;
    ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
    signal  gtx2_daddr_i                    : std_logic_vector(7 downto 0);
    signal  gtx2_dclk_i                     : std_logic;
    signal  gtx2_den_i                      : std_logic;
    signal  gtx2_di_i                       : std_logic_vector(15 downto 0);
    signal  gtx2_drdy_i                     : std_logic;
    signal  gtx2_drpdo_i                    : std_logic_vector(15 downto 0);
    signal  gtx2_dwe_i                      : std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    signal  gtx2_txcharisk_i                : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  gtx2_txdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx2_txoutclk_i                 : std_logic;
    signal  gtx2_txreset_i                  : std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    signal  gtx2_txdiffctrl_i               : std_logic_vector(3 downto 0);
    signal  gtx2_txpostemphasis_i           : std_logic_vector(4 downto 0);
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  gtx2_txpreemphasis_i            : std_logic_vector(3 downto 0);
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    signal  gtx2_gtxtxreset_i               : std_logic;
    signal  gtx2_txpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx2_txresetdone_i              : std_logic;


    --________________________________________________________________________
    --________________________________________________________________________
    --GTX3   (X0Y11)

    ------------------------ Loopback and Powerdown Ports ----------------------
    signal  gtx3_loopback_i                 : std_logic_vector(2 downto 0);
    ----------------------- Receive Ports - 8b10b Decoder ----------------------
    signal  gtx3_rxcharisk_i                : std_logic;
    signal  gtx3_rxdisperr_i                : std_logic;
    signal  gtx3_rxnotintable_i             : std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    signal  gtx3_rxclkcorcnt_i              : std_logic_vector(2 downto 0);
    --------------- Receive Ports - Comma Detection and Alignment --------------
    signal  gtx3_rxcommadet_i               : std_logic;
    signal  gtx3_rxenmcommaalign_i          : std_logic;
    signal  gtx3_rxenpcommaalign_i          : std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  gtx3_rxdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx3_rxreset_i                  : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  gtx3_rxeqmix_i                  : std_logic_vector(2 downto 0);
    ------------------------ Receive Ports - RX PLL Ports ----------------------
    signal  gtx3_gtxrxreset_i               : std_logic;
    signal  gtx3_pllrxreset_i               : std_logic;
    signal  gtx3_rxplllkdet_i               : std_logic;
    signal  gtx3_rxpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx3_rxresetdone_i              : std_logic;
    ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
    signal  gtx3_daddr_i                    : std_logic_vector(7 downto 0);
    signal  gtx3_dclk_i                     : std_logic;
    signal  gtx3_den_i                      : std_logic;
    signal  gtx3_di_i                       : std_logic_vector(15 downto 0);
    signal  gtx3_drdy_i                     : std_logic;
    signal  gtx3_drpdo_i                    : std_logic_vector(15 downto 0);
    signal  gtx3_dwe_i                      : std_logic;
    ---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
    signal  gtx3_txcharisk_i                : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  gtx3_txdata_i                   : std_logic_vector(7 downto 0);
    signal  gtx3_txoutclk_i                 : std_logic;
    signal  gtx3_txreset_i                  : std_logic;
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    signal  gtx3_txdiffctrl_i               : std_logic_vector(3 downto 0);
    signal  gtx3_txpostemphasis_i           : std_logic_vector(4 downto 0);
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  gtx3_txpreemphasis_i            : std_logic_vector(3 downto 0);
    ----------------------- Transmit Ports - TX PLL Ports ----------------------
    signal  gtx3_gtxtxreset_i               : std_logic;
    signal  gtx3_txpllrefseldy_i            : std_logic_vector(2 downto 0);
    signal  gtx3_txresetdone_i              : std_logic;




    signal  gtx0_tx_system_reset_c          : std_logic;
    signal  gtx0_rx_system_reset_c          : std_logic;
    signal  gtx1_tx_system_reset_c          : std_logic;
    signal  gtx1_rx_system_reset_c          : std_logic;
    signal  gtx2_tx_system_reset_c          : std_logic;
    signal  gtx2_rx_system_reset_c          : std_logic;
    signal  gtx3_tx_system_reset_c          : std_logic;
    signal  gtx3_rx_system_reset_c          : std_logic;
    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);
    signal  drp_clk_in_i                    : std_logic;
 

    ----------------------------- User Clocks ---------------------------------

    signal  gtx0_txusrclk_i                 : std_logic;
    signal  gtx0_txusrclk2_i                : std_logic;
	 signal  gtx0_txusrclk2_half_i : std_logic;
    signal  txoutclk_mmcm0_locked_i         : std_logic;
    signal  txoutclk_mmcm0_reset_i          : std_logic;
    signal  gtx0_txoutclk_to_mmcm_i         : std_logic;


    ----------------------------- Reference Clocks ----------------------------
    
    signal    q2_clk0_refclk_i                : std_logic;
    signal    q2_clk0_refclk_i_i              : std_logic_vector(1 downto 0);

begin

	--  Static signal Assigments
	tied_to_ground_i                             <= '0';
	tied_to_ground_vec_i                         <= x"0000000000000000";
	tied_to_vcc_i                                <= '1';
	tied_to_vcc_vec_i                            <= x"ff";


	Serdes_opt0: serdes_top PORT MAP(
		TX_DATA => gtx0_txdata_i,
		TX_CHARISK => gtx0_txcharisk_i,
		RX_DATA => gtx0_rxdata_i,
		RX_CHARISK => gtx0_rxcharisk_i,
		RX_DERR => gtx0_rxdisperr_i,
		RX_OOT => gtx0_rxnotintable_i,
		SYS_CLK => gtx0_txusrclk2_i,
		SYS_CLK_HALF => gtx0_txusrclk2_half_i,
		SYS_CLK_LOCKED => txoutclk_mmcm0_locked_i,
		IN_FIFO_RESET => opt0_fifo_reset,
		IN_FIFO_DOUT => opt0_in_fifo_dout,
		IN_FIFO_RDCLK => opt0_fifo_clk,
		IN_FIFO_RDEN => opt0_in_fifo_rden,
		IN_FIFO_RCNT => opt0_in_fifo_rcnt,
		OUT_FIFO_DIN => opt0_out_fifo_din,
		OUT_FIFO_WRCLK => opt0_fifo_clk,
		OUT_FIFO_WREN =>  opt0_out_fifo_wren,
		OUT_FIFO_WCNT => opt0_out_fifo_wcnt,
		I_OPT_CTRL_0 => not serdes_reset,
		O_OPT_CTRL_1 => open,
		O_OPT_CTRL_2 => open,
		O_OPT_CTRL_3 => open,
		I_OPT_CTRL_4 => tied1_to_vcc(0),
		I_OPT_CTRL_5 => tied1_to_vcc(0),
		I_OPT_CTRL_6 => tied1_to_vcc(0),
		O_OPT_CTRL_7 => open,
		O_OPT_CTRL_8 => open,
		O_OPT_CTRL_9 => open,
		O_OPT_CTRL_10 => open,
		I_OPT_CTRL_11 => tied33_to_gnd(0),
		O_OPT_CTRL_12 => open,
		O_OPT_CTRL_13 => open,
		O_OPT_CTRL_14 => open,
		O_OPT_CTRL_15 => open,
		O_OPT_CTRL_16 => open,
		O_OPT_CTRL_17 => open,
		I_OPT_CTRL_18 => tied33_to_gnd(0),
		I_OPT_CTRL_19 => tied33_to_gnd(0),
		I_OPT_CTRL_20 => tied33_to_gnd(0),
		I_OPT_CTRL_21 => tied33_to_gnd(0),
		I_OPT_CTRL_22 => tied33_to_gnd(0),
		P_LED => open,
		F_LED_ACTIVE => open,
		F_LED_LINK_OK => gtx_linkup(0),
		F_LED_D_UP => open,
		F_LED_D_DOWN => open,
		F_LED_USER_0 => open,
		F_LED_USER_1 => open,
		WATCHDOG => open,
		--
		remote_kchar_start_pulse => '0',
		remote_kchar_stop_pulse => '0',
		remote_kchar_error_pulse => '0',
		GET_REMOTE_ERROR_PULS => open
	);





	Serdes_opt1: serdes_top PORT MAP(
		TX_DATA => gtx1_txdata_i,
		TX_CHARISK => gtx1_txcharisk_i,
		RX_DATA => gtx1_rxdata_i,
		RX_CHARISK => gtx1_rxcharisk_i,
		RX_DERR => gtx1_rxdisperr_i,
		RX_OOT => gtx1_rxnotintable_i,
		SYS_CLK => gtx0_txusrclk2_i,
		SYS_CLK_HALF => gtx0_txusrclk2_half_i,
		SYS_CLK_LOCKED => txoutclk_mmcm0_locked_i,
		IN_FIFO_RESET => opt1_fifo_reset,
		IN_FIFO_DOUT => opt1_in_fifo_dout,
		IN_FIFO_RDCLK => opt1_fifo_clk,
		IN_FIFO_RDEN => opt1_in_fifo_rden,
		IN_FIFO_RCNT => opt1_in_fifo_rcnt,
		OUT_FIFO_DIN => opt1_out_fifo_din,
		OUT_FIFO_WRCLK => opt1_fifo_clk,
		OUT_FIFO_WREN =>  opt1_out_fifo_wren,
		OUT_FIFO_WCNT => opt1_out_fifo_wcnt,
		I_OPT_CTRL_0 => not serdes_reset,
		O_OPT_CTRL_1 => open,
		O_OPT_CTRL_2 => open,
		O_OPT_CTRL_3 => open,
		I_OPT_CTRL_4 => tied1_to_vcc(0),
		I_OPT_CTRL_5 => tied1_to_vcc(0),
		I_OPT_CTRL_6 => tied1_to_vcc(0),
		O_OPT_CTRL_7 => open,
		O_OPT_CTRL_8 => open,
		O_OPT_CTRL_9 => open,
		O_OPT_CTRL_10 => open,
		I_OPT_CTRL_11 => tied33_to_gnd(0),
		O_OPT_CTRL_12 => open,
		O_OPT_CTRL_13 => open,
		O_OPT_CTRL_14 => open,
		O_OPT_CTRL_15 => open,
		O_OPT_CTRL_16 => open,
		O_OPT_CTRL_17 => open,
		I_OPT_CTRL_18 => tied33_to_gnd(0),
		I_OPT_CTRL_19 => tied33_to_gnd(0),
		I_OPT_CTRL_20 => tied33_to_gnd(0),
		I_OPT_CTRL_21 => tied33_to_gnd(0),
		I_OPT_CTRL_22 => tied33_to_gnd(0),
		P_LED => open,
		F_LED_ACTIVE => open,
		F_LED_LINK_OK => gtx_linkup(1),
		F_LED_D_UP => open,
		F_LED_D_DOWN => open,
		F_LED_USER_0 => open,
		F_LED_USER_1 => open,
		WATCHDOG => open,
		--
		remote_kchar_start_pulse => '0',
		remote_kchar_stop_pulse => '0',
		remote_kchar_error_pulse => '0',
		GET_REMOTE_ERROR_PULS => open
	);
	
	Serdes_opt2: serdes_top PORT MAP(
		TX_DATA => gtx2_txdata_i,
		TX_CHARISK => gtx2_txcharisk_i,
		RX_DATA => gtx2_rxdata_i,
		RX_CHARISK => gtx2_rxcharisk_i,
		RX_DERR => gtx2_rxdisperr_i,
		RX_OOT => gtx2_rxnotintable_i,
		SYS_CLK => gtx0_txusrclk2_i,
		SYS_CLK_HALF => gtx0_txusrclk2_half_i,
		SYS_CLK_LOCKED => txoutclk_mmcm0_locked_i,
		IN_FIFO_RESET => opt2_fifo_reset,
		IN_FIFO_DOUT => opt2_in_fifo_dout,
		IN_FIFO_RDCLK => opt2_fifo_clk,
		IN_FIFO_RDEN => opt2_in_fifo_rden,
		IN_FIFO_RCNT => opt2_in_fifo_rcnt,
		OUT_FIFO_DIN => opt2_out_fifo_din,
		OUT_FIFO_WRCLK => opt2_fifo_clk,
		OUT_FIFO_WREN =>  opt2_out_fifo_wren,
		OUT_FIFO_WCNT => opt2_out_fifo_wcnt,
		I_OPT_CTRL_0 => not serdes_reset,
		O_OPT_CTRL_1 => open,
		O_OPT_CTRL_2 => open,
		O_OPT_CTRL_3 => open,
		I_OPT_CTRL_4 => tied1_to_vcc(0),
		I_OPT_CTRL_5 => tied1_to_vcc(0),
		I_OPT_CTRL_6 => tied1_to_vcc(0),
		O_OPT_CTRL_7 => open,
		O_OPT_CTRL_8 => open,
		O_OPT_CTRL_9 => open,
		O_OPT_CTRL_10 => open,
		I_OPT_CTRL_11 => tied33_to_gnd(0),
		O_OPT_CTRL_12 => open,
		O_OPT_CTRL_13 => open,
		O_OPT_CTRL_14 => open,
		O_OPT_CTRL_15 => open,
		O_OPT_CTRL_16 => open,
		O_OPT_CTRL_17 => open,
		I_OPT_CTRL_18 => tied33_to_gnd(0),
		I_OPT_CTRL_19 => tied33_to_gnd(0),
		I_OPT_CTRL_20 => tied33_to_gnd(0),
		I_OPT_CTRL_21 => tied33_to_gnd(0),
		I_OPT_CTRL_22 => tied33_to_gnd(0),
		P_LED => open,
		F_LED_ACTIVE => open,
		F_LED_LINK_OK => gtx_linkup(2),
		F_LED_D_UP => open,
		F_LED_D_DOWN => open,
		F_LED_USER_0 => open,
		F_LED_USER_1 => open,
		WATCHDOG => open,
		--
		remote_kchar_start_pulse => '0',
		remote_kchar_stop_pulse => '0',
		remote_kchar_error_pulse => '0',
		GET_REMOTE_ERROR_PULS => open
	);
	
	Serdes_opt3: serdes_top PORT MAP(
		TX_DATA => gtx3_txdata_i,
		TX_CHARISK => gtx3_txcharisk_i,
		RX_DATA => gtx3_rxdata_i,
		RX_CHARISK => gtx3_rxcharisk_i,
		RX_DERR => gtx3_rxdisperr_i,
		RX_OOT => gtx3_rxnotintable_i,
		SYS_CLK => gtx0_txusrclk2_i,
		SYS_CLK_HALF => gtx0_txusrclk2_half_i,
		SYS_CLK_LOCKED => txoutclk_mmcm0_locked_i,
		IN_FIFO_RESET => opt3_fifo_reset,
		IN_FIFO_DOUT => opt3_in_fifo_dout,
		IN_FIFO_RDCLK => opt3_fifo_clk,
		IN_FIFO_RDEN => opt3_in_fifo_rden,
		IN_FIFO_RCNT => opt3_in_fifo_rcnt,
		OUT_FIFO_DIN => opt3_out_fifo_din,
		OUT_FIFO_WRCLK => opt3_fifo_clk,
		OUT_FIFO_WREN =>  opt3_out_fifo_wren,
		OUT_FIFO_WCNT => opt3_out_fifo_wcnt,
		I_OPT_CTRL_0 => not serdes_reset,
		O_OPT_CTRL_1 => open,
		O_OPT_CTRL_2 => open,
		O_OPT_CTRL_3 => open,
		I_OPT_CTRL_4 => tied1_to_vcc(0),
		I_OPT_CTRL_5 => tied1_to_vcc(0),
		I_OPT_CTRL_6 => tied1_to_vcc(0),
		O_OPT_CTRL_7 => open,
		O_OPT_CTRL_8 => open,
		O_OPT_CTRL_9 => open,
		O_OPT_CTRL_10 => open,
		I_OPT_CTRL_11 => tied33_to_gnd(0),
		O_OPT_CTRL_12 => open,
		O_OPT_CTRL_13 => open,
		O_OPT_CTRL_14 => open,
		O_OPT_CTRL_15 => open,
		O_OPT_CTRL_16 => open,
		O_OPT_CTRL_17 => open,
		I_OPT_CTRL_18 => tied33_to_gnd(0),
		I_OPT_CTRL_19 => tied33_to_gnd(0),
		I_OPT_CTRL_20 => tied33_to_gnd(0),
		I_OPT_CTRL_21 => tied33_to_gnd(0),
		I_OPT_CTRL_22 => tied33_to_gnd(0),
		P_LED => open,
		F_LED_ACTIVE => open,
		F_LED_LINK_OK => gtx_linkup(3),
		F_LED_D_UP => open,
		F_LED_D_DOWN => open,
		F_LED_USER_0 => open,
		F_LED_USER_1 => open,
		WATCHDOG => open,
		--
		remote_kchar_start_pulse => '0',
		remote_kchar_stop_pulse => '0',
		remote_kchar_error_pulse => '0',
		GET_REMOTE_ERROR_PULS => open
	);





	q2_clk0_refclk_ibufds_i : IBUFDS_GTXE1
	port map
	(
		O                               =>      q2_clk0_refclk_i,
		ODIV2                           =>      open,
		CEB                             =>      tied_to_ground_i,
		I                               =>      gtx_refclk_p,
		IB                              =>      gtx_refclk_n
	);   
	
	q2_clk0_refclk_i_i <= ('0' & q2_clk0_refclk_i);



	----------------------------- The GTX Wrapper -----------------------------

	-- Use the instantiation template in the example directory to add the GTX wrapper to your design.
	-- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
	-- checker. The GTXs will reset, then attempt to align and transmit data. If channel bonding is 
	-- enabled, bonding should occur after alignment.


	v6_gtx_quad_backplane_p2p_114_i : v6_gtx_quad_backplane_p2p_114
	generic map
	(
		WRAPPER_SIM_GTXRESET_SPEEDUP    =>      0
	)
	port map
	(
		--_____________________________________________________________________
		--_____________________________________________________________________
		--GTX0  (X0Y8)
		------------------------ Loopback and Powerdown Ports ----------------------
		GTX0_LOOPBACK_IN                =>      "000",
		----------------------- Receive Ports - 8b10b Decoder ----------------------
		GTX0_RXCHARISK_OUT              =>      gtx0_rxcharisk_i,
		GTX0_RXDISPERR_OUT              =>      gtx0_rxdisperr_i,
		GTX0_RXNOTINTABLE_OUT           =>      gtx0_rxnotintable_i,
		------------------- Receive Ports - Clock Correction Ports -----------------
		GTX0_RXCLKCORCNT_OUT            =>      open,
		--------------- Receive Ports - Comma Detection and Alignment --------------
		GTX0_RXCOMMADET_OUT             =>      gtx0_rxcommadet_i,
		GTX0_RXENMCOMMAALIGN_IN         =>      gtx0_rxenmcommaalign_i,
		GTX0_RXENPCOMMAALIGN_IN         =>      gtx0_rxenpcommaalign_i,
		------------------- Receive Ports - RX Data Path interface -----------------
		GTX0_RXDATA_OUT                 =>      gtx0_rxdata_i,
		GTX0_RXRESET_IN                 =>      gtx0_rxreset_i,
		GTX0_RXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX0_RXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		GTX0_RXEQMIX_IN                 =>      gtx0_rxeqmix_i,
		GTX0_RXN_IN                     =>      gtx_rx_n(0),
		GTX0_RXP_IN                     =>      gtx_rx_p(0),
		------------------------ Receive Ports - RX PLL Ports ----------------------
		GTX0_GREFCLKRX_IN               =>      '0',
		GTX0_GTXRXRESET_IN              =>      gtx0_gtxrxreset_i,
		GTX0_MGTREFCLKRX_IN             =>      q2_clk0_refclk_i_i,
		GTX0_NORTHREFCLKRX_IN           =>      "00",
		GTX0_PERFCLKRX_IN               =>      '0',
		GTX0_PLLRXRESET_IN              =>      gtx0_pllrxreset_i,
		GTX0_RXPLLLKDET_OUT             =>      gtx0_rxplllkdet_i,
		GTX0_RXPLLREFSELDY_IN           =>      gtx0_rxpllrefseldy_i,
		GTX0_RXRESETDONE_OUT            =>      gtx0_rxresetdone_i,
		GTX0_SOUTHREFCLKRX_IN           =>      "00",
		----------------- Receive Ports - RX Polarity Control Ports ----------------
		GTX0_RXPOLARITY_IN              =>      '0',
		------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		GTX0_DADDR_IN                   =>      gtx0_daddr_i,
		GTX0_DCLK_IN                    =>      drp_clk_in_i,
		GTX0_DEN_IN                     =>      gtx0_den_i,
		GTX0_DI_IN                      =>      gtx0_di_i,
		GTX0_DRDY_OUT                   =>      gtx0_drdy_i,
		GTX0_DRPDO_OUT                  =>      gtx0_drpdo_i,
		GTX0_DWE_IN                     =>      gtx0_dwe_i,
		---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		GTX0_TXCHARISK_IN               =>      gtx0_txcharisk_i,
		------------------ Transmit Ports - TX Data Path interface -----------------
		GTX0_TXDATA_IN                  =>      gtx0_txdata_i,
		GTX0_TXOUTCLK_OUT               =>      gtx0_txoutclk_i,
		GTX0_TXRESET_IN                 =>      gtx0_txreset_i,
		GTX0_TXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX0_TXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		---------------- Transmit Ports - TX Driver and OOB signaling --------------
		GTX0_TXDIFFCTRL_IN              =>      gtx0_txdiffctrl_i,
		GTX0_TXN_OUT                    =>      gtx_tx_n(0),
		GTX0_TXP_OUT                    =>      gtx_tx_p(0),
		GTX0_TXPOSTEMPHASIS_IN          =>      gtx0_txpostemphasis_i,
		--------------- Transmit Ports - TX Driver and OOB signalling --------------
		GTX0_TXPREEMPHASIS_IN           =>      gtx0_txpreemphasis_i,
		----------------------- Transmit Ports - TX PLL Ports ----------------------
		GTX0_GREFCLKTX_IN               =>      '0',
		GTX0_GTXTXRESET_IN              =>      gtx0_gtxtxreset_i,
		GTX0_NORTHREFCLKTX_IN           =>      "00",
		GTX0_PERFCLKTX_IN               =>      '0',
		GTX0_SOUTHREFCLKTX_IN           =>      "00",
		GTX0_TXPLLREFSELDY_IN           =>      gtx0_txpllrefseldy_i,
		GTX0_TXRESETDONE_OUT            =>      gtx0_txresetdone_i,
		-------------------- Transmit Ports - TX Polarity Control ------------------
		GTX0_TXPOLARITY_IN              =>      '0',







		--_____________________________________________________________________
		--_____________________________________________________________________
		--GTX1  (X0Y9)
		------------------------ Loopback and Powerdown Ports ----------------------
		GTX1_LOOPBACK_IN                =>      gtx1_loopback_i,
		----------------------- Receive Ports - 8b10b Decoder ----------------------
		GTX1_RXCHARISK_OUT              =>      gtx1_rxcharisk_i,
		GTX1_RXDISPERR_OUT              =>      gtx1_rxdisperr_i,
		GTX1_RXNOTINTABLE_OUT           =>      gtx1_rxnotintable_i,
		------------------- Receive Ports - Clock Correction Ports -----------------
		GTX1_RXCLKCORCNT_OUT            =>      gtx1_rxclkcorcnt_i,
		--------------- Receive Ports - Comma Detection and Alignment --------------
		GTX1_RXCOMMADET_OUT             =>      gtx1_rxcommadet_i,
		GTX1_RXENMCOMMAALIGN_IN         =>      gtx1_rxenmcommaalign_i,
		GTX1_RXENPCOMMAALIGN_IN         =>      gtx1_rxenpcommaalign_i,
		------------------- Receive Ports - RX Data Path interface -----------------
		GTX1_RXDATA_OUT                 =>      gtx1_rxdata_i,
		GTX1_RXRESET_IN                 =>      gtx1_rxreset_i,
		GTX1_RXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX1_RXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		GTX1_RXEQMIX_IN                 =>      gtx1_rxeqmix_i,
		GTX1_RXN_IN                     =>      gtx_rx_n(1),
		GTX1_RXP_IN                     =>      gtx_rx_p(1),
		------------------------ Receive Ports - RX PLL Ports ----------------------
		GTX1_GREFCLKRX_IN               =>      '0',
		GTX1_GTXRXRESET_IN              =>      gtx1_gtxrxreset_i,
		GTX1_MGTREFCLKRX_IN             =>      q2_clk0_refclk_i_i,
		GTX1_NORTHREFCLKRX_IN           =>      "00",
		GTX1_PERFCLKRX_IN               =>      '0',
		GTX1_PLLRXRESET_IN              =>      gtx1_pllrxreset_i,
		GTX1_RXPLLLKDET_OUT             =>      gtx1_rxplllkdet_i,
		GTX1_RXPLLREFSELDY_IN           =>      gtx1_rxpllrefseldy_i,
		GTX1_RXRESETDONE_OUT            =>      gtx1_rxresetdone_i,
		GTX1_SOUTHREFCLKRX_IN           =>      "00",
		----------------- Receive Ports - RX Polarity Control Ports ----------------
		GTX1_RXPOLARITY_IN              =>      '0',
		------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		GTX1_DADDR_IN                   =>      gtx1_daddr_i,
		GTX1_DCLK_IN                    =>      drp_clk_in_i,
		GTX1_DEN_IN                     =>      gtx1_den_i,
		GTX1_DI_IN                      =>      gtx1_di_i,
		GTX1_DRDY_OUT                   =>      gtx1_drdy_i,
		GTX1_DRPDO_OUT                  =>      gtx1_drpdo_i,
		GTX1_DWE_IN                     =>      gtx1_dwe_i,
		---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		GTX1_TXCHARISK_IN               =>      gtx1_txcharisk_i,
		------------------ Transmit Ports - TX Data Path interface -----------------
		GTX1_TXDATA_IN                  =>      gtx1_txdata_i,
		GTX1_TXOUTCLK_OUT               =>      gtx1_txoutclk_i,
		GTX1_TXRESET_IN                 =>      gtx1_txreset_i,
		GTX1_TXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX1_TXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		---------------- Transmit Ports - TX Driver and OOB signaling --------------
		GTX1_TXDIFFCTRL_IN              =>      gtx1_txdiffctrl_i,
		GTX1_TXN_OUT                    =>      gtx_tx_n(1),
		GTX1_TXP_OUT                    =>      gtx_tx_p(1),
		GTX1_TXPOSTEMPHASIS_IN          =>      gtx1_txpostemphasis_i,
		--------------- Transmit Ports - TX Driver and OOB signalling --------------
		GTX1_TXPREEMPHASIS_IN           =>      gtx1_txpreemphasis_i,
		----------------------- Transmit Ports - TX PLL Ports ----------------------
		GTX1_GREFCLKTX_IN               =>      '0',
		GTX1_GTXTXRESET_IN              =>      gtx1_gtxtxreset_i,
		GTX1_NORTHREFCLKTX_IN           =>      "00",
		GTX1_PERFCLKTX_IN               =>      '0',
		GTX1_SOUTHREFCLKTX_IN           =>      "00",
		GTX1_TXPLLREFSELDY_IN           =>      gtx1_txpllrefseldy_i,
		GTX1_TXRESETDONE_OUT            =>      gtx1_txresetdone_i,
		-------------------- Transmit Ports - TX Polarity Control ------------------
		GTX1_TXPOLARITY_IN              =>      '0',







		--_____________________________________________________________________
		--_____________________________________________________________________
		--GTX2  (X0Y10)
		------------------------ Loopback and Powerdown Ports ----------------------
		GTX2_LOOPBACK_IN                =>      gtx2_loopback_i,
		----------------------- Receive Ports - 8b10b Decoder ----------------------
		GTX2_RXCHARISK_OUT              =>      gtx2_rxcharisk_i,
		GTX2_RXDISPERR_OUT              =>      gtx2_rxdisperr_i,
		GTX2_RXNOTINTABLE_OUT           =>      gtx2_rxnotintable_i,
		------------------- Receive Ports - Clock Correction Ports -----------------
		GTX2_RXCLKCORCNT_OUT            =>      gtx2_rxclkcorcnt_i,
		--------------- Receive Ports - Comma Detection and Alignment --------------
		GTX2_RXCOMMADET_OUT             =>      gtx2_rxcommadet_i,
		GTX2_RXENMCOMMAALIGN_IN         =>      gtx2_rxenmcommaalign_i,
		GTX2_RXENPCOMMAALIGN_IN         =>      gtx2_rxenpcommaalign_i,
		------------------- Receive Ports - RX Data Path interface -----------------
		GTX2_RXDATA_OUT                 =>      gtx2_rxdata_i,
		GTX2_RXRESET_IN                 =>      gtx2_rxreset_i,
		GTX2_RXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX2_RXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		GTX2_RXEQMIX_IN                 =>      gtx2_rxeqmix_i,
		GTX2_RXN_IN                     =>      gtx_rx_n(2),
		GTX2_RXP_IN                     =>      gtx_rx_p(2),
		------------------------ Receive Ports - RX PLL Ports ----------------------
		GTX2_GREFCLKRX_IN               =>      '0',
		GTX2_GTXRXRESET_IN              =>      gtx2_gtxrxreset_i,
		GTX2_MGTREFCLKRX_IN             =>      q2_clk0_refclk_i_i,
		GTX2_NORTHREFCLKRX_IN           =>      "00",
		GTX2_PERFCLKRX_IN               =>      '0',
		GTX2_PLLRXRESET_IN              =>      gtx2_pllrxreset_i,
		GTX2_RXPLLLKDET_OUT             =>      gtx2_rxplllkdet_i,
		GTX2_RXPLLREFSELDY_IN           =>      gtx2_rxpllrefseldy_i,
		GTX2_RXRESETDONE_OUT            =>      gtx2_rxresetdone_i,
		GTX2_SOUTHREFCLKRX_IN           =>      "00",
		----------------- Receive Ports - RX Polarity Control Ports ----------------
		GTX2_RXPOLARITY_IN              =>      '0',
		------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		GTX2_DADDR_IN                   =>      gtx2_daddr_i,
		GTX2_DCLK_IN                    =>      drp_clk_in_i,
		GTX2_DEN_IN                     =>      gtx2_den_i,
		GTX2_DI_IN                      =>      gtx2_di_i,
		GTX2_DRDY_OUT                   =>      gtx2_drdy_i,
		GTX2_DRPDO_OUT                  =>      gtx2_drpdo_i,
		GTX2_DWE_IN                     =>      gtx2_dwe_i,
		---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		GTX2_TXCHARISK_IN               =>      gtx2_txcharisk_i,
		------------------ Transmit Ports - TX Data Path interface -----------------
		GTX2_TXDATA_IN                  =>      gtx2_txdata_i,
		GTX2_TXOUTCLK_OUT               =>      gtx2_txoutclk_i,
		GTX2_TXRESET_IN                 =>      gtx2_txreset_i,
		GTX2_TXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX2_TXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		---------------- Transmit Ports - TX Driver and OOB signaling --------------
		GTX2_TXDIFFCTRL_IN              =>      gtx2_txdiffctrl_i,
		GTX2_TXN_OUT                    =>      gtx_tx_n(2),
		GTX2_TXP_OUT                    =>      gtx_tx_p(2),
		GTX2_TXPOSTEMPHASIS_IN          =>      gtx2_txpostemphasis_i,
		--------------- Transmit Ports - TX Driver and OOB signalling --------------
		GTX2_TXPREEMPHASIS_IN           =>      gtx2_txpreemphasis_i,
		----------------------- Transmit Ports - TX PLL Ports ----------------------
		GTX2_GREFCLKTX_IN               =>      '0',
		GTX2_GTXTXRESET_IN              =>      gtx2_gtxtxreset_i,
		GTX2_NORTHREFCLKTX_IN           =>      "00",
		GTX2_PERFCLKTX_IN               =>      '0',
		GTX2_SOUTHREFCLKTX_IN           =>      "00",
		GTX2_TXPLLREFSELDY_IN           =>      gtx2_txpllrefseldy_i,
		GTX2_TXRESETDONE_OUT            =>      gtx2_txresetdone_i,
		-------------------- Transmit Ports - TX Polarity Control ------------------
		GTX2_TXPOLARITY_IN              =>      '0',







		--_____________________________________________________________________
		--_____________________________________________________________________
		--GTX3  (X0Y11)
		------------------------ Loopback and Powerdown Ports ----------------------
		GTX3_LOOPBACK_IN                =>      gtx3_loopback_i,
		----------------------- Receive Ports - 8b10b Decoder ----------------------
		GTX3_RXCHARISK_OUT              =>      gtx3_rxcharisk_i,
		GTX3_RXDISPERR_OUT              =>      gtx3_rxdisperr_i,
		GTX3_RXNOTINTABLE_OUT           =>      gtx3_rxnotintable_i,
		------------------- Receive Ports - Clock Correction Ports -----------------
		GTX3_RXCLKCORCNT_OUT            =>      gtx3_rxclkcorcnt_i,
		--------------- Receive Ports - Comma Detection and Alignment --------------
		GTX3_RXCOMMADET_OUT             =>      gtx3_rxcommadet_i,
		GTX3_RXENMCOMMAALIGN_IN         =>      gtx3_rxenmcommaalign_i,
		GTX3_RXENPCOMMAALIGN_IN         =>      gtx3_rxenpcommaalign_i,
		------------------- Receive Ports - RX Data Path interface -----------------
		GTX3_RXDATA_OUT                 =>      gtx3_rxdata_i,
		GTX3_RXRESET_IN                 =>      gtx3_rxreset_i,
		GTX3_RXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX3_RXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
		GTX3_RXEQMIX_IN                 =>      gtx3_rxeqmix_i,
		GTX3_RXN_IN                     =>      gtx_rx_n(3),
		GTX3_RXP_IN                     =>      gtx_rx_p(3),
		------------------------ Receive Ports - RX PLL Ports ----------------------
		GTX3_GREFCLKRX_IN               =>      '0',
		GTX3_GTXRXRESET_IN              =>      gtx3_gtxrxreset_i,
		GTX3_MGTREFCLKRX_IN             =>      q2_clk0_refclk_i_i,
		GTX3_NORTHREFCLKRX_IN           =>      "00",
		GTX3_PERFCLKRX_IN               =>      '0',
		GTX3_PLLRXRESET_IN              =>      gtx3_pllrxreset_i,
		GTX3_RXPLLLKDET_OUT             =>      gtx3_rxplllkdet_i,
		GTX3_RXPLLREFSELDY_IN           =>      gtx3_rxpllrefseldy_i,
		GTX3_RXRESETDONE_OUT            =>      gtx3_rxresetdone_i,
		GTX3_SOUTHREFCLKRX_IN           =>      "00",
		----------------- Receive Ports - RX Polarity Control Ports ----------------
		GTX3_RXPOLARITY_IN              =>      '0',
		------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
		GTX3_DADDR_IN                   =>      gtx3_daddr_i,
		GTX3_DCLK_IN                    =>      drp_clk_in_i,
		GTX3_DEN_IN                     =>      gtx3_den_i,
		GTX3_DI_IN                      =>      gtx3_di_i,
		GTX3_DRDY_OUT                   =>      gtx3_drdy_i,
		GTX3_DRPDO_OUT                  =>      gtx3_drpdo_i,
		GTX3_DWE_IN                     =>      gtx3_dwe_i,
		---------------- Transmit Ports - 8b10b Encoder Control Ports --------------
		GTX3_TXCHARISK_IN               =>      gtx3_txcharisk_i,
		------------------ Transmit Ports - TX Data Path interface -----------------
		GTX3_TXDATA_IN                  =>      gtx3_txdata_i,
		GTX3_TXOUTCLK_OUT               =>      gtx3_txoutclk_i,
		GTX3_TXRESET_IN                 =>      gtx3_txreset_i,
		GTX3_TXUSRCLK_IN                =>      gtx0_txusrclk_i,
		GTX3_TXUSRCLK2_IN               =>      gtx0_txusrclk2_i,
		---------------- Transmit Ports - TX Driver and OOB signaling --------------
		GTX3_TXDIFFCTRL_IN              =>      gtx3_txdiffctrl_i,
		GTX3_TXN_OUT                    =>      gtx_tx_n(3),
		GTX3_TXP_OUT                    =>      gtx_tx_p(3),
		GTX3_TXPOSTEMPHASIS_IN          =>      gtx3_txpostemphasis_i,
		--------------- Transmit Ports - TX Driver and OOB signalling --------------
		GTX3_TXPREEMPHASIS_IN           =>      gtx3_txpreemphasis_i,
		----------------------- Transmit Ports - TX PLL Ports ----------------------
		GTX3_GREFCLKTX_IN               =>      '0',
		GTX3_GTXTXRESET_IN              =>      gtx3_gtxtxreset_i,
		GTX3_NORTHREFCLKTX_IN           =>      "00",
		GTX3_PERFCLKTX_IN               =>      '0',
		GTX3_SOUTHREFCLKTX_IN           =>      "00",
		GTX3_TXPLLREFSELDY_IN           =>      gtx3_txpllrefseldy_i,
		GTX3_TXRESETDONE_OUT            =>      gtx3_txresetdone_i,
		-------------------- Transmit Ports - TX Polarity Control ------------------
		GTX3_TXPOLARITY_IN              =>      '0'


	);
	
	gtx0_gtxrxreset_i	<= rst;
	gtx1_gtxrxreset_i	<= rst;
	gtx2_gtxrxreset_i	<= rst;
	gtx3_gtxrxreset_i	<= rst;
	
	gtx0_gtxtxreset_i	<= rst;
	gtx1_gtxtxreset_i	<= rst;
	gtx2_gtxtxreset_i	<= rst;
	gtx3_gtxtxreset_i	<= rst;
	

	-- Hold the TX in reset till the TX user clocks are stable
	gtx0_txreset_i <= not txoutclk_mmcm0_locked_i;


	gtx1_txreset_i <= not(gtx1_rxplllkdet_i and txoutclk_mmcm0_locked_i);




	gtx2_txreset_i <= not(gtx2_rxplllkdet_i and txoutclk_mmcm0_locked_i);




	gtx3_txreset_i <= not(gtx3_rxplllkdet_i and txoutclk_mmcm0_locked_i);



	-- Hold the RX in reset till the RX user clocks are stable

	gtx0_rxreset_i <= not txoutclk_mmcm0_locked_i;


	gtx1_rxreset_i <= not(gtx1_rxplllkdet_i and txoutclk_mmcm0_locked_i);



	gtx2_rxreset_i <= not(gtx2_rxplllkdet_i and txoutclk_mmcm0_locked_i);



	gtx3_rxreset_i <= not(gtx3_rxplllkdet_i and txoutclk_mmcm0_locked_i);
	
	gtx0_txdiffctrl_i <= "1010";
	gtx1_txdiffctrl_i <= "1010";
	gtx2_txdiffctrl_i <= "1010";
	gtx3_txdiffctrl_i <= "1010";
	gtx0_txpreemphasis_i <= "0000";
	gtx1_txpreemphasis_i <= "0000";
	gtx2_txpreemphasis_i <= "0000";
	gtx3_txpreemphasis_i <= "0000";
	gtx0_txpostemphasis_i <= "00000";
	gtx1_txpostemphasis_i <= "00000";
	gtx2_txpostemphasis_i <= "00000";
	gtx3_txpostemphasis_i <= "00000";
	
	gtx0_rxenmcommaalign_i <= '0';
	gtx1_rxenmcommaalign_i <= '0';
	gtx2_rxenmcommaalign_i <= '0';
	gtx3_rxenmcommaalign_i <= '0';
	gtx0_rxenpcommaalign_i <= '1';
	gtx1_rxenpcommaalign_i <= '1';
	gtx2_rxenpcommaalign_i <= '1';
	gtx3_rxenpcommaalign_i <= '1';
	
	gtx0_txpllrefseldy_i <= "000";
	gtx1_txpllrefseldy_i <= "000";
	gtx2_txpllrefseldy_i <= "000";
	gtx3_txpllrefseldy_i <= "000";
	
	gtx0_rxeqmix_i <= "000";
	gtx1_rxeqmix_i <= "000";
	gtx2_rxeqmix_i <= "000";
	gtx3_rxeqmix_i <= "000";

	txoutclk_mmcm0_reset_i                       <= not gtx0_rxplllkdet_i;
	txoutclk_mmcm0_i : MGT_USRCLK_SOURCE_MMCM
	generic map
	(
		MULT                            =>      8.0,
		DIVIDE                          =>      2,
		CLK_PERIOD                      =>      4.0,
		OUT0_DIVIDE                     =>      8.0,
		OUT1_DIVIDE                     =>      4,
		OUT2_DIVIDE                     =>      1,
		OUT3_DIVIDE                     =>      1
	)
	port map
	(
		CLKFBOUT                        =>      gtx0_txusrclk_i,
		CLK0_OUT                        =>      gtx0_txusrclk2_half_i,
		CLK1_OUT                        =>      gtx0_txusrclk2_i,
		CLK2_OUT                        =>      open,
		CLK3_OUT                        =>      open,
		CLK_IN                          =>      gtx0_txoutclk_i,
		MMCM_LOCKED_OUT                 =>      txoutclk_mmcm0_locked_i,
		MMCM_RESET_IN                   =>      txoutclk_mmcm0_reset_i
	);

end Behavioral;
