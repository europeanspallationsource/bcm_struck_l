----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:17:37 08/24/2010 
-- Design Name: 
-- Module Name:    channel_sample_block_v1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity two_channel_sample_block_v2 is
    Port ( adc_sample_logic_reset : in  STD_LOGIC;
           adc_clk : in  STD_LOGIC;
           adc_start_pulse : in  STD_LOGIC;
           adc_sample_block_length_reg : in   STD_LOGIC_VECTOR(23 downto 0);
           adc_ch1_din : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_ch2_din : in  STD_LOGIC_VECTOR (15 downto 0);
           adc_sample_logic_active : out   STD_LOGIC ;

           adc_buffer_fifo_rd_clk : in  STD_LOGIC;
           adc_buffer_fifo_rd_en : in  STD_LOGIC;
           adc_buffer_fifo_rd_dout : out   STD_LOGIC_VECTOR(127 downto 0);
           adc_buffer_fifo_rd_full : out  STD_LOGIC;
           adc_buffer_fifo_rd_empty : out  STD_LOGIC;
           adc_buffer_fifo_rd_rd_count : out   STD_LOGIC_VECTOR(10 downto 0) 
			  
		   );
end two_channel_sample_block_v2;



architecture Behavioral of two_channel_sample_block_v2 is

   Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
   begin
   if x then ret := '1';  else ret := '0'; end if;
   return ret;
   end to_std_logic;

--component blk_asy_fwft_fifo_4kx32_1kx128
--	port (
--	rst: IN std_logic;
--	wr_clk: IN std_logic;
--	rd_clk: IN std_logic;
--	din: IN std_logic_VECTOR(31 downto 0);
--	wr_en: IN std_logic;
--	rd_en: IN std_logic;
--	dout: OUT std_logic_VECTOR(127 downto 0);
--	full: OUT std_logic;
--	empty: OUT std_logic;
--	rd_data_count: OUT std_logic_VECTOR(10 downto 0);
--	wr_data_count: OUT std_logic_VECTOR(12 downto 0));
--end component;


component blk_asy_fwft_fifo_512x128
	port (
	rst: IN std_logic;
	wr_clk: IN std_logic;
	rd_clk: IN std_logic;
	din: IN std_logic_VECTOR(127 downto 0);
	wr_en: IN std_logic;
	rd_en: IN std_logic;
	dout: OUT std_logic_VECTOR(127 downto 0);
	full: OUT std_logic;
	empty: OUT std_logic;
	almost_empty: OUT std_logic;
	rd_data_count: OUT std_logic_VECTOR(9 downto 0);
	wr_data_count: OUT std_logic_VECTOR(9 downto 0));
end component;





signal fsm_reset: std_logic;
signal fsm_active: std_logic;
signal fsm_16bit_counter: std_logic_vector(3 downto 0);
signal fsm_block_counter: std_logic_vector(23 downto 0);

type fsm_type is(
		IDLE,	--  
		SAMPLE_LOOP 
);
signal fsm_state		: fsm_type;



signal pipe_reg_ce1: std_logic;
signal pipe_reg_ce2: std_logic;
signal pipe_reg_ce3: std_logic;
signal pipe_fifo_wr: std_logic;

signal adc_buffer_pipe1_reg: std_logic_vector(31 downto 0);
signal adc_buffer_pipe2_reg: std_logic_vector(31 downto 0);
signal adc_buffer_pipe3_reg: std_logic_vector(31 downto 0);
signal adc_buffer_fifo_din: std_logic_vector(127 downto 0);


begin


adc_util_logic: process (adc_clk) 
begin
   if rising_edge(adc_clk) then  -- 
		  fsm_reset       <= adc_sample_logic_reset ;
   end if;
end process;

-- State Maschine 


adc_sample_logic_active <=  fsm_active ;

ch_sample_controller_fsm: process (adc_clk) 
begin

if  rising_edge(adc_clk) then
   if (fsm_reset = '1')       then  
                                   fsm_active               <=   '0'    ;  
                                   fsm_16bit_counter        <=   X"0"   ;  
                                   fsm_block_counter        <=   X"000000"      ;  
                                   fsm_state                <=   IDLE    ;   
    else

     case fsm_state is

							 
---------- 
        when IDLE =>   
                 if (adc_start_pulse = '1') then --  
                                   fsm_active               <=   '1'    ;  
                                   fsm_16bit_counter        <=   X"0"   ;  
                                   fsm_block_counter        <=   adc_sample_block_length_reg      ;  
                                   fsm_state                <=   SAMPLE_LOOP    ;   
                  else
                                   fsm_active               <=   '0'    ;  
                                   fsm_16bit_counter        <=   X"0"   ;  
                                   fsm_block_counter        <=   adc_sample_block_length_reg      ;  
                                   fsm_state                <=   IDLE    ;   
                 end if;

---------- 
        when SAMPLE_LOOP =>   
                 if (fsm_16bit_counter /= X"F") then --  
                                   fsm_active               <=   '1'    ;  
                                   fsm_16bit_counter        <=   fsm_16bit_counter + 1    ;  
                                   fsm_block_counter        <=   fsm_block_counter      ;  
                                   fsm_state                <=   SAMPLE_LOOP    ;   
                  elsif (fsm_block_counter /= X"000000") then --  
                                   fsm_active               <=   '1'    ;  
                                   fsm_16bit_counter        <=   X"0"     ;  
                                   fsm_block_counter        <=   fsm_block_counter - 1      ;  
                                   fsm_state                <=   SAMPLE_LOOP    ;   
                  else
                                   fsm_active               <=   '0'    ;  
                                   fsm_16bit_counter        <=   fsm_16bit_counter     ;  
                                   fsm_block_counter        <=   fsm_block_counter      ;  
                                   fsm_state                <=   IDLE    ;   
                 end if;

---------- 

----------
        when OTHERS => NULL ;
     end case ;
      
   end if;
end if;

end process;




adc_sample_logic: process (adc_clk) 
begin
   if rising_edge(adc_clk) then  -- 
      pipe_reg_ce1   <=  fsm_active and to_std_logic(fsm_16bit_counter(1 downto 0) = "00") ; 
      pipe_reg_ce2   <=  fsm_active and to_std_logic(fsm_16bit_counter(1 downto 0) = "01") ; 
      pipe_reg_ce3   <=  fsm_active and to_std_logic(fsm_16bit_counter(1 downto 0) = "10") ; 
      pipe_fifo_wr   <=  fsm_active and to_std_logic(fsm_16bit_counter(1 downto 0) = "11") ; 
   end if;


   if rising_edge(adc_clk) then  -- 
      if (pipe_reg_ce1 = '1') then
		   adc_buffer_pipe1_reg(31 downto 16)    <= adc_ch2_din(15 downto 0) ;
		   adc_buffer_pipe1_reg(15 downto 0)     <= adc_ch1_din(15 downto 0) ;
      end if;
      if (pipe_reg_ce2 = '1') then
		   adc_buffer_pipe2_reg(31 downto 16)    <= adc_ch2_din(15 downto 0) ;
		   adc_buffer_pipe2_reg(15 downto 0)     <= adc_ch1_din(15 downto 0) ;
      end if;
      if (pipe_reg_ce3 = '1') then
		   adc_buffer_pipe3_reg(31 downto 16)    <= adc_ch2_din(15 downto 0) ;
		   adc_buffer_pipe3_reg(15 downto 0)     <= adc_ch1_din(15 downto 0)  ;
      end if;
   end if;




end process;



-- ******************************************************************************************


adc_buffer_fifo_din(127 downto 112)       <= adc_ch2_din(15 downto 0) ;
adc_buffer_fifo_din(111 downto 96)        <= adc_ch1_din(15 downto 0) ;

adc_buffer_fifo_din(95 downto 64)         <= adc_buffer_pipe3_reg(31 downto 0) ;
adc_buffer_fifo_din(63 downto 32)         <= adc_buffer_pipe2_reg(31 downto 0) ;
adc_buffer_fifo_din(31 downto 0)          <= adc_buffer_pipe1_reg(31 downto 0) ;

adc_sample_buffer_fifo : blk_asy_fwft_fifo_512x128
		port map (
			rst => fsm_reset,
			wr_clk => adc_clk,
			rd_clk => adc_buffer_fifo_rd_clk,
			din => adc_buffer_fifo_din, -- (127 downto 0),
			wr_en => pipe_fifo_wr,
			rd_en => adc_buffer_fifo_rd_en,
			dout => adc_buffer_fifo_rd_dout,
			full => adc_buffer_fifo_rd_full,
			empty => adc_buffer_fifo_rd_empty,
			almost_empty => open,
			rd_data_count => adc_buffer_fifo_rd_rd_count(9 downto 0), -- (9 downto 0)
			wr_data_count => open);

adc_buffer_fifo_rd_rd_count(10)  <=  '0' ;

end Behavioral;

