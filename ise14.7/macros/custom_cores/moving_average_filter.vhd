-------------------------------------------------------
--! @brief    Moving average filter.
-------------------------------------------------------
--! @details  When enabled the moving average filter is applied to the data.  
--!           The filter characteristics are set by changing the filter window.
--!  
--!           NOTE: Filter window has to have a value in order of 2 (1,2,4,8,16,32), otherwise
--!           the filter is not applied.
--!  
--!           NOTE: When window size is changed the MA filter has to be disabled.
--!           
--!           WARNING: The MA filter has to be disabled for 32 clocks before it can be re-enabled!
--!  
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Thu Sep 19 15:00:02 2013 2013 created
--! @date Thu Sep 19 15:00:02 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Thu Sep 19 15:00:02 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;

use unisim.vcomponents.all;

use work.bcm_package.all;

entity moving_average_filter is
    port(
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      --! Enable background average subtraction
      en_i           : in std_logic;
      
      --! The prebuffer delay (0,1,2,4,8,16,32)
      window_size_i  : in std_logic_vector(c_WINDOW_WIDTH-1 downto 0);
      
      --! ADC data 
      data_i         : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
      data_o         : out std_logic_vector(c_DATA_WIDTH-1 downto 0)
   );
end moving_average_filter;

architecture moving_average_filter_arch of moving_average_filter is

   signal s_SRL_di          : std_logic_vector(c_DATA_WIDTH-1 downto 0);
   signal s_sum,s_zero_sum  : std_logic_vector(27 downto 0);
   signal s_data_subst      : std_logic_vector(c_DATA_WIDTH-1 downto 0);
   signal s_raw_average     : std_logic_vector(27 downto 0);
   signal s_srl_addr        : std_logic_vector(5 downto 0);
   
   signal s_data_d1     : std_logic_vector(c_DATA_WIDTH-1 downto 0);

begin 
   --! SRL address 
   s_srl_addr <= std_logic_vector( unsigned(window_size_i) - 1);
   s_SRL_di   <= std_logic_vector(c_ZERO_OFFSET) when rst_i='1' or en_i ='0' else  data_i; 
    
  --! Multiplication mux for to calculate starting point for the window SUM at c_ZERO_OFFSET
   with window_size_i select
   s_zero_sum  <= (others => '0')                                                                       when "000000",
                  (s_zero_sum'left downto c_DATA_WIDTH  =>'0')&std_logic_vector(c_ZERO_OFFSET)          when "000001",
                  (s_zero_sum'left downto c_DATA_WIDTH+1=>'0')&std_logic_vector(c_ZERO_OFFSET)&'0'      when "000010",
                  (s_zero_sum'left downto c_DATA_WIDTH+2=>'0')&std_logic_vector(c_ZERO_OFFSET)&"00"     when "000100",
                  (s_zero_sum'left downto c_DATA_WIDTH+3=>'0')&std_logic_vector(c_ZERO_OFFSET)&"000"    when "001000",
                  (s_zero_sum'left downto c_DATA_WIDTH+4=>'0')&std_logic_vector(c_ZERO_OFFSET)&"0000"   when "010000",
                  (s_zero_sum'left downto c_DATA_WIDTH+5=>'0')&std_logic_vector(c_ZERO_OFFSET)&"00000"  when "100000",
                  (others => '0')                                                                       when others; 

   --! Shift register lookup table (SRL) is used to 
   --! shift the samples. This implementation was chosen due to 
   --! the lowest resource expense
   generate_shift_register : for index in 0 to c_DATA_WIDTH-1 generate
      -- SRLC32E: 32-bit variable length shift register LUT
      -- with clock enable
      -- Virtex-5
      -- Xilinx HDL Libraries Guide, version 10.1.2
      SRLC32E_inst : SRLC32E
      generic map (
         INIT => X"00000000")
      port map (
         Q => s_data_subst(index), -- SRL data output
         Q31 => open, -- SRL cascade output pin
         A => s_srl_addr(4 downto 0), -- 5-bit shift depth select input
         CE => '1', -- Clock enable input
         CLK => clk_i, -- Clock input
         D => s_SRL_di(index) -- SRL data input
      );
   end generate generate_shift_register;

   --! SUM of the samples in the filter window 
   --! The running SUM is implemented as such that 
   --! first sample is added to the sum and the last sample is 
   --! subtracted from the sum
   p_sync_sum : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if en_i = '0' then
               s_sum <= s_zero_sum;
           else
               s_sum <= std_logic_vector( unsigned(s_sum) + unsigned(data_i)- unsigned(s_data_subst) );
           end if;
       end if; -- rising_edge(clk_i)   
   end process p_sync_sum; 
   
   --! Division mux
   with window_size_i select
   s_raw_average  <=    (others => '0')                               when "000000",
                        (others => '0')                               when "000001",
                        '0'            & s_sum(s_sum'left downto 1)   when "000010",
                        "00"           & s_sum(s_sum'left downto 2)   when "000100",
                        "000"          & s_sum(s_sum'left downto 3)   when "001000",
                        "0000"         & s_sum(s_sum'left downto 4)   when "010000",
                        "00000"        & s_sum(s_sum'left downto 5)   when "100000",
                        (others => '0')                       when others;
                        
   --! delay data for 1 cc so that it can be output when filter is disabled
   p_sync_delay : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
              s_data_d1 <= (others => '0');
           else
              s_data_d1 <= data_i;
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_delay;
                        
   --! Output assignment (1 cc delay)
   data_o <= s_raw_average(c_DATA_WIDTH-1 downto 0) when en_i = '1' and s_raw_average(c_DATA_WIDTH-1 downto 0) /= (data_i'range=>'0') else s_data_d1;
-------------------------------------------------------
end moving_average_filter_arch;
