-------------------------------------------------------
--! @brief     Droop compensation for an ACCT beam current monitor.
-------------------------------------------------------
--! @details   When enabled and the s_droop_active register is
--!            high the filter adds a linear value
--!            to the signal according droop factor (droop rate).
--! 
--!            
--!            The droop factor and the droop rate are in the following linear relation:
--!            [0x0000-0x1FF]= [0%-24%] of amplitude after 2.86 ms,  clk = 88 MHz: 
--!
--!            NOTE: In order to work correctly amplitude has to be background compensated first.
--!                  The algorithm assumes that the signal offset is in level with c_ZERO_OFFSET
--!
--!
-------------------------------------------------------
--!
--! The original method was implemented by Matthias Werner, DESY
--! 
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--!
--! @date Tue Sep 17 16:30:54 2013 2013 created
--! @date 16. Jan 2014 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Tue Sep 17 16:30:54 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity droop_compensation is
    port(
       clk_i            : in std_logic;
       rst_i            : in std_logic;
       en_i             : in std_logic;
       
       --! Control signals for droop active register
       start_pulse_i : in std_logic;
       sampling_active_i : in std_logic;
      
       --! ADC dataI/O
       data_i           : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
       data_o           : out std_logic_vector(c_DATA_WIDTH-1 downto 0);
       
       --! Droop compensation factor adjustable from SW
       droop_factor_i   : in std_logic_vector(c_DROOP_FACTOR_WIDTH-1 downto 0) 
    );
end droop_compensation;


architecture droop_compensation_arch of droop_compensation is

    -- Component declaration of the "rsff(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component rsff
    port(
        s_i : in std_logic;
        r_i : in std_logic;
        q_o : out std_logic;
        q_n_o : out std_logic;
        clk_i : in std_logic;
        rst_i : in std_logic);
    end component;
    
    -- component declaration of the "fed(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component fed
    port(
        sig_i : in std_logic;
        rst_i : in std_logic;
        clk_i : in std_logic;
        sig_o : out std_logic);
    end component;

--! Intagers of IOs
    signal s_din            : unsigned(data_i'range);
    signal s_dout           : unsigned(data_o'range);
    signal s_droop_factor   : unsigned(droop_factor_i'range);

--! Internal computation signals
--! For integration of ADC values
	signal s39_akku			: unsigned (38 downto 0) := (others => '0');	
	signal s25_din_mul		: unsigned (24 downto 0) := (others => '0');
	signal s17_din_mul_div	: unsigned (16 downto 0) := (others => '0');
    
--! Logic signals
    signal s_data_in        : std_logic_vector(data_i'range);
    signal s_rst            : std_logic;
    
    
--! Droop active generation 
--! droop is active between the start_pulse_i and end of acquisition
    signal s_droop_active         : std_logic;
    signal s_sampling_active_fe   : std_logic;
    

begin 
    
    --! Falling edge of active sampling indicating end of acquisition
    comp_FED : FED
    port map(
        sig_i => sampling_active_i,
        rst_i => rst_i,
        clk_i => clk_i,
        sig_o => s_sampling_active_fe
    );
    
    --! The droop compensation is active
    --! from start_pulse_i until the end of acquisition.
    comp_REG : RSFF
    port map(
        s_i => start_pulse_i,
        r_i => s_sampling_active_fe,
        q_o => s_droop_active,
        q_n_o => open,
        clk_i => clk_i,
        rst_i => rst_i
    );
    
    --! Or-ed signals forming reset accumulator to zero
    s_rst     <= '1' when rst_i = '1' or s_droop_active = '0' or en_i = '0' else '0';
    
    
    --! Amplitude above c_ZERO_OFFSET is used therefore the background subtraction has to be enabled and applied before droop compensation.
    s_data_in <= std_logic_vector(unsigned(data_i) - unsigned(c_ZERO_OFFSET)) when unsigned(data_i) > unsigned(c_ZERO_OFFSET) else (s_data_in'range=>'0');
    s_din <= unsigned(s_data_in);
    
    s_droop_factor <= unsigned(droop_factor_i);
    
    sync_proc_droop : process(clk_i)
	begin
		if rising_edge(clk_i) then
			-- d_in * drooprate
			s25_din_mul <= s_din * s_droop_factor;
			-- / 2^8
			s17_din_mul_div <= s25_din_mul(s25_din_mul'left downto 8);
			if s_rst = '1' then
				s39_akku <= (others => '0');
			else
				-- Accumulate: over 2.86 ms:
				-- 88 MHz * 2.86 ms = 2.52e5 samples
                -- Duration of the pulse
				s39_akku <= s39_akku + s17_din_mul_div;
			end if;
			-- d_out: data_i + accu / 2^21
			s_dout <= unsigned(data_i) + s39_akku(36 downto 21);
		end if;
	end process sync_proc_droop;
    
    --! Output assignment
    data_o <=  std_logic_vector(s_dout);
-------------------------------------------
end droop_compensation_arch;
