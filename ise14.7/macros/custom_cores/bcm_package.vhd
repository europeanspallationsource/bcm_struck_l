-------------------------------------------------------
--! @brief    Components definition and
--!           Constant definitions for Beam Current Monitor on
--!           Struck sis8300 firmware.
-------------------------------------------------------
--! @details
--!
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Sep 19 10:54:16 2013 created
--! @date Aug 25 13:44:40 2014 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Sep 19 2013: Created
-------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package bcm_package is

   --! Firmware version
   constant c_FW_VER             : std_logic_vector(31 downto 0):= x"c00c0101";

    -----------------------------------------------------------------------
    --! general
    -----------------------------------------------------------------------
    constant c_NUM_OF_CHANNELS   : positive := 10;

    constant c_NUM_OF_ADCS       : positive := 5;

    constant c_PREBUFFER_WIDTH   : positive := 12;

    constant c_BUFFER_WIDTH      : positive := 28;

    constant c_DATA_WIDTH        : positive := 16;

    -----------------------------------------------------------------------
    --! interlock
    -----------------------------------------------------------------------

    -----------------------------------------------------------------------
    --! droop compensation
    -----------------------------------------------------------------------
    constant c_DIV_WIDTH            : positive := 8;
    constant c_DROOP_FACTOR_WIDTH   : positive := 9;

    -----------------------------------------------------------------------
    --! background subtraction
    -----------------------------------------------------------------------
    constant c_ZERO_OFFSET : std_logic_vector(c_DATA_WIDTH-1 downto 0):= x"8000";

    -----------------------------------------------------------------------
    --! moving average
    -----------------------------------------------------------------------
    constant c_WINDOW_WIDTH         : positive := 6;

    -----------------------------------------------------------------------
    --! record definitions
    -----------------------------------------------------------------------
    type t_pulse_ctrl_record is
    record
        s_start_trig   : std_logic;
        s_end_trig     : std_logic;
        s_start_pulse  : std_logic;
        s_end_pulse    : std_logic;
        s_pulse_valid  : std_logic;
    end record;

    -----------------------------------------------------------------------
    --! array definitions
    -----------------------------------------------------------------------
    type t_channel_array        is array (0 to c_NUM_OF_CHANNELS-1)     of std_logic_vector(15 downto 0);
    type t_pulse_ctrl_array     is array (0 to c_NUM_OF_CHANNELS-1)     of t_pulse_ctrl_record;
    type t_clock_array          is array (0 to c_NUM_OF_ADCS-1)         of std_logic;
    type t_trig_array           is array (0 to c_NUM_OF_CHANNELS-1)     of std_logic;
    type t_pulse_cntr_array     is array (0 to c_NUM_OF_CHANNELS-1)     of std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
    type t_droop_factor_array   is array (0 to c_NUM_OF_CHANNELS-1)     of std_logic_vector(c_DROOP_FACTOR_WIDTH-1 downto 0);


    -----------------------------------------------------------------------
    --! Control and status record
    -----------------------------------------------------------------------
    type t_ctrl_register_record is
    record
        --! bit 0: Reset BCM
        s_reg_rst       :  std_logic;

        --! General enable 1 bit for each channel
        s_droop_en      :  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);
        s_average_en    :  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);
        s_filter_en     :  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);

        --! Droop compensation
        s_droop_factor  :  t_droop_factor_array;

        --! Filter
        s_window_size   :  std_logic_vector(c_WINDOW_WIDTH-1 downto 0);

        --! Interlock
        s_itl_en        :  std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
        s_itl_clr       :  std_logic_vector(c_NUM_OF_ADCS-1 downto 0);

        s_itl_offset    :  std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
        s_itl_thr       :  std_logic_vector(15 downto 0);

    end record;

    type t_stat_register_record is
    record
        --! Status register
        --! bit 0: Buffer too short warning
        s_stat_reg       :  std_logic_vector(0 downto 0);

        --! Interlock register
        s_itl_reg       :  std_logic_vector(c_NUM_OF_ADCS-1 downto 0);

        --! Background average for every channel
        s_average       :  t_channel_array;
    end record;

------------------------------------------------------------------------------------------------------------------------
-- Memory map for register array
----------------------------------------------------------------------------------------------------------------------------
   --! Address space for Control registers
   constant c_REGARRAY_CTL_STARTADDR               : std_logic_vector(7 downto 0) := x"00";
   constant c_REGARRAY_CTL_ENDADDR                 : std_logic_vector(7 downto 0) := x"1f";

   --! Address space for Status registers
   constant c_REGARRAY_STAT_STARTADDR              : std_logic_vector(7 downto 0) := x"20";
   constant c_REGARRAY_STAT_ENDADDR                : std_logic_vector(7 downto 0) := x"2f";

   -----------------------------------------------------------------------------------------------------------
   --! Control Register Array
   -----------------------------------------------------------------------------------------------------------
   --! Firmware version
   constant c_FW_VER_REG             : integer range 0 to 31 := 16#0#;

   --! Software version
   constant c_SW_VER_REG             : integer range 0 to 31 := 16#1#;


   --! Control register (Auto clear)
   --! bit0: Reset the custom signal processing modules and custom register array
   constant c_CTRL_REG             : integer range 0 to 31 := 16#2#;

   --! Enable differential interlock
   --! bit0-4: Enable bit mask for 5 differential interlock pairs
   constant c_ITL_EN              : integer range 0 to 31 := 16#3#;

   --! Clear (put to OK state) differential interlock(Auto clear)
   --! bit0-4: Clear bit mask for 5 differential interlock pairs
   constant c_ITL_CLR             : integer range 0 to 31 := 16#4#;

   --! Differential interlock threshold
   --! bit0-15: Threshold value in sample counts
   constant c_ITL_THR             : integer range 0 to 31 := 16#5#;

   --! Differential interlock offset
   --! bit0-31: Offset value in sample counts
   constant c_ITL_OFFSET          : integer range 0 to 31 := 16#6#;

   --! Enable background noise subtraction
   --! bit0-9: Enable bit mask for 10 channels
   constant c_AVERAGE_EN          : integer range 0 to 31 := 16#7#;

   --! Enable moving average filter
   --! bit0-9: Enable bit mask for 10 channels
   constant c_FILTER_EN           : integer range 0 to 31 := 16#8#;

   --! Set moving average filter window size (2, 4, 8, 16, or 32)
   --! bit0-5: Window size
   constant c_FILTER_WINDOW       : integer range 0 to 31 := 16#9#;

   --! Enable droop compensation filter
   --! bit0-9: Enable bit mask for 10 channels
   constant c_DROOP_EN            : integer range 0 to 31 := 16#a#;

   --! Set droop compensation filter factor
   --! bit0-7: Droop factor size for each channel
   constant c_DROOP_FACTOR0       : integer range 0 to 31 := 16#b#;
   constant c_DROOP_FACTOR1       : integer range 0 to 31 := 16#c#;
   constant c_DROOP_FACTOR2       : integer range 0 to 31 := 16#d#;
   constant c_DROOP_FACTOR3       : integer range 0 to 31 := 16#e#;
   constant c_DROOP_FACTOR4       : integer range 0 to 31 := 16#f#;
   constant c_DROOP_FACTOR5       : integer range 0 to 31 := 16#10#;
   constant c_DROOP_FACTOR6       : integer range 0 to 31 := 16#11#;
   constant c_DROOP_FACTOR7       : integer range 0 to 31 := 16#12#;
   constant c_DROOP_FACTOR8       : integer range 0 to 31 := 16#13#;
   constant c_DROOP_FACTOR9       : integer range 0 to 31 := 16#14#;

   -----------------------------------------------------------------------------------------------------------
   --! Status Register Array
   -----------------------------------------------------------------------------------------------------------
   --! Status register
   --! bit0: Sample buffer too short warning
   constant c_STAT_REG            : integer range 0 to 31 := 16#0#;

   --! Differential interlock
   --! bit0-4: Differential interlock bit mask for 5 differential interlock pairs
   --!         - '1' interlock OK
   --!         - '0' interlock not OK
   constant c_ITL_REG             : integer range 0 to 31 := 16#1#;

   --! Background average
   --! bit0-15: Background average value for every channel
   constant c_BG_AVERAGE0         : integer range 0 to 31 := 16#2#;
-- constant c_BG_AVERAGE1         : integer range 0 to 31 := 16#3#;
--
-- constant c_BG_AVERAGE9         : integer range 0 to 31 := 16#b#;


   --! Length of the Array
   constant c_REG_ARRAY_LENGTH : integer range 1 to 256 := 32;

   --! Register data types
   type t_regarray32bit is array (c_REG_ARRAY_LENGTH-1 downto 0) of std_logic_vector(31 downto 0);
   type t_regarray1bit is array (c_REG_ARRAY_LENGTH-1 downto 0) of std_logic;

   -----------------------------------------------------------------------------------------------------------
   --! Heartbeat
   -----------------------------------------------------------------------------------------------------------

   constant c_HB_CNTR_WIDTH : positive := 28;
   constant c_HB_PERIOD     : std_logic_vector(c_HB_CNTR_WIDTH-1 downto 0) := x"29fc98a";

    -- Component declaration of the "custom_signal_processing(custom_signal_processing_arch)" unit defined in
    -- file: "./../../src/custom_cores/custom_signal_processing.vhd"
    component custom_signal_processing
    port(
        adc1_clk_i : in std_logic;
        adc2_clk_i : in std_logic;
        adc3_clk_i : in std_logic;
        adc4_clk_i : in std_logic;
        adc5_clk_i : in std_logic;
        rst_i : in std_logic;
        itl_o : out std_logic;
        itl_hb_o          : out std_logic;
        sampling_active_i : in std_logic;
        r_ctrl_i : in t_ctrl_register_record;
        r_stat_o : out t_stat_register_record;
        ch1_start_trig_i : in std_logic;
        ch2_start_trig_i : in std_logic;
        ch3_start_trig_i : in std_logic;
        ch4_start_trig_i : in std_logic;
        ch5_start_trig_i : in std_logic;
        ch6_start_trig_i : in std_logic;
        ch7_start_trig_i : in std_logic;
        ch8_start_trig_i : in std_logic;
        ch9_start_trig_i : in std_logic;
        ch10_start_trig_i : in std_logic;
        ch1_end_trig_i : in std_logic;
        ch2_end_trig_i : in std_logic;
        ch3_end_trig_i : in std_logic;
        ch4_end_trig_i : in std_logic;
        ch5_end_trig_i : in std_logic;
        ch6_end_trig_i : in std_logic;
        ch7_end_trig_i : in std_logic;
        ch8_end_trig_i : in std_logic;
        ch9_end_trig_i : in std_logic;
        ch10_end_trig_i : in std_logic;
        ch1_start_trig_o : out std_logic;
        ch2_start_trig_o : out std_logic;
        ch3_start_trig_o : out std_logic;
        ch4_start_trig_o : out std_logic;
        ch5_start_trig_o : out std_logic;
        ch6_start_trig_o : out std_logic;
        ch7_start_trig_o : out std_logic;
        ch8_start_trig_o : out std_logic;
        ch9_start_trig_o : out std_logic;
        ch10_start_trig_o : out std_logic;
        prebuf_dly_i : in std_logic_vector(11 downto 0);
        ch1_data_i : in std_logic_vector(15 downto 0);
        ch2_data_i : in std_logic_vector(15 downto 0);
        ch3_data_i : in std_logic_vector(15 downto 0);
        ch4_data_i : in std_logic_vector(15 downto 0);
        ch5_data_i : in std_logic_vector(15 downto 0);
        ch6_data_i : in std_logic_vector(15 downto 0);
        ch7_data_i : in std_logic_vector(15 downto 0);
        ch8_data_i : in std_logic_vector(15 downto 0);
        ch9_data_i : in std_logic_vector(15 downto 0);
        ch10_data_i : in std_logic_vector(15 downto 0);
        ch1_data_o : out std_logic_vector(15 downto 0);
        ch2_data_o : out std_logic_vector(15 downto 0);
        ch3_data_o : out std_logic_vector(15 downto 0);
        ch4_data_o : out std_logic_vector(15 downto 0);
        ch5_data_o : out std_logic_vector(15 downto 0);
        ch6_data_o : out std_logic_vector(15 downto 0);
        ch7_data_o : out std_logic_vector(15 downto 0);
        ch8_data_o : out std_logic_vector(15 downto 0);
        ch9_data_o : out std_logic_vector(15 downto 0);
        ch10_data_o : out std_logic_vector(15 downto 0));
    end component;

    -- Component declaration of the "custom_register_array(custom_register_array_arch)" unit defined in
    -- file: "./../../src/custom_cores/custom_register_array.vhd"
    component custom_register_array
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        data_i : in std_logic_vector(31 downto 0);
        data_o : out std_logic_vector(31 downto 0);
        addr_i : in std_logic_vector(7 downto 0);
        wr_en_i : in std_logic;
        rd_en_i : in std_logic;
        r_ctrl_o : out t_ctrl_register_record;
        r_stat_i : in t_stat_register_record);
    end component;

end package bcm_package;