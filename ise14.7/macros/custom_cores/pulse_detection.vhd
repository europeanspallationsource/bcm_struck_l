-------------------------------------------------------
--! @brief     Prepares the signals for other signal processing modules.
-------------------------------------------------------
--! @details   Delayes the start and end triggers for the ringbuffer delay
--!            and generates the pulse control record r_pulse_ctrl 
--!            that has the following signals:
--!                - s_start_trig: start trigger 
--!                - s_end_trig: end trigger
--!                - s_start_pulse: start trigger delayed for ringbuffer delay
--!                - s_end_pulse: end trigger delayed for ringbuffer delay
--!                - s_pulse_valid: active between start_pulse and end_pulse
--!
--!            s_pulse_cntr: Counts the time between tart_pulse and end_pulse
--!
--!            NOTE: This block is implemented so that the ADC sample data does not have to be delayed
--!                  which saves FPGA resources (flip-flops). The implementation uses some combinational logic where 
--!                  usualy only sequential logic is used.
--!     
--!            NOTE: sampling_end_i is used to put s_pulse_valid signal to low level if the end_pulse_i comes later than 
--!                  the sampling ends. This way even if the sample buffer is shorter than the pulse or the end trigger is missed 
--!                  the valid signal is disabled and the filters work correctly.
--!
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Thu Sep 12 10:10:17 2013 2013 created
--! @date Thu Sep 12 10:10:17 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Thu Sep 12 10:10:17 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity pulse_detection is
    port(
       clk_i               : in std_logic;
       rst_i               : in std_logic;
       
       --! Informs that the sampling has ended
       sampling_end_i      : in std_logic;
       
       prebuf_dly_i        : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0);
       
       --! Trigger inputs
       start_trig_i        : in  std_logic;
       end_trig_i          : in  std_logic;
       
       --! Counter of samples from the start of the pulse
       pulse_cntr_o        : out std_logic_vector(c_buffer_width-1 downto 0);
       
       --! Pulse control record       
       r_pulse_ctrl_o      : out t_pulse_ctrl_record
    );
end pulse_detection;

architecture pulse_detection_arch of pulse_detection is

   -- component declaration of the "cntr(cntr_arch)" unit defined in
    -- file: "./src/shared_components.vhd"
    component cntr
    generic(
        g_size : positive := 4;
        g_incr : std_logic := '1');
    port(
        rst_i : in std_logic;
        load_i : in std_logic;
        en_i : in std_logic;
        load_data_i : in std_logic_vector(g_size-1 downto 0);
        cntr_o : out std_logic_vector(g_size-1 downto 0);
        clk_i : in std_logic);
    end component; 
   
   -- component declaration of the "rsff(rtl)" unit defined in
    -- file: "./src/shared_components.vhd"
    component rsff
    port(
        s_i : in std_logic;
        r_i : in std_logic;
        q_o : out std_logic;
        q_n_o : out std_logic;
        clk_i : in std_logic;
        rst_i : in std_logic);
    end component;
   
   -- component declaration of the "fed(rtl)" unit defined in
    -- file: "./src/shared_components.vhd"
    component fed
    port(
        sig_i : in std_logic;
        sig_o : out std_logic;
        rst_i : in std_logic;
        clk_i : in std_logic);
    end component;

   --! Internal signals
   signal s_prebuf_dly        : std_logic_vector(prebuf_dly_i'range);
   
   signal s_start_pulse_en    : std_logic;
   signal s_start_pulse_cntr  : std_logic_vector(prebuf_dly_i'range);
   signal s_start_trig_cnt    : std_logic;
   signal s_start_trig_q      : std_logic;
   signal s_start_pulse       : std_logic;
   
   signal s_end_pulse_en      : std_logic;
   signal s_end_pulse_cntr    : std_logic_vector(prebuf_dly_i'range);
   signal s_end_trig_cnt      : std_logic;
   signal s_end_trig_q        : std_logic;
   signal s_end_pulse         : std_logic;
   
   signal s_pulse_cntr_en    : std_logic;
   signal s_pulse_en_reg     : std_logic;
   signal s_pulse_cntr       : std_logic_vector(pulse_cntr_o'range);
   
   signal s_rst_valid_reg    : std_logic;
   
   
begin 
   
  --! Handle delay 0 and 1
   with prebuf_dly_i select
   s_prebuf_dly <= (others => '0')                              when x"000", 
                   (others => '0')                              when x"001",
                   std_logic_vector(unsigned(prebuf_dly_i)-1)   when others;
                   
---------------------------------------------------------------------------------  
--! Start pulse delay counter
---------------------------------------------------------------------------------
   comp_start_pulse_counter : cntr
   generic map(
      g_size => c_PREBUFFER_WIDTH,
      g_incr => '0'      --! Decrement
   )
   port map(
      clk_i => clk_i,
      rst_i => rst_i,
      load_i => start_trig_i,
      en_i => s_start_pulse_en,
      load_data_i => s_prebuf_dly,
      cntr_o => s_start_pulse_cntr
   );
   
   --! Enable counter when non-zero
   s_start_pulse_en <= '0' when s_start_pulse_cntr = (s_start_pulse_cntr'range => '0') else '1';
  
   comp_falling_en_1 : fed
   port map(
      sig_i => s_start_pulse_en,
      sig_o => s_start_trig_cnt,
      rst_i => rst_i,
      clk_i => clk_i
   );
   
   --! Delay start trigger for 1 cc. Used for prebuffer delay 1.  
   p_sync_delay_start_trigger : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
               s_start_trig_q <='0';
           else
               s_start_trig_q <= start_trig_i;
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_delay_start_trigger;
   
   --! Handle delay 0 and 1
   with prebuf_dly_i select
   s_start_pulse <= start_trig_i      when x"000", 
                   s_start_trig_q     when x"001",
                   s_start_trig_cnt   when others;
   
---------------------------------------------------------------------------------     
--! End pulse delay counter
---------------------------------------------------------------------------------  
   comp_end_pulse_counter : cntr
   generic map(
      g_size => c_PREBUFFER_WIDTH,
      g_incr => '0'      --! Decrement
   )
   port map(
      clk_i => clk_i,
      rst_i => rst_i,
      load_i => end_trig_i,
      en_i => s_end_pulse_en,
      load_data_i => s_prebuf_dly,
      cntr_o => s_end_pulse_cntr
   );
   
   --! Enable counter when non-zero
   s_end_pulse_en <= '0' when s_end_pulse_cntr = (s_end_pulse_cntr'range => '0') else '1';
  
   comp_falling_en_2 : fed
   port map(
      sig_i => s_end_pulse_en,
      sig_o => s_end_trig_cnt,
      rst_i => rst_i,
      clk_i => clk_i
   );
   
   --! Delay end trigger for 1 cc. Used for prebuffer delay 1. 
   p_sync_delay_end_trigger : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
               s_end_trig_q <='0';
           else
               s_end_trig_q <= end_trig_i;
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_delay_end_trigger;
   
   --! Handle delay 0 and 1
   with prebuf_dly_i select
   s_end_pulse <=  end_trig_i       when x"000", 
                   s_end_trig_q     when x"001",
                   s_end_trig_cnt   when others;
 
--------------------------------------------------------------------------------     
--! Pulse counter 
---------------------------------------------------------------------------------  
   comp_pulse_counter : cntr
   generic map(
      g_size => c_BUFFER_WIDTH,
      g_incr => '1'      --! Increment
   )
   port map(
      rst_i => rst_i,
      load_i => start_trig_i,
      en_i => s_pulse_cntr_en,
      load_data_i => (c_BUFFER_WIDTH-1 downto 0 => '0'),
      cntr_o => s_pulse_cntr,
      clk_i => clk_i
   );
   
   s_rst_valid_reg <= s_end_pulse or sampling_end_i;
   
   comp_enable_buffer_cntr : rsff
   port map(
      s_i => s_start_pulse,
      r_i => s_rst_valid_reg,
      q_o => s_pulse_en_reg,
      q_n_o => open,
      clk_i => clk_i,
      rst_i => rst_i
   );
 
  --! Enable counter immediately after start pulse and stop at stop pulse
  --! This way a delay 1 cc delay is compensated
  s_pulse_cntr_en <= (s_pulse_en_reg or s_start_pulse) and not s_end_pulse;
  
---------------------------------------------------------------------------------  
--! Output assignment 
---------------------------------------------------------------------------------
r_pulse_ctrl_o.s_start_trig     <= start_trig_i;
r_pulse_ctrl_o.s_end_trig       <= end_trig_i;
r_pulse_ctrl_o.s_start_pulse    <= s_start_pulse;
r_pulse_ctrl_o.s_end_pulse      <= s_end_pulse;
r_pulse_ctrl_o.s_pulse_valid    <= s_pulse_cntr_en;
pulse_cntr_o                    <= s_pulse_cntr;
---------------------------------------------------------------------------------
end pulse_detection_arch;
