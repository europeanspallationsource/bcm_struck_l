-------------------------------------------------------
--! @brief    Wrapper for moving_average_filter block.
-------------------------------------------------------
--! @details  Generates moving_average_filter block for every channel.  
--!           Delays r_pulse_ctrl record array for 1 cc.
--!            
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Wed Sep 18 16:44:50 2013 2013 created
--! @date Wed Sep 18 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Wed Sep 18 16:44:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity moving_average_filter_wrapper is
    port(
        a_clk_i           : in  t_trig_array;
        rst_i             : in  std_logic;
        en_i              : in  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);
        window_size_i     : in  std_logic_vector(c_WINDOW_WIDTH-1 downto 0);
        
        a_pulse_ctrl_i    : in  t_pulse_ctrl_array;
        a_pulse_ctrl_o    : out t_pulse_ctrl_array; 
        a_channel_data_i  : in  t_channel_array;
        a_channel_data_o  : out t_channel_array
    );
end moving_average_filter_wrapper;

architecture moving_average_filter_wrapper_arch of moving_average_filter_wrapper is

   -- Component declaration of the "moving_average_filter(moving_average_filter_arch)" unit defined in
    -- file: "./src/moving_average_filter.vhd"
    component moving_average_filter
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        en_i : in std_logic;
        window_size_i : in std_logic_vector(c_WINDOW_WIDTH-1 downto 0);
        data_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        data_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0));
    end component;
   
   signal a_pulse_ctrl_d1 : t_pulse_ctrl_array;
   
begin   
   
   generate_channels : for index in 0 to c_NUM_OF_CHANNELS-1 generate  
       comp_moving_average_filter : moving_average_filter
       port map(
          clk_i         => a_clk_i(index),
          rst_i         => rst_i,
          en_i          => en_i(index),
          window_size_i => window_size_i,
          data_i        => a_channel_data_i(index),
          data_o        => a_channel_data_o(index)
       );   
   end generate generate_channels;  

   --! Delay all signals in the control record for 1 clock cycle 
   generate_delays : for index in 0 to c_NUM_OF_CHANNELS-1 generate    
       p_sync_delay : process(a_clk_i(index))
       begin
           if rising_edge(a_clk_i(index)) then
               a_pulse_ctrl_d1(index)  <= a_pulse_ctrl_i(index);
           end if; -- rising_edge(clk_i)   
       end process p_sync_delay;
   end generate generate_delays;
      
   --! Output assignment (1 CC delay)
   a_pulse_ctrl_o <= a_pulse_ctrl_d1;
  
end moving_average_filter_wrapper_arch;
