-------------------------------------------------------
--! @brief    Wrapper for pulse_detection block.
-------------------------------------------------------
--! @details  Generates pulse_detection block for every channel.  
--!            
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Thu Sep 12 10:10:17 2013 2013 created
--! @date Thu Sep 12 10:10:17 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Thu Sep 12 10:10:17 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;

use unisim.vcomponents.all;

use work.bcm_package.all;

entity pulse_detection_wrapper is
    port(
        --! Clocks from five ADCs
        adc1_clk_i          : in std_logic;
        adc2_clk_i          : in std_logic;
        adc3_clk_i          : in std_logic;
        adc4_clk_i          : in std_logic; 
        adc5_clk_i          : in std_logic;
        
        rst_i               : in std_logic;
        
        --! Informs that the sampling has ended
        sampling_end_i      : in std_logic;
        
        --! Start triggers for all 10 channels(timing system trigger line inputs)
        ch1_start_trig_i      : in  std_logic;
        ch2_start_trig_i      : in  std_logic;      
        ch3_start_trig_i      : in  std_logic;      
        ch4_start_trig_i      : in  std_logic;     
        ch5_start_trig_i      : in  std_logic;
        ch6_start_trig_i      : in  std_logic;
        ch7_start_trig_i      : in  std_logic;      
        ch8_start_trig_i      : in  std_logic;      
        ch9_start_trig_i      : in  std_logic;     
        ch10_start_trig_i     : in  std_logic;
        
        --! End triggers for all 10 channels(timing system trigger line inputs)
        ch1_end_trig_i      : in  std_logic;
        ch2_end_trig_i      : in  std_logic;      
        ch3_end_trig_i      : in  std_logic;      
        ch4_end_trig_i      : in  std_logic;     
        ch5_end_trig_i      : in  std_logic;
        ch6_end_trig_i      : in  std_logic;
        ch7_end_trig_i      : in  std_logic;      
        ch8_end_trig_i      : in  std_logic;      
        ch9_end_trig_i      : in  std_logic;     
        ch10_end_trig_i     : in  std_logic;
        
        prebuf_dly_i        : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0);
                
        a_clk_o             : out t_trig_array;
        a_pulse_cntr_o      : out t_pulse_cntr_array;
        a_pulse_ctrl_o      : out t_pulse_ctrl_array
    );
end pulse_detection_wrapper;

architecture pulse_detection_wrapper_arch of pulse_detection_wrapper is

    -- Component declaration of the "pulse_detection(pulse_detection_arch)" unit defined in
    -- file: "./../../src/custom_cores/pulse_detection.vhd"
    component pulse_detection
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        sampling_end_i : in std_logic;
        prebuf_dly_i : in std_logic_vector(c_prebuffer_width-1 downto 0);
        start_trig_i : in std_logic;
        end_trig_i : in std_logic;
        pulse_cntr_o : out std_logic_vector(c_buffer_width-1 downto 0);
        r_pulse_ctrl_o : out t_pulse_ctrl_record);
    end component;
    
    -- component declaration of the "extsig(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component extsig
    generic(
        cycles : positive := 2);
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        sig_i : in std_logic;
        sig_o : out std_logic);
    end component; 
    
    --! Internal signals
    signal a_clk             : t_trig_array;
    signal a_start_trig      : t_trig_array;
    signal a_end_trig        : t_trig_array;
    signal s_sampling_end_ext: std_logic;
   
begin
    
    --! Input assignment
    a_clk(0) <= adc1_clk_i;
    a_clk(1) <= adc1_clk_i;
    a_clk(2) <= adc2_clk_i;
    a_clk(3) <= adc2_clk_i;
    a_clk(4) <= adc3_clk_i;
    a_clk(5) <= adc3_clk_i;
    a_clk(6) <= adc4_clk_i;
    a_clk(7) <= adc4_clk_i;
    a_clk(8) <= adc5_clk_i;
    a_clk(9) <= adc5_clk_i;
    
    a_start_trig(0) <= ch1_start_trig_i;
    a_start_trig(1) <= ch2_start_trig_i;
    a_start_trig(2) <= ch3_start_trig_i;
    a_start_trig(3) <= ch4_start_trig_i;
    a_start_trig(4) <= ch5_start_trig_i;
    a_start_trig(5) <= ch6_start_trig_i;
    a_start_trig(6) <= ch7_start_trig_i;
    a_start_trig(7) <= ch8_start_trig_i;
    a_start_trig(8) <= ch9_start_trig_i;
    a_start_trig(9) <= ch10_start_trig_i;
    
    a_end_trig(0) <= ch1_end_trig_i;
    a_end_trig(1) <= ch2_end_trig_i;
    a_end_trig(2) <= ch3_end_trig_i;
    a_end_trig(3) <= ch4_end_trig_i;
    a_end_trig(4) <= ch5_end_trig_i;
    a_end_trig(5) <= ch6_end_trig_i;
    a_end_trig(6) <= ch7_end_trig_i;
    a_end_trig(7) <= ch8_end_trig_i;
    a_end_trig(8) <= ch9_end_trig_i;
    a_end_trig(9) <= ch10_end_trig_i;
   
    --! Extend the sampling end signal, so all the ADC CLOCKs can catch it at least once
    comp_extend : ExtSig
    generic map(
        CYCLES => 4
    )
    port map(
        clk_i => adc1_clk_i,
        rst_i => rst_i,
        sig_i => sampling_end_i,
        sig_o => s_sampling_end_ext
    );
   
   generate_channels : for index in 0 to c_NUM_OF_CHANNELS-1 generate 
       comp_pulse_detect : pulse_detection
        port map(
            clk_i => a_clk(index),
            rst_i => rst_i,
            sampling_end_i  => s_sampling_end_ext, 
            prebuf_dly_i => prebuf_dly_i,
            start_trig_i => a_start_trig(index),
            end_trig_i => a_end_trig(index),
            pulse_cntr_o => a_pulse_cntr_o(index),
            r_pulse_ctrl_o => a_pulse_ctrl_o(index)
        );
   end generate generate_channels;
   
   
   --! Output assignment 
   a_clk_o <= a_clk;

---------------------------------------------------------------------------------
end pulse_detection_wrapper_arch;
