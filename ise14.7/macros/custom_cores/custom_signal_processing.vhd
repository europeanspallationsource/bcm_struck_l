-------------------------------------------------------
--! @brief    Custom signal processing framework for sis8300 
--!           with implemented Beam Current Monitor (BCM) functionality.  
--!           
-------------------------------------------------------
--! @details  This module is placed in the original sis8300 firmware 
--!           after ringbuffer delay module and before the sampling 
--!           logic module.
--!
--!           The interface to this module consists of:
--!           - adcX_clk_i: Clocks from five ADCs. One ADC outputs the sampling data
--!             of two channels. The signal processing logic of the seperate ADCs
--!             runs on different clock domains.
--!           - chX_data_i: Sampling data input 16-bit. Comming from the output of the ringbuffer delay
--!           - chX_data_o: Sampling data output 16-bit. Should be connected to sampling logic.
--!           - chX_start_trig_i: Start trigger input. The trigger comes from one of the external trigger interfaces.
--!             Indicates the start of the pulse. This trigger also initiates the data sampling. 
--!           - chX_end_trig_i: End trigger input. The trigger comes from one of the external trigger interfaces. 
--!             Indicates end of the pulse.
--!           - chX_start_trig_o: Start trigger output. Delay start trigger that it will be alligned with 
--!             the processed data. The output is connected to sampling logic start pulse input.
--!           - r_ctrl_i: Control record from the custom_register_array. It contains registers that control the signal processing logic.
--!             The r_ctrl_i can be modified in bcm_package.vhd to add or remove registers.
--!           - r_stat_o: Status record connected to the custom_register_array. It contains registers that show status of the signal processing logic.
--!             The r_stat_i can be modified in bcm_package.vhd to add or remove registers.
--!           - prebuf_dly_i: Prebuffer dalay value comes from the original struck register array and holds the ringbuffer delay value.
--!           - sampling_active_i: Connected to sampling logic. Indicates vhen the sampling logic is active.  
--!           - itl_o: A signal indicating the interlock status. If used it should be connected to external Harlink output and User interrupt.
--!
--!           The BCM functionality has the following modules:
--!           - pulse_detection_wrapper: Prepares the signals for other signal processing modules.
--!           - droop_compensation_wrapper: Placed in pipeline stage 1. Compensates for signal droop.
--!           - bg_average_wrapper: Placed in pipeline stage 2. Calculatas the average of the samples  
--!             inside the brebuffer and subtracts this average from the pulse (sets the pulse offset to zero).
--!           - moving_average_filter_wrapper: Placed in pipeline stage 3. Low pass filter.
--!           - diff_itl_detection_wrapper: Placed in pipeline stage 4. Differential interlock for the channel pairs.
--! 
--!          How to add a custom signal processing module:
--!          - design your module and put it in a wrapper.
--!            The wrapper has to also prepare the signals for the next pipeline stage. 
--!          - insert your module into the desired pipeline stage.
--!          - add your control registers into r_ctrl_i
--!          - add your status registers into r_stat_i
--!          - add your control and status signals to register map in bcm_package.vhd and to custom_register_array.

-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Mon Sep  9 09:33:28 2013 2013 created
--! @date Mon Sep  9 09:33:28 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Mon Sep  9 09:33:28 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.bcm_package.all;

entity custom_signal_processing is
   port(
      --! Clocks from five ADCs
      adc1_clk_i          : in std_logic;
      adc2_clk_i          : in std_logic;
      adc3_clk_i          : in std_logic;
      adc4_clk_i          : in std_logic; 
      adc5_clk_i          : in std_logic;
      
      --! Module reset
      rst_i              : in std_logic;
      
      --! Sample logic active status input
      sampling_active_i : in std_logic;
      
      --! Controls from register array
      r_ctrl_i          : in t_ctrl_register_record;
      
      --! Statuses to register array
      r_stat_o          : out t_stat_register_record;
      
      --! Output of he interlock blocks, connect to Front panel output
      itl_o             : out std_logic;
      itl_hb_o          : out std_logic;     

      --! Start triggers for all 10 channels(timing system trigger line inputs)
      ch1_start_trig_i      : in  std_logic;
      ch2_start_trig_i      : in  std_logic;      
      ch3_start_trig_i      : in  std_logic;      
      ch4_start_trig_i      : in  std_logic;     
      ch5_start_trig_i      : in  std_logic;
      ch6_start_trig_i      : in  std_logic;
      ch7_start_trig_i      : in  std_logic;      
      ch8_start_trig_i      : in  std_logic;      
      ch9_start_trig_i      : in  std_logic;     
      ch10_start_trig_i     : in  std_logic;
      
      --! End triggers for all 10 channels(timing system trigger line inputs)
      ch1_end_trig_i      : in  std_logic;
      ch2_end_trig_i      : in  std_logic;      
      ch3_end_trig_i      : in  std_logic;      
      ch4_end_trig_i      : in  std_logic;     
      ch5_end_trig_i      : in  std_logic;
      ch6_end_trig_i      : in  std_logic;
      ch7_end_trig_i      : in  std_logic;      
      ch8_end_trig_i      : in  std_logic;      
      ch9_end_trig_i      : in  std_logic;     
      ch10_end_trig_i     : in  std_logic;
      
      --! Start triggers delayed for signal processing delay
      ch1_start_trig_o      : out std_logic;
      ch2_start_trig_o      : out std_logic;      
      ch3_start_trig_o      : out std_logic;      
      ch4_start_trig_o      : out std_logic;     
      ch5_start_trig_o      : out std_logic;
      ch6_start_trig_o      : out std_logic;
      ch7_start_trig_o      : out std_logic;      
      ch8_start_trig_o      : out std_logic;      
      ch9_start_trig_o      : out std_logic;     
      ch10_start_trig_o     : out std_logic;

      --! Ringbuffer delay size input
      prebuf_dly_i      : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0);
      
      --! Raw channel inputs
      ch1_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch2_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch3_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch4_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch5_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch6_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch7_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch8_data_i        : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch9_data_i     : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch10_data_i    : in  std_logic_vector (c_DATA_WIDTH-1 downto 0);
            
      --! Processed channel outputs
      ch1_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch2_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch3_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch4_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch5_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch6_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch7_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch8_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch9_data_o     : out  std_logic_vector (c_DATA_WIDTH-1 downto 0);
      ch10_data_o    : out  std_logic_vector (c_DATA_WIDTH-1 downto 0)
   );
end custom_signal_processing;

architecture custom_signal_processing_arch of custom_signal_processing is

    -- Component declaration of the "pulse_detection_wrapper(pulse_detection_wrapper_arch)" unit defined in
    -- file: "./../../src/custom_cores/pulse_detection_wrapper.vhd"
    component pulse_detection_wrapper
    port(
        adc1_clk_i : in std_logic;
        adc2_clk_i : in std_logic;
        adc3_clk_i : in std_logic;
        adc4_clk_i : in std_logic;
        adc5_clk_i : in std_logic;
        rst_i : in std_logic;
        sampling_end_i : in std_logic;
        ch1_start_trig_i : in std_logic;
        ch2_start_trig_i : in std_logic;
        ch3_start_trig_i : in std_logic;
        ch4_start_trig_i : in std_logic;
        ch5_start_trig_i : in std_logic;
        ch6_start_trig_i : in std_logic;
        ch7_start_trig_i : in std_logic;
        ch8_start_trig_i : in std_logic;
        ch9_start_trig_i : in std_logic;
        ch10_start_trig_i : in std_logic;
        ch1_end_trig_i : in std_logic;
        ch2_end_trig_i : in std_logic;
        ch3_end_trig_i : in std_logic;
        ch4_end_trig_i : in std_logic;
        ch5_end_trig_i : in std_logic;
        ch6_end_trig_i : in std_logic;
        ch7_end_trig_i : in std_logic;
        ch8_end_trig_i : in std_logic;
        ch9_end_trig_i : in std_logic;
        ch10_end_trig_i : in std_logic;
        prebuf_dly_i : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0);
        a_clk_o : out t_trig_array;
        a_pulse_cntr_o : out t_pulse_cntr_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array);
    end component;
    
--! Components of filters
    
    -- Component declaration of the "droop_compensation_wrapper(droop_compensation_wrapper_arch)" unit defined in
    -- file: "./../../src/custom_cores/droop_compensation_wrapper.vhd"
    component droop_compensation_wrapper
    port(
        a_clk_i : in t_trig_array;
        rst_i : in std_logic;
        en_i : in std_logic_vector(9 downto 0);
        droop_factor_i : in  t_droop_factor_array;
        sampling_active_i : in std_logic;
        a_pulse_ctrl_i : in t_pulse_ctrl_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array;
        a_channel_data_i : in t_channel_array;
        a_channel_data_o : out t_channel_array);
    end component;
    
    -- Component declaration of the "bg_average_wrapper(bg_average_wrapper_arch)" unit defined in
    -- file: "./../../src/custom_cores/bg_average_wrapper.vhd"
    component bg_average_wrapper
    port(
        a_clk_i : in t_trig_array;
        rst_i : in std_logic;
        en_i : in std_logic_vector(9 downto 0);
        prebuf_dly_i : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0);
        average_o : out t_channel_array;
        a_pulse_ctrl_i : in t_pulse_ctrl_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array;
        a_channel_data_i : in t_channel_array;
        a_channel_data_o : out t_channel_array);
    end component;
    
    -- Component declaration of the "moving_average_filter_wrapper(moving_average_filter_wrapper_arch)" unit defined in
    -- file: "./../../src/custom_cores/moving_average_filter_wrapper.vhd"
    component moving_average_filter_wrapper
    port(
        a_clk_i : in t_trig_array;
        rst_i : in std_logic;
        en_i : in std_logic_vector(9 downto 0);
        window_size_i : in std_logic_vector(c_WINDOW_WIDTH-1 downto 0);
        a_pulse_ctrl_i : in t_pulse_ctrl_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array;
        a_channel_data_i : in t_channel_array;
        a_channel_data_o : out t_channel_array);
    end component;
    
    -- Component declaration of the "diff_itl_detection_wrapper(diff_itl_detection_wrapper_arch)" unit defined in
    -- file: "./../../src/custom_cores/diff_itl_detection_wrapper.vhd"
    component diff_itl_detection_wrapper
    port(
        a_clk_i : in t_trig_array;
        rst_i : in std_logic;
        en_i : in std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
        itl_offset_i : in std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
        a_pulse_cntr_i : in t_pulse_cntr_array;
        itl_thr_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        itl_clr_i : in std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
        itl_reg_o : out std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
        itl_o : out std_logic;
        itl_hb_o : out std_logic;
        a_pulse_ctrl_i : in t_pulse_ctrl_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array;
        a_channel_data_i : in t_channel_array;
        a_channel_data_o : out t_channel_array);
    end component;
    
--! Support components

    -- Component declaration of the "SIS(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component sis
    generic(
        width : positive := 2);
    port(
        clk_i : in std_logic;
        sig_i : in std_logic;
        sig_o : out std_logic);
    end component;
    
    -- component declaration of the "extsig(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component extsig
    generic(
        cycles : positive := 2);
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        sig_i : in std_logic;
        sig_o : out std_logic);
    end component;
    
    -- Component declaration of the "rsff(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component rsff
    port(
        s_i : in std_logic;
        r_i : in std_logic;
        q_o : out std_logic;
        q_n_o : out std_logic;
        clk_i : in std_logic;
        rst_i : in std_logic);
    end component;
    
    -- component declaration of the "fed(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component fed
    port(
        sig_i : in std_logic;
        rst_i : in std_logic;
        clk_i : in std_logic;
        sig_o : out std_logic);
    end component;
    
    --! Pulse detection (no delay)  
    signal a_clk           : t_trig_array;
    signal a_pulse_ctrl    : t_pulse_ctrl_array;
    signal a_pulse_cntr    : t_pulse_cntr_array;
    signal a_channel_data  : t_channel_array;
    
    --! Reset combined from external and register reset
    signal s_rst : std_logic;
    
    --! Pipeline stage 1 - Background average calculation and subtruction  (1 cc delay)
    signal s_average_en_reg   : std_logic_vector(c_NUM_OF_CHANNELS-1 downto 0);
    signal a_pulse_ctrl_p2    : t_pulse_ctrl_array;
    signal a_channel_data_p2  : t_channel_array;
    
    --! Pipeline stage 2 - Droop compensation  (1 cc delay)
    signal s_droop_en_reg     : std_logic_vector(c_NUM_OF_CHANNELS-1 downto 0);
    signal a_pulse_ctrl_p1    : t_pulse_ctrl_array;
    signal a_channel_data_p1  : t_channel_array;
    
    --! Pipeline stage 3 - Moving average filter  (1 cc delay)
    signal s_filter_en_reg    : std_logic_vector(c_NUM_OF_CHANNELS-1 downto 0);
    signal a_pulse_ctrl_p3    : t_pulse_ctrl_array;
    signal a_channel_data_p3  : t_channel_array;
    
    --! Pipeline stage 4 - Differential interlock detection (0 cc delay)
    signal s_itl_en_reg       : std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
    signal a_pulse_ctrl_p4    : t_pulse_ctrl_array;
    signal a_channel_data_p4  : t_channel_array;
    
    --! Signals for short buffer warning status register
    signal s_set_reg        : std_logic;
    signal s_rst_reg        : std_logic;
    signal s_samp_act_fe    : std_logic;
    
    --! Signal conditioning
    signal s_itl_clr_sis  : std_logic_vector(c_NUM_OF_ADCS-1 downto 0);
    signal s_itl_clr_ext  : std_logic_vector(c_NUM_OF_ADCS-1 downto 0);

   
begin
    --! Input assignment 
    
    --! Combine reset
    s_rst <= rst_i or r_ctrl_i.s_reg_rst;
    
    --!  uncomment lines for more channels
    a_channel_data(0) <= ch1_data_i;
    a_channel_data(1) <= ch2_data_i;
    a_channel_data(2) <= ch3_data_i;
    a_channel_data(3) <= ch4_data_i;
    a_channel_data(4) <= ch5_data_i;
    a_channel_data(5) <= ch6_data_i;
    a_channel_data(6) <= ch7_data_i;
    a_channel_data(7) <= ch8_data_i;
    a_channel_data(8) <= ch9_data_i;
    a_channel_data(9) <= ch10_data_i;
   
   
    comp_p0_pulse_detection_wrapper : pulse_detection_wrapper
    port map(
        adc1_clk_i => adc1_clk_i,
        adc2_clk_i => adc2_clk_i,
        adc3_clk_i => adc3_clk_i,
        adc4_clk_i => adc4_clk_i,
        adc5_clk_i => adc5_clk_i,
        rst_i => s_rst,
        sampling_end_i => s_samp_act_fe,
        ch1_start_trig_i => ch1_start_trig_i,
        ch2_start_trig_i => ch2_start_trig_i,
        ch3_start_trig_i => ch3_start_trig_i,
        ch4_start_trig_i => ch4_start_trig_i,
        ch5_start_trig_i => ch5_start_trig_i,
        ch6_start_trig_i => ch6_start_trig_i,
        ch7_start_trig_i => ch7_start_trig_i,
        ch8_start_trig_i => ch8_start_trig_i,
        ch9_start_trig_i => ch9_start_trig_i,
        ch10_start_trig_i => ch10_start_trig_i,
        ch1_end_trig_i => ch1_end_trig_i,
        ch2_end_trig_i => ch2_end_trig_i,
        ch3_end_trig_i => ch3_end_trig_i,
        ch4_end_trig_i => ch4_end_trig_i,
        ch5_end_trig_i => ch5_end_trig_i,
        ch6_end_trig_i => ch6_end_trig_i,
        ch7_end_trig_i => ch7_end_trig_i,
        ch8_end_trig_i => ch8_end_trig_i,
        ch9_end_trig_i => ch9_end_trig_i,
        ch10_end_trig_i => ch10_end_trig_i,
        prebuf_dly_i => prebuf_dly_i,
        a_clk_o => a_clk,
        a_pulse_cntr_o => a_pulse_cntr,
        a_pulse_ctrl_o => a_pulse_ctrl
    );
     
    comp_p1_bg_average : bg_average_wrapper
    port map(
        a_clk_i => a_clk,
        rst_i => s_rst,
        en_i => s_average_en_reg,
        prebuf_dly_i => prebuf_dly_i,
        average_o => r_stat_o.s_average, 
        
        a_pulse_ctrl_i => a_pulse_ctrl,
        a_pulse_ctrl_o => a_pulse_ctrl_p1,
        a_channel_data_i => a_channel_data,
        a_channel_data_o => a_channel_data_p1
    );
     
   comp_p2_droop_compensation : droop_compensation_wrapper
    port map(
        a_clk_i => a_clk,
        rst_i => s_rst,
        en_i => s_droop_en_reg,
        droop_factor_i => r_ctrl_i.s_droop_factor,
        sampling_active_i => sampling_active_i,
        
        a_pulse_ctrl_i => a_pulse_ctrl_p1,
        a_pulse_ctrl_o => a_pulse_ctrl_p2,
        a_channel_data_i => a_channel_data_p1,
        a_channel_data_o => a_channel_data_p2
    );
    

    
    comp_p3_filter : moving_average_filter_wrapper
    port map(
        a_clk_i => a_clk,
        rst_i => s_rst,
        en_i => s_filter_en_reg,
        window_size_i => r_ctrl_i.s_window_size,      
        
        a_pulse_ctrl_i => a_pulse_ctrl_p2,      
        a_pulse_ctrl_o => a_pulse_ctrl_p3, 
        a_channel_data_i => a_channel_data_p2,
        a_channel_data_o => a_channel_data_p3
     );
    
    comp_p4_interlock : diff_itl_detection_wrapper 
    port map(
        a_clk_i => a_clk,
        rst_i => s_rst,
        en_i => s_itl_en_reg,
        itl_offset_i => r_ctrl_i.s_itl_offset,
        a_pulse_cntr_i => a_pulse_cntr,
        itl_thr_i => r_ctrl_i.s_itl_thr,
        itl_clr_i => s_itl_clr_ext,
        itl_reg_o => r_stat_o.s_itl_reg,
        itl_o => itl_o,
        itl_hb_o => itl_hb_o,
        a_pulse_ctrl_i => a_pulse_ctrl_p3,
        a_pulse_ctrl_o => a_pulse_ctrl_p4,
        a_channel_data_i => a_channel_data_p3,
        a_channel_data_o => a_channel_data_p4
    );
   
--! Output assignment output the last pipeline channel data 
--! uncomment lines for more channels  
--! FIXME check if the last pipeline stage is output
    ch1_data_o <= a_channel_data_p4(0);
    ch2_data_o <= a_channel_data_p4(1);   
    ch3_data_o <= a_channel_data_p4(2);
    ch4_data_o <= a_channel_data_p4(3);
    ch5_data_o <= a_channel_data_p4(4);
    ch6_data_o <= a_channel_data_p4(5);
    ch7_data_o <= a_channel_data_p4(6);
    ch8_data_o <= a_channel_data_p4(7);
    ch9_data_o <= a_channel_data_p4(8);
    ch10_data_o <= a_channel_data_p4(9);  
    
--! FIXME set the correct pipeline stage that the data will be alligned    
    ch1_start_trig_o <= a_pulse_ctrl_p1(0).s_start_trig;
    ch2_start_trig_o <= a_pulse_ctrl_p1(1).s_start_trig;
    ch3_start_trig_o <= a_pulse_ctrl_p1(2).s_start_trig;
    ch4_start_trig_o <= a_pulse_ctrl_p1(3).s_start_trig;
    ch5_start_trig_o <= a_pulse_ctrl_p1(4).s_start_trig;
    ch6_start_trig_o <= a_pulse_ctrl_p1(5).s_start_trig;
    ch7_start_trig_o <= a_pulse_ctrl_p1(6).s_start_trig;
    ch8_start_trig_o <= a_pulse_ctrl_p1(7).s_start_trig;
    ch9_start_trig_o <= a_pulse_ctrl_p1(8).s_start_trig;
    ch10_start_trig_o <= a_pulse_ctrl_p1(9).s_start_trig;
    
--! Syncronise the itl_clr to ADC clocks and extend the clear signal
    generate_sync : for index in 0 to c_NUM_OF_ADCS-1 generate
        comp_sis : SIS
        generic map(
            WIDTH => 2
        )
        port map(
            clk_i => a_clk(2*index),
            sig_i => r_ctrl_i.s_itl_clr(index),
            sig_o => s_itl_clr_sis(index)
        );
        
        comp_ext : ExtSig
        generic map(
            CYCLES => 5
        )
        port map(
            clk_i => a_clk(2*index),
            rst_i => s_rst,
            sig_i => s_itl_clr_sis(index),
            sig_o => s_itl_clr_ext(index)
        );
    end generate generate_sync;

--! Register enable signals when ch5_start_trig_i arrives, 
--! so the change of enable signals will not interfere in the middle of the pulse.
--!
--! NOTE: The moving average filter is enabled only during active sampling period
--!          - Enabled on ch5_start_trig_i
--!          - Disabled on the falling edge of sampling_active_i
   p_sync_register : process(adc3_clk_i)
   begin
       if rising_edge(adc3_clk_i) then
           if s_rst = '1' then
              s_droop_en_reg    <= (others => '0');
              s_average_en_reg  <= (others => '0');
              s_filter_en_reg   <= (others => '0');
              s_itl_en_reg      <= (others => '0');
              
           --! Register enable signals
           elsif ch5_start_trig_i = '1' then 
              s_droop_en_reg    <= r_ctrl_i.s_droop_en;
              s_average_en_reg  <= r_ctrl_i.s_average_en;
              s_filter_en_reg   <= r_ctrl_i.s_filter_en;
              s_itl_en_reg      <= r_ctrl_i.s_itl_en;
              
          --! Register disable filter enable and leave all other enables as they were 
          elsif s_samp_act_fe = '1' then
              s_droop_en_reg <= s_droop_en_reg;
              s_average_en_reg <= s_average_en_reg;
              s_itl_en_reg <= s_itl_en_reg;
              s_filter_en_reg <= (others => '0');
           else
              s_droop_en_reg <= s_droop_en_reg;
              s_average_en_reg <= s_average_en_reg;
              s_filter_en_reg <= s_filter_en_reg;
              s_itl_en_reg <= s_itl_en_reg;
           end if; -- s_rst
       end if; -- rising_edge(clk_i)   
   end process p_sync_register;
   
   
--! Check if the sample buffer is large enough and issue warning if it is not
    comp_active_fed : FED
    port map(
        sig_i => sampling_active_i,
        rst_i => s_rst,
        clk_i => adc3_clk_i,
        sig_o => s_samp_act_fe
    );
    
    s_set_reg <= '1' when s_samp_act_fe = '1' and a_pulse_ctrl_p4(0).s_pulse_valid = '1' else '0';    
    s_rst_reg <= '1' when s_samp_act_fe = '1' and a_pulse_ctrl_p4(0).s_pulse_valid = '0' else '0';    
    
    comp_short_buffer_reg : RSFF
    port map(
        s_i => s_set_reg,
        r_i => s_rst_reg,
        q_o => r_stat_o.s_stat_reg(0),
        q_n_o => open,
        clk_i => adc3_clk_i,
        rst_i => s_rst
    );

--------------------------------------------------------------------------- 
end custom_signal_processing_arch;
