-------------------------------------------------------
--! @brief      Calculates the average of the samples  
--!             inside the prebuffer and substracts this average from the pulse.
-------------------------------------------------------
--! @details    The background average is calculated between start_trig_i and start_pulse_i pulses.
--!             The new average value is updated when the start_pulse_i is received.
--!             The average is subtracted from the sample count if the block is enabled.
--!
--!             Note: The prebuf_dly_i has to have a value in order of 2 (1,2,4,8,16,.., 2048), otherwise
--!             the average of ZERO is returned. This filter assumes that the input is bipolar with c_ZERO_OFFSET.
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Wed Sep 18 16:44:50 2013 2013 created                      
--! @date Wed Sep 18 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Wed Sep 18 16:44:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity bg_average is
    port(
       clk_i : in std_logic;
       rst_i : in std_logic;
       
       --! Enable background average subtraction
       en_i  : in std_logic;
       
       --! Start of data ACQ
       start_trig_i : in std_logic;
       
       --! Start of pulse data
       start_pulse_i : in std_logic;
       
       --! Indicator that samples belong to the pulse 
       pulse_valid_i : in std_logic;
       
       --! ADC data 
       data_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
       data_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0);
       
       --! Average 
       average_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0); 
       
       --! The prebuffer delay (0,1,2,4,8,16,..,2048)
       prebuf_dly_i     : in std_logic_vector(c_PREBUFFER_WIDTH-1 downto 0)
    );
end bg_average;

architecture bg_average_arch of bg_average is      

   signal s_sum : std_logic_vector(27 downto 0);
   signal s_raw_average,  s_average_reg : std_logic_vector(27 downto 0);
   signal s_sub_data  : std_logic_vector(c_DATA_WIDTH downto 0);
   signal s_add_data  : std_logic_vector(c_DATA_WIDTH downto 0);
   
   
   signal s_data,s_sub_data_limit,s_add_data_limit   : std_logic_vector(c_DATA_WIDTH-1 downto 0);
   
   --! Delay data and valid signal to allign with the calculated average
   signal s_data_d1  : std_logic_vector(c_DATA_WIDTH-1 downto 0);
   signal s_valid_d1 : std_logic;
begin
   
   --! SUM of the prebuffer samples   
   p_sync_sum : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
              s_sum <= (others => '0');
           elsif start_trig_i = '1' then 
              s_sum <= x"000" & data_i;
           else
              s_sum <= std_logic_vector( unsigned(s_sum) + unsigned(data_i) );
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_sum;
   
   --! Division mux
   with prebuf_dly_i select
   s_raw_average  <=    (s_raw_average'left downto c_DATA_WIDTH => '0')&
                        c_ZERO_OFFSET                                 when x"000",
                        s_sum                                         when x"001",
                        "0"            & s_sum(s_sum'left downto 1)   when x"002",
                        "00"           & s_sum(s_sum'left downto 2)   when x"004",
                        "000"          & s_sum(s_sum'left downto 3)   when x"008",
                        "0000"         & s_sum(s_sum'left downto 4)   when x"010",
                        "00000"        & s_sum(s_sum'left downto 5)   when x"020",
                        "000000"       & s_sum(s_sum'left downto 6)   when x"040",
                        "0000000"      & s_sum(s_sum'left downto 7)   when x"080",
                        "00000000"     & s_sum(s_sum'left downto 8)   when x"100",
                        "000000000"    & s_sum(s_sum'left downto 9)   when x"200",
                        "0000000000"   & s_sum(s_sum'left downto 10)  when x"400",
                        "00000000000"  & s_sum(s_sum'left downto 11)  when x"800",
                        (s_raw_average'left downto c_DATA_WIDTH => '0')&
                        c_ZERO_OFFSET                                 when others; 
                        
   --! Register average when start pulse received   
   p_sync_av_reg: process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
              s_average_reg <= (s_average_reg'left downto c_DATA_WIDTH => '0')& c_ZERO_OFFSET;
           elsif start_pulse_i = '1' then 
              s_average_reg <= s_raw_average;
           else
              s_average_reg <= s_average_reg;
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_av_reg;
   
   --! Delay the data and valid signals for 1 cc
   p_sync_delay : process(clk_i)
   begin
       if rising_edge(clk_i) then
           if rst_i = '1' then
              s_data_d1  <= (others => '0');
              s_valid_d1 <= '0';
           else
              s_data_d1  <= data_i;
              s_valid_d1 <= pulse_valid_i;
           end if; -- rst_i
       end if; -- rising_edge(clk_i)   
   end process p_sync_delay;  
                       
  --! Output assignment
  average_o <= s_average_reg(c_DATA_WIDTH-1 downto 0);
  
  --! Subtract and add average to data c_ZERO_OFFSET
  s_sub_data <=  std_logic_vector(unsigned(s_data_d1) - (unsigned(s_average_reg(c_DATA_WIDTH downto 0)) - unsigned(c_ZERO_OFFSET) ) );  
  s_add_data <=  std_logic_vector(unsigned(s_data_d1) + (unsigned(c_ZERO_OFFSET) - unsigned(s_average_reg(c_DATA_WIDTH downto 0)) ) );
  
  --! Limit data if the value overflows
  s_sub_data_limit  <=  s_sub_data(c_DATA_WIDTH-1 downto 0)  when s_sub_data(c_DATA_WIDTH)= '0' else (c_DATA_WIDTH-1 downto 0 => '0');
  s_add_data_limit  <=  s_sub_data(c_DATA_WIDTH-1 downto 0)  when s_sub_data(c_DATA_WIDTH)= '0' else (c_DATA_WIDTH-1 downto 0 => '1');
  
  --! Output the ADD or SUB   
  s_data <= s_sub_data_limit when (unsigned(s_average_reg(c_DATA_WIDTH-1 downto 0)) >  unsigned(c_ZERO_OFFSET)) else s_add_data_limit;  
  --! Output data subtracted data if enabled  
  data_o <= s_data when (en_i = '1') else s_data_d1;

end bg_average_arch;
