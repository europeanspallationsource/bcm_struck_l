-------------------------------------------------------
--! @brief    Wrapper for diff_itl_detection block.
-------------------------------------------------------
--! @details  Generates diff_itl_detection block for every ADC or channel pair.  
--!           Combines the interlock registers into single interlock output (itl_o) 
--------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Mon Oct 21 16:44:50 2013 2013 created
--! @date Mon Oct 21 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Mon Oct 21 08:25:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity diff_itl_detection_wrapper is
    port(   
        a_clk_i        : in  t_trig_array;
        rst_i          : in  std_logic;
        en_i           : in  std_logic_vector (c_NUM_OF_ADCS-1 downto 0);
 
        --! Interlock register offset        
        itl_offset_i   : in  std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
        a_pulse_cntr_i : in  t_pulse_cntr_array;

        --! Interlock threshold        
        itl_thr_i      : in  std_logic_vector(c_DATA_WIDTH-1 downto 0);
        
        --! Clear interlock
        itl_clr_i      : in  std_logic_vector (c_NUM_OF_ADCS-1 downto 0);
        
        --! Interlock registers output        
        itl_reg_o      : out  std_logic_vector (c_NUM_OF_ADCS-1 downto 0);
        
        --! Interlock output
        itl_o          : out std_logic;
        itl_hb_o       : out std_logic;
         
        a_pulse_ctrl_i   : in  t_pulse_ctrl_array;
        a_pulse_ctrl_o   : out t_pulse_ctrl_array;
        a_channel_data_i : in  t_channel_array;
        a_channel_data_o : out t_channel_array
     );
end diff_itl_detection_wrapper;


architecture diff_itl_detection_wrapper_arch of diff_itl_detection_wrapper is

    -- Component declaration of the "diff_itl_detection(diff_itl_detection_arch)" unit defined in
    -- file: "./../../src/custom_cores/diff_itl_detection.vhd"
    component diff_itl_detection
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        en_i : in std_logic;
        data_1_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        data_2_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        itl_thr_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        itl_offset_i : in std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
        samp_cntr_i : in std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
        pulse_valid_i : in std_logic;
        itl_clr_i : in std_logic;
        itl_reg_o : out std_logic);
    end component;
    
    -- Component declaration of the "cntr(cntr_arch)" unit defined in
	-- file: "./../../src/custom_cores/shared_components.vhd"
	component cntr
	generic(
		g_size : positive := 4;
		g_incr : std_logic := '1');
	port(
		clk_i : in std_logic;
		rst_i : in std_logic;
		load_i : in std_logic;
		en_i : in std_logic;
		load_data_i : in std_logic_vector(g_size-1 downto 0);
		cntr_o : out std_logic_vector(g_size-1 downto 0));
	end component;
    
    -- Component declaration of the "RSFF(rtl)" unit defined in
	-- file: "./../../src/custom_cores/shared_components.vhd"
	component rsff
	port(
		s_i : in std_logic;
		r_i : in std_logic;
		q_o : out std_logic;
		q_n_o : out std_logic;
		clk_i : in std_logic;
		rst_i : in std_logic);
	end component;
   
    --! Internal signals
    signal s_itl_reg       : std_logic_vector(4 downto 0);
    signal s_itl           : std_logic;
    
    --! Heartbeat
    signal s_hb_stb        : std_logic;
    signal s_hb_cntr       : std_logic_vector(c_HB_CNTR_WIDTH-1 downto 0);
    signal s_hb            : std_logic;
    
begin
    
    --! For every channel pair on the same ADC generate an interlock block
    generate_adcs : for index in 0 to c_NUM_OF_ADCS-1 generate   
    comp_itl_detection : diff_itl_detection
    port map(
        clk_i           => a_clk_i(2*index),
        rst_i           => rst_i,
        en_i            => en_i(index),
        data_1_i        => a_channel_data_i(2*index),
        data_2_i        => a_channel_data_i(2*index+1),
        itl_thr_i       => itl_thr_i,
        itl_offset_i    => itl_offset_i,
        samp_cntr_i     => a_pulse_cntr_i(2*index),
        pulse_valid_i   => a_pulse_ctrl_i(2*index).s_pulse_valid,
        itl_clr_i       => itl_clr_i(index),
        itl_reg_o       => s_itl_reg(index)
    );
    end generate generate_adcs;

    --! Combine all itl registers into one output to connect to output
    s_itl <= '1' when s_itl_reg = (s_itl_reg'range => '1') else '0';
    itl_o <= s_itl; 
    
    --! Heartbeat (used for blinking led, it can also be used to drive itl output)   
    conp_HB_cntr : cntr
    generic map(
        g_size => c_HB_CNTR_WIDTH,
        g_incr => '1'
    )
    port map(
        clk_i => a_clk_i(0),
        rst_i => rst_i,
        load_i => s_hb_stb,
        en_i => s_itl,
        load_data_i => (s_hb_cntr'range => '0'),
        cntr_o => s_hb_cntr
    );  
    
    --! Tick one CC when counter reaches c_HB_period
    s_hb_stb <= '1' when s_hb_cntr = c_HB_PERIOD else '0';
    
    p_sync_HB_reg : process(a_clk_i(0))
    begin
       if rising_edge(a_clk_i(0)) then
           if rst_i = '1' then
              s_hb    <= '0';
           elsif s_hb_stb = '1' then
              s_hb    <= not s_hb;               
           else
              s_hb    <= s_hb;
           end if;
       end if;
    end process p_sync_HB_reg;
    
    --! Output assignament
    itl_HB_o <= s_hb;
    
    --! Assign the itl registers to output
    itl_reg_o  <= s_itl_reg;

    --! Assign pipeline (just pass through the module 0 cc delay)
    a_pulse_ctrl_o   <= a_pulse_ctrl_i;
    a_channel_data_o <= a_channel_data_i;

end diff_itl_detection_wrapper_arch;
