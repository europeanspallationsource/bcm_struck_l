-------------------------------------------------------
--! @brief      Differential interlock for the channel pairs.
-------------------------------------------------------
--! @details    The differential interlock block compares the 
--!             pulses of two adjacent ADC signals and triggers an 
--!             interlock response if the signal differences are 
--!             higher than threshold.
--!
--!             The start time of comparison can be adjusted by adjusting the 
--!             itl_offset_i register.
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Mon Oct 21 16:44:50 2013 2013 created
--! @date Mon Oct 21 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Mon Oct 21 08:25:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity diff_itl_detection is
     port(
         clk_i          : in std_logic;
         rst_i          : in std_logic;
         en_i           : in std_logic;
         data_1_i       : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
         data_2_i       : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
         itl_thr_i      : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
         itl_offset_i   : in std_logic_vector(c_BUFFER_WIDTH-1 downto 0);
         samp_cntr_i    : in std_logic_vector(c_BUFFER_WIDTH-1 downto 0);         
         pulse_valid_i  : in std_logic;
         itl_clr_i      : in std_logic;
         
         --! Interlock register output
         itl_reg_o      : out std_logic
     );
end diff_itl_detection;


architecture diff_itl_detection_arch of diff_itl_detection is

    -- component declaration of the "rsff(rtl)" unit defined in
    -- file: "./src/shared_components.vhd"
    component rsff
    port(
        s_i : in std_logic;
        r_i : in std_logic;
        q_o : out std_logic;
        q_n_o : out std_logic;
        clk_i : in std_logic;
        rst_i : in std_logic);
    end component;
        
    --! Internal signals
    signal s_rst_itl        : std_logic;
    signal s_set_itl        : std_logic;
    
    signal s_curr_diff      : std_logic_vector(c_DATA_WIDTH-1 downto 0);
    
    signal s_curr_diff_cmp  : std_logic;
    
    signal s_offset_cmp     : std_logic;    
    
begin
    --! Calculate current difference
    s_curr_diff     <= std_logic_vector(unsigned(data_2_i) - unsigned(data_1_i)) when unsigned(data_2_i) > unsigned(data_1_i) else std_logic_vector(unsigned(data_1_i) - unsigned(data_2_i));
    
    --! Compare the current difference to the threshold set by the user
    s_curr_diff_cmp <= '1' when unsigned(s_curr_diff)> unsigned(itl_thr_i) else '0';
    
    --! Compare the sample counter to interlock offset set by the user
    --! NOTE: we have to deduct three clock cycles from the sample counter
    --! because the data in this pipeline stage is delayed for 3 clock cycles
    --! from the pulse detection block. FIXME: If you add a pipeline stage check if this value is correct!
    s_offset_cmp    <= '1' when (unsigned(samp_cntr_i)-3)>= unsigned(itl_offset_i) else '0';
    
    --! Reset the Interlock to '0' Not OK if all the conditions are met
    s_rst_itl <= en_i and pulse_valid_i and s_curr_diff_cmp and s_offset_cmp;
    s_set_itl <= itl_clr_i;                
    
    --! Interlock register inverted logic:
    --!     - '1' Interlock OK, 
    --!     - '0' Interlock not OK
    comp_intrlock_reg : rsff
    port map(
      s_i => s_set_itl,
      r_i => s_rst_itl,
      q_o => itl_reg_o,
      q_n_o => open,
      clk_i => clk_i,
      rst_i => rst_i
    );
-----------------------------------------------------
end diff_itl_detection_arch;
