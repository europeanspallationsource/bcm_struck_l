-------------------------------------------------------
--! @brief    Wrapper for droop_compensation block.
-------------------------------------------------------
--! @details  Generates droop_compensation block for every channel.  
--!           Delays r_pulse_ctrl record array for 1 cc.
--!            
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Tue Sep 17 16:30:54 2013 2013 created
--! @date Tue Sep 17 16:30:54 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Tue Sep 17 16:30:54 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity droop_compensation_wrapper is
    port(
        a_clk_i           : in  t_trig_array;
        rst_i             : in  std_logic;
        en_i              : in  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);
        droop_factor_i    : in  t_droop_factor_array;
        sampling_active_i : in  std_logic;
        
        a_pulse_ctrl_i    : in  t_pulse_ctrl_array;
        a_pulse_ctrl_o    : out t_pulse_ctrl_array; 
        a_channel_data_i  : in  t_channel_array;
        a_channel_data_o  : out t_channel_array
    );
end droop_compensation_wrapper;

architecture droop_compensation_wrapper_arch of droop_compensation_wrapper is

    -- Component declaration of the "droop_compensation(droop_compensation_arch)" unit defined in
    -- file: "./src/droop_compensation.vhd"
    component droop_compensation
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        en_i : in std_logic;
        start_pulse_i : in std_logic;
        sampling_active_i : in std_logic;
        data_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        data_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0);
        droop_factor_i : in std_logic_vector(c_DROOP_FACTOR_WIDTH-1 downto 0));
    end component;
    
    signal a_pulse_ctrl_d1 : t_pulse_ctrl_array;
    
begin
   
generate_channels : for index in 0 to c_NUM_OF_CHANNELS-1 generate   
   comp_droop_compensation : droop_compensation
   port map(
      clk_i => a_clk_i(index),
      rst_i => rst_i,
      en_i => en_i(index),
      droop_factor_i => droop_factor_i(index),
      start_pulse_i => a_pulse_ctrl_i(index).s_start_pulse,
      sampling_active_i => sampling_active_i,
      
      data_i => a_channel_data_i(index),
      data_o => a_channel_data_o(index)
   );
end generate generate_channels;

--! Delay all signals in the control record for 1 clock cycle 
generate_delays : for index in 0 to c_NUM_OF_CHANNELS-1 generate    
   p_sync_delay : process(a_clk_i(index))
   begin
       if rising_edge(a_clk_i(index)) then
           a_pulse_ctrl_d1(index)  <= a_pulse_ctrl_i(index);
       end if; -- rising_edge(clk_i)   
   end process p_sync_delay;
end generate generate_delays;

--! Output assignment no delay
a_pulse_ctrl_o <= a_pulse_ctrl_d1;
   
end droop_compensation_wrapper_arch;
