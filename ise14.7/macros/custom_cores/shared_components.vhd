 -------------------------------------------------------
--! @brief     Counter
-------------------------------------------------------
--! @details   
--!            g_size    - determins counter width (counter is g_size bits wide)
--!            g_incr    - inctrement value or decrement value (example: 1 -> +, 0 -> -)
---------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Tue Mar 26 11:40:37 2013 created
--! @date Tue Mar 26 11:40:37 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Tue Mar 26: Created
-------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity cntr is
   generic (
      g_size      : positive:= 4;
      g_incr      : std_logic:= '1'
   );
   port( 
   --! Inputs 
      clk_i       : in std_logic;
      rst_i       : in std_logic; --! Reset clears counter to 0
      load_i      : in std_logic; --! Loads the counter with load data
      en_i        : in std_logic; --! Count enable
      load_data_i : in std_logic_vector(g_size-1 downto 0); --! Data to be loaded
      
      --! Outputs
      cntr_o      : out std_logic_vector(g_size-1 downto 0)
   );
end cntr;
architecture cntr_arch of cntr is

signal s_counter : unsigned(cntr_o'range);
signal s_load_data : unsigned(load_data_i'range);

begin
   
   s_load_data <= unsigned(load_data_i);  
   
   p_counter : process(clk_i)
   begin
      if rising_edge(clk_i) then
           if rst_i = '1' then
               s_counter <= (others => '0');  
           elsif load_i ='1' then
               s_counter <= s_load_data;      
           elsif en_i = '1' then
               if g_incr = '1' then
                  s_counter <= s_counter + 1;
               else
                  s_counter <= s_counter - 1;                     
               end if;
          else
             s_counter <= s_counter;
          end if;
      end if;    
   end process p_counter;

   cntr_o <= std_logic_vector(s_counter);
   
end cntr_arch;



library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- double sample sig_i signals to avoid metastable states
entity SIS is
    generic(
        WIDTH : positive := 2
    );
    port(
      clk_i   : in std_logic;
        sig_i   : in std_logic;
        sig_o   : out std_logic
    );
end SIS;

architecture RTL of SIS is
    signal s_sr : std_logic_vector(WIDTH-1 downto 0);
begin
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            s_sr <= s_sr(s_sr'high-1 downto 0) & sig_i;
        end if;
    end process;
    
    sig_o <= s_sr(s_sr'high);
    
end RTL;

-- *************************************************** 

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- detect rising edge
entity RED is
    port (
        sig_i   : in std_logic;
        sig_o   : out std_logic;
        rst_i   : in std_logic;
        clk_i   : in std_logic
    );
end RED;

architecture RTL of RED is
    signal s_1      : std_logic;
  --  signal s_re     : std_logic;
begin
    
    p_sync : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i='1' then 
                s_1     <= '0';
            else
                s_1     <= sig_i;
            end if;
        end if;
    end process;
    
    sig_o <= sig_i and not s_1;
end RTL;

-- *************************************************** 
library IEEE;
use IEEE.STD_LOGIC_1164.all;
                               
-- detect falling edge
entity FED is
    port (
        sig_i    : in std_logic;
        rst_i    : in std_logic;
        clk_i    : in std_logic;
        sig_o    : out std_logic 
    );
end FED;

architecture RTL of FED is
    signal s_1      : std_logic;
 --   signal s_fe     : std_logic;
begin                      
    
    p_sync : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i='1' then 
                s_1     <= '0';
            else
                s_1     <= sig_i;
            end if;
        end if;
    end process;
    
    sig_o <= not sig_i and s_1;
    
end RTL;

-- ***************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- extend signal for n cycles
entity ExtSig is
    generic(
        CYCLES : positive := 2
    );
    port(
        clk_i   : in std_logic;
        rst_i   : in std_logic;
        sig_i   : in std_logic;
        sig_o   : out std_logic
    );
end ExtSig;

architecture RTL of ExtSig is
    constant c_CYCLES: natural := CYCLES-1;
    signal s_sigExtended : std_logic;
    signal s_divider : natural range 0 to c_CYCLES;
    signal s_dividerZero: std_logic;
begin
    p_extendSignal: process(clk_i)
    begin
        if rising_edge(clk_i) then
            --s_sr <= s_sr(s_sr'high-1 downto 0) & sig_i;
            if rst_i='1' then
                s_sigExtended <= '0';
                s_divider <= 0;
            else
                if sig_i='1' then
                    s_sigExtended <= '1';
                    s_divider <= c_CYCLES;
                elsif s_dividerZero='1' then
                    s_sigExtended <= '0';
                    s_divider <= 0;
                else
                    s_sigExtended <= '1';
                    s_divider <= s_divider - 1;
                end if;
            end if;
        end if;
    end process p_extendSignal;
    
    s_dividerZero <= '1' when s_divider=0 else '0'; 
    
    sig_o <= s_sigExtended;
    
end RTL;


-- ***************************************************
library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity RSFF is
    port(        
        s_i     : in    std_logic;
        r_i     : in    std_logic;
        q_o     : out   std_logic;
        q_n_o   : out   std_logic;
        
        clk_i   : in    std_logic;
        rst_i   : in    std_logic
    );
end RSFF;


architecture RTL of RSFF is

    signal s_ff : std_logic;
    
begin

    p_syncRSFF : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i='1' then
                s_ff <= '0';
            else
                if(r_i='1') then 
                    s_ff <= '0';
                elsif(s_i='1') then
                    s_ff <= '1';
                else
                    s_ff <= s_ff;
                end if;
            end if;
        end if;
    end process p_syncRSFF;
    
    q_o <= s_ff;
    q_n_o <= not s_ff;
end RTL;

-- ***************************************************
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Description
-- RS FF with priority set to SET input

entity RSFF_S is
    port(        
        s_i     : in    std_logic;
        r_i     : in    std_logic;
        q_o     : out   std_logic;
        q_n_o   : out   std_logic;
        
        clk_i   : in    std_logic;
        rst_i   : in    std_logic
    );
end RSFF_S;
        
architecture RTL of RSFF_S is

    signal s_ff : std_logic;
    
begin

    p_syncRSFF_S : process(clk_i)
    begin
        if rising_edge(clk_i) then
            if rst_i='1' then
                s_ff <= '0';
            else
                if(s_i='1') then 
                    s_ff <= '1';
                elsif(r_i='1') then
                    s_ff <= '0';
                else
                    s_ff <= s_ff;
                end if;
            end if;
        end if;
    end process p_syncRSFF_S;
    
    q_o <= s_ff;
    q_n_o <= not s_ff;
    
end RTL;

    
 





