-------------------------------------------------------
--! @brief    Wrapper for bg_average block.
-------------------------------------------------------
--! @details  Generates bg_average block for every channel.  
--!           Delays r_pulse_ctrl record array for 1 cc.
--!            
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Wed Sep 18 16:44:50 2013 2013 created
--! @date Wed Sep 18 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Wed Sep 18 16:44:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.bcm_package.all;

entity bg_average_wrapper is
    port(
        a_clk_i : in t_trig_array;
        rst_i             : in  std_logic;
        en_i              : in  std_logic_vector (c_NUM_OF_CHANNELS-1 downto 0);
        prebuf_dly_i      : in  std_logic_vector(c_prebuffer_width-1 downto 0);
        average_o         : out  t_channel_array;
      
        a_pulse_ctrl_i : in t_pulse_ctrl_array;
        a_pulse_ctrl_o : out t_pulse_ctrl_array;
        a_channel_data_i : in t_channel_array;
        a_channel_data_o : out t_channel_array
    );
end bg_average_wrapper;

architecture bg_average_wrapper_arch of bg_average_wrapper is

    -- Component declaration of the "bg_average(bg_average_arch)" unit defined in
    -- file: "./src/bg_average.vhd"
    component bg_average
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        en_i : in std_logic;
        start_trig_i : in std_logic;
        start_pulse_i : in std_logic;
        pulse_valid_i : in std_logic;
        data_i : in std_logic_vector(c_DATA_WIDTH-1 downto 0);
        data_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0);
        average_o : out std_logic_vector(c_DATA_WIDTH-1 downto 0);
        prebuf_dly_i : in std_logic_vector(c_prebuffer_width-1 downto 0));
    end component;
   
   signal a_pulse_ctrl_d1 : t_pulse_ctrl_array;
   
begin
   
   generate_channels : for index in 0 to c_NUM_OF_CHANNELS-1 generate   
       comp_bg_average : bg_average
       port map(
          clk_i => a_clk_i(index),
          rst_i => rst_i,
          en_i => en_i(index),
          start_trig_i => a_pulse_ctrl_i(index).s_start_trig,
          start_pulse_i => a_pulse_ctrl_i(index).s_start_pulse,
          pulse_valid_i => a_pulse_ctrl_i(index).s_pulse_valid,
          data_i => a_channel_data_i(index),
          data_o => a_channel_data_o(index),
          average_o => average_o(index),
          prebuf_dly_i => prebuf_dly_i
       );
   end generate generate_channels;

   --! Delay all signals in the control record for 1 clock cycle 
   generate_delays : for index in 0 to c_NUM_OF_CHANNELS-1 generate    
       p_sync_delay : process(a_clk_i(index))
       begin
           if rising_edge(a_clk_i(index)) then
               a_pulse_ctrl_d1(index)  <= a_pulse_ctrl_i(index);
           end if; -- rising_edge(clk_i)   
       end process p_sync_delay;
   end generate generate_delays;
      
   --! Output assignment (1 CC delay)
   a_pulse_ctrl_o <= a_pulse_ctrl_d1;
  
end bg_average_wrapper_arch;
