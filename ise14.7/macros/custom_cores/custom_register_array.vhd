-------------------------------------------------------
--! @brief    Register array for custom registers
-------------------------------------------------------
--! @details  The interfaces:
--!           - Interface to Struck sis8300 PCIe module
--!           - r_ctrl_o: Control register record 
--!           - r_stat_i: Status register record
--! 
--!           The constants, arrays, and records are defined in BCM_package.vhd
--!           This register array can be accessed at the addresses from 0x400 to 0x42f
--!
-------------------------------------------------------
--! @author Uros Legat, Cosylab (urosDOTlegatATcosylabDOTcom)
--
--! @date Wed Sep 23 16:44:50 2013 2013 created
--! @date Wed Sep 23 16:44:50 2013 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! ulegat, Wed Sep 18 16:44:50 2013: Created
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

use work.bcm_package.all;

entity custom_register_array is
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        
        -- ! Interface with PCIe module
        data_i : in std_logic_vector(31 downto 0);
        data_o : out std_logic_vector(31 downto 0);
        addr_i : in std_logic_vector(7 downto 0);
        wr_en_i : in std_logic;
        rd_en_i : in std_logic;
        
        --! Control registers
        r_ctrl_o          : out t_ctrl_register_record;
      
        --! Status registers
        r_stat_i          : in t_stat_register_record

    );
end custom_register_array;

architecture custom_register_array_arch of custom_register_array is

    -- Component declaration of the "ExtSig(rtl)" unit defined in
    -- file: "./../../src/custom_cores/shared_components.vhd"
    component ExtSig
    generic(
        cycles : positive := 2);
    port(
        clk_i : in std_logic;
        rst_i : in std_logic;
        sig_i : in std_logic;
        sig_o : out std_logic);
    end component;

   --! Address decode signals shoving if the address is in the certain address space
   signal s_RegArrayCtlCE  : std_logic;
   signal s_RegArrayStatCE : std_logic;
   
   --! Control register the RW and auto-clear registers
   signal s_regArray_ctl_d,       s_regArray_ctl_q          : t_regArray32bit;
   signal s_regArraySelect_ctl_d, s_regArraySelect_ctl_q    : t_regArray1bit;

   --! Status register the Read-only registers
   signal s_regArray_stat_d,       s_regArray_stat_q        : t_regArray32bit;
   signal s_regArraySelect_stat_d, s_regArraySelect_stat_q  : t_regArray1bit;
   
   --! Reset from register array
   signal s_reg_rst  : std_logic;

begin
    
   --! CE signals if the s_addr is in the certain address space
   s_RegArrayCtlCE   <= '1' when addr_i>=c_REGARRAY_CTL_STARTADDR    and   addr_i<=c_REGARRAY_CTL_ENDADDR   else '0';
   s_RegArrayStatCE  <= '1' when addr_i>=c_REGARRAY_STAT_STARTADDR   and   addr_i<=c_REGARRAY_STAT_ENDADDR  else '0';
   
   --! regArray address decoding
   reg_select_gen : for index in c_REG_ARRAY_LENGTH-1 downto 0 generate
      s_regArraySelect_ctl_d(index)    <= '1' when (addr_i(4 downto 0)=index and wr_en_i='1' and s_RegArrayCtlCE='1')    else '0';
      s_regArraySelect_stat_d(index)   <= '1' when (addr_i(4 downto 0)=index and wr_en_i='1' and s_RegArrayStatCE='1')   else '0';
   end generate reg_select_gen;    
          
   --! Memory access registers
   p_syncReg : process(clk_i)
   begin
      if rising_edge(clk_i) then
         if rst_i='1' or s_reg_rst='1' then
            for index in c_REG_ARRAY_LENGTH-1 downto 0 loop
               s_regArray_ctl_q(index)          <= (others=>'0'); 
               s_regArraySelect_ctl_q(index)    <= '0';
               
               s_regArray_stat_q(index)         <= (others=>'0'); 
               s_regArraySelect_stat_q(index)   <= '0';
            end loop;
         else
            for index in c_REG_ARRAY_LENGTH-1 downto 0 loop
               s_regArray_stat_q(index)         <= s_regArray_stat_d(index);
               s_regArraySelect_stat_q(index)   <= s_regArraySelect_stat_d(index);
               
               s_regArray_ctl_q(index)          <= s_regArray_ctl_d(index);
               s_regArraySelect_ctl_q(index)    <= s_regArraySelect_ctl_d(index);
            end loop;
         end if;                
      end if;
   end process p_syncReg;
   
   --! Output MUX for PCI out (data_o)
   p_dataOutMux: process(clk_i)
   begin
      if rising_edge(clk_i) then
         if rst_i = '1' or s_reg_rst='1' then
            data_o <= (others=>'0');
         else
            if s_RegArrayCtlCE='1' then
               data_o <= s_regArray_ctl_q(conv_integer(addr_i(4 downto 0)));
            elsif s_RegArrayStatCE='1' then
               data_o <= s_regArray_stat_q(conv_integer(addr_i(4 downto 0)));   
            else
               data_o  <= (others=>'0');
            end if;
         end if;
      end if;
   end process;
   
   --! Control registers - WRITE and auto clear
   reg_gen : for index in 0 to c_REG_ARRAY_LENGTH-1 generate
       
         --! Firmware version 
         reg_gen_FW_ver: if  index = c_FW_VER_REG generate
             s_regArray_ctl_d(index) <= c_FW_VER;
         end generate reg_gen_FW_ver;
       
         --! Autoclear registers - value of the register is cleared after one Clock Cycle.
         reg_gen_AC_0: if  index = c_ITL_CLR generate
             s_regArray_ctl_d(index) <= data_i when (s_regArraySelect_ctl_q(index)='1') else (others=> '0');
         end generate reg_gen_AC_0;
         
         reg_gen_AC_1: if  index = c_CTRL_REG generate
             s_regArray_ctl_d(index) <= data_i when (s_regArraySelect_ctl_q(index)='1') else (others=> '0');
         end generate reg_gen_AC_1;
       
         --! Hold value registers - value of register is preserved until re-written
         reg_gen_REG: if  
                          index = c_SW_VER_REG     or
                          index = c_ITL_EN         or
                          index = c_ITL_THR        or
                          index = c_ITL_OFFSET     or
                          index = c_DROOP_EN       or
                          index = c_DROOP_FACTOR0  or
                          index = c_DROOP_FACTOR1  or
                          index = c_DROOP_FACTOR2  or
                          index = c_DROOP_FACTOR3  or
                          index = c_DROOP_FACTOR4  or
                          index = c_DROOP_FACTOR5  or
                          index = c_DROOP_FACTOR6  or
                          index = c_DROOP_FACTOR7  or
                          index = c_DROOP_FACTOR8  or
                          index = c_DROOP_FACTOR9  or
                          index = c_AVERAGE_EN     or
                          index = c_FILTER_EN      or
                          index = c_FILTER_WINDOW   
         generate
            s_regArray_ctl_d(index) <= data_i when (s_regArraySelect_ctl_q(index)='1') else s_regArray_ctl_q(index); 
         end generate reg_gen_REG;
    end generate reg_gen;
    
   --! Control registers output assignment
   r_ctrl_o.s_itl_en        <= s_regArray_ctl_q(c_ITL_EN)           (c_NUM_OF_ADCS-1    downto 0);
   r_ctrl_o.s_itl_offset    <= s_regArray_ctl_q(c_ITL_OFFSET)       (c_BUFFER_WIDTH-1   downto 0);
   r_ctrl_o.s_itl_thr       <= s_regArray_ctl_q(c_ITL_THR)          (c_DATA_WIDTH-1     downto 0);   
   
   r_ctrl_o.s_average_en     <= s_regArray_ctl_q(c_AVERAGE_EN)      (c_NUM_OF_CHANNELS-1    downto 0);
   r_ctrl_o.s_filter_en      <= s_regArray_ctl_q(c_FILTER_EN)       (c_NUM_OF_CHANNELS-1    downto 0);
   r_ctrl_o.s_window_size    <= s_regArray_ctl_q(c_FILTER_WINDOW)   (c_WINDOW_WIDTH-1       downto 0);
   r_ctrl_o.s_droop_en       <= s_regArray_ctl_q(c_DROOP_EN)        (c_NUM_OF_CHANNELS-1    downto 0);
   
   ch_gen_droop : for index in 0 to c_NUM_OF_CHANNELS-1 generate
       r_ctrl_o.s_droop_factor(index)   <= s_regArray_ctl_q(c_DROOP_FACTOR0+index)    (c_DROOP_FACTOR_WIDTH-1 downto 0);
   end generate ch_gen_droop;
   
   --! Extend itl_clr signal that it can be caught by the ADC clocks at synchronisation
   adc_gen_clr : for index in 0 to c_NUM_OF_ADCS-1 generate
       comp_extend_itl_clr : ExtSig
       generic map(
            CYCLES => 10
       )
       port map(
        clk_i => clk_i,
        rst_i => rst_i,
        sig_i => s_regArray_ctl_q(c_ITL_CLR)(index),
        sig_o => r_ctrl_o.s_itl_clr(index)
       );
   end generate adc_gen_clr;
   
   --! Extend reset signal that it can be caught by the ADC clocks at synchronisation
    comp_extend_reset : ExtSig
    generic map(
        CYCLES => 10
    )
    port map(
        clk_i => clk_i,
        rst_i => rst_i,
        sig_i => s_regArray_ctl_q(c_CTRL_REG)(0),
        sig_o => s_reg_rst
    ); 
    
   r_ctrl_o.s_reg_rst <= s_reg_rst;   
   
   --! Status Registers input assignment - READ only 
   s_regArray_stat_d(c_STAT_REG) (0 downto 0)  <=  r_stat_i.s_stat_reg;
   s_regArray_stat_d(c_ITL_REG) (c_NUM_OF_ADCS-1    downto 0) <=  r_stat_i.s_itl_reg;
   
   ch_gen_average : for index in 0 to c_NUM_OF_CHANNELS-1 generate
       s_regArray_stat_d(c_BG_AVERAGE0+index)(15 downto 0)   <= r_stat_i.s_average(index);
   end generate ch_gen_average;

-------------------------------------------------------------------------------------------  
end custom_register_array_arch;

