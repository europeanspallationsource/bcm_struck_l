-------------------------------------------------------
--! @brief    Components and Constant definitions for Struck sis8300 L board firmware.
-------------------------------------------------------
--! @details
--!
-------------------------------------------------------
--! @author Maurizio Donna ESS
--
--! @date Aug 25 2014 created
--! @date Sep 25 2014 last modify
--
--! @version 1.00
--
-------------------------------------------------------
--! @par Modifications:
--! maurizio.donna, Aug 25 2014: Created
-------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package sis8300l_top_package is

    component pcie_interface
    port(
        pcie_refclk : in std_logic;
        pcie_rst_n : in std_logic;
        per_n : in std_logic_vector(3 downto 0);
        per_p : in std_logic_vector(3 downto 0);
        addr_0x000_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x001_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x003_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x004_status_feedback : in std_logic_vector(15 downto 0);
        addr_0x005_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x10_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x11_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x13_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x14_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x15_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x16_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x17_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x100_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x101_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x102_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x103_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x104_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x105_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x106_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x107_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x108_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x109_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x110_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x111_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x112_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x113_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x114_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x115_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x116_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x117_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x118_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x119_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x120_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x121_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x122_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x123_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x124_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x125_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x126_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x127_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x128_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x129_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12a_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12b_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12c_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12d_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12e_reg_feedback : in std_logic_vector(31 downto 0);
        addr_0x12f_reg_feedback : in std_logic_vector(31 downto 0);
        reg_0x400_0x4ff_rd_data : in std_logic_vector(31 downto 0);
        bram_dma_rd_data : in std_logic_vector(63 downto 0);
        user_irq : in std_logic;
        dma_request_stall : in std_logic;
        sis_read_data_fifo_dout : in std_logic_vector(255 downto 0);
        sis_read_data_fifo_rd_count : in std_logic_vector(9 downto 0);
        sis_read_data_fifo_empty : in std_logic;
        sis_read_addr_fifo_wr_count : in std_logic_vector(9 downto 0);
        sis_write_data_fifo_wr_count : in std_logic_vector(9 downto 0);
        sis_write_addr_fifo_wr_count : in std_logic_vector(9 downto 0);
        pcie_trn_lnk_up : out std_logic;
        pet_n : out std_logic_vector(3 downto 0);
        pet_p : out std_logic_vector(3 downto 0);
        led_link_up : out std_logic;
        led_pcie_active : out std_logic;
        led_system_access : out std_logic;
        addr_0x003_reg_q : out std_logic_vector(32 downto 0);
        addr_0x004_reg_jk : out std_logic_vector(15 downto 0);
        addr_0x10_reg_q : out std_logic_vector(32 downto 0);
        addr_0x11_reg_q : out std_logic_vector(32 downto 0);
        addr_0x12_reg_q : out std_logic_vector(32 downto 0);
        addr_0x13_reg_q : out std_logic_vector(32 downto 0);
        addr_0x14_reg_q : out std_logic_vector(32 downto 0);
        addr_0x15_reg_q : out std_logic_vector(32 downto 0);
        addr_0x16_reg_q : out std_logic_vector(32 downto 0);
        addr_0x17_reg_q : out std_logic_vector(32 downto 0);
        addr_0x100_reg_q : out std_logic_vector(31 downto 0);
        addr_0x101_reg_q : out std_logic_vector(31 downto 0);
        addr_0x102_reg_q : out std_logic_vector(31 downto 0);
        addr_0x103_reg_q : out std_logic_vector(31 downto 0);
        addr_0x104_reg_q : out std_logic_vector(31 downto 0);
        addr_0x105_reg_q : out std_logic_vector(31 downto 0);
        addr_0x106_reg_q : out std_logic_vector(31 downto 0);
        addr_0x107_reg_q : out std_logic_vector(31 downto 0);
        addr_0x108_reg_q : out std_logic_vector(31 downto 0);
        addr_0x109_reg_q : out std_logic_vector(31 downto 0);
        addr_0x110_reg_q : out std_logic_vector(31 downto 0);
        addr_0x111_reg_q : out std_logic_vector(31 downto 0);
        addr_0x112_reg_q : out std_logic_vector(31 downto 0);
        addr_0x113_reg_q : out std_logic_vector(31 downto 0);
        addr_0x114_reg_q : out std_logic_vector(31 downto 0);
        addr_0x115_reg_q : out std_logic_vector(31 downto 0);
        addr_0x116_reg_q : out std_logic_vector(31 downto 0);
        addr_0x117_reg_q : out std_logic_vector(31 downto 0);
        addr_0x118_reg_q : out std_logic_vector(31 downto 0);
        addr_0x119_reg_q : out std_logic_vector(31 downto 0);
        addr_0x120_reg_q : out std_logic_vector(31 downto 0);
        addr_0x121_reg_q : out std_logic_vector(31 downto 0);
        addr_0x122_reg_q : out std_logic_vector(31 downto 0);
        addr_0x123_reg_q : out std_logic_vector(31 downto 0);
        addr_0x124_reg_q : out std_logic_vector(31 downto 0);
        addr_0x125_reg_q : out std_logic_vector(31 downto 0);
        addr_0x126_reg_q : out std_logic_vector(31 downto 0);
        addr_0x127_reg_q : out std_logic_vector(31 downto 0);
        addr_0x128_reg_q : out std_logic_vector(31 downto 0);
        addr_0x129_reg_q : out std_logic_vector(31 downto 0);
        addr_0x12a_reg_q : out std_logic_vector(31 downto 0);
        addr_0x12b_reg_q : out std_logic_vector(31 downto 0);
        addr_0x12c_reg_q : out std_logic_vector(32 downto 0);
        addr_0x12d_reg_q : out std_logic_vector(32 downto 0);
        addr_0x12e_reg_q : out std_logic_vector(32 downto 0);
        addr_0x12f_reg_q : out std_logic_vector(32 downto 0);
        addr_0x230_ddr3_pcie_test_select : out std_logic_vector(32 downto 0);
        reg_0x400_0x4ff_adr : out std_logic_vector(7 downto 0);
        reg_0x400_0x4ff_wr_data : out std_logic_vector(31 downto 0);
        reg_0x400_0x4ff_wr_en : out std_logic;
        reg_0x400_0x4ff_rd_en : out std_logic;
        bram_dma_clk : out std_logic;
        bram_dma_adr : out std_logic_vector(31 downto 0);
        bram_dma_rd_en : out std_logic;
        user_irq_clear : out std_logic;
        sis_read_data_fifo_clr : out std_logic;
        sis_read_data_fifo_rd_clk : out std_logic;
        sis_read_data_fifo_rd_en : out std_logic;
        sis_read_addr_fifo_clr : out std_logic;
        sis_read_addr_fifo_wr_clk : out std_logic;
        sis_read_addr_fifo_wr_en : out std_logic;
        sis_read_addr_fifo_din : out std_logic_vector(31 downto 0);
        sis_write_fifo_wr_clk : out std_logic;
        sis_write_data_fifo_wr_en : out std_logic;
        sis_write_data_fifo_din : out std_logic_vector(255 downto 0);
        sis_write_addr_fifo_wr_en : out std_logic;
        sis_write_addr_fifo_din : out std_logic_vector(31 downto 0);
        dac_ctrl_reg : out std_logic_vector(32 downto 0);
        dac_data_reg : out std_logic_vector(32 downto 0);
        adc_tap_delay_reg_q : out std_logic_vector(32 downto 0);
        adc_tap_delay_feedback : in std_logic_vector(31 downto 0);
        register_trn_clk : out std_logic;
        mgt_ck_int : in std_logic;
        mgt_ck_sda : inout std_logic;
        mgt_ck_scl : inout std_logic;
        flash_mosi : out std_logic;
        flash_miso : in std_logic;
        flash_cs_l : out std_logic;
        flash_clk : out std_logic;
        flash_mux_en : out std_logic;
        clk_muxab_sel : out std_logic_vector(1 downto 0);
        clk_mux1a_sel : out std_logic_vector(1 downto 0);
        clk_mux1b_sel : out std_logic_vector(1 downto 0);
        clk_mux2a_sel : out std_logic_vector(1 downto 0);
        clk_mux2b_sel : out std_logic_vector(1 downto 0);
        clk_div_spi_cs_l : out std_logic_vector(1 downto 0);
        clk_div_spi_sclk : out std_logic;
        clk_div_spi_di : out std_logic;
        clk_div_spi_do : in std_logic;
        clk_div_status : in std_logic_vector(1 downto 0);
        ad9510_spi_function_out : out std_logic_vector(2 downto 0);
        adc_spi_synch_out : out std_logic;
        adc_spi_cs_l : out std_logic_vector(5 downto 1);
        adc_spi_sclk : out std_logic;
        adc_spi_dio : inout std_logic;
        adc_spi_oe_l_out : out std_logic;
        adc_spi_pwdn_out : out std_logic;
        si_int_c1b : in std_logic;
        si_lol : in std_logic;
        si_rst_l : out std_logic; -- external reset
        si_spi_sclk : out std_logic;
        si_spi_ss_l : out std_logic; -- slave select
        si_spi_din : out std_logic;
        si_spi_dout : in std_logic
        );
    end component;

    component sys_clk_mmcm
    port(
        sys_clk125_in : in std_logic;
        sys_clk_rst_in : in std_logic;
        sys_clk_locked : out std_logic;
        sys_clk_200_out : out std_logic;
        sys_clk_125_out : out std_logic;
        sys_clk_ddr3 : out std_logic;
        sys_clk_ddr3_rd_base : out std_logic;
        sys_clk_ddr3_usr : out std_logic
        );
    end component;

    component adc_ringbuffer_delay_blocks
    port(
        adc_ringbuffer_logic_reset : in std_logic;
        adc1_clk : in std_logic;
        adc2_clk : in std_logic;
        adc3_clk : in std_logic;
        adc4_clk : in std_logic;
        adc5_clk : in std_logic;
        adc_ch1_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch2_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch3_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch4_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch5_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch6_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch7_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch8_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch9_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch10_ringbuffer_delay_val : in std_logic_vector(11 downto 0);
        adc_ch1_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch2_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch3_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch4_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch5_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch6_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch7_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch8_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch9_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch10_ringbuffer_din : in std_logic_vector(17 downto 0);
        adc_ch1_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch2_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch3_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch4_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch5_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch6_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch7_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch8_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch9_ringbuffer_dout : out std_logic_vector(17 downto 0);
        adc_ch10_ringbuffer_dout : out std_logic_vector(17 downto 0)
        );
    end component;

    component adc_sampling_block_v1
    port(
        adc_buffer_logic_reset : in std_logic;
        adc1_clk : in std_logic;
        adc2_clk : in std_logic;
        adc3_clk : in std_logic;
        adc4_clk : in std_logic;
        adc5_clk : in std_logic;
        adc_ch1_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch2_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch3_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch4_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch5_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch6_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch7_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch8_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch9_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch10_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_sample_length_reg : in std_logic_vector(23 downto 0);
        adc_ch1_start_pulse : in std_logic;
        adc_ch2_start_pulse : in std_logic;
        adc_ch3_start_pulse : in std_logic;
        adc_ch4_start_pulse : in std_logic;
        adc_ch5_start_pulse : in std_logic;
        adc_ch6_start_pulse : in std_logic;
        adc_ch7_start_pulse : in std_logic;
        adc_ch8_start_pulse : in std_logic;
        adc_ch9_start_pulse : in std_logic;
        adc_ch10_start_pulse : in std_logic;
        adc_ch1_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch2_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch3_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch4_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch5_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch6_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch7_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch8_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch9_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc_ch10_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        ddr3_write_logic_enable : in std_logic;
        ddr3_write_fifo_clk : in std_logic;
        ddr3_write_addr_fifo_wr_count : in std_logic_vector(9 downto 0);
        adc_ch1_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch2_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch3_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch4_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch5_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch6_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch7_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch8_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch9_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_ch10_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc_chx_sample_logic_active_or : out std_logic;
        adc_chx_sample_buffer_not_empty_or : out std_logic;
        ddr3_write_addr_fifo_wr_en : out std_logic;
        ddr3_write_addr_fifo_din : out std_logic_vector(31 downto 0);
        ddr3_write_data_fifo_wr_en : out std_logic;
        ddr3_write_data_fifo_din : out std_logic_vector(255 downto 0)
        );
    end component;

    component adc_clock_buffer
    port(
        adc_clk_p : in std_logic;
        adc_clk_n : in std_logic;
        adc_clk_bufr_ce : in std_logic;
        adc_clk_bufr_clr : in std_logic;
        adc_io_clk : out std_logic;
        adc_logic_clk : out std_logic
        );
    end component;

--	component dac_ddr_out_logic
--	port(
--		dac_clk_in : in std_logic;
--		dac_dcm_reset_pulse : in std_logic;
--		dac1_data_out : in std_logic_vector(15 downto 0);
--		dac2_data_out : in std_logic_vector(15 downto 0);
--		dac_seliq_p : out std_logic;
--		dac_seliq_n : out std_logic;
--		dac_d_p : out std_logic_vector(15 downto 0);
--		dac_d_n : out std_logic_vector(15 downto 0)
--		);
--	end component;

    component quad_gtx_serdes_interface
    port(
        rst : in std_logic;
        gtx_refclk_p : in std_logic;
        gtx_refclk_n : in std_logic;
        gtx_rx_p : in std_logic_vector(3 downto 0);
        gtx_rx_n : in std_logic_vector(3 downto 0);
        serdes_reset : in std_logic;
        opt0_fifo_reset : in std_logic;
        opt0_fifo_clk : in std_logic;
        opt0_in_fifo_rden : in std_logic;
        opt0_out_fifo_din : in std_logic_vector(32 downto 0);
        opt0_out_fifo_wren : in std_logic;
        opt1_fifo_reset : in std_logic;
        opt1_fifo_clk : in std_logic;
        opt1_in_fifo_rden : in std_logic;
        opt1_out_fifo_din : in std_logic_vector(32 downto 0);
        opt1_out_fifo_wren : in std_logic;
        opt2_fifo_reset : in std_logic;
        opt2_fifo_clk : in std_logic;
        opt2_in_fifo_rden : in std_logic;
        opt2_out_fifo_din : in std_logic_vector(32 downto 0);
        opt2_out_fifo_wren : in std_logic;
        opt3_fifo_reset : in std_logic;
        opt3_fifo_clk : in std_logic;
        opt3_in_fifo_rden : in std_logic;
        opt3_out_fifo_din : in std_logic_vector(32 downto 0);
        opt3_out_fifo_wren : in std_logic;
        gtx_tx_p : out std_logic_vector(3 downto 0);
        gtx_tx_n : out std_logic_vector(3 downto 0);
        gtx_linkup : out std_logic_vector(3 downto 0);
        opt0_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt0_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        opt0_out_fifo_wcnt : out std_logic_vector(8 downto 0);
        opt1_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt1_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        opt1_out_fifo_wcnt : out std_logic_vector(8 downto 0);
        opt2_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt2_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        opt2_out_fifo_wcnt : out std_logic_vector(8 downto 0);
        opt3_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt3_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        opt3_out_fifo_wcnt : out std_logic_vector(8 downto 0)
        );
    end component;

    component dual_gtx_serdes_interface
    port(
        rst : in std_logic;
        gtx_refclk_p : in std_logic;
        gtx_refclk_n : in std_logic;
        gtx_rx_p : in std_logic_vector(1 downto 0);
        gtx_rx_n : in std_logic_vector(1 downto 0);
        serdes_reset : in std_logic;
        opt0_fifo_reset : in std_logic;
        opt0_fifo_clk : in std_logic;
        opt0_in_fifo_rden : in std_logic;
        opt0_out_fifo_din : in std_logic_vector(32 downto 0);
        opt0_out_fifo_wren : in std_logic;
        opt1_fifo_reset : in std_logic;
        opt1_fifo_clk : in std_logic;
        opt1_in_fifo_rden : in std_logic;
        opt1_out_fifo_din : in std_logic_vector(32 downto 0);
        opt1_out_fifo_wren : in std_logic;
        gtx_tx_p : out std_logic_vector(1 downto 0);
        gtx_tx_n : out std_logic_vector(1 downto 0);
        gtx_linkup : out std_logic_vector(1 downto 0);
        opt0_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt0_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        opt0_out_fifo_wcnt : out std_logic_vector(8 downto 0);
        opt1_in_fifo_dout : out std_logic_vector(32 downto 0);
        opt1_in_fifo_rcnt : out std_logic_vector(8 downto 0);
        gtx_usrclk : out std_logic;
        gtx0_loopback_in : in std_logic_vector(2 downto 0);
        gtx1_loopback_in : in std_logic_vector(2 downto 0);
        opt1_out_fifo_wcnt : out std_logic_vector(8 downto 0)
        );
    end component;

    component ddr3_test
    port(
        clk : in std_logic;
        rst : in std_logic;
        phy_init_done : in std_logic;
        app_wdf_rdy : in std_logic;
        app_rd_data : in std_logic_vector(255 downto 0);
        app_rd_data_end : in std_logic;
        app_rd_data_valid : in std_logic;
        app_rdy : in std_logic;
        app_addr : out std_logic_vector(28 downto 0);
        app_cmd : out std_logic_vector(2 downto 0);
        app_en : out std_logic;
        app_hi_pri : out std_logic;
        app_wdf_data : out std_logic_vector(255 downto 0);
        app_wdf_end : out std_logic;
        app_wdf_wren : out std_logic
        );
    end component;

    component ddr3_interface
    port(
        sys_clk125 : in std_logic;
        sys_clk_ok : in std_logic;
        memory_controller_reset : in std_logic;
        read_data_fifo_clr : in std_logic;
        read_data_fifo_rd_clk : in std_logic;
        read_data_fifo_rd_en : in std_logic;
        read_addr_fifo_clr : in std_logic;
        read_addr_fifo_wr_clk : in std_logic;
        read_addr_fifo_wr_en : in std_logic;
        read_addr_fifo_din : in std_logic_vector(31 downto 0);
        write_fifo_wr_clk : in std_logic;
        write_fifo_wr_clr : in std_logic;
        write_data_fifo_wr_en : in std_logic;
        write_data_fifo_din : in std_logic_vector(255 downto 0);
        write_addr_fifo_wr_en : in std_logic;
        write_addr_fifo_din : in std_logic_vector(31 downto 0);
        ddr3_dq : inout std_logic_vector(63 downto 0);
        ddr3_dqs_p : inout std_logic_vector(7 downto 0);
        ddr3_dqs_n : inout std_logic_vector(7 downto 0);
        ddr3_usr_clk : out std_logic;
        ddr3_init_done : out std_logic;
        read_data_fifo_dout : out std_logic_vector(255 downto 0);
        read_data_fifo_rd_count : out std_logic_vector(9 downto 0);
        read_data_fifo_empty : out std_logic;
        read_addr_fifo_wr_count : out std_logic_vector(9 downto 0);
        write_data_fifo_wr_count : out std_logic_vector(9 downto 0);
        write_addr_fifo_wr_count : out std_logic_vector(9 downto 0);
        ddr3_dm : out std_logic_vector(7 downto 0);
        ddr3_addr : out std_logic_vector(14 downto 0);
        ddr3_ba : out std_logic_vector(2 downto 0);
        ddr3_ras_n : out std_logic;
        ddr3_cas_n : out std_logic;
        ddr3_we_n : out std_logic;
        ddr3_reset_n : out std_logic;
        ddr3_cs_n : out std_logic_vector(0 to 0);
        ddr3_odt : out std_logic_vector(0 to 0);
        ddr3_cke : out std_logic_vector(0 to 0);
        ddr3_ck_p : out std_logic_vector(0 to 0);
        ddr3_ck_n : out std_logic_vector(0 to 0)
        );
    end component;

    component pcie_to_ddr3_write_clockdomain_bridge
    port(
        pcie_rst : in std_logic;
        pcie_write_clk : in std_logic;
        pci_write_data_fifo_wr_en : in std_logic;
        pci_write_data_fifo_din : in std_logic_vector(255 downto 0);
        pci_write_addr_fifo_wr_en : in std_logic;
        pci_write_addr_fifo_din : in std_logic_vector(31 downto 0);
        ddr3_rst : in std_logic;
        ddr3_write_clk : in std_logic;
        ddr3_test_write_addr_fifo_wr_en : out std_logic;
        ddr3_test_write_addr_fifo_din : out std_logic_vector(31 downto 0);
        ddr3_test_write_data_fifo_wr_en : out std_logic;
        ddr3_test_write_data_fifo_din : out std_logic_vector(255 downto 0)
        );
    end component;

    component risingedge2pulse_generator
    port(
        adc_clk : in std_logic;
        rising_edge_signal_in : in std_logic;
        signal_pulse_out : out std_logic
        );
    end component;

    component dac_clk_out_mmcm
    port(
        dac_clkout_in : in std_logic;
        dac_mmcm_reset : in std_logic;
        dac_mmcm_lock : out std_logic;
        dac_clk_p : out std_logic;
        dac_clk_n : out std_logic
        );
    end component;

    component dac_fd_out_logic
    port(
        dac_clk_in : in std_logic;
        dac_odelay_clk : in std_logic;
        dac_odelay_rst : in std_logic;
        dac_odelay_value : in std_logic_vector(4 downto 0);
        dac_dcm_reset_pulse : in std_logic;
        dac_select_ch1 : in std_logic;
        dac_data_out : in std_logic_vector(15 downto 0);
      dac_clk_dcm_lock          : out std_logic;
        dac_out_synch_clk : out std_logic;
        dac_seliq_p : out std_logic;
        dac_seliq_n : out std_logic;
        dac_d_p : out std_logic_vector(15 downto 0);
        dac_d_n : out std_logic_vector(15 downto 0)
        );
    end component;

    component adc_sampling_block_v2
    port(
        adc_buffer_logic_reset : in std_logic;
        adc1_clk : in std_logic;
        adc2_clk : in std_logic;
        adc3_clk : in std_logic;
        adc4_clk : in std_logic;
        adc5_clk : in std_logic;
        adc_ch1_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch2_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch3_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch4_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch5_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch6_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch7_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch8_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch9_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_ch10_ringbuffer_dout : in std_logic_vector(15 downto 0);
        adc_sample_length_reg : in std_logic_vector(23 downto 0);
        adc1_start_pulse : in std_logic;
        adc2_start_pulse : in std_logic;
        adc3_start_pulse : in std_logic;
        adc4_start_pulse : in std_logic;
        adc5_start_pulse : in std_logic;
        adc1_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc2_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc3_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc4_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        adc5_sample_memory_start_addr : in std_logic_vector(23 downto 0);
        ddr2_write_logic_enable : in std_logic;
        ddr2_write_fifo_clk : in std_logic;
        ddr2_write_addr_fifo_wr_count : in std_logic_vector(9 downto 0);
        adc1_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc2_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc3_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc4_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adc5_adc_memory_addr_counter : out std_logic_vector(31 downto 0);
        adcx_sample_logic_active_or : out std_logic;
        adcx_sample_buffer_not_empty_or : out std_logic;
        ddr2_write_addr_fifo_wr_en : out std_logic;
        ddr2_write_addr_fifo_din : out std_logic_vector(31 downto 0);
        ddr2_write_data_fifo_wr_en : out std_logic;
        ddr2_write_data_fifo_din : out std_logic_vector(127 downto 0)
        );
    end component;

end package sis8300l_top_package;
