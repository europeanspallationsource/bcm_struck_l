----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:23:35 10/22/2009 
-- Design Name: 
-- Module Name:    adc_clock_buffer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity adc_clock_buffer is
    Port ( adc_clk_p : in  STD_LOGIC;
           adc_clk_n : in  STD_LOGIC;
           adc_clk_bufr_ce : in  STD_LOGIC;
           adc_clk_bufr_clr : in  STD_LOGIC;
           adc_io_clk : out  STD_LOGIC; 
           adc_logic_clk : out  STD_LOGIC
			  );
end adc_clock_buffer;

architecture Behavioral of adc_clock_buffer is

  component IODELAY 
  generic(
      IDELAY_TYPE	: string	:= "DEFAULT";
      IDELAY_VALUE	: integer	:= 0;
      ODELAY_VALUE	: integer	:= 0;
      REFCLK_FREQUENCY	: real		:= 200.0;
      HIGH_PERFORMANCE_MODE	: string		:= "TRUE"
      );
  port(
      DATAOUT	: out std_logic;

      C		: in  std_logic;
      CE	: in  std_logic;
      DATAIN	: in  std_logic;
      IDATAIN	: in  std_logic;
      INC	: in  std_logic;
      ODATAIN	: in  std_logic;
      RST	: in  std_logic;
      T		: in  std_logic
      );
end component;

signal adc_clk_buf:  std_logic;   
signal adc_clk_iob_delay_out:  std_logic;   

begin


-- LVDS Input   
   adc_clock_in : IBUFDS 
      GENERIC MAP(
         DIFF_TERM => TRUE, IOSTANDARD =>"LVDS_25") 
      PORT MAP (
         O => adc_clk_buf,
         I => adc_clk_p,
         IB => adc_clk_n);   

-- IOB delay in clock path
   adc_clock_delay : IODELAY 
      GENERIC MAP(
--         IDELAY_TYPE => "FIXED", IDELAY_VALUE => 4, -- 4x78ps = 312ps
         IDELAY_TYPE => "FIXED", IDELAY_VALUE => 3, -- 3x78ps = 234ps
         ODELAY_VALUE => 0, REFCLK_FREQUENCY => 200.00,  
        HIGH_PERFORMANCE_MODE => "TRUE") 
      PORT MAP (
         DATAOUT => adc_clk_iob_delay_out,
         IDATAIN => adc_clk_buf,
         ODATAIN => '0',
         DATAIN => '0',
         T => '1',
         CE => '0',
         INC => '0',
         C => '0',
         RST => '0');   

--CLOCK BUFFER FOR SERIAL SIDE CLOCK	
   adc_clock_BUFIO : BUFIO 
      PORT MAP (
         O => adc_io_clk,
         I => adc_clk_iob_delay_out);   
   
 --CLOCK BUFFER/DIVIDER FOR PARALLEL SIDE CLOCK
   adc_clock_BUFR : BUFR 
      GENERIC MAP( 
--         BUFR_DIVIDE => "1" )
         BUFR_DIVIDE => "BYPASS" )
      PORT MAP (
         O => adc_logic_clk,
         CE => adc_clk_bufr_ce,
         CLR => adc_clk_bufr_clr,
         I => adc_clk_iob_delay_out);   

end Behavioral;

