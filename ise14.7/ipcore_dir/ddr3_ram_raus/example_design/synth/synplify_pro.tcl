project -new
#project files
add_file -vhdl "../rtl/controller/arb_mux.vhd"
add_file -vhdl "../rtl/controller/arb_row_col.vhd"
add_file -vhdl "../rtl/controller/arb_select.vhd"
add_file -vhdl "../rtl/controller/bank_cntrl.vhd"
add_file -vhdl "../rtl/controller/bank_common.vhd"
add_file -vhdl "../rtl/controller/bank_compare.vhd"
add_file -vhdl "../rtl/controller/bank_mach.vhd"
add_file -vhdl "../rtl/controller/bank_queue.vhd"
add_file -vhdl "../rtl/controller/bank_state.vhd"
add_file -vhdl "../rtl/controller/col_mach.vhd"
add_file -vhdl "../rtl/controller/mc.vhd"
add_file -vhdl "../rtl/controller/rank_cntrl.vhd"
add_file -vhdl "../rtl/controller/rank_common.vhd"
add_file -vhdl "../rtl/controller/rank_mach.vhd"
add_file -vhdl "../rtl/controller/round_robin_arb.vhd"
add_file -vhdl "../rtl/ecc/ecc_buf.vhd"
add_file -vhdl "../rtl/ecc/ecc_dec_fix.vhd"
add_file -vhdl "../rtl/ecc/ecc_gen.vhd"
add_file -vhdl "../rtl/ecc/ecc_merge_enc.vhd"
add_file -vhdl "../rtl/ip_top/clk_ibuf.vhd"
add_file -vhdl "../rtl/ip_top/ddr2_ddr3_chipscope.vhd"
add_file -vhdl "../rtl/ip_top/example_top.vhd"
add_file -vhdl "../rtl/ip_top/infrastructure.vhd"
add_file -vhdl "../rtl/ip_top/iodelay_ctrl.vhd"
add_file -vhdl "../rtl/ip_top/mem_intfc.vhd"
add_file -vhdl "../rtl/ip_top/memc_ui_top.vhd"
add_file -vhdl "../rtl/phy/circ_buffer.vhd"
add_file -vhdl "../rtl/phy/phy_ck_iob.vhd"
add_file -vhdl "../rtl/phy/phy_clock_io.vhd"
add_file -vhdl "../rtl/phy/phy_control_io.vhd"
add_file -vhdl "../rtl/phy/phy_data_io.vhd"
add_file -vhdl "../rtl/phy/phy_dly_ctrl.vhd"
add_file -vhdl "../rtl/phy/phy_dm_iob.vhd"
add_file -vhdl "../rtl/phy/phy_dq_iob.vhd"
add_file -vhdl "../rtl/phy/phy_dqs_iob.vhd"
add_file -vhdl "../rtl/phy/phy_init.vhd"
add_file -vhdl "../rtl/phy/phy_pd.vhd"
add_file -vhdl "../rtl/phy/phy_pd_top.vhd"
add_file -vhdl "../rtl/phy/phy_rdclk_gen.vhd"
add_file -vhdl "../rtl/phy/phy_rdctrl_sync.vhd"
add_file -vhdl "../rtl/phy/phy_rddata_sync.vhd"
add_file -vhdl "../rtl/phy/phy_rdlvl.vhd"
add_file -vhdl "../rtl/phy/phy_read.vhd"
add_file -vhdl "../rtl/phy/phy_top.vhd"
add_file -vhdl "../rtl/phy/phy_write.vhd"
add_file -vhdl "../rtl/phy/phy_wrlvl.vhd"
add_file -vhdl "../rtl/phy/rd_bitslip.vhd"
add_file -vhdl "../rtl/traffic_gen/afifo.vhd"
add_file -vhdl "../rtl/traffic_gen/cmd_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/cmd_prbs_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/data_prbs_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/init_mem_pattern_ctr.vhd"
add_file -vhdl "../rtl/traffic_gen/mcb_flow_control.vhd"
add_file -vhdl "../rtl/traffic_gen/mcb_traffic_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/rd_data_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/read_data_path.vhd"
add_file -vhdl "../rtl/traffic_gen/read_posted_fifo.vhd"
add_file -vhdl "../rtl/traffic_gen/sp6_data_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/tg_status.vhd"
add_file -vhdl "../rtl/traffic_gen/v6_data_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/wr_data_gen.vhd"
add_file -vhdl "../rtl/traffic_gen/write_data_path.vhd"
add_file -vhdl "../rtl/ui/ui_cmd.vhd"
add_file -vhdl "../rtl/ui/ui_rd_data.vhd"
add_file -vhdl "../rtl/ui/ui_top.vhd"
add_file -vhdl "../rtl/ui/ui_wr_data.vhd"

#implementation: "rev_1"
impl -add rev_1 -type fpga

#device options
set_option -technology virtex6
set_option -part xc6vlx130t
set_option -package ff1156
set_option -speed_grade -2
set_option -part_companion ""

#compilation/mapping options
set_option -use_fsm_explorer 0
set_option -top_module "example_top"

# sequential_optimization_options
set_option -symbolic_fsm_compiler 1

# Compiler Options
set_option -compiler_compatible 0
set_option -resource_sharing 1

# mapper_options
set_option -frequency 400
set_option -write_verilog 0
set_option -write_vhdl 0

# Xilinx Virtex2
set_option -run_prop_extract 1
set_option -maxfan 10000
set_option -disable_io_insertion 0
set_option -pipe 1
set_option -update_models_cp 0
set_option -retiming 0
set_option -no_sequential_opt 0
set_option -fixgatedclocks 3
set_option -fixgeneratedclocks 3

# Xilinx Virtex6
set_option -enable_prepacking 1

#VIF options
set_option -write_vif 1

#automatic place and route (vendor) options
set_option -write_apr_constraint 1

#set result format/file last
project -result_file "../synth/rev_1/example_top.edf"

#
#implementation attributes

set_option -vlog_std v2001
set_option -project_relative_includes 1
impl -active "../synth/rev_1"
project -run
project -save
