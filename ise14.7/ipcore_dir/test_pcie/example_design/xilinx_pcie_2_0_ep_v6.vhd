-------------------------------------------------------------------------------
--
-- (c) Copyright 2009-2011 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------
-- Project    : Virtex-6 Integrated Block for PCI Express
-- File       : xilinx_pcie_2_0_ep_v6.vhd
-- Version    : 2.4
--
-- Description:  PCI Express Endpoint example FPGA design
--
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.vcomponents.all;

entity xilinx_pcie_2_0_ep_v6 is
  generic (
  PL_FAST_TRAIN                     : boolean := FALSE
    );
  port (
  pci_exp_txp                   : out std_logic_vector(3 downto 0);
  pci_exp_txn                   : out std_logic_vector(3 downto 0);
  pci_exp_rxp                   : in std_logic_vector(3 downto 0);
  pci_exp_rxn                   : in std_logic_vector(3 downto 0);

  sys_clk_p                     : in std_logic;
  sys_clk_n                     : in std_logic;
  sys_reset_n                   : in std_logic
);
end xilinx_pcie_2_0_ep_v6;

architecture rtl of xilinx_pcie_2_0_ep_v6 is
  component test_pcie    generic (
      PL_FAST_TRAIN                     : boolean := FALSE
    );
    port (
      pci_exp_txp                    : out std_logic_vector(3 downto 0);
      pci_exp_txn                    : out std_logic_vector(3 downto 0);
      pci_exp_rxp                    : in std_logic_vector(3 downto 0);
      pci_exp_rxn                    : in std_logic_vector(3 downto 0);
      user_clk_out                   : out std_logic;
      user_reset_out                 : out std_logic;
      user_lnk_up                    : out std_logic;
      tx_buf_av                      : out std_logic_vector(5 downto 0);
      tx_cfg_req                     : out std_logic;
      tx_err_drop                    : out std_logic;
      s_axis_tx_tready               : out std_logic;
      s_axis_tx_tdata                : in std_logic_vector(63 downto 0);
      s_axis_tx_tlast                : in std_logic;
      s_axis_tx_tvalid               : in std_logic;
      s_axis_tx_tkeep                : in std_logic_vector(7 downto 0);
      s_axis_tx_tuser                : in std_logic_vector(3 downto 0);
      tx_cfg_gnt                     : in std_logic;

      m_axis_rx_tdata                : out std_logic_vector(63 downto 0);
      m_axis_rx_tkeep                : out std_logic_vector(7 downto 0);
      m_axis_rx_tlast                : out std_logic;
      m_axis_rx_tvalid               : out std_logic;
      m_axis_rx_tready               : in std_logic;
      m_axis_rx_tuser                : out std_logic_vector(21 downto 0);
      rx_np_ok                       : in std_logic;
      fc_cpld                        : out std_logic_vector(11 downto 0);
      fc_cplh                        : out std_logic_vector(7 downto 0);
      fc_npd                         : out std_logic_vector(11 downto 0);
      fc_nph                         : out std_logic_vector(7 downto 0);
      fc_pd                          : out std_logic_vector(11 downto 0);
      fc_ph                          : out std_logic_vector(7 downto 0);
      fc_sel                         : in std_logic_vector(2 downto 0);
      cfg_do                         : out std_logic_vector(31 downto 0);
      cfg_rd_wr_done                 : out std_logic;
      cfg_di                         : in std_logic_vector(31 downto 0);
      cfg_byte_en                    : in std_logic_vector(3 downto 0);
      cfg_dwaddr                     : in std_logic_vector(9 downto 0);
      cfg_wr_en                      : in std_logic;
      cfg_rd_en                      : in std_logic;
      cfg_err_cor                    : in std_logic;
      cfg_err_ur                     : in std_logic;
      cfg_err_ecrc                   : in std_logic;
      cfg_err_cpl_timeout            : in std_logic;
      cfg_err_cpl_abort              : in std_logic;
      cfg_err_cpl_unexpect           : in std_logic;
      cfg_err_posted                 : in std_logic;
      cfg_err_locked                 : in std_logic;
      cfg_err_tlp_cpl_header         : in std_logic_vector(47 downto 0);
      cfg_err_cpl_rdy                : out std_logic;
      cfg_interrupt                  : in std_logic;
      cfg_interrupt_rdy              : out std_logic;
      cfg_interrupt_assert           : in std_logic;
      cfg_interrupt_di               : in std_logic_vector(7 downto 0);
      cfg_interrupt_do               : out std_logic_vector(7 downto 0);
      cfg_interrupt_mmenable         : out std_logic_vector(2 downto 0);
      cfg_interrupt_msienable        : out std_logic;
      cfg_interrupt_msixenable       : out std_logic;
      cfg_interrupt_msixfm           : out std_logic;
      cfg_turnoff_ok                 : in std_logic;
      cfg_to_turnoff                 : out std_logic;
      cfg_trn_pending                : in std_logic;
      cfg_pm_wake                    : in std_logic;
      cfg_bus_number                 : out std_logic_vector(7 downto 0);
      cfg_device_number              : out std_logic_vector(4 downto 0);
      cfg_function_number            : out std_logic_vector(2 downto 0);
      cfg_status                     : out std_logic_vector(15 downto 0);
      cfg_command                    : out std_logic_vector(15 downto 0);
      cfg_dstatus                    : out std_logic_vector(15 downto 0);
      cfg_dcommand                   : out std_logic_vector(15 downto 0);
      cfg_lstatus                    : out std_logic_vector(15 downto 0);
      cfg_lcommand                   : out std_logic_vector(15 downto 0);
      cfg_dcommand2                  : out std_logic_vector(15 downto 0);
      cfg_pcie_link_state            : out std_logic_vector(2 downto 0);
      cfg_dsn                        : in std_logic_vector(63 downto 0);
      cfg_pmcsr_pme_en               : out std_logic;
      cfg_pmcsr_pme_status           : out std_logic;
      cfg_pmcsr_powerstate           : out std_logic_vector(1 downto 0);
      pl_initial_link_width          : out std_logic_vector(2 downto 0);
      pl_lane_reversal_mode          : out std_logic_vector(1 downto 0);
      pl_link_gen2_capable           : out std_logic;
      pl_link_partner_gen2_supported : out std_logic;
      pl_link_upcfg_capable          : out std_logic;
      pl_ltssm_state                 : out std_logic_vector(5 downto 0);
      pl_received_hot_rst            : out std_logic;
      pl_sel_link_rate               : out std_logic;
      pl_sel_link_width              : out std_logic_vector(1 downto 0);
      pl_directed_link_auton         : in std_logic;
      pl_directed_link_change        : in std_logic_vector(1 downto 0);
      pl_directed_link_speed         : in std_logic;
      pl_directed_link_width         : in std_logic_vector(1 downto 0);
      pl_upstream_prefer_deemph      : in std_logic;
      sys_clk                        : in std_logic;
      sys_reset                      : in std_logic);
  end component;

  component pcie_app_v6
    port  (
      user_clk                       : in std_logic;
      user_reset                     : in std_logic;
      user_lnk_up                    : in std_logic;
      s_axis_tx_tdata                : out std_logic_vector(63 downto 0);
      s_axis_tx_tkeep                : out std_logic_vector(7 downto 0);
      s_axis_tx_tlast                : out std_logic;
      s_axis_tx_tvalid               : out std_logic;
      s_axis_tx_tready               : in std_logic;
      s_axis_tx_tuser                : out std_logic_vector(3 downto 0);
      tx_cfg_req                     : in std_logic;
      tx_cfg_gnt                     : out std_logic;
      tx_err_drop                    : in std_logic;
      tx_buf_av                      : in std_logic_vector(5 downto 0);
      m_axis_rx_tdata                : in std_logic_vector(63 downto 0);
      m_axis_rx_tkeep                : in std_logic_vector(7 downto 0);
      m_axis_rx_tlast                : in std_logic;
      m_axis_rx_tvalid               : in std_logic;
      m_axis_rx_tready               : out std_logic;
      m_axis_rx_tuser                : in std_logic_vector(21 downto 0);
      rx_np_ok                       : out std_logic;
      fc_nph                         : in std_logic_vector(7 downto 0);
      fc_npd                         : in std_logic_vector(11 downto 0);
      fc_ph                          : in std_logic_vector(7 downto 0);
      fc_pd                          : in std_logic_vector(11 downto 0);
      fc_cplh                        : in std_logic_vector(7 downto 0);
      fc_cpld                        : in std_logic_vector(11 downto 0);
      fc_sel                         : out std_logic_vector(2 downto 0);
      cfg_do                         : in std_logic_vector(31 downto 0);
      cfg_di                         : out std_logic_vector(31 downto 0);
      cfg_byte_en                    : out std_logic_vector(3 downto 0);
      cfg_dwaddr                     : out std_logic_vector(9 downto 0);
      cfg_rd_wr_done                 : in std_logic;
      cfg_wr_en                      : out std_logic;
      cfg_rd_en                      : out std_logic;
      cfg_err_cor                    : out std_logic;
      cfg_err_ur                     : out std_logic;
      cfg_err_cpl_rdy                : in std_logic;
      cfg_err_ecrc                   : out std_logic;
      cfg_err_cpl_timeout            : out std_logic;
      cfg_err_cpl_abort              : out std_logic;
      cfg_err_cpl_unexpect           : out std_logic;
      cfg_err_posted                 : out std_logic;
      cfg_err_locked                 : out std_logic;
      cfg_interrupt                  : out std_logic;
      cfg_interrupt_rdy              : in std_logic;
      cfg_interrupt_assert           : out std_logic;
      cfg_interrupt_di               : out std_logic_vector(7 downto 0);
      cfg_interrupt_do               : in  std_logic_vector(7 downto 0);
      cfg_interrupt_mmenable         : in  std_logic_vector(2 downto 0);
      cfg_interrupt_msienable        : in  std_logic;
      cfg_interrupt_msixenable       : in  std_logic;
      cfg_interrupt_msixfm           : in  std_logic;
      cfg_turnoff_ok                 : out std_logic;
      cfg_to_turnoff                 : in std_logic;
      cfg_pm_wake                    : out std_logic;
      cfg_pcie_link_state            : in std_logic_vector(2 downto 0);
      cfg_trn_pending                : out std_logic;
      cfg_err_tlp_cpl_header         : out std_logic_vector(47 downto 0);
      cfg_bus_number                 : in std_logic_vector(7 downto 0);
      cfg_device_number              : in std_logic_vector(4 downto 0);
      cfg_function_number            : in std_logic_vector(2 downto 0);
      cfg_status                     : in std_logic_vector(15 downto 0);
      cfg_command                    : in std_logic_vector(15 downto 0);
      cfg_dstatus                    : in std_logic_vector(15 downto 0);
      cfg_dcommand                   : in std_logic_vector(15 downto 0);
      cfg_lstatus                    : in std_logic_vector(15 downto 0);
      cfg_lcommand                   : in std_logic_vector(15 downto 0);
      cfg_dcommand2                  : in std_logic_vector(15 downto 0);
      pl_directed_link_change        : out std_logic_vector(1 downto 0);
      pl_ltssm_state                 : in std_logic_vector(5 downto 0);
      pl_directed_link_width         : out std_logic_vector(1 downto 0);
      pl_directed_link_speed         : out std_logic;
      pl_directed_link_auton         : out std_logic;
      pl_upstream_prefer_deemph      : out std_logic;
      pl_sel_link_width              : in std_logic_vector(1 downto 0);
      pl_sel_link_rate               : in std_logic;
      pl_link_gen2_capable           : in std_logic;
      pl_link_partner_gen2_supported : in std_logic;
      pl_initial_link_width          : in std_logic_vector(2 downto 0);
      pl_link_upcfg_capable          : in std_logic;
      pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);
      pl_received_hot_rst            : in std_logic;
      cfg_dsn                        : out std_logic_vector(63 downto 0));
  end component;


  -- Tx
  signal tx_buf_av : std_logic_vector(5 downto 0);
  signal tx_cfg_req : std_logic;
  signal tx_err_drop : std_logic;
  signal s_axis_tx_tready  : std_logic;
  signal s_axis_tx_tdata : std_logic_vector(63 downto 0);
  signal s_axis_tx_tlast : std_logic;
  signal s_axis_tx_tvalid : std_logic;
  signal s_axis_tx_tuser : std_logic_vector (3 downto 0);
    signal s_axis_tx_tkeep : std_logic_vector (7 downto 0);
    signal tx_cfg_gnt : std_logic;

  -- Rx
  signal m_axis_rx_tdata : std_logic_vector(63 downto 0);
  signal m_axis_rx_tkeep : std_logic_vector (7 downto 0);
  signal m_axis_rx_tlast : std_logic;
  signal m_axis_rx_tvalid : std_logic;
  signal m_axis_rx_tuser : std_logic_vector (21 downto 0);

  signal m_axis_rx_tready : std_logic;
  signal rx_np_ok : std_logic;

  -- Flow Control
  signal fc_cpld : std_logic_vector(11 downto 0);
  signal fc_cplh : std_logic_vector(7 downto 0);
  signal fc_npd : std_logic_vector(11 downto 0);
  signal fc_nph : std_logic_vector(7 downto 0);
  signal fc_pd : std_logic_vector(11 downto 0);
  signal fc_ph : std_logic_vector(7 downto 0);
  signal fc_sel : std_logic_vector(2 downto 0);

  signal user_lnk_up : std_logic;
  signal user_lnk_up_int1 : std_logic;
  signal user_clk : std_logic;
  signal user_reset : std_logic;
  signal user_reset_int1 : std_logic;

  ---------------------------------------------------------
  -- 3. Configuration (CFG) Interface
  ---------------------------------------------------------

  signal cfg_do : std_logic_vector(31 downto 0);
  signal cfg_rd_wr_done : std_logic;
  signal cfg_di : std_logic_vector(31 downto 0);
  signal cfg_byte_en : std_logic_vector(3 downto 0);
  signal cfg_dwaddr : std_logic_vector(9 downto 0);
  signal cfg_wr_en : std_logic;
  signal cfg_rd_en : std_logic;

  signal cfg_err_cor: std_logic;
  signal cfg_err_ur : std_logic;
  signal cfg_err_ecrc : std_logic;
  signal cfg_err_cpl_timeout : std_logic;
  signal cfg_err_cpl_abort : std_logic;
  signal cfg_err_cpl_unexpect : std_logic;
  signal cfg_err_posted : std_logic;
  signal cfg_err_locked : std_logic;
  signal cfg_err_tlp_cpl_header : std_logic_vector(47 downto 0);
  signal cfg_err_cpl_rdy : std_logic;
  signal cfg_interrupt : std_logic;
  signal cfg_interrupt_rdy : std_logic;
  signal cfg_interrupt_assert : std_logic;
  signal cfg_interrupt_di : std_logic_vector(7 downto 0);
  signal cfg_interrupt_do : std_logic_vector(7 downto 0);
  signal cfg_interrupt_mmenable : std_logic_vector(2 downto 0);
  signal cfg_interrupt_msienable : std_logic;
  signal cfg_interrupt_msixenable : std_logic;
  signal cfg_interrupt_msixfm : std_logic;
  signal cfg_turnoff_ok : std_logic;
  signal cfg_to_turnoff : std_logic;
  signal cfg_trn_pending : std_logic;
  signal cfg_pm_wake : std_logic;
  signal cfg_bus_number : std_logic_vector(7 downto 0);
  signal cfg_device_number : std_logic_vector(4 downto 0);
  signal cfg_function_number : std_logic_vector(2 downto 0);
  signal cfg_status : std_logic_vector(15 downto 0);
  signal cfg_command : std_logic_vector(15 downto 0);
  signal cfg_dstatus : std_logic_vector(15 downto 0);
  signal cfg_dcommand : std_logic_vector(15 downto 0);
  signal cfg_lstatus : std_logic_vector(15 downto 0);
  signal cfg_lcommand : std_logic_vector(15 downto 0);
  signal cfg_dcommand2 : std_logic_vector(15 downto 0);
  signal cfg_pcie_link_state : std_logic_vector(2 downto 0);
  signal cfg_dsn : std_logic_vector(63 downto 0);

  ---------------------------------------------------------
  -- 4. Physical Layer Control and Status (PL) Interface
  ---------------------------------------------------------

  signal pl_initial_link_width : std_logic_vector(2 downto 0);
  signal pl_lane_reversal_mode : std_logic_vector(1 downto 0);
  signal pl_link_gen2_capable : std_logic;
  signal pl_link_partner_gen2_supported : std_logic;
  signal pl_link_upcfg_capable : std_logic;
  signal pl_ltssm_state : std_logic_vector(5 downto 0);
  signal pl_received_hot_rst : std_logic;
  signal pl_sel_link_rate : std_logic;
  signal pl_sel_link_width : std_logic_vector(1 downto 0);
  signal pl_directed_link_auton : std_logic;
  signal pl_directed_link_change : std_logic_vector(1 downto 0);
  signal pl_directed_link_speed : std_logic;
  signal pl_directed_link_width : std_logic_vector(1 downto 0);
  signal pl_upstream_prefer_deemph : std_logic;

  signal sys_clk_c : std_logic;
  signal sys_reset_n_c : std_logic;
  signal sys_reset     : std_logic;

  -------------------------------------------------------

begin

  sys_reset <= not(sys_reset_n_c);

  refclk_ibuf : IBUFDS_GTXE1
     port map(
       O       => sys_clk_c,
       ODIV2   => open,
       I       => sys_clk_p,
       IB      => sys_clk_n,
       CEB     => '0');

  sys_reset_n_ibuf : IBUF
     port map(
       O       => sys_reset_n_c,
       I       => sys_reset_n);

  user_lnk_up_n_int_i: FDCP
     generic map(
       INIT    => '1')
     port map(
       Q       => user_lnk_up,
       D       => user_lnk_up_int1,
       C       => user_clk,
       CLR     => '0',
       PRE     => '0');

  user_reset_n_i : FDCP
     generic map(
       INIT        => '1')
     port map(
        Q      => user_reset,
        D      => user_reset_int1,
        C      => user_clk,
        CLR    => '0',
        PRE    => '0');


  core_i : test_pcie  generic map(
      PL_FAST_TRAIN                   => PL_FAST_TRAIN)
  port map(
  pci_exp_txp                     => pci_exp_txp,
  pci_exp_txn                     => pci_exp_txn,
  pci_exp_rxp                     => pci_exp_rxp,
  pci_exp_rxn                     => pci_exp_rxn,
  user_clk_out                    => user_clk ,
  user_reset_out                  => user_reset_int1,
  user_lnk_up                     => user_lnk_up_int1,
  tx_buf_av                       => tx_buf_av ,
  tx_cfg_req                      => tx_cfg_req ,
  tx_err_drop                     => tx_err_drop ,
  s_axis_tx_tready                => s_axis_tx_tready ,
  s_axis_tx_tdata                 => s_axis_tx_tdata ,
  s_axis_tx_tkeep                 => s_axis_tx_tkeep ,
  s_axis_tx_tlast                 => s_axis_tx_tlast ,
  s_axis_tx_tvalid                => s_axis_tx_tvalid ,
  s_axis_tx_tuser                 => s_axis_tx_tuser,
  tx_cfg_gnt                      => tx_cfg_gnt ,
  m_axis_rx_tdata                 => m_axis_rx_tdata ,
  m_axis_rx_tkeep                 => m_axis_rx_tkeep ,
  m_axis_rx_tlast                 => m_axis_rx_tlast ,
  m_axis_rx_tvalid                => m_axis_rx_tvalid ,
  m_axis_rx_tready                => m_axis_rx_tready ,
  m_axis_rx_tuser                 => m_axis_rx_tuser,
  rx_np_ok                        => rx_np_ok ,
  fc_cpld                         => fc_cpld ,
  fc_cplh                         => fc_cplh ,
  fc_npd                          => fc_npd ,
  fc_nph                          => fc_nph ,
  fc_pd                           => fc_pd ,
  fc_ph                           => fc_ph ,
  fc_sel                          => fc_sel ,
  cfg_do                          => cfg_do ,
  cfg_rd_wr_done                  => cfg_rd_wr_done,
  cfg_di                          => cfg_di ,
  cfg_byte_en                     => cfg_byte_en ,
  cfg_dwaddr                      => cfg_dwaddr ,
  cfg_wr_en                       => cfg_wr_en ,
  cfg_rd_en                       => cfg_rd_en ,

  cfg_err_cor                     => cfg_err_cor ,
  cfg_err_ur                      => cfg_err_ur ,
  cfg_err_ecrc                    => cfg_err_ecrc ,
  cfg_err_cpl_timeout             => cfg_err_cpl_timeout ,
  cfg_err_cpl_abort               => cfg_err_cpl_abort ,
  cfg_err_cpl_unexpect            => cfg_err_cpl_unexpect ,
  cfg_err_posted                  => cfg_err_posted ,
  cfg_err_locked                  => cfg_err_locked ,
  cfg_err_tlp_cpl_header          => cfg_err_tlp_cpl_header ,
  cfg_err_cpl_rdy                 => cfg_err_cpl_rdy ,
  cfg_interrupt                   => cfg_interrupt ,
  cfg_interrupt_rdy               => cfg_interrupt_rdy ,
  cfg_interrupt_assert            => cfg_interrupt_assert ,
  cfg_interrupt_di                => cfg_interrupt_di ,
  cfg_interrupt_do                => cfg_interrupt_do ,
  cfg_interrupt_mmenable          => cfg_interrupt_mmenable ,
  cfg_interrupt_msienable         => cfg_interrupt_msienable ,
  cfg_interrupt_msixenable        => cfg_interrupt_msixenable ,
  cfg_interrupt_msixfm            => cfg_interrupt_msixfm ,
  cfg_turnoff_ok                  => cfg_turnoff_ok ,
  cfg_to_turnoff                  => cfg_to_turnoff ,
  cfg_trn_pending                 => cfg_trn_pending ,
  cfg_pm_wake                     => cfg_pm_wake ,
  cfg_bus_number                  => cfg_bus_number ,
  cfg_device_number               => cfg_device_number ,
  cfg_function_number             => cfg_function_number ,
  cfg_status                      => cfg_status ,
  cfg_command                     => cfg_command ,
  cfg_dstatus                     => cfg_dstatus ,
  cfg_dcommand                    => cfg_dcommand ,
  cfg_lstatus                     => cfg_lstatus ,
  cfg_lcommand                    => cfg_lcommand ,
  cfg_dcommand2                   => cfg_dcommand2 ,
  cfg_pcie_link_state             => cfg_pcie_link_state ,
  cfg_dsn                         => cfg_dsn ,
  cfg_pmcsr_pme_en                => open,
  cfg_pmcsr_pme_status            => open,
  cfg_pmcsr_powerstate            => open,
  pl_initial_link_width           => pl_initial_link_width ,
  pl_lane_reversal_mode           => pl_lane_reversal_mode ,
  pl_link_gen2_capable            => pl_link_gen2_capable ,
  pl_link_partner_gen2_supported  => pl_link_partner_gen2_supported ,
  pl_link_upcfg_capable           => pl_link_upcfg_capable ,
  pl_ltssm_state                  => pl_ltssm_state ,
  pl_received_hot_rst             => pl_received_hot_rst ,
  pl_sel_link_rate                => pl_sel_link_rate ,
  pl_sel_link_width               => pl_sel_link_width ,
  pl_directed_link_auton          => pl_directed_link_auton ,
  pl_directed_link_change         => pl_directed_link_change ,
  pl_directed_link_speed          => pl_directed_link_speed ,
  pl_directed_link_width          => pl_directed_link_width ,
  pl_upstream_prefer_deemph       => pl_upstream_prefer_deemph ,
  sys_clk                         =>  sys_clk_c ,
  sys_reset                       =>  sys_reset

);


app : pcie_app_v6
  port map(
  user_clk                        => user_clk ,
  user_reset                     => user_reset_int1 ,
  user_lnk_up                    => user_lnk_up_int1 ,
  tx_buf_av                      => tx_buf_av ,
  tx_cfg_req                     => tx_cfg_req ,
  tx_err_drop                    => tx_err_drop ,
  s_axis_tx_tready               => s_axis_tx_tready ,
  s_axis_tx_tdata                => s_axis_tx_tdata ,
  s_axis_tx_tkeep                => s_axis_tx_tkeep ,
  s_axis_tx_tlast                => s_axis_tx_tlast ,
  s_axis_tx_tvalid               => s_axis_tx_tvalid ,
  s_axis_tx_tuser                => s_axis_tx_tuser,
  tx_cfg_gnt                     => tx_cfg_gnt ,
  m_axis_rx_tdata                => m_axis_rx_tdata ,
  m_axis_rx_tkeep                => m_axis_rx_tkeep ,
  m_axis_rx_tlast                => m_axis_rx_tlast ,
  m_axis_rx_tvalid               => m_axis_rx_tvalid ,
  m_axis_rx_tuser                => m_axis_rx_tuser,
  m_axis_rx_tready               => m_axis_rx_tready ,
  rx_np_ok                       => rx_np_ok ,
  fc_cpld                        => fc_cpld ,
  fc_cplh                        => fc_cplh ,
  fc_npd                         => fc_npd ,
  fc_nph                         => fc_nph ,
  fc_pd                          => fc_pd ,
  fc_ph                          => fc_ph ,
  fc_sel                         => fc_sel ,
  cfg_do                         => cfg_do ,
  cfg_rd_wr_done                 => cfg_rd_wr_done,
  cfg_di                         => cfg_di ,
  cfg_byte_en                    => cfg_byte_en ,
  cfg_dwaddr                     => cfg_dwaddr ,
  cfg_wr_en                      => cfg_wr_en ,
  cfg_rd_en                      => cfg_rd_en ,
  cfg_err_cor                    => cfg_err_cor ,
  cfg_err_ur                     => cfg_err_ur ,
  cfg_err_ecrc                   => cfg_err_ecrc ,
  cfg_err_cpl_timeout            => cfg_err_cpl_timeout ,
  cfg_err_cpl_abort              => cfg_err_cpl_abort ,
  cfg_err_cpl_unexpect           => cfg_err_cpl_unexpect ,
  cfg_err_posted                 => cfg_err_posted ,
  cfg_err_locked                 => cfg_err_locked ,
  cfg_err_tlp_cpl_header         => cfg_err_tlp_cpl_header ,
  cfg_err_cpl_rdy                => cfg_err_cpl_rdy ,
  cfg_interrupt                  => cfg_interrupt ,
  cfg_interrupt_rdy              => cfg_interrupt_rdy ,
  cfg_interrupt_assert           => cfg_interrupt_assert ,
  cfg_interrupt_di               => cfg_interrupt_di ,
  cfg_interrupt_do               => cfg_interrupt_do ,
  cfg_interrupt_mmenable         => cfg_interrupt_mmenable ,
  cfg_interrupt_msienable        => cfg_interrupt_msienable ,
  cfg_interrupt_msixenable       => cfg_interrupt_msixenable ,
  cfg_interrupt_msixfm           => cfg_interrupt_msixfm ,
  cfg_turnoff_ok                 => cfg_turnoff_ok ,
  cfg_to_turnoff                 => cfg_to_turnoff ,
  cfg_trn_pending                => cfg_trn_pending ,
  cfg_pm_wake                    => cfg_pm_wake ,
  cfg_bus_number                 => cfg_bus_number ,
  cfg_device_number              => cfg_device_number ,
  cfg_function_number            => cfg_function_number ,
  cfg_status                     => cfg_status ,
  cfg_command                    => cfg_command ,
  cfg_dstatus                    => cfg_dstatus ,
  cfg_dcommand                   => cfg_dcommand ,
  cfg_lstatus                    => cfg_lstatus ,
  cfg_lcommand                   => cfg_lcommand ,
  cfg_dcommand2                  => cfg_dcommand2 ,
  cfg_pcie_link_state            => cfg_pcie_link_state ,
  cfg_dsn                        => cfg_dsn ,
  pl_initial_link_width          => pl_initial_link_width ,
  pl_lane_reversal_mode          => pl_lane_reversal_mode ,
  pl_link_gen2_capable           => pl_link_gen2_capable ,
  pl_link_partner_gen2_supported => pl_link_partner_gen2_supported ,
  pl_link_upcfg_capable          => pl_link_upcfg_capable ,
  pl_ltssm_state                 => pl_ltssm_state ,
  pl_received_hot_rst            => pl_received_hot_rst ,
  pl_sel_link_rate               => pl_sel_link_rate ,
  pl_sel_link_width              => pl_sel_link_width ,
  pl_directed_link_auton         => pl_directed_link_auton ,
  pl_directed_link_change        => pl_directed_link_change ,
  pl_directed_link_speed         => pl_directed_link_speed ,
  pl_directed_link_width         => pl_directed_link_width ,
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph);

end rtl;
