
--*****************************************************************************
-- (c) Copyright 2009 - 2010 Xilinx, Inc. All rights reserved.
--
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
--
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
--
--*****************************************************************************
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor             : Xilinx
-- \   \   \/    Version            : 3.92
--  \   \        Application        : MIG
--  /   /        Filename           : ddr3_ram.vho
-- /___/   /\    Date Last Modified : $Date: 2011/06/02 07:18:08 $
-- \   \  /  \   Date Created       : Wed May 13 2009
--  \___\/\___\
--
-- Purpose          : Template file containing code that can be used as a model
--                    for instantiating a CORE Generator module in a HDL design.
-- Revision History :
--*****************************************************************************

-- The following code must appear in the VHDL architecture header:

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
component ddr3_ram
  generic(
    REFCLK_FREQ               : real := 200.0;
                                    -- # = 200 for all design frequencies of
                                    --         -1 speed grade devices
                                    --   = 200 when design frequency < 480 MHz
                                    --         for -2 and -3 speed grade devices.
                                    --   = 300 when design frequency >= 480 MHz
                                    --         for -2 and -3 speed grade devices.
    MMCM_ADV_BANDWIDTH        : string  := "OPTIMIZED";
                                         -- MMCM programming algorithm
    CLKFBOUT_MULT_F           : integer := 6;
                                         -- write PLL VCO multiplier.
    DIVCLK_DIVIDE             : integer := 2;
                                         -- write PLL VCO divisor.
    CLKOUT_DIVIDE             : integer := 3;
                                         -- VCO output divisor for fast (memory) clocks.
    nCK_PER_CLK               : integer := 2;
                                         -- # of memory CKs per fabric clock.
    tCK                       : integer := 2500;
                                         -- memory tCK paramter.
                                         -- # = Clock Period.
    DEBUG_PORT                : string := "OFF";
                                    -- # = "ON" Enable debug signals/controls.
                                    --   = "OFF" Disable debug signals/controls.
    SIM_BYPASS_INIT_CAL       : string := "OFF";
                                    -- # = "OFF" -  Complete memory init &
                                    --              calibration sequence
                                    -- # = "SKIP" - Skip memory init &
                                    --              calibration sequence
                                    -- # = "FAST" - Skip memory init & use
                                    --              abbreviated calib sequence
    nCS_PER_RANK              : integer := 1;
                                    -- # of unique CS outputs per Rank for
                                    -- phy.
    DQS_CNT_WIDTH             : integer := 3;
                                    -- # = ceil(log2(DQS_WIDTH)).
    RANK_WIDTH                : integer := 1;
                                    -- # = ceil(log2(RANKS)).
    BANK_WIDTH                : integer := 3;
                                    -- # of memory Bank Address bits.
    CK_WIDTH                  : integer := 1;
                                    -- # of CK/CK# outputs to memory.
    CKE_WIDTH                 : integer := 1;
                                    -- # of CKE outputs to memory.
    COL_WIDTH                 : integer := 10;
                                    -- # of memory Column Address bits.
    CS_WIDTH                  : integer := 1;
                                    -- # of unique CS outputs to memory.
    DM_WIDTH                  : integer := 8;
                                        -- # of Data Mask bits.
    DQ_WIDTH                  : integer := 64;
                                    -- # of Data (DQ) bits.
    DQS_WIDTH                 : integer := 8;
                                    -- # of DQS/DQS# bits.
    ROW_WIDTH                 : integer := 15;
                                    -- # of memory Row Address bits.
    BURST_MODE                : string := "8";
                                    -- Burst Length (Mode Register 0).
                                    -- # = "8", "4", "OTF".
    BM_CNT_WIDTH              : integer := 2;
                                    -- # = ceil(log2(nBANK_MACHS)).
    ADDR_CMD_MODE             : string := "1T" ;
                                    -- # = "2T", "1T".
    ORDERING                  : string := "STRICT";
                                    -- # = "NORM", "STRICT".
    WRLVL                     : string := "ON";
                                    -- # = "ON" - DDR3 SDRAM
                                    --   = "OFF" - DDR2 SDRAM.
    PHASE_DETECT              : string := "ON";
                                    -- # = "ON", "OFF".
    RTT_NOM                   : string := "60";
                                    -- RTT_NOM (ODT) (Mode Register 1).
                                    -- # = "DISABLED" - RTT_NOM disabled,
                                    --   = "120" - RZQ/2,
                                    --   = "60"  - RZQ/4,
                                    --   = "40"  - RZQ/6.
    RTT_WR                    : string := "OFF";
                                    -- RTT_WR (ODT) (Mode Register 2).
                                    -- # = "OFF" - Dynamic ODT off,
                                    --   = "120" - RZQ/2,
                                    --   = "60"  - RZQ/4,
    OUTPUT_DRV                : string := "HIGH";
                                    -- Output Driver Impedance Control (Mode Register 1).
                                    -- # = "HIGH" - RZQ/7,
                                    --   = "LOW" - RZQ/6.
    REG_CTRL                  : string := "OFF";
                                    -- # = "ON" - RDIMMs,
                                    --   = "OFF" - Components, SODIMMs, UDIMMs.
    nDQS_COL0                 : integer := 4;
                                    -- Number of DQS groups in I/O column #1.
    nDQS_COL1                 : integer := 2;
                                    -- Number of DQS groups in I/O column #2.
    nDQS_COL2                 : integer := 2;
                                    -- Number of DQS groups in I/O column #3.
    nDQS_COL3                 : integer := 0;
                                    -- Number of DQS groups in I/O column #4.
    DQS_LOC_COL0              : std_logic_vector(31 downto 0) := X"05040302";
                                    -- DQS groups in column #1.
    DQS_LOC_COL1              : std_logic_vector(15 downto 0) := X"0706";
                                    -- DQS groups in column #2.
    DQS_LOC_COL2              : std_logic_vector(15 downto 0) := X"0100";
                                    -- DQS groups in column #3.
    DQS_LOC_COL3              : std_logic_vector(0 downto 0) := "0";
                                    -- DQS groups in column #4.
    tPRDI                     : integer := 1000000;
                                    -- memory tPRDI paramter.
    tREFI                     : integer := 7800000;
                                    -- memory tREFI paramter.
    tZQI                      : integer := 128000000;
                                    -- memory tZQI paramter.
    ADDR_WIDTH                : integer := 29;
                                    -- # = RANK_WIDTH + BANK_WIDTH
                                    --     + ROW_WIDTH + COL_WIDTH;
    ECC                       : string := "OFF";
    ECC_TEST                  : string := "OFF";
    TCQ                       : integer := 100;
    DATA_WIDTH                : integer := 64;
    PAYLOAD_WIDTH             : integer := 64;
    RST_ACT_LOW            : integer := 1;
                              -- =1 for active low reset,
                              -- =0 for active high.
    IODELAY_GRP     : string := "IODELAY_MIG";
                              --to phy_top
    INPUT_CLK_TYPE         : string := "SINGLE_ENDED";
                              -- input clock type DIFFERENTIAL or SINGLE_ENDED
    STARVE_LIMIT           : integer := 2
                              -- # = 2,3,4.
    );
  port(
      sys_clk                 : in    std_logic;
      clk_ref                 : in    std_logic;
    ddr3_dq                   : inout std_logic_vector(DQ_WIDTH-1 downto 0);
      ddr3_dm                 : out   std_logic_vector(DM_WIDTH-1 downto 0);
    ddr3_addr                 : out   std_logic_vector(ROW_WIDTH-1 downto 0);
    ddr3_ba                   : out   std_logic_vector(BANK_WIDTH-1 downto 0);
    ddr3_ras_n                : out   std_logic;
    ddr3_cas_n                : out   std_logic;
    ddr3_we_n                 : out   std_logic;
    ddr3_reset_n              : out   std_logic;
    ddr3_cs_n                 : out   std_logic_vector((CS_WIDTH*nCS_PER_RANK)-1 downto 0);
    ddr3_odt                  : out   std_logic_vector((CS_WIDTH*nCS_PER_RANK)-1 downto 0);
    ddr3_cke                  : out   std_logic_vector(CKE_WIDTH-1 downto 0);
    ddr3_dqs_p                : inout std_logic_vector(DQS_WIDTH-1 downto 0);
    ddr3_dqs_n                : inout std_logic_vector(DQS_WIDTH-1 downto 0);
    ddr3_ck_p                 : out   std_logic_vector(CK_WIDTH-1 downto 0);
    ddr3_ck_n                 : out   std_logic_vector(CK_WIDTH-1 downto 0);
    phy_init_done             : out   std_logic;
    app_wdf_wren              : in    std_logic;
    app_wdf_data              : in    std_logic_vector((4*PAYLOAD_WIDTH)-1 downto 0);
    app_wdf_mask              : in    std_logic_vector((4*PAYLOAD_WIDTH)/8-1 downto 0);
    app_wdf_end               : in    std_logic;
    app_addr                  : in    std_logic_vector(ADDR_WIDTH-1 downto 0);
    app_cmd                   : in    std_logic_vector(2 downto 0);
    app_en                    : in    std_logic;
    app_rdy                   : out   std_logic;
    app_wdf_rdy               : out   std_logic;
    app_rd_data               : out   std_logic_vector((4*PAYLOAD_WIDTH)-1 downto 0);
    app_rd_data_end           : out   std_logic;
    app_rd_data_valid         : out   std_logic;
    ui_clk_sync_rst           : out   std_logic;
    ui_clk                    : out   std_logic;
    sys_rst               : in   std_logic
    );
end component ddr3_ram;
-- COMP_TAG_END ------ End COMPONENT Declaration ------------

-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.

------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
u_ddr3_ram : ddr3_ram
  generic map(
    REFCLK_FREQ               => REFCLK_FREQ,
    MMCM_ADV_BANDWIDTH         => MMCM_ADV_BANDWIDTH,
    CLKFBOUT_MULT_F           => CLKFBOUT_MULT_F,
    DIVCLK_DIVIDE             => DIVCLK_DIVIDE,
    CLKOUT_DIVIDE             => CLKOUT_DIVIDE,
    nCK_PER_CLK               => nCK_PER_CLK,
    tCK                       => tCK,
    DEBUG_PORT                => DEBUG_PORT,
    SIM_BYPASS_INIT_CAL       => SIM_BYPASS_INIT_CAL,
    nCS_PER_RANK              => nCS_PER_RANK,
    DQS_CNT_WIDTH             => DQS_CNT_WIDTH,
    RANK_WIDTH                => RANK_WIDTH,
    BANK_WIDTH                => BANK_WIDTH,
    CK_WIDTH                  => CK_WIDTH,
    CKE_WIDTH                 => CKE_WIDTH,
    COL_WIDTH                 => COL_WIDTH,
    CS_WIDTH                  => CS_WIDTH,
    DQ_WIDTH                  => DQ_WIDTH,
    DM_WIDTH                  => DM_WIDTH,
    DQS_WIDTH                 => DQS_WIDTH,
    ROW_WIDTH                 => ROW_WIDTH,
    BURST_MODE                => BURST_MODE,
    BM_CNT_WIDTH              => BM_CNT_WIDTH,
    ADDR_CMD_MODE             => ADDR_CMD_MODE,
    ORDERING                  => ORDERING,
    WRLVL                     => WRLVL,
    PHASE_DETECT              => PHASE_DETECT,
    RTT_NOM                   => RTT_NOM,
    RTT_WR                    => RTT_WR,
    OUTPUT_DRV                => OUTPUT_DRV,
    REG_CTRL                  => REG_CTRL,
    nDQS_COL0                 => nDQS_COL0,
    nDQS_COL1                 => nDQS_COL1,
    nDQS_COL2                 => nDQS_COL2,
    nDQS_COL3                 => nDQS_COL3,
    DQS_LOC_COL0              => DQS_LOC_COL0,
    DQS_LOC_COL1              => DQS_LOC_COL1,
    DQS_LOC_COL2              => DQS_LOC_COL2,
    DQS_LOC_COL3              => DQS_LOC_COL3,
    tPRDI                     => tPRDI,
    tREFI                     => tREFI,
    tZQI                      => tZQI,
    ADDR_WIDTH                => ADDR_WIDTH,
    ECC                       => ECC,
    ECC_TEST                  => ECC_TEST,
    TCQ                       => TCQ,
    DATA_WIDTH                => DATA_WIDTH,
    PAYLOAD_WIDTH             => PAYLOAD_WIDTH,
    RST_ACT_LOW            => 1,
    IODELAY_GRP     => "IODELAY_MIG",
    INPUT_CLK_TYPE         => "SINGLE_ENDED",
    STARVE_LIMIT           => 2
    )
  port map(
    sys_clk                   => sys_clk,
    clk_ref                   => clk_ref,
    ddr3_dq                   => ddr3_dq,
    ddr3_dm                   => ddr3_dm,
    ddr3_addr                 => ddr3_addr,
    ddr3_ba                   => ddr3_ba,
    ddr3_ras_n                => ddr3_ras_n,
    ddr3_cas_n                => ddr3_cas_n,
    ddr3_we_n                 => ddr3_we_n,
    ddr3_reset_n              => ddr3_reset_n,
    ddr3_cs_n                 => ddr3_cs_n,
    ddr3_odt                  => ddr3_odt,
    ddr3_cke                  => ddr3_cke,
    ddr3_dqs_p                => ddr3_dqs_p,
    ddr3_dqs_n                => ddr3_dqs_n,
    ddr3_ck_p                 => ddr3_ck_p,
    ddr3_ck_n                 => ddr3_ck_n,
    phy_init_done             => phy_init_done,
    app_wdf_wren              => app_wdf_wren,
    app_wdf_data              => app_wdf_data,
    app_wdf_mask              => app_wdf_mask,
    app_wdf_end               => app_wdf_end,
    app_addr                  => app_addr,
    app_cmd                   => app_cmd,
    app_en                    => app_en,
    app_rdy                   => app_rdy,
    app_wdf_rdy               => app_wdf_rdy,
    app_rd_data               => app_rd_data,
    app_rd_data_end           => app_rd_data_end,
    app_rd_data_valid         => app_rd_data_valid,
    ui_clk_sync_rst           => ui_clk_sync_rst,
    ui_clk                    => ui_clk,
    sys_rst                => sys_rst
    );

-- INST_TAG_END ------ End INSTANTIATION Template ---------

-- You must compile the wrapper file ddr3_ram.vhd when simulating
-- the core, ddr3_ram. When compiling the wrapper file, be sure to
-- reference the XilinxCoreLib Verilog simulation library. For detailed
-- instructions, please refer to the "CORE Generator Help".

  