-- (c) Copyright 2009 - 2011 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.

-- The following code must appear in the VHDL architecture header:

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
component ip_dac_data_output
generic
 (-- width of the data for the system
  sys_w       : integer := 16;
  -- width of the data for the device
  dev_w       : integer := 16);
port
 (
  -- From the device out to the system
  DATA_OUT_FROM_DEVICE    : in    std_logic_vector(dev_w-1 downto 0);
  DATA_OUT_TO_PINS_P      : out   std_logic_vector(sys_w-1 downto 0);
  DATA_OUT_TO_PINS_N      : out   std_logic_vector(sys_w-1 downto 0);

-- Input, Output delay control signals
  DELAY_CLK            : in    std_logic;
  DELAY_RESET          : in    std_logic;                    -- Active high synchronous reset for input delay
  DELAY_DATA_CE        : in    std_logic_vector(sys_w -1 downto 0);            -- Enable signal for delay for bit 
  DELAY_DATA_INC       : in    std_logic_vector(sys_w -1 downto 0);            -- Delay increment, decrement signal for bit 
  DELAY_LOCKED            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  REF_CLOCK               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.
 
-- Clock and reset signals
  CLK_IN                  : in    std_logic;                    -- Fast clock from PLL/MMCM 
  IO_RESET                : in    std_logic);                   -- Reset signal for IO circuit
end component;

-- COMP_TAG_END ------ End COMPONENT Declaration ------------
-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.
------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG

-- User Notes:
-- None

your_instance_name : ip_dac_data_output
  port map
   (
  -- From the device out to the system
  DATA_OUT_FROM_DEVICE =>   DATA_OUT_FROM_DEVICE, --Input pins
  DATA_OUT_TO_PINS_P =>   DATA_OUT_TO_PINS_P, --Output pins
  DATA_OUT_TO_PINS_N =>   DATA_OUT_TO_PINS_N, --Output pins

-- Input, Output delay control signals
  DELAY_CLK =>   DELAY_CLK,            --Input clk
  DELAY_RESET =>   DELAY_RESET,        -- Active high synchronous reset for input delay
  DELAY_DATA_CE0    =>  DELAY_DATA_CE0,    -- Enable signal for IODELAYE1 of bit 0
  DELAY_DATA_INC0   =>  DELAY_DATA_INC0,   -- Delay increment, decrement signal of bit 0
  DELAY_DATA_CE1    =>  DELAY_DATA_CE1,    -- Enable signal for IODELAYE1 of bit 1
  DELAY_DATA_INC1   =>  DELAY_DATA_INC1,   -- Delay increment, decrement signal of bit 1
  DELAY_DATA_CE2    =>  DELAY_DATA_CE2,    -- Enable signal for IODELAYE1 of bit 2
  DELAY_DATA_INC2   =>  DELAY_DATA_INC2,   -- Delay increment, decrement signal of bit 2
  DELAY_DATA_CE3    =>  DELAY_DATA_CE3,    -- Enable signal for IODELAYE1 of bit 3
  DELAY_DATA_INC3   =>  DELAY_DATA_INC3,   -- Delay increment, decrement signal of bit 3
  DELAY_DATA_CE4    =>  DELAY_DATA_CE4,    -- Enable signal for IODELAYE1 of bit 4
  DELAY_DATA_INC4   =>  DELAY_DATA_INC4,   -- Delay increment, decrement signal of bit 4
  DELAY_DATA_CE5    =>  DELAY_DATA_CE5,    -- Enable signal for IODELAYE1 of bit 5
  DELAY_DATA_INC5   =>  DELAY_DATA_INC5,   -- Delay increment, decrement signal of bit 5
  DELAY_DATA_CE6    =>  DELAY_DATA_CE6,    -- Enable signal for IODELAYE1 of bit 6
  DELAY_DATA_INC6   =>  DELAY_DATA_INC6,   -- Delay increment, decrement signal of bit 6
  DELAY_DATA_CE7    =>  DELAY_DATA_CE7,    -- Enable signal for IODELAYE1 of bit 7
  DELAY_DATA_INC7   =>  DELAY_DATA_INC7,   -- Delay increment, decrement signal of bit 7
  DELAY_DATA_CE8    =>  DELAY_DATA_CE8,    -- Enable signal for IODELAYE1 of bit 8
  DELAY_DATA_INC8   =>  DELAY_DATA_INC8,   -- Delay increment, decrement signal of bit 8
  DELAY_DATA_CE9    =>  DELAY_DATA_CE9,    -- Enable signal for IODELAYE1 of bit 9
  DELAY_DATA_INC9   =>  DELAY_DATA_INC9,   -- Delay increment, decrement signal of bit 9
  DELAY_DATA_CE10    =>  DELAY_DATA_CE10,    -- Enable signal for IODELAYE1 of bit 10
  DELAY_DATA_INC10   =>  DELAY_DATA_INC10,   -- Delay increment, decrement signal of bit 10
  DELAY_DATA_CE11    =>  DELAY_DATA_CE11,    -- Enable signal for IODELAYE1 of bit 11
  DELAY_DATA_INC11   =>  DELAY_DATA_INC11,   -- Delay increment, decrement signal of bit 11
  DELAY_DATA_CE12    =>  DELAY_DATA_CE12,    -- Enable signal for IODELAYE1 of bit 12
  DELAY_DATA_INC12   =>  DELAY_DATA_INC12,   -- Delay increment, decrement signal of bit 12
  DELAY_DATA_CE13    =>  DELAY_DATA_CE13,    -- Enable signal for IODELAYE1 of bit 13
  DELAY_DATA_INC13   =>  DELAY_DATA_INC13,   -- Delay increment, decrement signal of bit 13
  DELAY_DATA_CE14    =>  DELAY_DATA_CE14,    -- Enable signal for IODELAYE1 of bit 14
  DELAY_DATA_INC14   =>  DELAY_DATA_INC14,   -- Delay increment, decrement signal of bit 14
  DELAY_DATA_CE15    =>  DELAY_DATA_CE15,    -- Enable signal for IODELAYE1 of bit 15
  DELAY_DATA_INC15   =>  DELAY_DATA_INC15,   -- Delay increment, decrement signal of bit 15
  DELAY_LOCKED =>   DELAY_LOCKED,      -- Locked signal from IDELAYCTRL
  REF_CLOCK =>   REF_CLOCK,            -- Reference clock for IDELAYCTRL. Has to come from BUFG.
 
  
-- Clock and reset signals
  CLK_IN =>   CLK_IN,      -- Fast clock input from PLL/MMCM
  CLK_RESET =>   CLK_RESET,         --clocking logic reset
  IO_RESET =>   IO_RESET);          --system reset

-- INST_TAG_END ------ End INSTANTIATION Template ------------
