
The design files are located at
D:/work/8300-L/xilinx/sis8300l/ipcore_dir:

   - ddr3_ram.vho:
        vho template file containing code that can be used as a model
        for instantiating a CORE Generator module in a HDL design.

   - ddr3_ram.xco:
       CORE Generator input file containing the parameters used to
       regenerate a core.

   - ddr3_ram_flist.txt:
        Text file listing all of the output files produced when a customized
        core was generated in the CORE Generator.

   - ddr3_ram_readme.txt:
        Text file indicating the files generated and how they are used.

   - ddr3_ram_xmdf.tcl:
        ISE Project Navigator interface file. ISE uses this file to determine
        how the files output by CORE Generator for the core can be integrated
        into your ISE project.

   - ddr3_ram.gise and ddr3_ram.xise:
        ISE Project Navigator support files. These are generated files and
        should not be edited directly.

   - ddr3_ram directory.

In the ddr3_ram directory, three folders are created:
   - docs:
        This folder contains Virtex-6 FPGA Memory Interface Solutions user guide
        and data sheet.

   - example_design:
        This folder includes the design with synthesizable test bench.

   - user_design:
        This folder includes the design without test bench modules.

The example_design and user_design folders contain several other folders
and files. All these output folders are discussed in more detail in
Virtex-6 FPGA Memory Interface Solutions User Guide (UG406) located
in docs folder.
    